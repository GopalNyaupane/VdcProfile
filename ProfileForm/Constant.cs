﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ProfileForm
{
    public static  class Constant
    {
        private static string constantpath = null;
        public static string constantApppath
        {
            get
            {
                constantpath =  ConfigurationManager.AppSettings["ConstantPath"];
                return constantpath;
            }
            
        }
    }
}