﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ProfileForm.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>VDC</title>

    <!-- Core CSS - Include with every page -->
    <link href="/Pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="/Pages/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/Pages/css/style.css "rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="/Pages/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="/Pages/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="/Pages/css/sb-admin.css" rel="stylesheet">
    <!--for calander-->
     
    <!-- Core Scripts - Include with every page -->
    <script src="/Pages/js/jquery-1.10.2.js"></script>
    <script src="/Pages/js/bootstrap.min.js"></script>
    <script src="/Pages/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="/Pages/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="/Pages/js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="/Pages/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="/Pages/js/demo/dashboard-demo.js"></script>
</head>
<body class="login-bg">
    <form id="Form1" runat="server">
        <div id="loader"><img src="img/loader.gif"/></div>

        <div class="login">

            <div class="page-header">
                <div class="icon">
                    <span class=" glyphicon glyphicon-log-in"></span>
                </div>
                <h1>Login <small>VDC ADMIN PANEL</small></h1>
            </div>        

            <div style="float: left;">
                <div class="row-form">
                    <div class="col-md-12">
                        Username:
                         <asp:TextBox ID="txtUsername" runat="server" placeholder="login" AutoComplete="False"></asp:TextBox>
                        
                    </div>
                </div>
                <div class="row-form">
                    <div class="col-md-12">
                        Password:&nbsp;
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
                    </div>            
                </div>
                <div class="row-form">
                    <div class="col-md-12">
                       <asp:CheckBox ID="CheckBox1" runat="server" Text="Remember Me" />
                    </div>
                </div>
                <div class="row-form">
                    <div class="col-md-12">
                         <asp:Button ID="btnLogin" runat="server" Text="Sign In" 
                        CssClass="btn btn-primary" onclick="btnLogin_Click"/>
                    </div>                
                </div>
            </div>
        </div>
        </form>    
    </body>

</html>
