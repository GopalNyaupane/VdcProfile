﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        public string appPath = Constant.constantApppath;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null || Convert.ToInt32(Session["UserId"]) < 0)
            {
                Response.Redirect(appPath+"/Login.aspx");
            }
            lnkAnimalDetails.HRef = appPath + "/Pages/AddEditItems/AnimalsDetails.aspx";
            lnkCashCrps.HRef = appPath + "/Pages/AddEditItems/CashCrop.aspx";
            lnkCaste.HRef = appPath + "/Pages/AddEditItems/Caste.aspx";
            lnkChDiff.HRef = appPath + "/Pages/AddEditItems/ChildDifferentiate.aspx";
            lnkChldDis.HRef = appPath + "/Pages/AddEditItems/ChildDisease.aspx";
            lnkChLabor.HRef = appPath + "/Pages/AddEditItems/ChildLabourProblem.aspx";
            lnkChPunish.HRef = appPath + "/Pages/AddEditItems/ChildPunishment.aspx";
            lnkCokFuNa.HRef = appPath + "/Pages/AddEditItems/CookingFuelName.aspx";
            lnkCokOvName.HRef = appPath + "/Pages/AddEditItems/CookingOvenName.aspx";
            lnkCrps.HRef = appPath + "/Pages/AddEditItems/Crops.aspx";
            lnkDecision.HRef = appPath + "/Pages/AddEditItems/Decision.aspx";
            lnkDisease.HRef = appPath + "/Pages/AddEditItems/DiseaseName.aspx";
            lnkDrkWater.HRef = appPath + "/Pages/AddEditItems/DrinkingWater.aspx";
            lnkDrkWatTime.HRef = appPath + "/Pages/AddEditItems/DrinkingWaterTime.aspx";
            lnkElSr.HRef = appPath + "/Pages/AddEditItems/ElectricitySource.aspx";
            lnkFlName.HRef = appPath + "/Pages/AddEditItems/FloorName.aspx";
            lnkFrt.HRef = appPath + "/Pages/AddEditItems/FruitName.aspx";
            lnkHancap.HRef = appPath + "/Pages/AddEditItems/HandiCapped.aspx";
            lnkHw.HRef = appPath + "/Pages/AddEditItems/HandWash.aspx";
            lnkHlthCk.HRef = appPath + "/Pages/AddEditItems/HealthCheckup.aspx";
            lnkLanguage.HRef = appPath + "/Pages/AddEditItems/Language.aspx";
            lnkmatadd.HRef = appPath + "/Pages/AddEditItems/MaternityAddress.aspx";
            lnkmigrsn.HRef = appPath + "/Pages/AddEditItems/MigrationReason.aspx";
            lnkHancap.HRef = appPath + "/Pages/AddEditItems/HandiCapped.aspx";
            lnkOcc.HRef = appPath + "/Pages/AddEditItems/Occupation.aspx";

            lnkparacom.HRef = appPath + "/Pages/AddEditItems/ParaLegalCommunity.aspx";
            lnkRel.HRef = appPath + "/Pages/AddEditItems/Relation.aspx";
            lnkReligion.HRef = appPath + "/Pages/AddEditItems/Religion.aspx";
            lnkRoof.HRef = appPath + "/Pages/AddEditItems/RoofStructure.aspx";
            lnkSclpun.HRef = appPath + "/Pages/AddEditItems/SchoolPunishment.aspx";
            lnkSocvio.HRef = appPath + "/Pages/AddEditItems/SocialViolance.aspx";
            lnkTel.HRef = appPath + "/Pages/AddEditItems/Telecommunication.aspx";
            lnkToilet.HRef = appPath + "/Pages/AddEditItems/Toilet.aspx";
            lnkTrain.HRef = appPath + "/Pages/AddEditItems/Training.aspx";
            lnkTrainType.HRef = appPath + "/Pages/AddEditItems/TrainingType.aspx";
            lnkVeg.HRef = appPath + "/Pages/AddEditItems/Vegetables.aspx";

            lnkSeniorCitizen.HRef = appPath + "/ReportViewer/AgedPeopleReportViewer.aspx";
            lnkVdcWiseReport.HRef = appPath + "/ReportViewer/DetailsReportViewer.aspx";
        }

        protected void lbtnLogout_OnClick(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect(appPath + "/Login.aspx");
        }
    }
}
