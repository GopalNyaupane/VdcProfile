﻿using Common.Entity;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DataAccessLayer.DbConnection;
using DataAccessLayer.Pages;
using ProfileForm.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.ReportViewer
{
    public partial class DetailsReportViewer : System.Web.UI.Page
    {

        InfoDao objInfoDao = new InfoDao();
        InfoBo objInfoBo = new InfoBo();

        ReportDocument cr = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["owner_id"] != null)
                {
                    Session["OwnerId"] = Request.QueryString["owner_id"];
                    int OwnerId = Convert.ToInt32(Session["OwnerId"]);
                    cr.Load(Server.MapPath("../Reports/DetailsReport.rpt"));
                    cr.SetDatabaseLogon("", "", ".", "VdcProfile");
                    //cr.SetDatabaseLogon("", "", "deshar", "VdcProfile");
                    ReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
                    PopulateCrystalReport(OwnerId, cr);
                }
                PopulateVDC();
                PopulateToleBasti(objInfoBo.VdcId, objInfoBo.WardNum);
            }

           

            if (ViewState["ReportLoad"] != null)
            {
                OwnerId = Convert.ToInt32(Session["OwnerId"]);
                //ReportDocument cr = ViewState["ReportLoad"];
                cr.Load(Server.MapPath("../Reports/DetailsReport.rpt"));
                cr.SetDatabaseLogon("", "", ".", "VdcProfile");
                //cr.SetDatabaseLogon("", "", "deshar", "VdcProfile");
                ReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
                PopulateCrystalReport(OwnerId,cr);
            }
            

        }

        private void PopulateVDC()
        {
            DataTable dt = objInfoDao.PopulateMyagdiVdc();

            ddlVDC.DataSource = dt;
            ddlVDC.DataTextField = "VDC_NAME_NEP";
            ddlVDC.DataValueField = "VDC_ID";
            ddlVDC.DataBind();
            ddlVDC.Items.Insert(0, "-गाविस छान्नुहोस्-");
        }

        protected void ddlVDC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlWoda.SelectedIndex = 0;
            ddlToleBastiName.SelectedIndex = 0;
            if (ddlVDC.SelectedIndex < 1)
            {
                ddlWoda.Enabled = false;
            }
            else
                ddlWoda.Enabled = true;

            //Run();
        }

        private int OwnerId;
        protected void btnView_click(object sender, EventArgs e)
        {
           //vdc wise report case
            Session["OwnerId"] = null;
           // ReportDocument cr = new ReportDocument();

            cr.Load(Server.MapPath("../Reports/DetailsReport.rpt"));
            cr.SetDatabaseLogon("", "", ".", "VdcProfile");
            //cr.SetDatabaseLogon("", "", "deshar", "VdcProfile");
            ReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            ReportViewer1.SeparatePages = false;

            //vdc wise report generetion
            if(ddlVDC.SelectedIndex>0 && ddlWoda.SelectedIndex<1 && ddlToleBastiName.SelectedIndex<1)
            {
                objInfoBo.VdcId = int.Parse(ddlVDC.SelectedValue);
                DataTable dt = new DataTable();
                dt = objInfoDao.FetchOwnerIdByVDCId(objInfoBo);
                if(dt != null && dt.Rows.Count>0 )
                {
                    for(int i=0; i<dt.Rows.Count; i++)
                    {
                        OwnerId = int.Parse(dt.Rows[i]["OWNER_ID"].ToString());
                        Session["OwnerId"] = OwnerId;
                        cr= PopulateCrystalReport(OwnerId, cr);
                    }
                }

            }
            
            //ward no. wise report case
            if(ddlVDC.SelectedIndex>0 && ddlWoda.SelectedIndex>0 && ddlToleBastiName.SelectedIndex<1)
            {
                objInfoBo.VdcId = int.Parse(ddlVDC.SelectedValue);
                objInfoBo.WardNum = int.Parse(ddlWoda.SelectedValue);
                DataTable dt = new DataTable();
                dt = objInfoDao.FetchOwnerIdByVDCWard(objInfoBo);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OwnerId = int.Parse(dt.Rows[i]["OWNER_ID"].ToString());
                        Session["OwnerId"] = OwnerId;
                        cr = PopulateCrystalReport(OwnerId, cr);
                    }
                }
            }
            
            //tole basti name wise report case
            if (ddlVDC.SelectedIndex > 0 && ddlWoda.SelectedIndex > 0 && ddlToleBastiName.SelectedIndex >0)
            {
                objInfoBo.VdcId = int.Parse(ddlVDC.SelectedValue);
                objInfoBo.WardNum = int.Parse(ddlWoda.SelectedValue);
                objInfoBo.BastiId = int.Parse(ddlToleBastiName.SelectedValue);
                DataTable dt = new DataTable();
                dt = objInfoDao.FetchOwnerIdByVDCWardBasti(objInfoBo);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OwnerId = int.Parse(dt.Rows[i]["OWNER_ID"].ToString());
                        Session["OwnerId"] = OwnerId;
                        cr=PopulateCrystalReport(OwnerId, cr);
                    }
                }
            }
            ReportViewer1.ReportSource = cr;
         
            ReportViewer1.RefreshReport();
            ViewState["ReportLoad"] = "Load";
          
        }

        private ReportDocument PopulateCrystalReport(int OwnerId, ReportDocument cr)
        {
            //ReportDocument cr = new ReportDocument();

            //cr.Load(Server.MapPath("../Reports/DetailsReport.rpt"));
            //cr.SetDatabaseLogon("", "", "MILANASHIK\\MSSQLSERVER2", "VdcProfile");
            ////cr.SetDatabaseLogon("", "", "deshar", "VdcProfile");
            //ReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;



            ////Provide parameter values
            cr.SetParameterValue("@OwnerId", OwnerId);
            cr.SetParameterValue("@owner_id", OwnerId);
            //cr.SetParameterValue("@InvNo", Session("InvoiceNo"), "rptInvoicePrintSub")
            cr.SetParameterValue("@owner_id", OwnerId, "FamilyMemberDisplay");
            cr.SetParameterValue("@ownerId", OwnerId, "MaternalHealth");
            cr.SetParameterValue("@OwnerId", OwnerId, "TeenAgeHealth");
            cr.SetParameterValue("@OwnerId", OwnerId, "TeenAgeFatality");
            cr.SetParameterValue("@OwnerId", OwnerId, "ChildFatality");
            cr.SetParameterValue("@owner_id", OwnerId, "ChildWeight");
            cr.SetParameterValue("@ownerId", OwnerId, "MarriageRecord");
            cr.SetParameterValue("@owner_id", OwnerId, "ChildLabour");
            // cr.SetParameterValue("@owner_id", OwnerId, "ChildAbuse");
            //cr.SetParameterValue("@owner_id", OwnerId, "ChildDrugAbuse");
            cr.SetParameterValue("@owner_id", OwnerId, "ChildShelter");
            cr.SetParameterValue("@owner_id", OwnerId, "Parallegal");
            cr.SetParameterValue("@owner_id", OwnerId, "Training");
            cr.SetParameterValue("@owner_id", OwnerId, "EconomicTraining");
            cr.SetParameterValue("@owner_id", OwnerId, "MigrationReason");
            cr.SetParameterValue("@ownerId", OwnerId, "MemberOccupation");
            cr.SetParameterValue("@ownerId", OwnerId, "OutMigration");
            cr.SetParameterValue("@ownerId", OwnerId, "Handicapped");
            // cr.SetParameterValue("@OwnerId", OwnerId, "Loan");
            cr.SetParameterValue("@ownerId", OwnerId, "Crops");
            //  cr.SetParameterValue("@ownerId", OwnerId, "CashCrops");
            cr.SetParameterValue("@ownerId", OwnerId, "Veg");
            return cr;

        }

        protected void ddlWARD_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlWoda.SelectedIndex > 0 && ddlVDC.SelectedIndex > 0)
            {
                int vdcId = Convert.ToInt32(ddlVDC.SelectedValue);
                int wardID = Convert.ToInt32(ddlWoda.SelectedValue);
                PopulateToleBasti(vdcId, wardID);
            }

        }

        private void PopulateToleBasti(int vdcId, int wardId)
        {
            DataTable dt = objInfoDao.PopulateToleBasti(vdcId, wardId);
            ddlToleBastiName.DataSource = dt;
            ddlToleBastiName.DataTextField = "VILLAGE_NAME";
            ddlToleBastiName.DataValueField = "BASTI_ID";
            ddlToleBastiName.DataBind();
            ddlToleBastiName.Items.Insert(0, "Select Tole absti");
        }

    }
}