﻿using Common.Entity;
using CrystalDecisions.CrystalReports.Engine;
using DataAccessLayer.Pages;
using ProfileForm.Reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.ReportViewer
{
    public partial class AgedPeopleReportViewer : System.Web.UI.Page
    {
        InfoDao objInfoDao = new InfoDao();
        InfoBo objInfoBo = new InfoBo();

        ReportDocument cr = new ReportDocument();
        int vdc_id = 0, ward_num = 0, basti_id = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateVDC();
                PopulateToleBasti(objInfoBo.VdcId, objInfoBo.WardNum);
            }
            if (ViewState["ReportLoad"] != null)
            {
                vdc_id = int.Parse(Session["vdc_id"].ToString());
                ward_num = int.Parse(Session["ward_num"].ToString());
                basti_id = int.Parse(Session["basti_id"].ToString());
                    PopulateCrystalReport(vdc_id, ward_num, basti_id);
            }
            


        }

        private void PopulateVDC()
        {
            DataTable dt = objInfoDao.PopulateMyagdiVdc();

            ddlVDC.DataSource = dt;
            ddlVDC.DataTextField = "VDC_NAME_NEP";
            ddlVDC.DataValueField = "VDC_ID";
            ddlVDC.DataBind();
            ddlVDC.Items.Insert(0, "-गाविस छान्नुहोस्-");
        }

        protected void ddlVDC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlWoda.SelectedIndex = 0;
            ddlToleBastiName.SelectedIndex = 0;
            if (ddlVDC.SelectedIndex < 1)
            {
                ddlWoda.Enabled = false;
            }
            else
                ddlWoda.Enabled = true;

            //Run();
        }

        protected void ddlWARD_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlWoda.SelectedIndex > 0 && ddlVDC.SelectedIndex > 0)
            {
                int vdcId = Convert.ToInt32(ddlVDC.SelectedValue);
                int wardID = Convert.ToInt32(ddlWoda.SelectedValue);
                PopulateToleBasti(vdcId, wardID);
            }

        }

        private void PopulateToleBasti(int vdcId, int wardId)
        {
            DataTable dt = objInfoDao.PopulateToleBasti(vdcId, wardId);
            ddlToleBastiName.DataSource = dt;
            ddlToleBastiName.DataTextField = "VILLAGE_NAME";
            ddlToleBastiName.DataValueField = "BASTI_ID";
            ddlToleBastiName.DataBind();
            ddlToleBastiName.Items.Insert(0, "Select Tole absti");
        }

        private int OwnerId;
        protected void btnView_click(object sender, EventArgs e)
        {
            if(ddlVDC.SelectedIndex>0)
            vdc_id =int.Parse(ddlVDC.SelectedValue);
            if (ddlWoda.SelectedIndex > 0)
            ward_num = int.Parse(ddlWoda.SelectedValue);
            if (ddlToleBastiName.SelectedIndex > 0)
            basti_id = int.Parse(ddlToleBastiName.SelectedValue);
            Session["vdc_id"] = vdc_id;
            Session["ward_num"] = ward_num;
            Session["basti_id"] = basti_id;
            PopulateCrystalReport(vdc_id, ward_num, basti_id);
        }

        private void PopulateCrystalReport(int VdcId, int WardNum, int BastiId)
        {
            ReportDocument cr = new ReportDocument();

            cr.Load(Server.MapPath("../Reports/SeniorCitizen.rpt"));
            cr.SetDatabaseLogon("", "", "MILANASHIK\\MSSQLSERVER2", "VdcProfile");
            //cr.SetDatabaseLogon("", "", "deshar", "VdcProfile");
            ReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            cr.SetParameterValue("@vdc_id", VdcId);
            cr.SetParameterValue("@ward_num", WardNum);
            cr.SetParameterValue("@basti_id", BastiId);
            ReportViewer1.ReportSource = cr;
            //ReportViewer1.RefreshReport();
            ViewState["ReportLoad"] = "Load";
       
       


        }

        

    }
}