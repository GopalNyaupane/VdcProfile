﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetailsReportViewer.aspx.cs" Inherits="ProfileForm.ReportViewer.DetailsReportViewer" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-hover">
        <tr>
             <td>
                    गाविस :
               
                    <asp:DropDownList ID="ddlVDC" runat="server" OnSelectedIndexChanged="ddlVDC_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
             
       
            <td>
                    वडा नं. :
                
                    <asp:DropDownList ID="ddlWoda" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlWARD_SelectedIndexChanged" ViewStateMode="Enabled" EnableViewState="true">
                        <asp:ListItem Value="0">-वडा नं. छान्नुहोस्-</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                       <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                        <asp:ListItem Value="17">17</asp:ListItem>
                        <asp:ListItem Value="18">18</asp:ListItem>
                        <asp:ListItem Value="19">19</asp:ListItem>
                        <asp:ListItem Value="20">20</asp:ListItem>
                    </asp:DropDownList>
                </td>
        
             <td>
                    टोल बस्तीको नाम :
                 
                     <asp:DropDownList ID="ddlToleBastiName" runat="server"  />
                 </td>
        </tr>
        
    </table>
    <asp:Button CssClass="btn btn-primary" runat="server" ID="ViewRpt" Text="View" OnClick="btnView_click"/>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upnlFamily" runat="server" UpdateMode="Conditional">
        
        <ContentTemplate>
   <CR:CrystalReportViewer ID="ReportViewer1" runat="server" 
        AutoDataBind="True" EnableDatabaseLogonPrompt="False" 
        EnableParameterPrompt="False" Height="1039px" 
      
        ReuseParameterValuesOnRefresh="True" 
        Width="901px"  />
            </ContentTemplate>
        </asp:UpdatePanel>
   




   
</asp:Content>
