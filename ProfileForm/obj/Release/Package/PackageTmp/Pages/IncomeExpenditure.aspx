﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="IncomeExpenditure.aspx.cs" Inherits="ProfileForm.Pages.IncomeExpenditure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');

        }
    </script>
    <table class="table table-striped" width="100%">
        <tr>
            <td>६७ क)</td>
            <td>तपाईँको परिवारको आम्दानी विवरण</td>
            <td>

                <asp:HiddenField runat="server" ID="hidIncomeSourceId" Value='<%#Eval("INCOME_SOURCE_ID")%>' />


                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>१ </td>
            <td>कृषि तथा पशुपालन</td>
            <td>
                <asp:TextBox ID="txtAgriculture" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>२ </td>
            <td>व्यापार तथा उद्योग </td>
            <td>
                <asp:TextBox ID="txtBusiness" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>३</td>
            <td>नोकरी तथा पेन्सन</td>
            <td>
                <asp:TextBox ID="txtJobs" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>४</td>
            <td>वैदेशिक रोजगार</td>
            <td>
                <asp:TextBox ID="txtForeignEmployment" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>५</td>
            <td>ज्याला मजदुरी</td>
            <td>
                <asp:TextBox ID="txtWageLabour" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>६ </td>
            <td>घरभाडा</td>
            <td>
                <asp:TextBox ID="txtHouseRent" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>७</td>
            <td>अन्य</td>
            <td>
                <asp:TextBox ID="txtOthersIncome" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td>६७ ख)</td>
            <td>परिवारको खर्च विवरण (वार्षिक)</td>
            <td>&nbsp;</td>
            <td>&nbsp;
            
                                <asp:HiddenField runat="server" ID="hidAnnualExpenditureId" Value='<%#Eval("ANNUAL_EXPENDITURE_ID")%>' />

            </td>
        </tr>


        <tr>
            <td>१ </td>
            <td>खाना खर्च</td>
            <td>
                <asp:TextBox runat="server" ID="txtFooding" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>२ </td>
            <td>लत्ता कपडा</td>
            <td>
                <asp:TextBox ID="txtClothing" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>३</td>
            <td>शिक्षा</td>
            <td>
                <asp:TextBox ID="txtEducation" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>४</td>
            <td>स्वास्थ्य उपचार खर्च</td>
            <td>
                <asp:TextBox ID="txtHealth" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>५</td>
            <td>चाडपर्व, विवाह खर्च तथा
मनोरञ्जन</td>
            <td>
                <asp:TextBox ID="txtFestivity" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>६ </td>
            <td>महसूल भुक्तानी खर्च ( विद्युत, सञ्चार, खानेपानी,आदि</td>
            <td>
                <asp:TextBox ID="txtPaymentCharge" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>७</td>
            <td>अन्य</td>
            <td>
                <asp:TextBox ID="txtOthersExpenditure" runat="server" Text="0" TextMode="Number"></asp:TextBox></td>
            <td></td>
            <td></td>
        </tr>




        <tr>
            <td>&nbsp;</td>
        </tr>


        <tr>
            <td>६८</td>

            <td>तपाईँको परिवारको कृषिजन्य आम्दानीले वर्षभरीमा कति महिना खान पुग्?</td>
            <td>

                <asp:RadioButtonList ID="rbdAgriTime" runat="server">

                    <asp:ListItem Value="0">महिना</asp:ListItem>
                    <asp:ListItem Value="1">वर्ष भरी खान पूगेर पनि उब्रिन्छ</asp:ListItem>
                    
                    <asp:ListItem Value="2">कृषि पेशा नभए अन्य पेशाबाट खाने</asp:ListItem>

                </asp:RadioButtonList>
                <asp:TextBox ID="txtMonth" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:HiddenField runat="server" ID="hidAgroIncomeId" Value='<%#Eval("AGRO_INCOME_ID") %>' />

            </td>


        </tr>
        <tr>
            <td>६९</td>
            <td>तपाईँको परिवारको कुनै प्रयोजनका लागि ऋण लिनु भएको छ?</td>
            <td>

                <asp:RadioButtonList ID="rbdLoan" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbdLoan_SelectedIndexChanged" AutoPostBack="true">

                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:HiddenField runat="server" ID="hidLoanId" Value='<%#Eval("LOAN_ID")%>' />

            </td>
        </tr>
        <tr>
            <td></td>
            <td>यदि छ भने ऋण कहाँबाट लिनुभयो ?
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rbdInstitution">
                    <asp:ListItem Value="0"> बैंक वा वित्तीय संस्था</asp:ListItem>
                    <asp:ListItem Value="1"> सहकारी संस्था</asp:ListItem>
                    <asp:ListItem Value="2">  समूह वा सामुदायिक संस्था</asp:ListItem>
                    <asp:ListItem Value="3"> साहु महाजन</asp:ListItem>
                    <asp:ListItem Value="4"> अन्य ..</asp:ListItem>
                </asp:RadioButtonList></td>
        </tr>

        <tr>
            <td colspan="3">
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <table class="table table-striped" width="100%">
                            <tr>
                                <td>७०</td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hidLoanDetailsId" Value='<%#Eval("LOAN_DETAIL_ID")%>' />

                                </td>
                                <td>के के प्रयोजनका लागि ऋण लिनु भएको छ?<br />
                                    (एक भन्दा बढि उत्तर आउन सक्छ)</td>
                                <td>
                                    <asp:CheckBoxList ID="chkLoanUse" runat="server">
                                        <asp:ListItem Value="0">परिवाको सदस्यलाई विदेश पठाउन</asp:ListItem>
                                        <asp:ListItem Value="1">खानपानको लागि</asp:ListItem>
                                        <asp:ListItem Value="2">स्वाथ्य उपचारको लागि</asp:ListItem>
                                        <asp:ListItem Value="3">शिक्षाको लागि / पढाउने कर्चका लागि</asp:ListItem>
                                        <asp:ListItem Value="4">व्यवसाय गर्न</asp:ListItem>
                                        <asp:ListItem Value="5">विवाह गर्न</asp:ListItem>
                                        <asp:ListItem Value="6">सामाजिक/ धार्मिक कार्य</asp:ListItem>
                                        <asp:ListItem Value="7">अन्य (खुलाउने)</asp:ListItem>
                                    </asp:CheckBoxList>
                                    <br />
                                    <asp:TextBox ID="txtLoanUse" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>



        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td align="right">
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnNext_Click" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btnUpdate_Click"
                    Visible="False" />
            </td>

        </tr>
    </table>
</asp:Content>
