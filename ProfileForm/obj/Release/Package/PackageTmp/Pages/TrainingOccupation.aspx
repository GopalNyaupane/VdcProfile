﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TrainingOccupation.aspx.cs" Inherits="ProfileForm.Pages.TrainingOccupation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');

        }
    </script>
    <asp:ScriptManager ID="ScriptManager" runat="server" />
    <asp:UpdatePanel ID="upnlMarrige" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddNewTraining" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table class="table table-bordered">
                <tr>
                    <td>५५.</td>
                    <td>परिवारका सदस्यहरुको जनचेतनामूलक तालीम
                        <br />
                        (स्वास्थ्य, सुरक्षित मातृत्व, परिवार नियोजन, सशक्तीकरण, लैंगिक समविकास,<br />
                        बाल विकास, संस्थागत नेतृत्व, खानेपानी तथा सरसफाई, उपभोक्ता समिति,
                        <br />
                        बाल अधिकार, बाल संरक्षण, आदि) प्राप्त गरेको विवरण</td>

                    <td><b>लम्बाई</b> </td>
                    <td><b>संख्या</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td>तालिमको नाम</td>
                    <td></td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>पुरुष </td>
                                <td>महिला </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <asp:Panel runat="server" ID="pnlA">

                    <asp:Repeater ID="rptrFamilyAwarenessTraining" runat="server">

                        <ItemTemplate>
                            <tr id="<%#Eval("FAMILY_AWARENESS_TRAINING_ID") %>" class="div_row">
                                <td></td>
                                <td>
                                    <asp:TextBox ID="txtTrainingName" runat="server" Text='<%# Eval("TRAINING_NAME") %>'></asp:TextBox></td>
                                <td>
                                    <asp:RadioButtonList ID="rblDuration" runat="server" RepeatDirection="Vertical" SelectedValue='<%#Eval("DURATION_ID") %>'>
                                        <asp:ListItem Value="0">३ दिनसम्मको तालिम</asp:ListItem>
                                        <asp:ListItem Value="1">३ देखि ७ दिनसम्मको</asp:ListItem>
                                        <asp:ListItem Value="2">१ देखि ४ हप्तासम्मको</asp:ListItem>
                                        <asp:ListItem Value="3">४ हप्ताभन्दा बढी अवधिको</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtMaleNo" runat="server" TextMode="Number" Text='<%# Eval("MALE_NO") %>'></asp:TextBox></td>
                                            <td>
                                                <asp:TextBox ID="txtFemaleNo" runat="server" TextMode="Number" Text='<%# Eval("FEMALE_NO") %>'> </asp:TextBox></td>
                                        </tr>
                                    </table>
                                    <td>
                                        <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn  btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                                    </td>
                                </td>
                                <td>
                                    <asp:HiddenField ID="hidAwareness" runat="server" Value='<%#Eval("FAMILY_AWARENESS_TRAINING_ID") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <asp:Button runat="server" Text="Add Another" CssClass="btn btn-primary" ID="btnAddNewTraining" OnClick="btnAddNewTraining_OnClick" /></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddNewEconomicTraining" EventName="Click" />
        </Triggers>
        <ContentTemplate>

            <table class="table table-bordered">
                <tr>
                    <td>५६.</td>
                    <td>परिवारका सदस्यहरुको व्यवसायिक
                        <br />
                        (जे.टि.ए. कृषि वा पशु, विद्युत जडान, कम्प्युटर, मेकानिक्स, सिलाई वुनाई, आदि)
                        <br />
                        तालिम प्राप्त गरेको विवरण</td>

                    <td><b>लम्बाई</b> </td>
                    <td><b>संख्या</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td>तालिमको नाम</td>
                    <td></td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>पुरुष </td>
                                <td>महिला </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <asp:Panel runat="server" ID="pnlE">
                    <asp:Repeater ID="rptrEconomicTraining" runat="server">

                        <ItemTemplate>
                            <tr id="<%#Eval("FAMILY_ECONOMIC_TRAINING_ID") %>" class="div_row">
                                <td></td>
                                <td>
                                    <asp:TextBox ID="txtEcoTrainingName" runat="server" Text='<%# Eval("TRAINING_NAME") %>'></asp:TextBox></td>
                                <td>
                                    <asp:RadioButtonList ID="rblEcoDuration" runat="server" RepeatDirection="Vertical" SelectedValue='<%#Eval("DURATION_ID") %>'>
                                        <asp:ListItem Value="0">१ महिनाभन्दा कम अवधिको तालिम प्राप्तवाप्रारम्भिक तहको प्राविधिक</asp:ListItem>
                                        <asp:ListItem Value="1">१ देखि ६ महिनासम्म अवधिको तालिम प्राप्त वा पहिलो तहको प्राविधिक</asp:ListItem>
                                        <asp:ListItem Value="2">६ देखि १२ महिनासम्मको तालिम प्राप्त वा दोस्रो तहको प्राविधिक</asp:ListItem>
                                        <asp:ListItem Value="3">१२ महिनाभन्दा वढी अवधिको तालिम प्राप्त वा तेस्रो तह भन्दा माथिको प्राविधिक</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtEcoMaleNo" runat="server" TextMode="Number" Text='<%# Eval("MALE_NO") %>'></asp:TextBox></td>
                                            <td>
                                                <asp:TextBox ID="txtEcoFemaleNo" runat="server" TextMode="Number" Text='<%# Eval("FEMALE_NO") %>'> </asp:TextBox></td>
                                        </tr>
                                    </table>
                                    <td>
                                        <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                                    </td>
                                </td>
                                <td>
                                    <asp:HiddenField ID="hidEconomic" runat="server" Value='<%#Eval("FAMILY_ECONOMIC_TRAINING_ID") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <asp:Button runat="server" Text="Add Another" CssClass="btn btn-primary" ID="btnAddNewEconomicTraining" OnClick="btnAddNewEconomicTraining_OnClick" /></td>
                </tr>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="rblPreShleter" EventName="SelectedIndexChanged" />
        </Triggers>
        <ContentTemplate>
            <table class="table table-striped">

                <tr>
                    <td>५७.</td>
                    <td>परिवारको मुख्य पेशा ? (एकमा मात्र चिन्ह लगाउने)</td>
                    <td>
                        <asp:RadioButtonList ID="rblOccupation" runat="server"
                            DataSourceID="XmlDataSource_Occupation" DataTextField="name"
                            DataValueField="id">
                        </asp:RadioButtonList>
                        <asp:XmlDataSource ID="XmlDataSource_Occupation" runat="server"
                            DataFile="~/XMLDataSource/Occupation.xml"></asp:XmlDataSource>
                    </td>
                    <asp:HiddenField ID="hidOccupation" runat="server" Value='<%#Eval("") %>' />
                </tr>
                <asp:Panel ID="migPanel" runat="server">
                    <tr>

                        <td>५८.</td>
                        <td>आजभन्दा पाँच वर्ष पहिले तपाईंको परिवारको बसोवास कहाँ थियो ?</td>
                        <td>
                            <asp:RadioButtonList ID="rblPreShleter" runat="server" RepeatDirection="Vertical" OnSelectedIndexChanged="rblPreShleter_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">अन्यत्र स्थानमा</asp:ListItem>
                                <asp:ListItem Value="1">यसै स्थानमा</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <asp:HiddenField ID="hidShelter" runat="server" />
                    </tr>
                    <tr>
                        <td></td>
                        <td>अन्यत्र स्थानबाट आएको भए यहाँ किन आउनु भएको ?</td>
                        <td>
                            <asp:RadioButtonList ID="rblMigrationReason" runat="server"
                                DataSourceID="XmlDataSource_MigrationReason" DataTextField="name"
                                DataValueField="id">
                            </asp:RadioButtonList>
                            <asp:XmlDataSource ID="XmlDataSource_MigrationReason" runat="server"
                                DataFile="~/XMLDataSource/MigrationReason.xml"></asp:XmlDataSource>
                        </td>
                        <asp:HiddenField ID="hidReason" runat="server" />
                    </tr>
                </asp:Panel>

                <tr>
                    <td>५९.</td>
                    <td>घरको छानामा प्रयोग भएको सामाग्री</td>
                    <td colspan="2">
                        <asp:RadioButtonList ID="rblRoof" runat="server" DataSourceID="XmlDataSource_Roof"
                            DataTextField="name" DataValueField="id">
                        </asp:RadioButtonList>
                        <asp:XmlDataSource ID="XmlDataSource_Roof" runat="server"
                            DataFile="~/XMLDataSource/Roof.xml"></asp:XmlDataSource>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td></td>
                    <td>घरको प्रकार</td>
                    <td colspan="2">
                        <asp:RadioButtonList ID="rblHomeType" runat="server" DataSourceID="XmlDataSource_HomeType"
                            DataTextField="name" DataValueField="id">
                        </asp:RadioButtonList>
                        <asp:XmlDataSource ID="XmlDataSource_HomeType" runat="server"
                            DataFile="~/XMLDataSource/HomeType.xml"></asp:XmlDataSource>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>६०.</td>
                    <td>तपाईको घरमा वत्तीको मुख्य स्रोत के हो ?</td>
                    <td>
                        <asp:RadioButtonList ID="rblLightSource" runat="server" DataSourceID="XmlDataSource_ElectricitySource"
                            DataTextField="name" DataValueField="id">
                        </asp:RadioButtonList>
                        <asp:XmlDataSource ID="XmlDataSource_ElectricitySource" runat="server"
                            DataFile="~/XMLDataSource/ElectricitySource.xml"></asp:XmlDataSource>
                    </td>
                </tr>

                <asp:Panel ID="pnlFuel" runat="server">
                    <tr>
                        <td>६१.</td>
                        <td>तपाईँको परिवारमा खाना पकाउन प्रयोग हुने मुख्य इन्धन कुन हो?</td>
                        <td colspan="2">
                            <asp:RadioButtonList ID="rblFuel" runat="server"
                                DataSourceID="XmlDataSource_CookingFuel" DataTextField="name" OnSelectedIndexChanged="rblFuel_SelectedIndexChanged" AutoPostBack="true"
                                DataValueField="id">
                            </asp:RadioButtonList>
                            <asp:XmlDataSource ID="XmlDataSource_CookingFuel" runat="server"
                                DataFile="~/XMLDataSource/CookingFuel.xml"></asp:XmlDataSource>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>दाउरा हो भने ,दाउराको मुख्य श्रोत के हो ?</td>
                        <td colspan="2">
                            <asp:RadioButtonList ID="rblFireWoodSource" runat="server" RepeatDirection="Vertical">
                                <asp:ListItem Value="0">सामूदायिक वन</asp:ListItem>
                                <asp:ListItem Value="1">निजी वन </asp:ListItem>
                                <asp:ListItem Value="2">नदी नाला अन्य  </asp:ListItem>
                                <asp:ListItem Value="3">अन्य  </asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </asp:Panel>

                <tr>
                    <td>६२. </td>
                    <td>तपाईंको घरमा कस्तो प्रकारको चुल्हो प्रयोग गर्नुहुन्छ ?</td>
                    <td colspan="2">
                        <asp:RadioButtonList ID="rblStove" runat="server"
                            DataSourceID="XmlDataSource_CookingOven" DataTextField="name"
                            DataValueField="id">
                        </asp:RadioButtonList>
                        <asp:XmlDataSource ID="XmlDataSource_CookingOven" runat="server"
                            DataFile="~/XMLDataSource/CookingOven.xml"></asp:XmlDataSource>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <asp:HiddenField ID="hidStatus" runat="server" />
                </tr>
                <tr>
                    <td>६३. </td>
                    <td>तपार्इंको घरमा संचारका साधनहरु के के रहेका छन् ? (वहु उत्तर)</td>
                    <td colspan="2">
                        <asp:CheckBoxList ID="cblTeleSource" runat="server"
                            DataSourceID="XmlDataSource_Telecommunication" DataTextField="name"
                            DataValueField="id">
                        </asp:CheckBoxList>
                        <asp:XmlDataSource ID="XmlDataSource_Telecommunication" runat="server"
                            DataFile="~/XMLDataSource/Telecommunication.xml"></asp:XmlDataSource>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <asp:HiddenField ID="hidComm" runat="server" />
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                        <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary"
                            OnClick="btnNext_Click" OnClientClick="return validatePage();" />
                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary"
                            OnClick="btnUpdate_Click" OnClientClick="return validatePage();" />
                    </td>

                </tr>


            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
