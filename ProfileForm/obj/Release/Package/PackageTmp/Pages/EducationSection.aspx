﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EducationSection.aspx.cs" Inherits="ProfileForm.Pages.EducationSection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');

        }
    </script>
    <table class="table table-striped">
        <tr>
            <td>४२.</td>
            <td>तपाईको परिवारमा बाल विकास केन्द्र वा पूर्व प्रा.वि. तहमा जाने ३ देखि ४ वर्षसम्मका बालबालिका संख्या कति छ?</td>
            <td>बालक:
                <br />
                <asp:TextBox ID="txtBoyCnt" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                <br />
                बालिका:
                <br />
                <asp:TextBox ID="txtGrlCnt" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                <br />
                तेस्रो लिंगी :<br />
                <asp:TextBox ID="txtOthrCnt" runat="server" TextMode="Number" Text="0"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td>४३.</td>
            <td>परिवारको शैक्षिक स्थिति</td>
            <td></td>
        </tr>
    </table>

    <table class="table table-bordered">
        <tr>
            <td>उमेर समूह </td>
            <td>लेखपढ गर्न नसक्ने/निरक्षर</td>
            <td>लेखपढ गर्न सक्ने/साक्षर </td>
            <td>एस. एल. सी. वा सो सरह पास गरेका संख्या</td>
            <td>प्रविणता प्रमाण पत्र, इन्टरमिडियट १०+२ वा सो सरह उत्तीर्ण गरेको</td>
            <td>स्नातक (बि. ए. वा सो सरह ) तह पास गरेका </td>
            <td>स्नातकोत्तर (एम. ए. वा सो सर ह) तह पास गरेका </td>
            <td>विद्यावारि िध (पि. एच. डी.) हासिल गरेका </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>पुरुष
                        </td>
                        <td>महिला
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>पुरुष
                        </td>
                        <td>महिला
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>पुरुष
                        </td>
                        <td>महिला
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>पुरुष
                        </td>
                        <td>महिला
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>पुरुष
                        </td>
                        <td>महिला
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>पुरुष
                        </td>
                        <td>महिला
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>पुरुष </td>
                        <td>महिला </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>६ – १८ वर्ष </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtIletratMaleG0" Text="0"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtIletratFemaleG0"
                                Text="0"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtLtrtMaleG0" Text="0"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtLtrtFemaleG0" Text="0"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtSlcPsMaleG0" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtSlcPsFemaleG0" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPlsPsMaleG0" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPlsPsFemaleG0" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtBchlrMaleG0" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtBchlrFemaleG0" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMsPsMaleG0" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMsPsFemaleG0" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPhdPsMaleG0" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPhdPsFemaleG0" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>१९ – ४५ वर्ष  </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtIletratMaleG1" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtIletratFemaleG1" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtLtrtMaleG1" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtLtrtFemaleG1" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtSlcPsMaleG1" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtSlcPsFemaleG1" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPlsPsMaleG1" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPlsPsFemaleG1" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtBchlrMaleG1" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtBchlrFemaleG1" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMsPsMaleG1" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMsPsFemaleG1" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPhdPsMaleG1" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPhdPsFemaleG1" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>४६ – ६० वर्ष</td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtIletratMaleG2" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtIletratFemaleG2" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtLtrtMaleG2" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtLtrtFemaleG2" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtSlcPsMaleG2" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtSlcPsFemaleG2" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPlsPsMaleG2" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPlsPsFemaleG2" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtBchlrMaleG2" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtBchlrFemaleG2" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMsPsMaleG2" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMsPsFemaleG2" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPhdPsMaleG2" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPhdPsFemaleG2" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>६० वर्षभन्दा माथि  </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtIletratMaleG3" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtIletratFemaleG3" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtLtrtMaleG3" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtLtrtFemaleG3" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtSlcPsMaleG3" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtSlcPsFemaleG3" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPlsPsMaleG3" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPlsPsFemaleG3" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtBchlrMaleG3" Text="0"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtBchlrFemaleG3" Text="0"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>

            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMsPsMaleG3" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMsPsFemaleG3" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPhdPsMaleG3" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPhdPsFemaleG3" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>

        </tr>


    </table>
    <table class="table table-striped">

        <tr>
            <td>४४.</td>
            <td>विद्यालयस्तरमा अध्ययन गरिरहेका बालबालिकाको विवरण</td>
            <td></td>


        </tr>
    </table>
    <table class="table table-bordered">
        <tr>
            <td>अध्ययनरत तह  </td>
            <td>प्रारम्भिक बालविकास केन्द्रमा जाने बालबालिकाको संख्या</td>
            <td>गा.वि.स. अन्तर्गतका विद्यालयमा अध्ययनरत संख्या  </td>
            <td>गा.वि.स. वाहिरका विद्यालयमा अध्ययनरत संख्या</td>

        </tr>
        <tr>
            <td></td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>बालक</td>
                        <td>बालिका </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>बालक</td>
                        <td>बालिका </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>बालक</td>
                        <td>बालिका </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>प्रा.वि. तह  </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPraBoy" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPraGirl" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPraVdcSchBoy" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPraVdcSchGirl" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPraOutVdcSchBoy" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtPraOutVdcSchGirl"
                                Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td>नि.मा.वि. तह   </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtNiBoy" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtNiGirl" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtNiVdcSchBoy" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtNiVdcSchGirl" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtNiOutVdcSchBoy" Text="0"></asp:TextBox></td>
                        <td>
                            <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtNiOutVdcSchGirl" Text="0"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>

                <tr>
                    <td>मा.वि. तह  </td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMabiBoy" Text="0"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMabiGirl" Text="0"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMabiVdcSchBoy" Text="0"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMabiVdcSchGirl" Text="0"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMabiOutVdcSchBoy" Text="0"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtMabiOutVdcSchGirl" Text="0"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td>उच्च मा.वि. तह   </td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtHghrBoy" Text="0"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtHghrGirl" Text="0"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtHghrVdcBoy" Text="0"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtHghrVdcGirl" Text="0"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtHghrOutVdcBoy" Text="0"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox runat="server" Width="40" TextMode="Number" ID="txtHghrOutVdcGirl" Text="0"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>

                </tr>
    </table>
    <table class="table table-striped">
        <tr>
            <td>४५.</td>
            <td>घरबाट विद्यालय जान लाग्ने समय</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><b>प्रा.वि.: </b></td>
            <td>
                <asp:RadioButtonList ID="rdoTimToRchPra" runat="server">
                    <asp:ListItem Value="0"> १५ मिनेट भन्दा कम </asp:ListItem>
                    <asp:ListItem Value="1"> १५–३० मि.सम्म</asp:ListItem>
                    <asp:ListItem Value="2"> ३० मि.– १ घण्टा</asp:ListItem>
                    <asp:ListItem Value="3"> १ घण्टाभन्दा बढी</asp:ListItem>

                </asp:RadioButtonList>
                <br />
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><b>नि.मा.वि.: </b></td>
            <td>
                <asp:RadioButtonList ID="rdoTimToRchNima" runat="server">
                    <asp:ListItem Value="0"> १५ मिनेट भन्दा कम </asp:ListItem>
                    <asp:ListItem Value="1"> १५–३० मि.सम्म</asp:ListItem>
                    <asp:ListItem Value="2"> ३० मि.– १ घण्टा</asp:ListItem>
                    <asp:ListItem Value="3"> १ घण्टाभन्दा बढी</asp:ListItem>

                </asp:RadioButtonList>
                <br />
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><b>मा.वि.:</b></td>
            <td>
                <asp:RadioButtonList ID="rdoTimToRchMabi" runat="server">
                    <asp:ListItem Value="0"> १५ मिनेट भन्दा कम </asp:ListItem>
                    <asp:ListItem Value="1"> १५–३० मि.सम्म</asp:ListItem>
                    <asp:ListItem Value="2"> ३० मि.– १ घण्टा</asp:ListItem>
                    <asp:ListItem Value="3"> १ घण्टाभन्दा बढी</asp:ListItem>

                </asp:RadioButtonList>
                <br />
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><b>उच्च मा.वि. :</b></td>
            <td>
                <asp:RadioButtonList ID="rdoTimToRchHigher" runat="server">
                    <asp:ListItem Value="0"> १५ मिनेट भन्दा कम </asp:ListItem>
                    <asp:ListItem Value="1"> १५–३० मि.सम्म</asp:ListItem>
                    <asp:ListItem Value="2"> ३० मि.– १ घण्टा</asp:ListItem>
                    <asp:ListItem Value="3"> १ घण्टाभन्दा बढी</asp:ListItem>

                </asp:RadioButtonList>
                <br />
            </td>
        </tr>



        <tr>
            <td>४६.</td>
            <td>तपाईको बालवालिका पढ्ने विद्यालयमा बालमैत्री शौचालय छ ?</td>
            <td>
                <asp:RadioButtonList ID="rdoFrndlyToiletPresnt" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>


                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>४७.</td>
            <td>तपाईंको परिवारका बालबालिका पढ्ने विद्यालयमा अपाङ्गता भएका विद्यार्थीका लागि विशेष शिक्षाको सुविधा छ ?</td>
            <td>
                <asp:RadioButtonList ID="rdoSpecEduForDisable" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>
                    <asp:ListItem Value="2">थाहा छैन</asp:ListItem>


                </asp:RadioButtonList>
            </td>
        </tr>


        <tr>
            <td></td>
            <td>&nbsp;</td>
            <td></td>
            <td>
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary"
                    OnClick="btnNext_Click" OnClientClick="return validatePage();" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btnUpdate_OnClick"
                    OnClientClick="return validatePage();" Visible="False" />
            </td>

        </tr>
    </table>
</asp:Content>
