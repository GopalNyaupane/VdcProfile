﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Section1Introduction.aspx.cs" Inherits="ProfileForm.Pages.Section1Introduction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
<script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
<script src="../Scripts/jquery/chosen.jquery.js" type="text/javascript"></script>
<script src="../Scripts/jquery/chosen.jquery.min.js" type="text/javascript"></script>
    
<script type="text/javascript">

    $(document).ready(function () {
        $(".chosen-select").chosen();
        $(".chosen-choices").chosen();
    });

    </script>
    <script type="text/javascript">

        function validatePage() {
            var check = false;

            if ($("#MainContent_txtTole").val() == '') {
                alert("टोल बस्तीको नाम राख्नुहोस");
                check = false;
            }
            else if ($("#MainContent_rdoSex").val() < 0) {
                alert("घरमुलीको लिङ्ग ?");
                check = false;

            }
            else if ($("#MainContent_ddlCaste").val() < 0) {
                alert("जातिको नाम राख्नुहोस");
                check = false;

            }
            else if ($("#MainContent_ddlLanguage").val() < 0) {
                alert("भाषाको नाम राख्नुहोस");
                check = false;

            }
            else if ($("#MainContent_ddlReligion").val() < 0) {
                alert("धर्मको नाम राख्नुहोस");
                check = false;

            }

            else if ($("#MainContent_txtGharMuli").val() == '') {
                alert("घरमुलीको नाम राख्नुहोस");
                check = false;

            }
           
           
           
            else {
                check = true;
            }

            if (check == false) {
                //alert('Missing data. Please complete');
                return false;
            }
            else {
                $("#MainContent_btnNext").val('Processing...');
                $("#MainContent_btnUpdate").val('Processing...');

                //                $("#ContentPlaceHolder1_btnNext").attr("disabled", true);
                return true;
            }
        }
    </script>
    <div>
    
       <%-- ID#:<br />
        गाउँको वश्तुस्थिती विवरण तयारीका लागि घरधुरी सर्वेक्षण फाराम
        <br />
        इटहरा गा. वि. स., मोरङ<br />
        २०७१<br />--%>
        <h3>Introduction</h3><br />
        <table class="table" width="100%">
            <tr>
                <td width="10%">
                    </td>
                <td width="40%">
                     V.D.C. :</td>
                <td width="50%">
                    <asp:TextBox ID="txtVdcName" runat="server" ReadOnly="True"></asp:TextBox> </td>
            </tr>
            <tr>
                <td width="10%">
                    </td>
                <td width="40%">
                    वडा नं. :</td>
                <td width="50%">
                    <asp:TextBox ID="txtWard" runat="server" ReadOnly="True"></asp:TextBox> </td>
            </tr>
           
            <tr>
                <td>
                    </td>
                <td>
                    घर नं. :</td>
                <td>
                    <asp:TextBox ID="txtGharNo" runat="server" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    १.</td>
                <td>
                    घरमुलीको नामथर:</td>
                <td>
                   <asp:TextBox ID="txtGharMuli" runat="server" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    </td>
                <td>
                    घरमुलीको लिङ्ग: </td>
                <td>
                    <asp:RadioButtonList ID="rdoSex" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0">पुरुष</asp:ListItem>
                        <asp:ListItem Value="1">महिला</asp:ListItem>
                         <asp:ListItem Value="2">तेस्रो</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
             <tr>
                <td>
                    २.</td>
                <td>
                    घरमुलीको जात/जातिय:</td>
                <td>
                     <asp:DropDownList ID="ddlCaste" runat="server" DataSourceID="XmlDataSource_Caste" DataTextField="name" CssClass="chosen-choices"
                        DataValueField="id">
                       </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Caste" runat="server" 
                        DataFile="~/XMLDataSource/Caste.xml"></asp:XmlDataSource>
              
                         </td>
            </tr>
            <tr>
                <td>
                    ३.</td>
                <td>
                    घरमुलिको धर्म:</td>
                <td>
                    <asp:DropDownList ID="ddlReligion" runat="server"  DataSourceID="XmlDataSource_Religion" DataTextField="name" 
                        DataValueField="id">
                       </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Religion" runat="server" 
                        DataFile="~/XMLDataSource/Religion.xml"></asp:XmlDataSource>
                        </td>
            </tr>
            <tr>
                <td>
                    ४.</td>
                <td>
                    घर परिवारमा प्राय बोलिने भाषा:</td>
                <td>
                    <asp:DropDownList ID="ddlLanguage" runat="server" DataSourceID="XmlDataSource_Language" DataTextField="name" CssClass="chosen-choices"
                        DataValueField="id">
                    </asp:DropDownList>
                     <asp:XmlDataSource ID="XmlDataSource_Language" runat="server" 
                        DataFile="~/XMLDataSource/Language.xml"></asp:XmlDataSource>
                </td>
            </tr>
             <tr>
                <td>
                    &nbsp;</td>
                 <td>
                    &nbsp;</td>
                <td align="right">
                     <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" 
                         onclick="btnBack_Click"/>
                   <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" 
                         onclick="btnNext_Click" OnClientClick ="return validatePage();"/>
                   <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" 
                         onclick="btnUpdate_Click" OnClientClick ="return validatePage();"/>
                </td>
                
            </tr>
        </table>
    
    </div>
</asp:Content>