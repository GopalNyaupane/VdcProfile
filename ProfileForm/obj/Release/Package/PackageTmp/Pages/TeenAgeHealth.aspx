﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TeenAgeHealth.aspx.cs" Inherits="ProfileForm.Pages.TeenAgeHealth" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" />
    <script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');

        }
    </script>

    <asp:UpdatePanel ID="upnlMarrige" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddAnother" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table class="table table-bordered">
                <tr>
                    <td>१९.</td>
                    <td>तपाईको परिवारको १८ वर्षभन्दा मुनिको सदस्यलाई कुनै दीर्घ रोग लागेको छ ? </td>
                    <td>
                        <asp:RadioButtonList ID="rdoIsDisease" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdoIsDisease_SIC">
                            <asp:ListItem Value="0">छ</asp:ListItem>
                            <asp:ListItem Value="1">छैन</asp:ListItem>

                        </asp:RadioButtonList></td>


                </tr>
                <tr>
                    <td></td>
                    <td>परिवारको १८ वर्षभन्दा मुनिको कति जना सदस्यलाई कुनै दीर्घ रोग लागेको छ? </td>
                    <td>
                        <asp:Button runat="server" Text="AddAnother" ID="btnAddAnother" OnClick="btnAddHealth_Click" CssClass="btn btn-primary" /></td>
                </tr>
            </table>


            <table class="table table-striped">
                <tr>
                    <td>सि. नं.</td>
                    <td>रोगको नाम </td>
                    <td>लिङ्ग   </td>

                </tr>
                <asp:Panel runat="server" ID="pnlTeenAgeHealth">
                    <asp:Repeater runat="server" ID="RpterTeenAgeHealth">
                        <ItemTemplate>
                            <tr id="<%#Eval("TEENAGE_HEALTH_ID") %>" class="div_row">
                                <td></td>
                                <td>

                                    <asp:DropDownList ID="ddlDisease" runat="server" DataSourceID="XmlDataSource_ChildDisease"
                                        DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DISEASE_ID")%>'>
                                    </asp:DropDownList>
                                    <asp:XmlDataSource ID="XmlDataSource_ChildDisease" runat="server" DataFile="~/XMLDataSource/ChildDisease.xml"></asp:XmlDataSource>
                                </td>

                                <td>
                                    <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID")%>'>
                                        <asp:ListItem Value="0">बालक </asp:ListItem>
                                        <asp:ListItem Value="1">बालिका</asp:ListItem>
                                        <asp:ListItem Value="2">तेस्रो लिंगी</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn  btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                                </td>

                                <td>
                                    <asp:HiddenField runat="server" ID="hidTeenHealthId" Value='<%#Eval("TEENAGE_HEALTH_ID") %>' />

                                </td>



                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddAnother" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table class="table table-bordered">
                <tr>
                    <td></td>
                    <td>तपाईको परिवारको १८ वर्षभन्दा माथिको सदस्यलाई कुनै दीर्घ रोग लागेको छ ? </td>
                    <td>
                        <asp:RadioButtonList ID="rdoAdult" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdoIsAdult_SIC">
                            <asp:ListItem Value="0">छ</asp:ListItem>
                            <asp:ListItem Value="1">छैन</asp:ListItem>

                        </asp:RadioButtonList></td>


                </tr>
                <tr>
                    <td></td>
                    <td>परिवारको १८ वर्षभन्दा माथिको कति जना सदस्यलाई कुनै दीर्घ रोग लागेको छ? </td>
                    <td>
                        <asp:Button runat="server" Text="AddAnother" ID="btnAddAnother1" OnClick="btnAddAdultHealth_Click" CssClass="btn btn-primary" /></td>
                </tr>
            </table>


            <table class="table table-striped">
                <tr>
                    <td>सि. नं.</td>
                    <td>रोगको नाम </td>
                    <td>लिङ्ग   </td>

                </tr>
                <asp:Panel runat="server" ID="Panel3">
                    <asp:Repeater runat="server" ID="rptrAdultHealth">
                        <ItemTemplate>
                            <tr id="<%#Eval("ADULT_HEALTH_ID") %>" class="div_row">
                                <td></td>
                                <td>

                                    <asp:DropDownList ID="ddlAdultDisease" runat="server" DataSourceID="XmlDataSource_Disease"
                                        DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DISEASE_ID")%>'>
                                    </asp:DropDownList>
                                    <asp:XmlDataSource ID="XmlDataSource_Disease" runat="server" DataFile="~/XMLDataSource/Disease.xml"></asp:XmlDataSource>
                                </td>

                                <td>
                                    <asp:RadioButtonList ID="rdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID")%>'>
                                        <asp:ListItem Value="0">महिला </asp:ListItem>
                                        <asp:ListItem Value="1">पुरुस</asp:ListItem>
                                        <asp:ListItem Value="2">तेस्रो लिंगी</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn  btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                                </td>

                                <td>
                                    <asp:HiddenField runat="server" ID="hidAdultHealthId" Value='<%#Eval("ADULT_HEALTH_ID") %>' />

                                </td>



                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddFatility" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table class="table table-striped">
                <tr>
                    <td>२०.</td>
                    <td>तपाईको परिवारमा दीर्घ रोगको कारण विगत १० वर्षमा १८ वर्ष भन्दामुनिका कुनै सदस्यको मृत्यु भएको छ ?</td>
                    <td>
                        <asp:RadioButtonList ID="rdoIsTeenFatality" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdoIsTeenFatality_SIC">
                            <asp:ListItem Value="0">छ</asp:ListItem>
                            <asp:ListItem Value="1">छैन</asp:ListItem>

                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>तपाईको परिवारमा दीर्घ रोगको कारण विगत १० वर्षमा १८ वर्ष भन्दामुनिका कति जना सदस्यको मृत्यु भएको छ? </td>
                    <td>
                        <asp:Button runat="server" Text="AddAnother" ID="btnAddFatility" OnClick="btnAddFatility_Click" CssClass="btn btn-primary" /></td>
                </tr>

            </table>

            <table class="table table-striped">
                <tr>
                    <td>सि. नं.</td>
                    <td>मृत्यु गराउने रोगको नाम </td>
                    <td>लिङ्ग   </td>


                </tr>
                <asp:Panel runat="server" ID="Panel1">
                    <asp:Repeater runat="server" ID="rptrTeenFatality">
                        <ItemTemplate>
                            <tr id="<%#Eval("TEENAGE_FATALITY_ID") %>" class="div_row">
                                <td></td>
                                <td>
                                    <asp:DropDownList ID="ddlDisease" runat="server" DataSourceID="XmlDataSource_ChildDisease"
                                        DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DISEASE_ID")%>'>
                                    </asp:DropDownList>
                                    <asp:XmlDataSource ID="XmlDataSource_ChildDisease" runat="server" DataFile="~/XMLDataSource/ChildDisease.xml"></asp:XmlDataSource>
                                </td>

                                <td>
                                    <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID")%>'>
                                        <asp:ListItem Value="0">बालक </asp:ListItem>
                                        <asp:ListItem Value="1">बालिका</asp:ListItem>
                                        <asp:ListItem Value="2">तेस्रो लिंगी</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn  btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hidTeenageFatalityId" Value='<%#Eval("TEENAGE_FATALITY_ID") %>' />
                                </td>


                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddTeen" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table class="table table-striped">

                <tr>
                    <td>२१.</td>
                    <td>बितेको १ वर्षभित्र तपाईंको परिवारमा ५ वर्षमुनिका कुनै बालबालिकाको मृत्यु भएको छ ?</td>
                    <td>
                        <asp:RadioButtonList ID="rdoIsChildFatality" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdoIsChildFatality_SIC">
                            <asp:ListItem Value="0">छ</asp:ListItem>
                            <asp:ListItem Value="1">छैन</asp:ListItem>

                        </asp:RadioButtonList>
                    </td>

                </tr>
                <tr>
                    <td></td>
                    <td>बितेको १ वर्षभित्र तपाईंको परिवारमा ५ वर्षमुनिका कति जना बालबालिकाको मृत्यु भएको छ? </td>
                    <td>
                        <asp:Button runat="server" Text="AddAnother" ID="btnAddTeen" OnClick="btnAddChild_Click" CssClass="btn btn-primary" /></td>
                </tr>

            </table>


            <table class="table table-striped">
                <tr>
                    <td>सि. नं.</td>
                    <td>मृत्यु गराउने रोगको नाम </td>
                    <td>लिङ्ग   </td>

                </tr>
                <asp:Panel runat="server" ID="Panel2">
                    <asp:Repeater runat="server" ID="rptrChildFatality">
                        <ItemTemplate>
                            <tr id="<%#Eval("CHILD_FATALITY_ID") %>" class="div_row">
                                <td></td>

                                <td>
                                    <asp:DropDownList ID="ddlDisease" runat="server" DataSourceID="XmlDataSource_ChildDisease"
                                        DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DEAD_REASON")%>'>
                                    </asp:DropDownList>
                                    <asp:XmlDataSource ID="XmlDataSource_ChildDisease" runat="server" DataFile="~/XMLDataSource/ChildDisease.xml"></asp:XmlDataSource>

                                </td>

                                <td>
                                    <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID")%>'>
                                        <asp:ListItem Value="0">बालक </asp:ListItem>
                                        <asp:ListItem Value="1">बालिका</asp:ListItem>
                                        <asp:ListItem Value="2">तेस्रो लिंगी</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn  btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hidChildFatalityId" Value='<%#Eval("CHILD_FATALITY_ID") %>' />
                                </td>






                            </tr>


                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table class="table table-responsive">
        <tr>

            <td align="right">
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />

                <asp:Button ID="Next" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="Next_Click" />

                <asp:Button ID="Update" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="Update_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

