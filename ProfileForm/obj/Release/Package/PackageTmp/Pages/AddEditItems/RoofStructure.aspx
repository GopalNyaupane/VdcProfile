﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RoofStructure.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.RoofStructure" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="RoofStrructure Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddRoofStrure" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddRoofStructureClicked" runat="server" Text="Add Roof Structure" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Roof Structure List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="ROOF_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="ROOF_ID" ReadOnly="true" HeaderText="ROOF_ID" SortExpression="ROOF_ID"></asp:BoundField>
            <asp:BoundField DataField="ROOF_NAME" HeaderText="ROOF_NAME" SortExpression="ROOF_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_ROOFSTRUCTURE] WHERE [ROOF_ID] = @ROOF_ID" InsertCommand="INSERT INTO [COM_ROOFSTRUCTURE] ([ROOF_ID], [ROOF_NAME]) VALUES (@ROOF_ID, @ROOF_NAME)" SelectCommand="SELECT * FROM [COM_ROOFSTRUCTURE]" UpdateCommand="UPDATE [COM_ROOFSTRUCTURE] SET [ROOF_NAME] = @ROOF_NAME WHERE [ROOF_ID] = @ROOF_ID">
        <DeleteParameters>
            <asp:Parameter Name="ROOF_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="ROOF_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="ROOF_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ROOF_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="ROOF_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
