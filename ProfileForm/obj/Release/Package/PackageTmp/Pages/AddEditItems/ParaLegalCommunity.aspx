﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ParaLegalCommunity.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.ParaLegalCommunity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Para Legal Community Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddParalagalCommunity" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddParalegalCommunityClicked" runat="server" Text="Add ParalegalCommunity" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="ParalegalCommunity List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="COMMUNITY_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="COMMUNITY_ID" ReadOnly="true" HeaderText="COMMUNITY_ID" SortExpression="COMMUNITY_ID"></asp:BoundField>
            <asp:BoundField DataField="COMMUNITY_NAME" HeaderText="COMMUNITY_NAME" SortExpression="COMMUNITY_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_PARALEGALCOMMUNITY] WHERE [COMMUNITY_ID] = @COMMUNITY_ID" InsertCommand="INSERT INTO [COM_PARALEGALCOMMUNITY] ([COMMUNITY_ID], [COMMUNITY_NAME]) VALUES (@COMMUNITY_ID, @COMMUNITY_NAME)" SelectCommand="SELECT * FROM [COM_PARALEGALCOMMUNITY]" UpdateCommand="UPDATE [COM_PARALEGALCOMMUNITY] SET [COMMUNITY_NAME] = @COMMUNITY_NAME WHERE [COMMUNITY_ID] = @COMMUNITY_ID">
        <DeleteParameters>
            <asp:Parameter Name="COMMUNITY_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="COMMUNITY_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="COMMUNITY_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="COMMUNITY_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="COMMUNITY_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
