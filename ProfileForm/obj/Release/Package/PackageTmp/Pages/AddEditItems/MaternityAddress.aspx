﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaternityAddress.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.MaternityAddress" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Maternity Address" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddMaternityAddress" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnMaternityAdderssClicked" runat="server" Text="Add Maternity Address" />
            </td>
        </tr>
    </table>
    <asp:Label ID="Label1"  runat="server" Text="Maternity List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="ADDRESS_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="ADDRESS_ID" ReadOnly="true" HeaderText="ADDRESS_ID" SortExpression="ADDRESS_ID"></asp:BoundField>
            <asp:BoundField DataField="ADDRESS_NAME" HeaderText="ADDRESS_NAME" SortExpression="ADDRESS_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_MATERNITYADDRESS] WHERE [ADDRESS_ID] = @ADDRESS_ID" InsertCommand="INSERT INTO [COM_MATERNITYADDRESS] ([ADDRESS_ID], [ADDRESS_NAME]) VALUES (@ADDRESS_ID, @ADDRESS_NAME)" SelectCommand="SELECT * FROM [COM_MATERNITYADDRESS]" UpdateCommand="UPDATE [COM_MATERNITYADDRESS] SET [ADDRESS_NAME] = @ADDRESS_NAME WHERE [ADDRESS_ID] = @ADDRESS_ID">
        <DeleteParameters>
            <asp:Parameter Name="ADDRESS_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="ADDRESS_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="ADDRESS_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ADDRESS_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="ADDRESS_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
