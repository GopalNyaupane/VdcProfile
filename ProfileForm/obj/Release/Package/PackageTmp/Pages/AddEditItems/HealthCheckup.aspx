﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HealthCheckup.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.HealthCheckup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="HealthCheckUp Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddHealthCheckup" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddHealthCheckUpClicked" runat="server" Text="Add HealthCheckUp" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="HealthCheckUp List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="HEALTHCHECKUP_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="HEALTHCHECKUP_ID" ReadOnly="true" HeaderText="HEALTHCHECKUP_ID" SortExpression="HEALTHCHECKUP_ID"></asp:BoundField>
            <asp:BoundField DataField="HEALTHCHECKUP_NAME" HeaderText="HEALTHCHECKUP_NAME" SortExpression="HEALTHCHECKUP_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_HEALTHCHECKUP] WHERE [HEALTHCHECKUP_ID] = @HEALTHCHECKUP_ID" InsertCommand="INSERT INTO [COM_HEALTHCHECKUP] ([HEALTHCHECKUP_ID], [HEALTHCHECKUP_NAME]) VALUES (@HEALTHCHECKUP_ID, @HEALTHCHECKUP_NAME)" SelectCommand="SELECT * FROM [COM_HEALTHCHECKUP]" UpdateCommand="UPDATE [COM_HEALTHCHECKUP] SET [HEALTHCHECKUP_NAME] = @HEALTHCHECKUP_NAME WHERE [HEALTHCHECKUP_ID] = @HEALTHCHECKUP_ID">
        <DeleteParameters>
            <asp:Parameter Name="HEALTHCHECKUP_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="HEALTHCHECKUP_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="HEALTHCHECKUP_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="HEALTHCHECKUP_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="HEALTHCHECKUP_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
