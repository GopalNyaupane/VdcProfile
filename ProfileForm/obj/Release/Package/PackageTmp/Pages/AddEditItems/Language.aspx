﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Language.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Language" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
       <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Language Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddLanguage" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddLanguageClicked" runat="server" Text="Add Language" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Language List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="LANGUAGE_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="LANGUAGE_ID" ReadOnly="true" HeaderText="LANGUAGE_ID" SortExpression="LANGUAGE_ID"></asp:BoundField>
            <asp:BoundField DataField="LANGUAGE_NAME" HeaderText="LANGUAGE_NAME" SortExpression="LANGUAGE_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_LANGUAGE] WHERE [LANGUAGE_ID] = @LANGUAGE_ID" InsertCommand="INSERT INTO [COM_LANGUAGE] ([LANGUAGE_ID], [LANGUAGE_NAME]) VALUES (@LANGUAGE_ID, @LANGUAGE_NAME)" SelectCommand="SELECT * FROM [COM_LANGUAGE]" UpdateCommand="UPDATE [COM_LANGUAGE] SET [LANGUAGE_NAME] = @LANGUAGE_NAME WHERE [LANGUAGE_ID] = @LANGUAGE_ID">
        <DeleteParameters>
            <asp:Parameter Name="LANGUAGE_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="LANGUAGE_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="LANGUAGE_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="LANGUAGE_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="LANGUAGE_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
