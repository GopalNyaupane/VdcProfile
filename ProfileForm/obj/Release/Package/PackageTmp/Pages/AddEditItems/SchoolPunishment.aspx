﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SchoolPunishment.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.SchoolPunishment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="School Punishment" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddSchoolPunishment" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddSchoolPunsihmentClicked" runat="server" Text="Add School Punishment Type" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="School Punishment List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="PUNISHMENT_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="PUNISHMENT_ID" ReadOnly="true" HeaderText="PUNISHMENT_ID" SortExpression="PUNISHMENT_ID"></asp:BoundField>
            <asp:BoundField DataField="PUNISHMENT_NAME" HeaderText="PUNISHMENT_NAME" SortExpression="PUNISHMENT_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_SCHOOL_PUNISHMENT] WHERE [PUNISHMENT_ID] = @PUNISHMENT_ID" InsertCommand="INSERT INTO [COM_SCHOOL_PUNISHMENT] ([PUNISHMENT_ID], [PUNISHMENT_NAME]) VALUES (@PUNISHMENT_ID, @PUNISHMENT_NAME)" SelectCommand="SELECT * FROM [COM_SCHOOL_PUNISHMENT]" UpdateCommand="UPDATE [COM_SCHOOL_PUNISHMENT] SET [PUNISHMENT_NAME] = @PUNISHMENT_NAME WHERE [PUNISHMENT_ID] = @PUNISHMENT_ID">
        <DeleteParameters>
            <asp:Parameter Name="PUNISHMENT_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="PUNISHMENT_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="PUNISHMENT_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="PUNISHMENT_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="PUNISHMENT_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
