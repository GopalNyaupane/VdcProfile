﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Telecommunication.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Telecommunication" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Telecommunication Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddTelecommunication" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddTelecommunicationClicked" runat="server" Text="Add Telecommunication" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Telecommunication List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="SOURCE_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="SOURCE_ID" ReadOnly="true" HeaderText="SOURCE_ID" SortExpression="SOURCE_ID"></asp:BoundField>
            <asp:BoundField DataField="SOURCE_NAME" HeaderText="SOURCE_NAME" SortExpression="SOURCE_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_TELECOMMUNICATION] WHERE [SOURCE_ID] = @SOURCE_ID" InsertCommand="INSERT INTO [COM_TELECOMMUNICATION] ([SOURCE_ID], [SOURCE_NAME]) VALUES (@SOURCE_ID, @SOURCE_NAME)" SelectCommand="SELECT * FROM [COM_TELECOMMUNICATION]" UpdateCommand="UPDATE [COM_TELECOMMUNICATION] SET [SOURCE_NAME] = @SOURCE_NAME WHERE [SOURCE_ID] = @SOURCE_ID">
        <DeleteParameters>
            <asp:Parameter Name="SOURCE_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="SOURCE_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="SOURCE_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="SOURCE_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="SOURCE_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
