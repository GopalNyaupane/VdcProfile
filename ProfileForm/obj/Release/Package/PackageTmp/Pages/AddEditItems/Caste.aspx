﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Caste.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Caste" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Caste" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddCaste" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnCasteClicked" runat="server" Text="Add Caste" />
            </td>
        </tr>
    </table>
    <asp:Label ID="Label1"  runat="server" Text="Caste List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" AutoGenerateColumns="False" DataKeyNames="CASTE_ID" DataSourceID="SqlDataSource1" AllowSorting="True" Width="156px" CssClass="table table-hover">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"></asp:CommandField>
            <asp:BoundField DataField="CASTE_ID" HeaderText="CASTE_ID" ReadOnly="True" SortExpression="CASTE_ID"></asp:BoundField>
            <asp:BoundField DataField="CASTE_NAME" HeaderText="CASTE_NAME" SortExpression="CASTE_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_CASTE] WHERE [CASTE_ID] = @CASTE_ID" InsertCommand="INSERT INTO [COM_CASTE] ([CASTE_ID], [CASTE_NAME]) VALUES (@CASTE_ID, @CASTE_NAME)" SelectCommand="SELECT * FROM [COM_CASTE]" UpdateCommand="UPDATE [COM_CASTE] SET [CASTE_NAME] = @CASTE_NAME WHERE [CASTE_ID] = @CASTE_ID">
        <DeleteParameters>
            <asp:Parameter Name="CASTE_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="CASTE_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="CASTE_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="CASTE_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="CASTE_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
