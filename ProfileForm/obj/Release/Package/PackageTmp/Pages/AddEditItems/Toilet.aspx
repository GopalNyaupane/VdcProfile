﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Toilet.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Toilet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Toilet Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddToilet" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnToiletClicked" runat="server" Text="Add Toilet" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Toilet" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="TOILET_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="TOILET_ID" ReadOnly="true" HeaderText="TOILET_ID" SortExpression="TOILET_ID"></asp:BoundField>
            <asp:BoundField DataField="TOILET_NAME" HeaderText="TOILET_NAME" SortExpression="TOILET_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' SelectCommand="SELECT * FROM [COM_TOILET]" DeleteCommand="DELETE FROM [COM_TOILET] WHERE [TOILET_ID] = @TOILET_ID" InsertCommand="INSERT INTO [COM_TOILET] ([TOILET_ID], [TOILET_NAME]) VALUES (@TOILET_ID, @TOILET_NAME)" UpdateCommand="UPDATE [COM_TOILET] SET [TOILET_NAME] = @TOILET_NAME WHERE [TOILET_ID] = @TOILET_ID">
        <DeleteParameters>
            <asp:Parameter Name="TOILET_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="TOILET_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="TOILET_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="TOILET_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="TOILET_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
