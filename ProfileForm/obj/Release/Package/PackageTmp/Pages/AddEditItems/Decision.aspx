﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Decision.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Decision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Decision Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddDecision" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddDecisionClicked" runat="server" Text="Add Decision" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Decision List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="DECISION_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="DECISION_ID" ReadOnly="true" HeaderText="DECISION_ID" SortExpression="DECISION_ID"></asp:BoundField>
            <asp:BoundField DataField="DECISION_NAME" HeaderText="DECISION_NAME" SortExpression="DECISION_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_DECISION] WHERE [DECISION_ID] = @DECISION_ID" InsertCommand="INSERT INTO [COM_DECISION] ([DECISION_ID], [DECISION_NAME]) VALUES (@DECISION_ID, @DECISION_NAME)" SelectCommand="SELECT * FROM [COM_DECISION]" UpdateCommand="UPDATE [COM_DECISION] SET [DECISION_NAME] = @DECISION_NAME WHERE [DECISION_ID] = @DECISION_ID">
        <DeleteParameters>
            <asp:Parameter Name="DECISION_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="DECISION_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="DECISION_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="DECISION_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="DECISION_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
