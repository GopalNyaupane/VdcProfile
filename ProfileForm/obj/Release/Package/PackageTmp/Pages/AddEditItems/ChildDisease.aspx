﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChildDisease.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.ChildDisease" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblnew" visible="false" runat="server" Text="Child Disease" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtnew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnChildDisease" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnChildDiseaseClicked" runat="server" Text="Add Child Disease Name" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Child Disease List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" AutoPostBack="true" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" DataKeyNames="CHILD_DISEASE_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"></asp:CommandField>
            <asp:BoundField DataField="CHILD_DISEASE_ID" ReadOnly="true" HeaderText="CHILD DISEASE ID" SortExpression="CHILD_DISEASE_ID"></asp:BoundField>
            <asp:BoundField DataField="CHILD_DISEASE_NAME" HeaderText="CHILD DISEASE NAME" SortExpression="CHILD_DISEASE_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_CHILD_DISEASE] WHERE [CHILD_DISEASE_ID] = @CHILD_DISEASE_ID" InsertCommand="INSERT INTO [COM_CHILD_DISEASE] ([CHILD_DISEASE_ID], [CHILD_DISEASE_NAME]) VALUES (@CHILD_DISEASE_ID, @CHILD_DISEASE_NAME)" SelectCommand="SELECT * FROM [COM_CHILD_DISEASE]" UpdateCommand="UPDATE [COM_CHILD_DISEASE] SET [CHILD_DISEASE_NAME] = @CHILD_DISEASE_NAME WHERE [CHILD_DISEASE_ID] = @CHILD_DISEASE_ID">
        <DeleteParameters>
            <asp:Parameter Name="CHILD_DISEASE_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="CHILD_DISEASE_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="CHILD_DISEASE_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="CHILD_DISEASE_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="CHILD_DISEASE_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
