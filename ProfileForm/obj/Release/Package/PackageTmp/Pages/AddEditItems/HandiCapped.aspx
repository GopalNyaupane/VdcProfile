﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HandiCapped.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.HandiCapped" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Handicapped Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddHandiCapped" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddHandicappedClicked" runat="server" Text="Add Handicapped Name" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Handicapped List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="HANDICAPPED_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="HANDICAPPED_ID" ReadOnly="true" HeaderText="HANDICAPPED_ID" SortExpression="HANDICAPPED_ID"></asp:BoundField>
            <asp:BoundField DataField="HANDICAPPED_NAME" HeaderText="HANDICAPPED_NAME" SortExpression="HANDICAPPED_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_HANDICAPPED] WHERE [HANDICAPPED_ID] = @HANDICAPPED_ID" InsertCommand="INSERT INTO [COM_HANDICAPPED] ([HANDICAPPED_ID], [HANDICAPPED_NAME]) VALUES (@HANDICAPPED_ID, @HANDICAPPED_NAME)" SelectCommand="SELECT * FROM [COM_HANDICAPPED]" UpdateCommand="UPDATE [COM_HANDICAPPED] SET [HANDICAPPED_NAME] = @HANDICAPPED_NAME WHERE [HANDICAPPED_ID] = @HANDICAPPED_ID">
        <DeleteParameters>
            <asp:Parameter Name="HANDICAPPED_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="HANDICAPPED_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="HANDICAPPED_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="HANDICAPPED_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="HANDICAPPED_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
