﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DrinkingWaterTime.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.DrinkingWaterTime" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Drinking Water Time Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddDrinkingWater" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddDrinkingWaterTime" runat="server" Text="Add Drinking Water Time" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Drinking Water Time" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="DRINKINGWATER_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="DRINKINGWATER_ID" ReadOnly="true" HeaderText="DRINKINGWATER_ID" SortExpression="DRINKINGWATER_ID"></asp:BoundField>
            <asp:BoundField DataField="DRINKINGWATER_NAME" HeaderText="DRINKINGWATER_NAME" SortExpression="DRINKINGWATER_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_DRINKINGWATERTIME] WHERE [DRINKINGWATER_ID] = @DRINKINGWATER_ID" InsertCommand="INSERT INTO [COM_DRINKINGWATERTIME] ([DRINKINGWATER_ID], [DRINKINGWATER_NAME]) VALUES (@DRINKINGWATER_ID, @DRINKINGWATER_NAME)" SelectCommand="SELECT * FROM [COM_DRINKINGWATERTIME]" UpdateCommand="UPDATE [COM_DRINKINGWATERTIME] SET [DRINKINGWATER_NAME] = @DRINKINGWATER_NAME WHERE [DRINKINGWATER_ID] = @DRINKINGWATER_ID">
        <DeleteParameters>
            <asp:Parameter Name="DRINKINGWATER_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="DRINKINGWATER_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="DRINKINGWATER_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="DRINKINGWATER_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="DRINKINGWATER_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
