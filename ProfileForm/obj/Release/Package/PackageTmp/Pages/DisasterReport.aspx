﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DisasterReport.aspx.cs" Inherits="ProfileForm.Pages.DisasterReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <script type="text/javascript" src="Nepali_Datepicker/nepali.datepicker.v2.min.js"></script>
    
    <script type="text/javascript">
        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');

        }
    </script>
    <table class="table table-striped">
        <tr>
            <td>
                ७४.
            </td>
            <td>विगत ३ वर्ष भित्रमा तपाईंको परिवारमा कति मानिसहरु प्रकोपवाट प्रभावित भएका छन् ?  </td>
        </tr>
        <tr>
            <td></td>
            <td> </td>
        </tr>
        
        
    </table>
      <table class="table table-striped table-bordered">
        <tr>
            <td>
               
            </td>
            <td> बाढी</td>
            <td>पहिरो</td>
            <td>असिना</td>
            <td>हुरीबतास</td>
            <td>आगलागी</td>
            <td>भूकम्प</td>
            <td>खडेरी</td>
            <td>अतिबृष्टी</td>
            <td>महामारी </td>
            <td>सलह किरा आदी </td>
            <td>अन्य प्रकोप</td>
            <td></td>
        </tr>
        <tr>
            <td> प्रभावित संख्या</td>
            <td><asp:TextBox ID="txtFlood" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtLandslide" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtHailstorm" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtStrom" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtFire" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtEarthquake" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtDrought" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtArtibristi" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtInfluenza" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtInsecticide" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtOther" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:HiddenField ID ="hidAffect" runat="server" /></td>
        </tr>
         <tr>
            <td>नोक्सानी (अन्दाजी रु) </td>
            <td><asp:TextBox ID="txtFloodLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtLandslideLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtHailstormLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtStromLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtFireLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtEarthquakeLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtDroughtLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtArtibristiLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtInfluenzaLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtInsecticideLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
            <td><asp:TextBox ID="txtOtherLoss" runat="server" TextMode="Number" Text="0" Width="70"></asp:TextBox></td>
             <td><asp:HiddenField ID ="hidLoss" runat="server" /></td>
        </tr>
        

    </table>
    <table class="table table-striped">
        <tr>
            <td></td>
            <td>यति धेरै सूचना दिनु भएकोमा धन्यवाद ।</td>
            <td></td>
            <td>सही</td>
        </tr>
        <tr>
            <td></td>
            <td><b> सूचनादाताको नाम</b> </td>
            <td><asp:TextBox ID="txtProviderName" runat="server"></asp:TextBox></td>
            <td></td>
        </tr>
         <tr>
            <td></td>
            <td><b> सूचना संकलकको नाम </b> </td>
            <td><asp:TextBox ID="txtCollectorName" runat="server"></asp:TextBox></td>
            <td></td>
        </tr>
         <tr>
            <td></td>
            <td><b> प्रमाणित गर्नेको नाम  </b> </td>
            <td><asp:TextBox ID="txtValidaterName" runat="server"></asp:TextBox></td>
            <td></td>
        </tr>
          <tr>
            <td></td>
            <td><b> सूचना लिएको मितिः    </b> </td>
            <td><asp:TextBox ID="txtDate" runat="server" TextMode="Date"></asp:TextBox></td>
            <td></td><td><asp:HiddenField ID ="hidIO" runat="server" /></td>
        </tr>
          <tr>
                <td>
                    &nbsp;</td>
                    <td> </td>
                <td align="right">
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                   <asp:Button ID="btnNext" runat="server" Text="Save" CssClass="btn btn-primary" 
                        onclick="btnNext_Click" OnClientClick="return validatePage();"/>
                     <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" 
                        onclick="btnUpdate_Click" OnClientClick="return validatePage();"/>
                </td>
                
            </tr>
    </table>
</asp:Content>
