﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Section2FamilyInfo.aspx.cs" Inherits="ProfileForm.Pages.Section2FamilyInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:ScriptManager ID="ScriptManager" runat ="server" />
    <script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');

        }
    </script>
    
    <div>
        <h3>
            Section 2: Household Information</h3>
        <br />
        
        <table class="table table-bordered table-striped">
            <tr>
                <td>
                    ५. परिवार सख्या
                </td>
                <td colspan="10">
                   <%-- <asp:TextBox runat="server" TextMode="Number" ID ="txtMemberCount"></asp:TextBox>--%>
                    <asp:Button ID="btnAdd" runat="server" Text="Add Another" CssClass="btn btn-primary"
                        OnClick="btnAdd_Click" />                       
                        
         
                </td>
            </tr>
            </table>
         <asp:UpdatePanel ID="upnlFamily" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table class="table table-hover">
            <tr>
                <td>
                    <strong>सि. नं.</strong>
                </td>
                <%--<td>
                    <asp:Label runat="server" ID="lblSn"></asp:Label>
                </td>--%>
            
                <td>
                    <b>नामथर</b>
                </td>
                <%--<td>
                    <asp:TextBox runat="server" ID="txtMemberName"></asp:TextBox>
                </td>--%>
                
                <td >
                    <b>घरमुली सँगको नाता</b>
                </td>
                <%--<td>
                     <asp:DropDownList ID="ddlRelation" runat="server" DataSourceID="XmlDataSource_Relation" DataTextField="name" 
                        DataValueField="id">
                       </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Relation" runat="server" 
                        DataFile="~/XMLDataSource/Relation.xml"></asp:XmlDataSource>
                </td>--%>
                
                <td>
                    <b>लिङ्ग</b>
                </td>
                <%--<td>
                    <asp:RadioButtonList ID="rdoSex" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Male">पुरुष</asp:ListItem>
                        <asp:ListItem Value="Female">महिला</asp:ListItem>
                         <asp:ListItem Value="Third">तेस्रो</asp:ListItem>
                    </asp:RadioButtonList>
                </td>--%>
               
                <td>
                    <b>उमेर</b>
                </td>
               <%-- <td>
                    <asp:TextBox runat="server" ID="txtMemberAge" TextMode="Number"></asp:TextBox>
                </td>--%>
                </tr> 
            
                <asp:Panel runat="server" ID="pnlHousehold">
                <asp:Repeater runat="server" ID="RpterHousehold">
                    <ItemTemplate>
                        <tr id="<%#Eval("FAMILY_COUNT_ID") %>" class="div_row">
                            <td>
                               <asp:HiddenField runat="server" ID="hidMemberId"  Value='<%#Eval("FAMILY_COUNT_ID") %>'/>
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" Text='<%#Eval("NAME") %>'></asp:TextBox>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlRelation" runat="server" DataSourceID="XmlDataSource_Relation"
                                    DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("REL_TYPE_ID") %>'>
                                </asp:DropDownList>
                                <asp:XmlDataSource ID="XmlDataSource_Relation" runat="server" DataFile="~/XMLDataSource/Relation.xml">
                                </asp:XmlDataSource>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID") %>'>
                                    <asp:ListItem Value="0">पुरुष</asp:ListItem>
                                    <asp:ListItem Value="1">महिला</asp:ListItem>
                                    <asp:ListItem Value="2">अन्य</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAge" runat="server" Text='<%#Eval("AGE") %>' TextMode="Number" ></asp:TextBox>
                            </td>
                             <td>
                               <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </asp:Panel>
            
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <table class="table table-bordered table-striped">
            <tr>
                <td>
                    <b>QN</b>
                </td>
                <td>
                    <b>Questions</b>
                </td>
                <td>
                    <b></b>
                </td>
            </tr>
            <tr>
                <td>
                    ६.
                </td>
                <td>
                    तपाईँको परिवारको खानेपानीको मुख्य स्रोत के होला?
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rblWater" runat="server" DataSourceID="XmlDataSource_DrinkingWater"
                        DataTextField="name" DataValueField="id">
                    </asp:RadioButtonList>
                    <asp:XmlDataSource ID="XmlDataSource_DrinkingWater" runat="server" DataFile="~/XMLDataSource/DrinkingWater.xml">
                    </asp:XmlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ७.
                </td>
                <td>
                    खानेपानी ल्याउन (जाँदा आउँदा) लाग्ने समय
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rbldrinkingWaterFetchTime" runat="server" DataSourceID="XmlDataSource_DrinkingWaterTime"
                        DataTextField="name" DataValueField="id">
                    </asp:RadioButtonList>
                    <asp:XmlDataSource ID="XmlDataSource_DrinkingWaterTime" runat="server" DataFile="~/XMLDataSource/DrinkingWaterTime.xml">
                    </asp:XmlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ८.
                </td>
                <td>
                    तपाईँको परिवारमा शौचालयको अवश्था कस्तो छ?
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rblToilet" runat="server" DataSourceID="XmlDataSource_Toilet"
                        DataTextField="name" DataValueField="id">
                    </asp:RadioButtonList>
                    <asp:XmlDataSource ID="XmlDataSource_Toilet" runat="server" DataFile="~/XMLDataSource/Toilet.xml">
                    </asp:XmlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ९.
                </td>
                <td>
                    तपाईँको परिवारमा हात के ले धुनुहुन्छ?
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rblHandWash" runat="server" DataSourceID="XmlDataSource_Handwash"
                        DataTextField="name" DataValueField="id">
                    </asp:RadioButtonList>
                    <asp:XmlDataSource ID="XmlDataSource_Handwash" runat="server" DataFile="~/XMLDataSource/Handwash.xml">
                    </asp:XmlDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    १०.
                </td>
                <td>
                    तपाईँको परिवारमा कोही बिरामी परमा पहिलो पटक कहाँ जानुहुनछ?
                </td>
                <td>
                    <asp:XmlDataSource ID="XmlDataSource_Health" runat="server" DataFile="~/XMLDataSource/HealthCheckup.xml">
                    </asp:XmlDataSource>
                    <asp:RadioButtonList ID="rblHealthCheckUp" runat="server" DataSourceID="XmlDataSource_Health"
                        DataTextField="name" DataValueField="id">
                    </asp:RadioButtonList>
                    अन्य (खुलाउने)....<asp:TextBox ID="txtHealthMortality" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    ११.
                </td>
                <td>
                    नजिकको स्वास्थ्य संस्थामा पुग्न लाग्ने समय
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    उपस्वास्थ्य चौकी \ स्वास्थ्य चौकी \ प्राथमिक स्वास्थ्य केन्द्र
                </td>
                <td>
                    दुरी :
                    <asp:TextBox runat="server" ID="txtHealthPostdistance" Text="0"></asp:TextBox>
                    लाग्ने समय:
                    <asp:TextBox ID="txtHealthPostTime" runat="server" Text="0"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    अस्पताल:
                </td>
                <td>
                    दुरी :
                    <asp:TextBox ID="txtHospitalDistance" runat="server" Text="0"></asp:TextBox>
                    लाग्ने समय:
                    <asp:TextBox ID="txtHospitalTime" runat="server" Text="0"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                 <td>
                    &nbsp;
                </td>
                <td align="right">
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnNext_Click"
                        OnClientClick="return validatePage();" />
                         <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btnUpdate_Click"
                        OnClientClick="return validatePage();" />
                </td>
               
            </tr>
        </table>
    </div>
</asp:Content>
