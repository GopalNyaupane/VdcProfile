﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChildLabour.aspx.cs" Inherits="ProfileForm.Pages.ChildLabour" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');

        }
    </script>
   
     <asp:ScriptManager ID="ScriptManager" runat ="server" />
    <h4>Child Labour</h4>

    <asp:UpdatePanel ID="upnlLabour" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddAnother" EventName="Click" />
        </Triggers>
        <ContentTemplate>
    <table class="table table-bordered table-striped">
        <tr>
            <td>२७. </td>
            <td>तपाईको परिवारका १८ वर्षसम्मका बालबालिका अरुको घरमा काम गर्न बसेका छन् ?</td>
             <td> <asp:RadioButtonList ID="rblChildLabour" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" OnSelectedIndexChanged ="rblChildLabour_SelectedIndexChanged" >
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>
        </tr>

       <%-- xa vaney matra aru field dekhaney else skip to another form--%>

    </table>
    
    <table class="table table-responsive table-striped">
        <tr>
        <asp:Panel ID="pnlLabour" runat="server">
        <asp:Repeater runat="server" ID="rptrChildLabour">
            <ItemTemplate>
              <table class="div_row table table-bordered table-striped">
                 <tr>
                     
                    <td>उमेर</td>
                    <td><asp:TextBox ID="txtAge" runat="server" Text='<%# Eval("AGE") %>'></asp:TextBox></td>
                    <td>लिंग</td>
                    <td><asp:RadioButtonList ID="rblSex" runat="server" RepeatDirection="Horizontal"  SelectedValue='<%#Eval("SEX_ID") %>'>
                                                <asp:ListItem Value="0">पुरुष</asp:ListItem>
                                <asp:ListItem Value="1">महिला</asp:ListItem>
                                    <asp:ListItem Value="2">अन्य</asp:ListItem>
                                        </asp:RadioButtonList></td>
                </tr>
                <tr>
                    <td> कामको किसिम </td>
                    <td><asp:RadioButtonList ID="rblLabourType" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("LABOUR_TYPE_ID") %>'  >
                                               <asp:ListItem Value="0">व्यक्तिगत घर</asp:ListItem>
                                <asp:ListItem Value="1">होटल, पसल</asp:ListItem>
                               <%--  Dropdown qustion no.28 done here--%>
                                        </asp:RadioButtonList> </td>
                    <td> कति घण्टा काम गर्छन्</td>
                    <td></td> <td><asp:TextBox ID="txtWorkHour" runat="server" Text='<%# Eval("WORK_HOUR") %>'></asp:TextBox></td>
               </tr>
                <tr>
                    <td>यो काम गरेको कति वर्ष भयो</td> <td><asp:TextBox ID="txtWorkYear" runat="server" Text='<%# Eval("WORK_YEAR") %>'></asp:TextBox></td>
                    <td>वार्षिक कति रुपिया पाउछन्</td>
                    <td></td> <td><asp:TextBox ID="txtYearlyIncome" runat="server" Text='<%# Eval("YEARLY_INCOME") %>'></asp:TextBox></td>
                </tr>
                <tr>
                    <td>वर्षमा कति पटक तपाई आफ्नो वच्चालाई भेट्न सक्नुहुन्छ</td> <td><asp:TextBox ID="txtVisit" runat="server" Text='<%# Eval("ANNUAL_VISIT") %>'></asp:TextBox></td>
                    <td>विद्यालयमा पढ्ने गरेको</td>
                    <td></td> <td> <asp:RadioButtonList ID="rblIsSchool" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("IS_SCHOOL") %>'>
                                               <asp:ListItem Value="0">छ</asp:ListItem>
                                <asp:ListItem Value="1">छैन</asp:ListItem>
                      
                                        </asp:RadioButtonList> </td>
                    <td>
                         <asp:Button CssClass="btn  btn-danger" EnableViewState="true" ID="btnRemove" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" runat="server" Text="X" />
                         
                    <asp:HiddenField ID ="hidLabour" runat ="server" Value ='<%#Eval("CHILD_LABOUR_ID") %>' /></td>  
                </tr>
             </table> 
            </ItemTemplate>
       </asp:Repeater>
            </asp:Panel>
        </tr>
        <tr>
           <td></td><td></td><td><td></td></td><td align="right"><asp:Button runat="server" CssClass="btn btn-primary" Text="Add Another" ID="btnAddAnother" OnClick="btnAddAnother_Click"/></td>
        </tr>
    </table>
    </ContentTemplate>
        </asp:UpdatePanel>
    <table class="table table-striped">
        <tr>
            <td>२९.</td><asp:HiddenField ID="hidLabourExp" runat="server" Value='<%#Eval("LABOUR_EXPLOITATION_ID") %>' />
            <td>बालबालिकाहरु घरबाहिर काम गरेको भए गरेको ठाउँमा के कस्तो समस्या भोग्ने गरेका छन् ?</td>
            <td><asp:RadioButtonList ID="rblLabourProblem" runat="server" 
                        DataSourceID="XmlDataSource_ChildLabourProblem" DataTextField="name" 
                        DataValueField="id">
                    </asp:RadioButtonList>
                    <asp:XmlDataSource ID="XmlDataSource_ChildLabourProblem" runat="server" 
                        DataFile="~/XMLDataSource/ChildLabourProblem.xml"></asp:XmlDataSource></td>
        </tr>
         <tr>
                <td>
                    &nbsp;</td>
                    <td> </td>
                <td align="right">
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                   <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" 
                        OnClick="btnNext_Click"  OnClientClick="return validatePage();"/>
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" 
                         OnClick="btnUpdate_Click" OnClientClick="return validatePage();"/>
                </td>
                
            </tr>
    </table>

</asp:Content>
