﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;

namespace ProfileForm.Pages
{
    public partial class MemberInfo : System.Web.UI.Page
    {
        MemberInfoBo objMmbr = null;
        MemberInfoDao objMmbrDal = new MemberInfoDao();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];

                    int ownerId = Convert.ToInt32(Session["Owner_Id"].ToString());

                    BindRepeaterOccupation(ownerId);
                    BindMigration(ownerId);
                    BindRptrMigrationDetailInfo(ownerId);
                    BindRptrDisabilityInfo(ownerId);

                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdateMemberInfo.Enabled = true;
                    btnUpdateMemberInfo.Visible = true;
                }
                else
                {
                    btnAddAbroad.Enabled = false;
                    btnNext.Enabled = true;
                    btnUpdateMemberInfo.Enabled = false;
                }
                if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
                }
            }
            if(RdoAbroad.SelectedIndex==1)
            btnAddAbroad.Enabled = false;
        }       

        private void BindMigration(int ownerId)
        {
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = ownerId;

            DataTable dt = new DataTable();
            dt = objMmbrDal.FetchMigration(objMmbr);

            if (dt != null && dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["IS_OUT_MIGRATION"].ToString())>-1)
                RdoAbroad.SelectedValue = dt.Rows[0]["IS_OUT_MIGRATION"].ToString();
            }
        }

        private void BindRepeaterOccupation(int ownerId)
        {
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = ownerId;

            DataTable dt = new DataTable();
            dt = objMmbrDal.FetchOccupation(objMmbr);           

            rptrFamilyMemberOccupation.DataSource = null;
            rptrFamilyMemberOccupation.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {

                rptrFamilyMemberOccupation.DataSource = dt;
                rptrFamilyMemberOccupation.DataBind();
            }
          
        }

        private void BindRptrMigrationDetailInfo(int ownerId)
        {
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = ownerId;

            DataTable dt = new DataTable();
            dt = objMmbrDal.FetchMigrationDetail(objMmbr);

            rpterAbroad.DataSource = null;
            rpterAbroad.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rpterAbroad.DataSource = dt;
                rpterAbroad.DataBind();
                
            }

        }

        private void BindRptrDisabilityInfo(int ownerId)
        {
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = ownerId;

            DataTable dt = new DataTable();
            dt = objMmbrDal.FetchDisability(objMmbr);

            rptrHandicapped.DataSource = null;
            rptrHandicapped.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrHandicapped.DataSource = dt;
                rptrHandicapped.DataBind();
            }
        }

        

        protected void btnNext_Click(object sender, EventArgs e)
        {
           // Session["Owner_ID"] = "99";
            int val;

            InsertMemberOccupation();
            InsertAbroad();
            InsertHandicapped();
    
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            if (RdoAbroad.SelectedIndex > -1)
                objMmbr.IsOutMigrant = Convert.ToInt32(RdoAbroad.SelectedValue);
            else objMmbr.IsOutMigrant = -1;

            val = objMmbrDal.InsertMigration(objMmbr);          

           Response.Redirect(Constant.constantApppath+"/pages/IncomeExpenditure.aspx");
           
            }       
        private void InsertMemberOccupation()
        {
            foreach (RepeaterItem rptr in rptrFamilyMemberOccupation.Items)
            {
                objMmbr = new MemberInfoBo();

                DropDownList ddlOccupation = (DropDownList)rptr.FindControl("ddlOccupation");
                DropDownList ddlAgeGroup = (DropDownList)rptr.FindControl("ddlAgeGroup");
                TextBox txtMaleNo = (TextBox)rptr.FindControl("txtMaleNo");
                TextBox txtFemaleNo = (TextBox)rptr.FindControl("txtFemaleNo");
                if (txtMaleNo.Text != "")
                {
                    objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                    
                    objMmbr.OccupationType = Convert.ToInt32(ddlOccupation.SelectedValue);
                    objMmbr.AgeGroup = Convert.ToInt32(ddlAgeGroup.SelectedValue);
                    objMmbr.MaleNum = Convert.ToInt32(txtMaleNo.Text);
                    objMmbr.FemaleNum = Convert.ToInt32(txtFemaleNo.Text);
                }
               int val = objMmbrDal.InsertOccupation(objMmbr);


            }
        }
        
        private void InsertAbroad()
        {
            foreach (RepeaterItem rptr in rpterAbroad.Items)
            {
                objMmbr = new MemberInfoBo();

                TextBox txtAbName = (TextBox)rptr.FindControl("txtAbName");
                TextBox txtAbAge = (TextBox)rptr.FindControl("txtAbAge");
                RadioButtonList RdoSex = (RadioButtonList)rptr.FindControl("RdoSex");
                TextBox txtAbCountry = (TextBox)rptr.FindControl("txtAbCountry");
                TextBox txtAbReason = (TextBox)rptr.FindControl("txtAbReason");
                if (txtAbName.Text != "")
                {
                    objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                    objMmbr.Name = txtAbName.Text;
                    objMmbr.Age = Convert.ToInt32(txtAbAge.Text);
                    
                    objMmbr.SexId = Convert.ToInt32(RdoSex.SelectedValue);
                    objMmbr.MigratedPlace = txtAbCountry.Text;
                    objMmbr.OutReason = txtAbReason.Text;
                }
              int val = objMmbrDal.InsertMigrationDeatil(objMmbr);

            }
        }

        private void InsertHandicapped()
        {
            foreach (RepeaterItem rptr in rptrHandicapped.Items)
            {
                objMmbr = new MemberInfoBo();

                DropDownList ddlHandddlHandicapped = (DropDownList)rptr.FindControl("ddlHandicapped");
                TextBox txtBoyNo = (TextBox)rptr.FindControl("txtBoyNo");
                TextBox txtGirlNo = (TextBox)rptr.FindControl("txtGirlNo");
                if (txtBoyNo.Text != "")
                {
                    objMmbr.OwnerId = Convert.ToInt32(Session["Owner_ID"].ToString());
                    objMmbr.DisableType = Convert.ToInt32(ddlHandddlHandicapped.SelectedValue);
                    objMmbr.MaleChildCount = Convert.ToInt32(txtBoyNo.Text);
                    objMmbr.FemaleChildCount = Convert.ToInt32(txtGirlNo.Text);
                }
               int val = objMmbrDal.InsertDisableInfo(objMmbr);

            }
        }

        private int occu_count = -1;
        protected void btnAddMemberOccupation_OnClick(object sender, EventArgs e)
        {
            occu_count++;
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("OCCUPATION_ID");
            dtA.Columns.Add("OCCUPATION_TYPE_ID");
            dtA.Columns.Add("AGE_GROUP_ID");
            dtA.Columns.Add("MALE_NO");
            dtA.Columns.Add("FEMALE_NO");

            HiddenField occuId = new HiddenField();
           DropDownList ddlOccupation = new DropDownList();
           DropDownList ddlAgeGroup = new DropDownList();
           
            TextBox txtMaleNo = new TextBox();
            TextBox txtFemaleNo = new TextBox();

            foreach (RepeaterItem rptItem in rptrFamilyMemberOccupation.Items)
            {
                DataRow drA = dtA.NewRow();
                occuId = (HiddenField)rptItem.FindControl("hidOccupationId");
                ddlAgeGroup = (DropDownList)rptItem.FindControl("ddlAgeGroup");
                ddlOccupation = (DropDownList)rptItem.FindControl("ddlOccupation");
                txtMaleNo = (TextBox)rptItem.FindControl("txtMaleNo");
                txtFemaleNo = (TextBox)rptItem.FindControl("txtFemaleNo");
                drA["OCCUPATION_ID"] = occuId.Value;
                drA["OCCUPATION_TYPE_ID"] = ddlOccupation.SelectedValue;
                drA["AGE_GROUP_ID"] = ddlAgeGroup.SelectedValue;
                drA["MALE_NO"] = txtMaleNo.Text;
                drA["FEMALE_NO"] = txtFemaleNo.Text;
                if(txtMaleNo.Text!="" && txtFemaleNo.Text!="")
                dtA.Rows.Add(drA);
            }
           
            DataRow drB = dtA.NewRow();            
            drB["OCCUPATION_ID"] = occu_count;
            drB["OCCUPATION_TYPE_ID"] = 0;
            drB["AGE_GROUP_ID"] = 0;
            drB["MALE_NO"] = 0;
            drB["FEMALE_NO"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrFamilyMemberOccupation.DataSource = db;
            rptrFamilyMemberOccupation.DataBind();

            rptrFamilyMemberOccupation.DataSource = dtA;
            rptrFamilyMemberOccupation.DataBind();
        }

        private int out_count = -1;

        public void AddOutMigrationDetails()
        {
            out_count++;
            DataTable dtA = new DataTable();
            dtA.Clear();
//            dtA.Columns.Add("SN");
            dtA.Columns.Add("OUT_MIGRATION_DETAIL_ID");
            dtA.Columns.Add("NAME");
            dtA.Columns.Add("AGE");
            dtA.Columns.Add("SEX_ID");
            dtA.Columns.Add("MIGRATED_PLACE");
            dtA.Columns.Add("OUT_MIGRATION_REASON");

           // DropDownList ddlOccupation = new DropDownList();
            HiddenField omdId = new HiddenField();
           RadioButtonList RdoSex = new RadioButtonList();
            TextBox txtAbName = new TextBox();
            TextBox txtAbAge = new TextBox();
            TextBox txtAbCountry = new TextBox();
            TextBox txtAbReason = new TextBox();

            foreach(RepeaterItem rptItem in rpterAbroad.Items)
            {
                DataRow drA = dtA.NewRow();
                omdId = (HiddenField)rptItem.FindControl("hidMigrationId");
                RdoSex = (RadioButtonList)rptItem.FindControl("RdoSex");
                txtAbName = (TextBox)rptItem.FindControl("txtAbName");
                txtAbAge = (TextBox)rptItem.FindControl("txtAbAge");
                txtAbCountry = (TextBox)rptItem.FindControl("txtAbCountry");
                txtAbReason = (TextBox)rptItem.FindControl("txtAbReason");
                drA["OUT_MIGRATION_DETAIL_ID"] = omdId.Value;
                drA["SEX_ID"] = RdoSex.SelectedIndex;
                drA["NAME"] = txtAbName.Text;
                drA["AGE"] = txtAbAge.Text;
                drA["MIGRATED_PLACE"] = txtAbCountry.Text;
                drA["OUT_MIGRATION_REASON"] = txtAbReason.Text;
               if(RdoSex.SelectedIndex>=0)

                dtA.Rows.Add(drA);
            }
            DataRow drB = dtA.NewRow();
            drB["OUT_MIGRATION_DETAIL_ID"] = out_count;           
            drB["SEX_ID"] = 0;
            drB["NAME"] = null;
            drB["AGE"] = 0;
            drB["MIGRATED_PLACE"] = null;
            drB["OUT_MIGRATION_REASON"] = null;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rpterAbroad.DataSource = db;
            rpterAbroad.DataBind();

            rpterAbroad.DataSource = dtA;
            rpterAbroad.DataBind();
            btnAddAbroad.Enabled = true;
        }       

        protected void btnAddAbroad_OnClick(object sender, EventArgs e)
        {
            AddOutMigrationDetails();
        }

       
        protected void RdoAbroad_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            rpterAbroad.DataSource = null;
            rpterAbroad.DataBind();
            if (RdoAbroad.SelectedItem.Value == "0")
            {

                btnAddAbroad.Enabled = true;

            }
            // if(RdoAbroad.SelectedIndex<1)
            //AddOutMigrationDetails();
        }

        private int handy_count = -1;    
      protected void btnAddHandicapped_OnClick(object sender, EventArgs e)
      {
          handy_count++;
          DataTable dtA = new DataTable();
          dtA.Clear();
         // dtA.Columns.Add("SN");
          dtA.Columns.Add("DISABLED_ID");
          dtA.Columns.Add("DISABLED_TYPE_ID");
          dtA.Columns.Add("MALE_CHILD_COUNT");
          dtA.Columns.Add("FEMALE_CHILD_COUNT");

          DropDownList ddlHandicapped = new DropDownList();
          TextBox txtMaleNo = new TextBox();
          TextBox txtFemaleNo = new TextBox();
          HiddenField hiddenId = new HiddenField();

          foreach (RepeaterItem rptItem in rptrHandicapped.Items)
          {
              DataRow drA = dtA.NewRow();
              hiddenId = (HiddenField)rptItem.FindControl("hidDisableId");
              ddlHandicapped = (DropDownList)rptItem.FindControl("ddlHandicapped");
              txtMaleNo = (TextBox)rptItem.FindControl("txtBoyNo");
              txtFemaleNo = (TextBox)rptItem.FindControl("txtGirlNo");
              drA["DISABLED_TYPE_ID"] = ddlHandicapped.SelectedValue;
              drA["MALE_CHILD_COUNT"] = txtMaleNo.Text;
              drA["FEMALE_CHILD_COUNT"] = txtFemaleNo.Text;
              drA["DISABLED_ID"] = hiddenId.Value;

              if (txtMaleNo.Text != "" && txtFemaleNo.Text != "")
              dtA.Rows.Add(drA);
          }

          DataRow drB = dtA.NewRow();
          drB["DISABLED_TYPE_ID"] = 0;
          drB["DISABLED_ID"] = handy_count;
          drB["MALE_CHILD_COUNT"] = 0;
          drB["FEMALE_CHILD_COUNT"] = 0;

          dtA.Rows.Add(drB);

          // to empty the repeater
          DataTable db = new DataTable();
          rptrHandicapped.DataSource = db;
          rptrHandicapped.DataBind();

          rptrHandicapped.DataSource = dtA;
          rptrHandicapped.DataBind();
      }



        protected void btnUpdateMemberInfo_OnClick(object sender, EventArgs e)
        {
            int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objMmbr = new MemberInfoBo();
            UpdateMigration(objMmbr);
            objMmbrDal.DeleteOldRecord(OwnerId);
            InsertAbroad();
            InsertHandicapped();
            InsertMemberOccupation();
           Response.Redirect(Constant.constantApppath+"/pages/IncomeExpenditure.aspx?Owner_Id="+Session["Owner_Id"]);
        }

        //private void UpdateDisability(MemberInfoBo objMmbr)
        //{
        //    foreach (RepeaterItem rptr in rptrHandicapped.Items)
        //    {

        //        DropDownList ddlHandicapped = (DropDownList) rptr.FindControl("ddlHandicapped");
        //        TextBox txtBoyNo = (TextBox)rptr.FindControl("txtBoyNo");
        //        TextBox txtGirlNo = (TextBox)rptr.FindControl("txtGirlNo");
        //        HiddenField hidDisableId = (HiddenField)rptr.FindControl("hidDisableId");


        //        objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
        //        objMmbr.userId = Convert.ToInt32(hidDisableId.Value);

        //        objMmbr.DisableType = ddlHandicapped.SelectedIndex;
        //        objMmbr.MaleChildCount = Convert.ToInt32(txtBoyNo.Text);
        //        objMmbr.FemaleChildCount = Convert.ToInt32(txtGirlNo.Text);
               


        //        objMmbrDal.UpdateDisabilityData(objMmbr);

        //    }
        //}

        private void UpdateMigration(MemberInfoBo objMmbr)
        {
            objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objMmbr.IsOutMigrant = RdoAbroad.SelectedIndex;
            objMmbrDal.UpdateIsOutMigrationData(objMmbr);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
           Response.Redirect(Constant.constantApppath+"/pages/TrainingOccupation.aspx?Owner_Id="+Session["Owner_Id"]);
        }

        //private void UpdateMigrationDetail(MemberInfoBo objMmbr)
        //{
        //    foreach (RepeaterItem rptr in rpterAbroad.Items)
        //    {

        //        TextBox txtAbName = (TextBox) rptr.FindControl("txtAbName");
        //        TextBox txtAbAge = (TextBox)rptr.FindControl("txtAbAge");
        //        RadioButtonList RdoSex = (RadioButtonList)rptr.FindControl("RdoSex");
        //        TextBox txtAbCountry = (TextBox) rptr.FindControl("txtAbCountry");
        //        TextBox txtAbReason = (TextBox)rptr.FindControl("txtAbReason");
        //        HiddenField hidMigrationId = (HiddenField) rptr.FindControl("hidMigrationId");
        

        //        objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
        //        objMmbr.userId = Convert.ToInt32(hidMigrationId.Value);
        //        objMmbr.Name = txtAbName.Text;
        //        objMmbr.Age = Convert.ToInt32(txtAbAge.Text);
        //        objMmbr.SexId = RdoSex.SelectedIndex;
        //        objMmbr.MigratedPlace = txtAbCountry.Text;
        //        objMmbr.OutReason = txtAbReason.Text;

                
        //        objMmbrDal.UpdateMigrationDetailData(objMmbr);

        //    }
        //}

        //private void UpdateOccupation(MemberInfoBo objMmbr)
        //{

        //    foreach (RepeaterItem rptr in rptrFamilyMemberOccupation.Items)
        //    {

        //        DropDownList ddlOccupation = (DropDownList) rptr.FindControl("ddlOccupation");
        //        DropDownList ddlAgeGroup = (DropDownList)rptr.FindControl("ddlAgeGroup");
        //        TextBox txtMaleNo = (TextBox) rptr.FindControl("txtMaleNo");
        //        TextBox txtFemaleNo = (TextBox) rptr.FindControl("txtFemaleNo");
        //        HiddenField hidOccupationId = (HiddenField) rptr.FindControl("hidOccupationId");

        //        objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
        //        objMmbr.userId = Convert.ToInt32(hidOccupationId.Value);
        //        objMmbr.OccupationType = ddlOccupation.SelectedIndex;
        //        objMmbr.MaleNum = Convert.ToInt32(txtMaleNo.Text);
        //        objMmbr.FemaleNum = Convert.ToInt32(txtFemaleNo.Text);
        //        objMmbr.AgeGroup = ddlAgeGroup.SelectedIndex;

        //        objMmbrDal.UpdateOccupationData(objMmbr);

        //    }


        //}

       
    }
    
}
