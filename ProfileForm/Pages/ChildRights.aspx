﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChildRights.aspx.cs" Inherits="ProfileForm.Pages.ChildRights" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');

        }
    </script>
    <h4>Child Labour</h4>
    <table class="table table-striped">

        <tr>
            <td>३०. </td>
            <asp:HiddenField ID="hidChildAbuse" runat="server" Value='<%#Eval("CHILD_ABUSE_ID") %>' />
            <td>तपाईंको परिवारमा बालबालिकाले कमजोरी गरेमा के गर्नु हुन्छ ?</td>
            <td>
                <asp:RadioButtonList ID="rblHomeChild" runat="server"
                    DataSourceID="XmlDataSource_ChildPunishment" DataTextField="name"
                    DataValueField="id">
                </asp:RadioButtonList>
                <asp:XmlDataSource ID="XmlDataSource_ChildPunishment" runat="server"
                    DataFile="~/XMLDataSource/ChildPunishment.xml"></asp:XmlDataSource>
            </td>
        </tr>
        <tr>
            <td>३१.</td>
            <asp:HiddenField ID="hidSex" runat="server" />
            <td>तपाईंको घरमा छोरा र छोरीमा कुनै फरक व्यवहार गर्ने गर्नुभएको छ ?</td>
            <td>
                <asp:RadioButtonList ID="rblSexDescrimination" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>' OnSelectedIndexChanged="rblSexDescrimination_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>

                </asp:RadioButtonList>
                <br />
                <asp:RadioButtonList ID="rblSexDescriminatonType" runat="server"
                    DataSourceID="XmlDataSource_ChildDifferentiate" DataTextField="name"
                    DataValueField="id">
                </asp:RadioButtonList>
                <asp:XmlDataSource ID="XmlDataSource_ChildDifferentiate" runat="server"
                    DataFile="~/XMLDataSource/ChildDifferentiate.xml"></asp:XmlDataSource>
            </td>
        </tr>
        <tr>
            <td>३२.</td>
            <asp:HiddenField ID="hidSPunishment" runat="server" Value='<%#Eval(" ") %>' />
            <td>तपाईंको परिवारका बालबालिकाहरुले विद्यालयमा गल्ती गरेमा बिद्यालयमा निम्न यातना पाउने गरेका छन्?</td>
            <td>
                <asp:RadioButtonList ID="rblSchoolPunishment" runat="server"
                    DataSourceID="XmlDataSource_SchoolPunishment" DataTextField="name"
                    DataValueField="id">
                </asp:RadioButtonList>
                <asp:XmlDataSource ID="XmlDataSource_SchoolPunishment" runat="server"
                    DataFile="~/XMLDataSource/SchoolPunishment.xml"></asp:XmlDataSource>
            </td>
        </tr>
        <tr>
            <td>३३.</td>
            <asp:HiddenField ID="hidViolence" runat="server" Value='<%#Eval(" ") %>' />
            <td>तपाईंको परिवारको बालबालिकाहरुमा यौनशोषणको कुनै घटना घटेको छ ?</td>
            <td>
                <asp:RadioButtonList ID="rblChildViolence" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>३४.</td>
            <asp:HiddenField ID="hidChildExp" runat="server" />
            <td>एक वर्षभित्र तपाईंको परिवारमा महिला तथा बालबालिका उपर हिंसा तथा सामाजिक कुरिती सम्बन्धी निम्न कुनै घटना घटेका छन/छैनन् ?</td>
            <td>
                <asp:RadioButtonList ID="rblSexExp" runat="server"
                    DataSourceID="XmlDataSource_SocialVoilence" DataTextField="name"
                    DataValueField="id">
                </asp:RadioButtonList>
                <asp:XmlDataSource ID="XmlDataSource_SocialVoilence" runat="server"
                    DataFile="~/XMLDataSource/SocialVoilence.xml"></asp:XmlDataSource>
            </td>
        </tr>
        <tr>
            <td>३५.</td>
            <td>तपाईंको परिवारका कुनै बालबालिका बालगृह वा कुनै संस्थामा बसेका छन् ? </td>
            <td>
                <asp:RadioButtonList ID="rblOrg" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>

                </asp:RadioButtonList>
            </td>
            <%-- if yes then other info--%>
        </tr>

        <tr>
            <td>३६.</td>
            <td>तपाईंको परिवारका कुनै बालबालिका सडक बालबालिकाको रुपमा रहेका छन् ?</td>
            <td>
                <asp:RadioButtonList ID="rblStreet" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>

                </asp:RadioButtonList>
            </td>
            <%-- if yes then other info--%>
        </tr>
        <tr>
            <td>३७.</td>
            <td>तपाईंलाई बाल अधिकारको बारेमा जानकारी छ ?</td>
            <td>
                <asp:RadioButtonList ID="rblRights" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>
                    <asp:ListItem Value="2"> केही कुरा सुनेको छु</asp:ListItem>

                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>३८.</td>
            <asp:HiddenField ID="hidDrug" runat="server" Value="" />
            <asp:HiddenField ID="hidDrug1" runat="server" Value="" />
            <asp:HiddenField ID="hidDrug2" runat="server" Value="" />
            <td>तपाईंको परिवारका कुनै बालबालिकामा लागू पदार्थ दुव्र्यसनको लत रहे नरहेको स्थिति के छ ?</td>
        </tr>

    </table>
    <table class="table table-bordered">
        <tr>
            <td>लिङ्ग</td>
            <td>धुम्रपान गर्ने संख्या</td>
            <td>नसालु पदार्थ सेवन गर्ने संख्या</td>
            <td>कुनै पनि लत नरहेको संख्या</td>

        </tr>
        <tr>
            <td>बालक</td>
            <td>
                <asp:TextBox ID="txtBoysSmoke" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtBoysAddicted" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtBoysNon" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
        </tr>
        <tr>
            <td>बालिका</td>
            <td>
                <asp:TextBox ID="txtGirlsSmoke" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtGirlsAddicted" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtGirlsNon" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
        </tr>
        <tr>
            <td>तेस्रो लिंगी </td>
            <td>
                <asp:TextBox ID="txtOtherSmoke" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtOtherAddicted" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtOtherNon" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
        </tr>
    </table>
    <table class="table table-striped">
        <tr>
            <td>३९.</td>
            <asp:HiddenField ID="hidShelter" runat="server" Value='<%#Eval(" ") %>' />
            <td>तपाईंले बालबालिकालाई वर्षमा कति जोर लुगा दिनुहुन्छ ? (सरदर)</td>
            <td>
                <asp:RadioButtonList ID="rblClothing" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">१ जोर</asp:ListItem>
                    <asp:ListItem Value="1">२ जोर</asp:ListItem>
                    <asp:ListItem Value="2"> ३ जोर</asp:ListItem>
                    <asp:ListItem Value="2"> ४ जोर वा सोभन्दा बढी</asp:ListItem>

                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>४०.</td>
            <td>बालबालिका बस्ने, पढ्ने कोठा अलग्गै छ ?</td>
            <td>
                <asp:RadioButtonList ID="rblDiffRoom" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>

                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>४१.</td>
            <td>मानिस बस्ने घर (मूल घर) र गाईवस्तुहरु बाँध्ने गोठ अलग्गै छ ?</td>
            <td>
                <asp:RadioButtonList ID="rblHomeShed" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>
                    <asp:ListItem Value="2">गोठ अलग्गै भए पनि गोठमाथि पनि सुत्ने गरेको छ</asp:ListItem>

                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td align="right">
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary"
                    OnClick="btnNext_Click" OnClientClick="return validatePage();" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary"
                    OnClick=" btnUpdate_Click" OnClientClick="return validatePage();" />
            </td>

        </tr>

    </table>
</asp:Content>
