﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer.Pages;

namespace ProfileForm.Pages
{
    public partial class Section1Introduction : System.Web.UI.Page
    {

        OwnerDao objOwnerDao = new OwnerDao();
        OwnerBo objBo = new OwnerBo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtWard.Text = Session["WardNo"].ToString();
                txtGharNo.Text = Session["GharNo"].ToString();
                txtVdcName.Text = Session["VDC"].ToString();
                if (Request.QueryString["Info_Id"] != null)
                {
                    Session["Info_Id"] = Request.QueryString["Info_Id"];
                    int Info_Id = Convert.ToInt32(Session["Info_Id"].ToString());
                    BindData(Info_Id);
                    btnUpdate.Visible = true;
                    btnNext.Visible = false;
                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                }
                //if (Request.QueryString["infoId"] != null)
                //{
                //    int info_id = Convert.ToInt32(Request.QueryString["infoId"]);
                //    //BindData(info_id);
                //    //btnUpdate.Visible = true;
                //    //btnNext.Visible = false;
                //}
            }
        }
        string a;
        private void BindData(int Info_Id)
        {
            DataTable dt = new DataTable();
            dt = objOwnerDao.FetchSection1(Info_Id);
            if (dt != null && dt.Rows.Count > 0)
            {

                txtGharMuli.Text = dt.Rows[0]["OWNER_NAME"].ToString();
                if (Convert.ToInt32(dt.Rows[0]["SEX_ID"].ToString()) == -1)
                    rdoSex.SelectedValue = null;
                else
                    a = dt.Rows[0]["SEX_ID"].ToString();
                rdoSex.SelectedValue = a;

                ddlCaste.SelectedValue = dt.Rows[0]["CASTE_ID"].ToString();
                ddlLanguage.SelectedValue = dt.Rows[0]["LANGUAGE_ID"].ToString();
                ddlReligion.SelectedValue = dt.Rows[0]["RELIGION_ID"].ToString();
                Session["Owner_Id"] = dt.Rows[0]["OWNER_ID"].ToString();
            }
        }


        protected void btnNext_Click(object sender, EventArgs e)
        {
            objBo.VillageId = int.Parse(Session["InfoId"].ToString());
            objBo.OwnerName = txtGharMuli.Text;
            objBo.SexId = rdoSex.SelectedIndex;
            objBo.CasteId = ddlCaste.SelectedIndex;
            objBo.ReligionId = Convert.ToInt32(ddlReligion.SelectedValue);
            objBo.LanguageId = Convert.ToInt32(ddlLanguage.SelectedValue);
            int i = objOwnerDao.InsertSection1(objBo);
            if (i > 0) //i मा OwnerId आउँछ 
            {
                Session["Owner_Id"] = i;
               Response.Redirect(Constant.constantApppath+"/pages/Section2FamilyInfo.aspx?ownerId=" + i);
            }
            else
            {
                Response.Write("<Script>alert('Error')</script>");
            }

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["Info_Id"] != null)
            {
               Response.Redirect(Constant.constantApppath+"/pages/Info.aspx?Info_Id=" + Session["Info_Id"]);
            }
            if (Request.QueryString["infoId"] != null)
            {
                string infoId = Request.QueryString["infoId"];
               Response.Redirect(Constant.constantApppath+"/pages/Info.aspx?infoId=" + infoId);

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            objBo.VillageId = int.Parse(Session["InfoId"].ToString());
            objBo.OwnerName = txtGharMuli.Text;
            objBo.SexId = rdoSex.SelectedIndex;
            objBo.CasteId = ddlCaste.SelectedIndex;
            objBo.ReligionId = Convert.ToInt32(ddlReligion.SelectedValue);
            objBo.LanguageId = Convert.ToInt32(ddlLanguage.SelectedValue);
            int i = objOwnerDao.UpdateSection(objBo);
            if (i > 0) //i मा OwnerId आउँछ 
            {
                //Session["Owner_Id"] = i;
               Response.Redirect(Constant.constantApppath+"/pages/Section2FamilyInfo.aspx?Owner_Id=" + Session["Owner_Id"]);
            }
            else
            {
                Response.Write("<Script>alert('Error')</script>");
            }
        }
    }
}