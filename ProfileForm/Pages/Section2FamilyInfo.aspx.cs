﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer.Pages;

namespace ProfileForm.Pages
{
    public partial class Section2FamilyInfo : System.Web.UI.Page
    {

        FamilyInfoBo objMemebrBo = new FamilyInfoBo();
        FamilyInfoDao objMemberDo = new FamilyInfoDao();
        WashBo objWash = new WashBo();

        private int Wash_Id;

        protected void Page_Load(object sender, EventArgs e)
        {
            //DataSet ds = obj.FetchSection2(int.Parse(Session["InfoId"].ToString()));
            // Session["Owner_Id"] = 90;
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {

                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"].ToString());

                    BindData(Owner_Id);
                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;
                }
                else
                {
                    btnUpdate.Visible = false;
                    /* btnNext.Enabled = true;
                     btnNext.Visible = true;*/

                }

                if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
                }
            }
        }

        private void BindData(int ownerId)
        {
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            objMemebrBo.OwnerId = ownerId;
            dt = objMemberDo.FetchFamilyMember(objMemebrBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                RpterHousehold.DataSource = dt;
                RpterHousehold.DataBind();
            }
            objWash.OwnerId = ownerId;
            dt1 = objMemberDo.FetchWash(objWash);
            if (dt1 != null && dt1.Rows.Count > 0)
            {
                Wash_Id = Convert.ToInt32(dt1.Rows[0]["WASH_ID"].ToString());
                if (Convert.ToInt32(dt1.Rows[0]["WATER_SOURCE_ID"].ToString()) > -1)
                    rblWater.SelectedValue = dt1.Rows[0]["WATER_SOURCE_ID"].ToString();
                if (Convert.ToInt32(dt1.Rows[0]["WATER_FETCH_TIME_ID"].ToString()) > -1)
                    rbldrinkingWaterFetchTime.SelectedValue = dt1.Rows[0]["WATER_FETCH_TIME_ID"].ToString();
                if (Convert.ToInt32(dt1.Rows[0]["TOILET_STATUS_ID"].ToString()) > -1)
                    rblToilet.SelectedValue = dt1.Rows[0]["TOILET_STATUS_ID"].ToString();
                if (Convert.ToInt32(dt1.Rows[0]["HAND_WASH_ID"].ToString()) > -1)
                    rblHandWash.SelectedValue = dt1.Rows[0]["HAND_WASH_ID"].ToString();
                if (Convert.ToInt32(dt1.Rows[0]["HEALTH_CHECKUP_ID"].ToString()) > -1)
                    rblHealthCheckUp.SelectedValue = dt1.Rows[0]["HEALTH_CHECKUP_ID"].ToString();

                txtHealthPostdistance.Text = dt1.Rows[0]["HEALTHPOST_DISTANCE"].ToString();
                txtHealthPostTime.Text = dt1.Rows[0]["HEALTHPOST_TIME"].ToString();
                txtHospitalDistance.Text = dt1.Rows[0]["HOSPITAL_DISTANCE"].ToString();
                txtHospitalTime.Text = dt1.Rows[0]["HOSPITAL_TIME"].ToString();
            }
        }

        private int i = -1;
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            i++;
            DataTable dtA = new DataTable();
            dtA.Clear();

            dtA.Columns.Add("NAME");
            dtA.Columns.Add("REL_TYPE_ID");
            dtA.Columns.Add("SEX_ID");
            dtA.Columns.Add("AGE");
            dtA.Columns.Add("FAMILY_COUNT_ID");

            TextBox txtName = new TextBox();
            TextBox txtAge = new TextBox();
            DropDownList ddlRelation = new DropDownList();
            RadioButtonList rdoSex = new RadioButtonList();
            HiddenField hidMemberId = new HiddenField();

            foreach (RepeaterItem rptItem in RpterHousehold.Items)
            {
                DataRow drA = dtA.NewRow();
                ddlRelation = (DropDownList)rptItem.FindControl("ddlRelation");
                hidMemberId = (HiddenField)rptItem.FindControl("hidMemberId");
                txtName = (TextBox)rptItem.FindControl("txtName");
                txtAge = (TextBox)rptItem.FindControl("txtAge");
                rdoSex = (RadioButtonList)rptItem.FindControl("rdoSex");

                drA["NAME"] = txtName.Text;
                drA["REL_TYPE_ID"] = ddlRelation.SelectedValue;
                drA["SEX_ID"] = rdoSex.SelectedIndex;
                drA["AGE"] = txtAge.Text;
                drA["FAMILY_COUNT_ID"] = hidMemberId.Value;
                if (rdoSex.SelectedIndex >= 0)
                    dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["NAME"] = "";
            drB["REL_TYPE_ID"] = 0;
            drB["SEX_ID"] = 0;
            drB["AGE"] = 0;
            drB["FAMILY_COUNT_ID"] = i;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            RpterHousehold.DataSource = db;
            RpterHousehold.DataBind();

            RpterHousehold.DataSource = dtA;
            RpterHousehold.DataBind();


        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            InsertIntoFamilyCountTable();
            InsertIntoWashTable();
           Response.Redirect(Constant.constantApppath+"/pages/ChildInfo.aspx");

        }

        private void InsertIntoWashTable()
        {
            objWash.OwnerId = Convert.ToInt32(Session["Owner_ID"]);
            if (rblHandWash.SelectedIndex > -1)
                objWash.HandWashId = Convert.ToInt32(rblHandWash.SelectedValue);
            else objWash.HandWashId = -1;
            if (rblHealthCheckUp.SelectedIndex > -1)
                objWash.HealthCheckUpId = Convert.ToInt32(rblHealthCheckUp.SelectedValue);
            else objWash.HealthCheckUpId = -1;
            if (rblWater.SelectedIndex > -1)
                objWash.WaterSourceId = Convert.ToInt32(rblWater.SelectedValue);
            else objWash.WaterSourceId = -1;
            if (rbldrinkingWaterFetchTime.SelectedIndex > -1)
                objWash.WaterFetchTimeId = Convert.ToInt32(rbldrinkingWaterFetchTime.SelectedValue);
            else objWash.WaterFetchTimeId = -1;
            if (rblToilet.SelectedIndex > -1)
                objWash.ToiletStatusId = Convert.ToInt32(rblToilet.SelectedValue);
            else objWash.ToiletStatusId = -1;

            objWash.HealthPostDistance = txtHealthPostdistance.Text;
            objWash.HealthPostTime = txtHealthPostTime.Text;
            objWash.HospitalDistance = txtHospitalDistance.Text;
            objWash.HospitalTime = txtHospitalTime.Text;
            int i = objMemberDo.InsertWash(objWash);
        }

        private void InsertIntoFamilyCountTable()
        {
            List<FamilyInfoBo> objListMemberBo = new List<FamilyInfoBo>();
            foreach (RepeaterItem rptItem3 in RpterHousehold.Items)
            {
                objMemebrBo = new FamilyInfoBo();
                TextBox txtName = (TextBox)rptItem3.FindControl("txtName");
                DropDownList ddlRelationId = (DropDownList)rptItem3.FindControl("ddlRelation");
                TextBox txtAge = (TextBox)rptItem3.FindControl("txtAge");
                RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");

                if (txtName.Text != "")
                {
                    objMemebrBo.Name = txtName.Text;
                    objMemebrBo.Age = Convert.ToInt32(txtAge.Text);
                    objMemebrBo.RelationTypeId = Convert.ToInt32(ddlRelationId.SelectedValue);
                    objMemebrBo.SexId = Convert.ToInt32(RdoSex.SelectedValue);
                    objMemebrBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListMemberBo.Add(objMemebrBo);

                }
            }
            objMemberDo.InsertFamilyMember(objListMemberBo);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objMemberDo.DeleteOldRecord(OwnerId);
            InsertIntoFamilyCountTable();
            InsertIntoWashTable();
           Response.Redirect(Constant.constantApppath+"/pages/ChildInfo.aspx?Owner_Id=" + Session["Owner_Id"]);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["Owner_Id"] != null)
            {
               Response.Redirect(Constant.constantApppath+"/pages/Section1Introduction.aspx?Owner_Id=" + Session["Owner_Id"]);
            }
            if (Request.QueryString["ownerId"] != null)
            {
                string ownerId = Request.QueryString["ownerId"];
               Response.Redirect(Constant.constantApppath+"/pages/Section1Introduction.aspx?ownerId=" + Session["Owner_Id"]);
            }
        }


    }
}