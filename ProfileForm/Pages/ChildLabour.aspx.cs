﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class ChildLabour : System.Web.UI.Page
    {
        ChildLabourBo objBo = new ChildLabourBo();
        ChildLabourDao objDao = new ChildLabourDao();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //PopulateBudgetHead();              

                rptrChildLabour.DataBind();

                if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadLabourDetails(Owner_Id);
                    LoadLabourExpDetails(Owner_Id);

                    btnUpdate.Visible = true;
                    btnNext.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                    btnAddAnother.Enabled = false;
                }

                if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
                }
                if (rblChildLabour.SelectedIndex == 1)
                {
                    btnAddAnother.Enabled = false;
                }
                else btnAddAnother.Enabled = true;
            }
            

        }
        private int count = -1;

        protected void btnAddAnother_Click(object sender, EventArgs e)
        {
            count++;
            DataTable dtChildLabour = new DataTable();
            dtChildLabour.Clear();
            dtChildLabour.Columns.Add("CHILD_LABOUR_ID");
            dtChildLabour.Columns.Add("AGE");
            dtChildLabour.Columns.Add("SEX_ID");
            dtChildLabour.Columns.Add("LABOUR_TYPE_ID");
            dtChildLabour.Columns.Add("WORK_HOUR");
            dtChildLabour.Columns.Add("WORK_YEAR");
            dtChildLabour.Columns.Add("YEARLY_INCOME");
            dtChildLabour.Columns.Add("ANNUAL_VISIT");
            dtChildLabour.Columns.Add("IS_SCHOOL");

            HiddenField hidLabour = new HiddenField();
            TextBox txtAge = new TextBox();
            RadioButtonList rblSex = new RadioButtonList();
            RadioButtonList rblLabourType = new RadioButtonList();
            TextBox txtWorkHour = new TextBox();
            TextBox txtWorkYear = new TextBox();
            TextBox txtYearlyIncome = new TextBox();
            TextBox txtVisit = new TextBox();
            RadioButtonList rblIsSchool = new RadioButtonList();

            foreach (RepeaterItem rptItem in rptrChildLabour.Items)
            {
                DataRow drA = dtChildLabour.NewRow();
                hidLabour = (HiddenField)rptItem.FindControl("hidLabour");
                txtAge = (TextBox)rptItem.FindControl("txtAge");
                rblSex = (RadioButtonList)rptItem.FindControl("rblSex");
                rblLabourType = (RadioButtonList)rptItem.FindControl("rblLabourType");
                txtWorkHour = (TextBox)rptItem.FindControl("txtWorkHour");
                txtWorkYear = (TextBox)rptItem.FindControl("txtWorkYear");
                txtYearlyIncome = (TextBox)rptItem.FindControl("txtYearlyIncome");
                txtVisit = (TextBox)rptItem.FindControl("txtVisit");
                rblIsSchool = (RadioButtonList)rptItem.FindControl("rblIsSchool");

                drA["CHILD_LABOUR_ID"] = hidLabour.Value;
                drA["AGE"] = txtAge.Text;
                drA["SEX_ID"] = rblSex.SelectedIndex;
                drA["LABOUR_TYPE_ID"] = rblLabourType.SelectedIndex;
                drA["WORK_HOUR"] = txtWorkHour.Text;
                drA["WORK_YEAR"] = txtWorkYear.Text;
                drA["YEARLY_INCOME"] = txtYearlyIncome.Text;
                drA["ANNUAL_VISIT"] = txtVisit.Text;
                drA["IS_SCHOOL"] = rblIsSchool.SelectedIndex;

                if (rblSex.SelectedIndex >= 0 && rblLabourType.SelectedIndex >= 0)
                    dtChildLabour.Rows.Add(drA);
            }
            DataRow drB = dtChildLabour.NewRow();

            drB["CHILD_LABOUR_ID"] = count;
            drB["AGE"] = 0;
            drB["SEX_ID"] = 0;
            drB["LABOUR_TYPE_ID"] = 0;
            drB["WORK_HOUR"] = 0;
            drB["WORK_YEAR"] = 0;
            drB["YEARLY_INCOME"] = 0;
            drB["ANNUAL_VISIT"] = 0;
            drB["IS_SCHOOL"] = 0;
            dtChildLabour.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrChildLabour.DataSource = db;
            rptrChildLabour.DataBind();

            rptrChildLabour.DataSource = dtChildLabour;
            rptrChildLabour.DataBind();

            btnAddAnother.Enabled = true;


        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            //For Repeater inserting
            //Session["Owner_Id"] = 11;

            InsertChildLabour();

            //For Table Child_labour_exploitation 
            if (rblLabourProblem.SelectedIndex > -1)
            {
                objBo.labourExploitationType = Convert.ToInt32(rblLabourProblem.SelectedValue);
            }
            else objBo.labourExploitationType = -1;
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            int exp = objDao.Child_Labour_Exploitation(objBo);

           Response.Redirect(Constant.constantApppath+"/pages/ChildRights.aspx");

        }
        private void InsertChildLabour()
        {
            if (rblChildLabour.SelectedIndex > -1)
                objBo.childLabour = Convert.ToInt32(rblChildLabour.SelectedValue);
            List<ChildLabourBo> objList = new List<ChildLabourBo>();
            foreach (RepeaterItem rptItem in rptrChildLabour.Items)
            {
                objBo = new ChildLabourBo();
                HiddenField hidLabour = (HiddenField)rptItem.FindControl("hidLabour");
                TextBox txtAge = (TextBox)rptItem.FindControl("txtAge");
                RadioButtonList rblSex = (RadioButtonList)rptItem.FindControl("rblSex");
                RadioButtonList rblLabourType = (RadioButtonList)rptItem.FindControl("rblLabourType");
                TextBox txtWorkHour = (TextBox)rptItem.FindControl("txtWorkHour");
                TextBox txtWorkYear = (TextBox)rptItem.FindControl("txtWorkYear");
                TextBox txtYearlyIncome = (TextBox)rptItem.FindControl("txtYearlyIncome");
                TextBox txtVisit = (TextBox)rptItem.FindControl("txtVisit");
                RadioButtonList rblIsSchool = (RadioButtonList)rptItem.FindControl("rblIsSchool");

                if (txtWorkHour.Text != "")
                {
                    objBo.hiddenId1 = Convert.ToInt32(hidLabour.Value);

                    objBo.age = Convert.ToInt32(txtAge.Text);

                    objBo.sexId = rblSex.SelectedIndex;
                    if (rblLabourType.SelectedIndex > -1)
                        objBo.labourType = Convert.ToInt32(rblLabourType.SelectedValue);
                    objBo.workHour = Convert.ToInt32(txtWorkHour.Text);
                    objBo.workYear = Convert.ToInt32(txtWorkYear.Text);
                    objBo.yearlyIncome = Convert.ToInt32(txtYearlyIncome.Text);
                    objBo.annualVisit = Convert.ToInt32(txtVisit.Text);
                    if (rblIsSchool.SelectedIndex > -1)
                        objBo.isSchool = Convert.ToInt32(rblIsSchool.SelectedValue);
                    else objBo.isSchool = -1;
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    objList.Add(objBo);
                }
            }

            objDao.Child_Labour_Insert(objList);
        }

        protected void rblChildLabour_SelectedIndexChanged(object sender, EventArgs e)
        {
            rptrChildLabour.DataSource = null;
            rptrChildLabour.DataBind();
            if (rblChildLabour.SelectedIndex == 0)
            {
                btnAddAnother.Enabled = true;

            }
            else btnAddAnother.Enabled = false;
        }
        public void LoadLabourDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_ChildLabour(objBo);

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrChildLabour.DataSource = dt;
                rptrChildLabour.DataBind();

                rblChildLabour.SelectedValue = "0";
                btnAddAnother.Enabled = true;

            }
        }
        public void LoadLabourExpDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_ChildLabour_Exp(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidLabourExp.Value = dt.Rows[0]["LABOUR_EXPLOITATION_ID"].ToString();
                if (Convert.ToInt32(dt.Rows[0]["LABOUR_EXPLOITATION_TYPE_ID"].ToString()) > -1)
                    rblLabourProblem.SelectedValue = dt.Rows[0]["LABOUR_EXPLOITATION_TYPE_ID"].ToString();

            }




        }
        public void btnUpdate_Click(object sender, EventArgs e)
        {
            int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objDao.DeleteOldRecord(OwnerId);
            InsertChildLabour();
            objBo.ownerId = OwnerId;
            if (hidLabourExp.Value != "")
            {
                objBo.hiddenId2 = Convert.ToInt32(hidLabourExp.Value);
                if (rblLabourProblem.SelectedIndex > -1)
                    objBo.labourExploitationType = Convert.ToInt32(rblLabourProblem.SelectedValue);
                int i = objDao.Update_Child_Labour_Exp(objBo);
            }
           Response.Redirect(Constant.constantApppath+"/pages/ChildRights.aspx?Owner_Id=" + Session["Owner_Id"]);


        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
           Response.Redirect(Constant.constantApppath+"/pages/Marriage.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
    }
}