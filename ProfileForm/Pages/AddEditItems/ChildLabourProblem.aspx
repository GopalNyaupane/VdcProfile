﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChildLabourProblem.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.ChildLabourProblem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Child Labour Problem" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddChildProblem" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddChildProblemClicked" runat="server" Text="Add Child Problem" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Animal List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" DataKeyNames="PROBLEM_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="PROBLEM_ID" ReadOnly="true" HeaderText="PROBLEM_ID" SortExpression="PROBLEM_ID"></asp:BoundField>
            <asp:BoundField DataField="PROBLEM_NAME" HeaderText="PROBLEM_NAME" SortExpression="PROBLEM_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_CHILD_LABOUR_PROBLEM] WHERE [PROBLEM_ID] = @PROBLEM_ID" InsertCommand="INSERT INTO [COM_CHILD_LABOUR_PROBLEM] ([PROBLEM_ID], [PROBLEM_NAME]) VALUES (@PROBLEM_ID, @PROBLEM_NAME)" SelectCommand="SELECT * FROM [COM_CHILD_LABOUR_PROBLEM]" UpdateCommand="UPDATE [COM_CHILD_LABOUR_PROBLEM] SET [PROBLEM_NAME] = @PROBLEM_NAME WHERE [PROBLEM_ID] = @PROBLEM_ID">
        <DeleteParameters>
            <asp:Parameter Name="PROBLEM_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="PROBLEM_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="PROBLEM_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="PROBLEM_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="PROBLEM_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_ANIMAL_DETAILS] WHERE [ANIMAL_ID] = @ANIMAL_ID" InsertCommand="INSERT INTO [COM_ANIMAL_DETAILS] ([ANIMAL_ID], [ANIMAL_NAME]) VALUES (@ANIMAL_ID, @ANIMAL_NAME)" SelectCommand="SELECT * FROM [COM_ANIMAL_DETAILS]" UpdateCommand="UPDATE [COM_ANIMAL_DETAILS] SET [ANIMAL_NAME] = @ANIMAL_NAME WHERE [ANIMAL_ID] = @ANIMAL_ID">
        <DeleteParameters>
            <asp:Parameter Name="ANIMAL_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="ANIMAL_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="ANIMAL_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ANIMAL_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="ANIMAL_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
