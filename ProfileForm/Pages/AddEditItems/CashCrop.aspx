﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CashCrop.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.CashCrop" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblnew" visible="false" runat="server" Text="Cash Crop" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtnew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddCashCrop" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnCashCropClicked" runat="server" Text="Add Cash Crop" />
            </td>
        </tr>
    </table>
    <asp:Label ID="Label1"  runat="server" Text="Animal List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="CASH_CROP_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"></asp:CommandField>
            <asp:BoundField DataField="CASH_CROP_ID" ReadOnly="true" HeaderText="CASH_CROP_ID" SortExpression="CASH_CROP_ID"></asp:BoundField>
            <asp:BoundField DataField="CASH_CROP_NAME" HeaderText="CASH_CROP_NAME" SortExpression="CASH_CROP_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' SelectCommand="SELECT * FROM [COM_CASH_CROP]" DeleteCommand="DELETE FROM [COM_CASH_CROP] WHERE [CASH_CROP_ID] = @CASH_CROP_ID" InsertCommand="INSERT INTO [COM_CASH_CROP] ([CASH_CROP_ID], [CASH_CROP_NAME]) VALUES (@CASH_CROP_ID, @CASH_CROP_NAME)" UpdateCommand="UPDATE [COM_CASH_CROP] SET [CASH_CROP_NAME] = @CASH_CROP_NAME WHERE [CASH_CROP_ID] = @CASH_CROP_ID">
        <DeleteParameters>
            <asp:Parameter Name="CASH_CROP_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="CASH_CROP_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="CASH_CROP_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="CASH_CROP_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="CASH_CROP_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
