﻿using Common.AddEditItemBO;
using DataAccessLayer.AddEditItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace ProfileForm.Pages.AddEditItems
{
    public partial class FruitName : System.Web.UI.Page
    {
        FruitBo obj = new FruitBo();
        FruitDAO objDao = new FruitDAO();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                txtNew.Visible = true;
                lblNew.Visible = true;
            }

        }

        protected void btnAddFruitClicked(object sender, EventArgs e)
        {
            if (txtNew.Text != null && txtNew.Text != "")
            {
                obj.fruitName = txtNew.Text;
                objDao.InsertFloor(obj);
                XmlDocument doc = new XmlDocument();
                string path = Server.MapPath("../../XMLDataSource/Fruits.xml");
                doc.Load(path);
                XmlElement root = doc.DocumentElement;
                XmlElement animals = doc.CreateElement("Fruits");
                XmlAttribute id = doc.CreateAttribute("id");
                XmlAttribute Fruits = doc.CreateAttribute("name");

                XDocument xdoc;
                xdoc = XDocument.Load(path);
                string aid = Convert.ToString((from b in xdoc.Descendants("Fruits") orderby (int)b.Attribute("id") descending select (int)b.Attribute("id")).FirstOrDefault() + 1);
                id.Value = aid;
                Fruits.Value = txtNew.Text;
                root.AppendChild(animals);
                animals.Attributes.Append(id);
                animals.Attributes.Append(Fruits);
                doc.Save(path);
                txtNew.Text = null;
                Response.Write("<script>alert('" + "Data Properly Inserted" + "')</script>");
               Response.Redirect(Constant.constantApppath+"/pages/FruitName.aspx");


            }

        }
        XDocument data;
        protected void GrdRowDeleting(object sender, GridViewDeleteEventArgs e)
        {


            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = row.Cells[1].Text;

            data = XDocument.Load(Constant.constantApppath+"/XmlDatasource/Fruits.xml");
            var person =
                from p in data.Descendants("Fruits")
                where p.Attribute("id").Value == a
                select p;
            person.Remove();
            //data.Root.Elements("Person").Where(i => (int)i.Attribute("id") == id).Remove();
            data.Save(Constant.constantApppath+"/XmlDatasource/Fruits.xml");
        }
        protected void GrdRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id;
            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = ((TextBox)(row.Cells[2].Controls[0])).Text;
            // string b=((TextBox)(row.Cells[1]).Controls[0])).Text;
            int b = Convert.ToInt32(row.Cells[1].Text);

            // id = Convert.ToInt32(e.RowIndex);
            XDocument data = XDocument.Load(Constant.constantApppath+"/XmlDatasource/Fruits.xml");
            XElement animalExisting = data.Root.Elements("Fruits").Where(i => (int)i.Attribute("id") == b).FirstOrDefault();
            animalExisting.Attribute("name").Value = a;
            data.Save(Constant.constantApppath+"/XmlDataSource/Fruits.xml");

        }
    }
}