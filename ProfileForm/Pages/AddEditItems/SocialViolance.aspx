﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SocialViolance.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.SocialViolance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Social Violance Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddSocialViolance" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddSocialViolanceClicked" runat="server" Text="Add Social Violance" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Social Violance List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="VIOLENCE_ID" AutoGenerateColumns="False" Width="156px" CssClass="table table-hover" DataSourceID="SqlDataSource1" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"></asp:CommandField>
            <asp:BoundField DataField="VIOLENCE_ID" ReadOnly="true" HeaderText="VIOLENCE_ID" SortExpression="VIOLENCE_ID"></asp:BoundField>
            <asp:BoundField DataField="VIOLENCE_NAME" HeaderText="VIOLENCE_NAME" SortExpression="VIOLENCE_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_SOCIALVIOLENCE] WHERE [VIOLENCE_ID] = @VIOLENCE_ID" InsertCommand="INSERT INTO [COM_SOCIALVIOLENCE] ([VIOLENCE_ID], [VIOLENCE_NAME]) VALUES (@VIOLENCE_ID, @VIOLENCE_NAME)" SelectCommand="SELECT * FROM [COM_SOCIALVIOLENCE]" UpdateCommand="UPDATE [COM_SOCIALVIOLENCE] SET [VIOLENCE_NAME] = @VIOLENCE_NAME WHERE [VIOLENCE_ID] = @VIOLENCE_ID">
        <DeleteParameters>
            <asp:Parameter Name="VIOLENCE_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="VIOLENCE_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="VIOLENCE_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="VIOLENCE_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="VIOLENCE_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
