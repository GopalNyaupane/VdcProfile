﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Vegetables.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Vegetables" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Vegetables Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddVegetables" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddVegetablesClicked" runat="server" Text="Add Vegetables" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Vegetables List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" DataKeyNames="VEGETABLES_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="VEGETABLES_ID" ReadOnly="true" HeaderText="VEGETABLES_ID" SortExpression="VEGETABLES_ID"></asp:BoundField>
            <asp:BoundField DataField="VEGETABLES_NAME" HeaderText="VEGETABLES_NAME" SortExpression="VEGETABLES_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_VEGETABLES] WHERE [VEGETABLES_ID] = @VEGETABLES_ID" InsertCommand="INSERT INTO [COM_VEGETABLES] ([VEGETABLES_ID], [VEGETABLES_NAME]) VALUES (@VEGETABLES_ID, @VEGETABLES_NAME)" SelectCommand="SELECT * FROM [COM_VEGETABLES]" UpdateCommand="UPDATE [COM_VEGETABLES] SET [VEGETABLES_NAME] = @VEGETABLES_NAME WHERE [VEGETABLES_ID] = @VEGETABLES_ID">
        <DeleteParameters>
            <asp:Parameter Name="VEGETABLES_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="VEGETABLES_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="VEGETABLES_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="VEGETABLES_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="VEGETABLES_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
