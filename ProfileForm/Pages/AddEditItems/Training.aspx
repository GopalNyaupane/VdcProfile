﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Training.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Training" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Trainin Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddTraining" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddTrainingClicked" runat="server" Text="Add Training" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Training List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" DataKeyNames="TRAINING_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="TRAINING_ID" ReadOnly="true" HeaderText="TRAINING_ID" SortExpression="TRAINING_ID"></asp:BoundField>
            <asp:BoundField DataField="TRAINING_NAME" HeaderText="TRAINING_NAME" SortExpression="TRAINING_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_TRAINING] WHERE [TRAINING_ID] = @TRAINING_ID" InsertCommand="INSERT INTO [COM_TRAINING] ([TRAINING_ID], [TRAINING_NAME]) VALUES (@TRAINING_ID, @TRAINING_NAME)" SelectCommand="SELECT * FROM [COM_TRAINING]" UpdateCommand="UPDATE [COM_TRAINING] SET [TRAINING_NAME] = @TRAINING_NAME WHERE [TRAINING_ID] = @TRAINING_ID">
        <DeleteParameters>
            <asp:Parameter Name="TRAINING_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="TRAINING_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="TRAINING_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="TRAINING_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="TRAINING_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
