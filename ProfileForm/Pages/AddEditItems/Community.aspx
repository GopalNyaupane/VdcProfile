﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Community.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Community" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Community" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddCaste" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddCommunityClicked" runat="server" Text="Add Community" />
            </td>
        </tr>
    </table>
    <asp:Label ID="Label1"  runat="server" Text="Community List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" AutoGenerateColumns="False" DataKeyNames="COMMUNITY_ID" DataSourceID="SqlDataSource1" AllowSorting="True" Width="156px" CssClass="table table-hover">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"></asp:CommandField>
            <asp:BoundField DataField="COMMUNITY_ID" HeaderText="COMMUNITY_ID" ReadOnly="True" SortExpression="COMMUNITY_ID"></asp:BoundField>
            <asp:BoundField DataField="COMMUNITY_NAME" HeaderText="COMMUNITY_NAME" SortExpression="COMMUNITY_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_COMMUNITY] WHERE [COMMUNITY_ID] = @COMMUNITY_ID" InsertCommand="INSERT INTO [COM_COMMUNITY] ([COMMUNITY_ID], [COMMUNITY_NAME]) VALUES (@COMMUNITY_ID, @COMMUNITY_NAME)" SelectCommand="SELECT * FROM [COM_COMMUNITY]" UpdateCommand="UPDATE [COM_COMMUNITY] SET [COMMUNITY_NAME] = @COMMUNITY_NAME WHERE [COMMUNITY_ID] = @COMMUNITY_ID">
        <DeleteParameters>
            <asp:Parameter Name="COMMUNITY_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="COMMUNITY_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="COMMUNITY_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="COMMUNITY_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="COMMUNITY_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
