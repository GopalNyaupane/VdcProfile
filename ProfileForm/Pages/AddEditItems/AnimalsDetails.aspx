﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AnimalsDetails.aspx.cs" Inherits="ProfileForm.Pages.AnimalsDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblnew" visible="false" runat="server" Text="Animal Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtnew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddAnimal" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddAnimalClicked" runat="server" Text="Add Animal" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Animal List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:HiddenField runat="server" ID="hidAnimalId" />
     <asp:GridView ID="GridView1" runat="server" AutoPostBack="true" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" DataKeyNames="ANIMAL_ID" AutoGenerateColumns="False" DataSourceID="DSAnimalDetails" Width="156px" CssClass="table table-hover" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" >
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="true">
            </asp:CommandField>
            <asp:BoundField DataField="ANIMAL_ID" ReadOnly="true" HeaderText="ANIMAL_ID" SortExpression="ANIMAL_ID"></asp:BoundField>
            <asp:BoundField DataField="ANIMAL_NAME" HeaderText="ANIMAL_NAME" SortExpression="ANIMAL_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="DSAnimalDetails" ConnectionString='<%$ ConnectionStrings:constr %>' SelectCommand="SELECT * FROM [COM_ANIMAL_DETAILS]" UpdateCommand="UPDATE [COM_ANIMAL_DETAILS] SET [ANIMAL_NAME] = @ANIMAL_NAME WHERE [ANIMAL_ID] = @ANIMAL_ID" DeleteCommand="DELETE FROM [COM_ANIMAL_DETAILS] WHERE [ANIMAL_ID] = @ANIMAL_ID" InsertCommand="INSERT INTO [COM_ANIMAL_DETAILS] ([ANIMAL_ID], [ANIMAL_NAME]) VALUES (@ANIMAL_ID, @ANIMAL_NAME)">
        <DeleteParameters>
            <asp:Parameter Name="ANIMAL_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="ANIMAL_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="ANIMAL_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ANIMAL_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="ANIMAL_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
