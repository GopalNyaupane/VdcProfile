﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FruitName.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.FruitName" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Fruit Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddFruit" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddFruitClicked" runat="server" Text="Add Fruit" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Fruit List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="FRUIT_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"></asp:CommandField>
            <asp:BoundField DataField="FRUIT_ID" ReadOnly="true" HeaderText="FRUIT_ID" SortExpression="FRUIT_ID"></asp:BoundField>
            <asp:BoundField DataField="FRUIT_NAME" HeaderText="FRUIT_NAME" SortExpression="FRUIT_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_FRUITS] WHERE [FRUIT_ID] = @FRUIT_ID" InsertCommand="INSERT INTO [COM_FRUITS] ([FRUIT_ID], [FRUIT_NAME]) VALUES (@FRUIT_ID, @FRUIT_NAME)" SelectCommand="SELECT * FROM [COM_FRUITS]" UpdateCommand="UPDATE [COM_FRUITS] SET [FRUIT_NAME] = @FRUIT_NAME WHERE [FRUIT_ID] = @FRUIT_ID">
        <DeleteParameters>
            <asp:Parameter Name="FRUIT_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="FRUIT_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="FRUIT_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="FRUIT_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="FRUIT_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
