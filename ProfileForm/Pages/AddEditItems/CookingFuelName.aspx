﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CookingFuelName.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.CookingFuelName" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Cooking Fuel Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddCookingFuelName" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddCokkingFuelNameClicked" runat="server" Text="Add Cooking Fuel Name" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Cooking Fuel List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" DataKeyNames="COOKINGFUEL_ID" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="COOKINGFUEL_ID" HeaderText="COOKINGFUEL_ID" ReadOnly="True" SortExpression="COOKINGFUEL_ID"></asp:BoundField>
            <asp:BoundField DataField="COOKINGFUEL_NAME" HeaderText="COOKINGFUEL_NAME" SortExpression="COOKINGFUEL_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_COOKINGFUEL] WHERE [COOKINGFUEL_ID] = @COOKINGFUEL_ID" InsertCommand="INSERT INTO [COM_COOKINGFUEL] ([COOKINGFUEL_ID], [COOKINGFUEL_NAME]) VALUES (@COOKINGFUEL_ID, @COOKINGFUEL_NAME)" SelectCommand="SELECT * FROM [COM_COOKINGFUEL]" UpdateCommand="UPDATE [COM_COOKINGFUEL] SET [COOKINGFUEL_NAME] = @COOKINGFUEL_NAME WHERE [COOKINGFUEL_ID] = @COOKINGFUEL_ID">
        <DeleteParameters>
            <asp:Parameter Name="COOKINGFUEL_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="COOKINGFUEL_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="COOKINGFUEL_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="COOKINGFUEL_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="COOKINGFUEL_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
