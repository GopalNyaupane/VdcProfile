﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChildDifferentiate.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.ChildDifferentiate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblnew" visible="false" runat="server" Text="Differentiate Type" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtnew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddCashCrop" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnCashCropClicked" runat="server" Text="Add Differentiate" />
            </td>
        </tr>
    </table>
    <asp:Label ID="Label1"  runat="server" Text="Differentiate List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" DataKeyNames="DIFFERENTIATE_ID" AllowSorting="True" Width="156px" CssClass="table table-hover">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="DIFFERENTIATE_ID" HeaderText="DIFFERENTIATE_ID" ReadOnly="True" SortExpression="DIFFERENTIATE_ID"></asp:BoundField>
            <asp:BoundField DataField="DIFFERENTIATE_NAME" HeaderText="DIFFERENTIATE_NAME" SortExpression="DIFFERENTIATE_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_CHILDDIFFERENTIATE] WHERE [DIFFERENTIATE_ID] = @DIFFERENTIATE_ID" InsertCommand="INSERT INTO [COM_CHILDDIFFERENTIATE] ([DIFFERENTIATE_ID], [DIFFERENTIATE_NAME]) VALUES (@DIFFERENTIATE_ID, @DIFFERENTIATE_NAME)" SelectCommand="SELECT * FROM [COM_CHILDDIFFERENTIATE]" UpdateCommand="UPDATE [COM_CHILDDIFFERENTIATE] SET [DIFFERENTIATE_NAME] = @DIFFERENTIATE_NAME WHERE [DIFFERENTIATE_ID] = @DIFFERENTIATE_ID">
        <DeleteParameters>
            <asp:Parameter Name="DIFFERENTIATE_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="DIFFERENTIATE_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="DIFFERENTIATE_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="DIFFERENTIATE_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="DIFFERENTIATE_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
