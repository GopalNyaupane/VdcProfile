﻿using Common.Entity;
using DataAccessLayer.AddEditItem;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace ProfileForm.Pages
{
    public partial class AnimalsDetails : System.Web.UI.Page
    {
        AddAnimal obj = new AddAnimal();
        AddAnimalDAO objDao = new AddAnimalDAO();
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if(IsPostBack)
            {
                txtnew.Visible = true;
                lblnew.Visible = true;
            }
        }

        protected void btnAddAnimalClicked(object sender, EventArgs e)
        {
           
            if (txtnew.Text != null && txtnew.Text!="")
            {
                obj.animalName = txtnew.Text;
                objDao.InsertAnimal(obj);
                XmlDocument doc = new XmlDocument();
                string path = Server.MapPath("../../XMLDataSource/Animals.xml");
                doc.Load(path);
                XmlElement root = doc.DocumentElement;
                XmlElement animals = doc.CreateElement("Animals");
                XmlAttribute id = doc.CreateAttribute("id");
                XmlAttribute name = doc.CreateAttribute("name");

                XDocument xdoc;
                xdoc = XDocument.Load(path);
                string aid = Convert.ToString((from b in xdoc.Descendants("Animals") orderby (int)b.Attribute("id") descending select (int)b.Attribute("id")).FirstOrDefault() + 1);
                id.Value = aid;
                name.Value = txtnew.Text;
                root.AppendChild(animals);
                animals.Attributes.Append(id);
                animals.Attributes.Append(name);
                doc.Save(path);
                txtnew.Text = null;
                Response.Write("<script>alert('" + "Data Properly Inserted" + "')</script>");
               Response.Redirect(Constant.constantApppath+"/pages/AnimalsDetails.aspx");

            }
        }
        XDocument data;
        protected void GrdRowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            
            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = row.Cells[1].Text;
           
            data = XDocument.Load(Constant.constantApppath+"/XmlDatasource/Animals.xml");
            var person =
                from p in data.Descendants("Animals")
                where p.Attribute("id").Value ==a
                select p;
            person.Remove();
            //data.Root.Elements("Person").Where(i => (int)i.Attribute("id") == id).Remove();
            data.Save(Constant.constantApppath+"/XmlDatasource/Animals.xml");
        }
        protected void GrdRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id;
            GridViewRow  row = GridView1.Rows[e.RowIndex];
            string a = ((TextBox)(row.Cells[2].Controls[0])).Text;
           // string b=((TextBox)(row.Cells[1]).Controls[0])).Text;
           int b = Convert.ToInt32(row.Cells[1].Text);
            
           // id = Convert.ToInt32(e.RowIndex);
            XDocument data = XDocument.Load(Constant.constantApppath+"/XmlDatasource/Animals.xml");
            XElement animalExisting = data.Root.Elements("Animals").Where(i => (int)i.Attribute("id") == b).FirstOrDefault();
            animalExisting.Attribute("name").Value = a;
            data.Save(Constant.constantApppath+"/XmlDataSource/Animals.xml");

        }
        public string animalId;
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
             animalId = GridView1.SelectedRow.Cells[1].Text;
             hidAnimalId.Value = animalId;
        }

        

       
    }
}