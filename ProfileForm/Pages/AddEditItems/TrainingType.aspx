﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TrainingType.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.TrainingType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Training Type Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddTrainingType" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddTrainingTypeClicked" runat="server" Text="Add Training Type" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Training Type List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" DataKeyNames="TRAININGTYPE_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="TRAININGTYPE_ID" ReadOnly="true" HeaderText="TRAININGTYPE_ID" SortExpression="TRAININGTYPE_ID"></asp:BoundField>
            <asp:BoundField DataField="TRAININGTYPE_NAME" HeaderText="TRAININGTYPE_NAME" SortExpression="TRAININGTYPE_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_TRAININGTYPE] WHERE [TRAININGTYPE_ID] = @TRAININGTYPE_ID" InsertCommand="INSERT INTO [COM_TRAININGTYPE] ([TRAININGTYPE_ID], [TRAININGTYPE_NAME]) VALUES (@TRAININGTYPE_ID, @TRAININGTYPE_NAME)" SelectCommand="SELECT * FROM [COM_TRAININGTYPE]" UpdateCommand="UPDATE [COM_TRAININGTYPE] SET [TRAININGTYPE_NAME] = @TRAININGTYPE_NAME WHERE [TRAININGTYPE_ID] = @TRAININGTYPE_ID">
        <DeleteParameters>
            <asp:Parameter Name="TRAININGTYPE_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="TRAININGTYPE_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="TRAININGTYPE_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="TRAININGTYPE_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="TRAININGTYPE_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
