﻿using Common.AddEditItemBO;
using DataAccessLayer.AddEditItem;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace ProfileForm.Pages.AddEditItems
{
    public partial class CashCrop : System.Web.UI.Page
    {
        AddCashCropBo obj = new AddCashCropBo();
        AddCashCropDAO objDao = new AddCashCropDAO();
        protected void Page_Load(object sender, EventArgs e)
        {
             if(IsPostBack)
            {
                txtnew.Visible = true;
                lblnew.Visible = true;
            }

        }

        protected void btnCashCropClicked(object sender, EventArgs e)
        {
            
           
            if (txtnew.Text != null && txtnew.Text!="")
            {
                obj.cashCropName = txtnew.Text;
                objDao.InsertCashCrop(obj);
                XmlDocument doc = new XmlDocument();
                string path = Server.MapPath("../../XMLDataSource/CashCrops.xml");
                doc.Load(path);
                XmlElement root = doc.DocumentElement;
                XmlElement CashCrops = doc.CreateElement("CashCrops");
                XmlAttribute id = doc.CreateAttribute("id");
                XmlAttribute name = doc.CreateAttribute("name");

                XDocument xdoc;
                xdoc = XDocument.Load(path);
                string aid = Convert.ToString((from b in xdoc.Descendants("CashCrops") orderby (int)b.Attribute("id") descending select (int)b.Attribute("id")).FirstOrDefault() + 1);
                id.Value = aid;
                name.Value = txtnew.Text;
                root.AppendChild(CashCrops);
                CashCrops.Attributes.Append(id);
                CashCrops.Attributes.Append(name);
                doc.Save(path);
                txtnew.Text = null;
                Response.Write("<script>alert('" + "Data Properly Inserted" + "')</script>");
               Response.Redirect(Constant.constantApppath+"/pages/CashCrop.aspx");

            }
        }
        XDocument data;
        protected void GrdRowDeleting(object sender, GridViewDeleteEventArgs e)
        {


            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = row.Cells[1].Text;

            data = XDocument.Load(Constant.constantApppath+"/XmlDatasource/CashCrops.xml");
            var person =
                from p in data.Descendants("CashCrops")
                where p.Attribute("id").Value == a
                select p;
            person.Remove();
            //data.Root.Elements("Person").Where(i => (int)i.Attribute("id") == id).Remove();
            data.Save(Constant.constantApppath+"/XmlDatasource/CashCrops.xml");
        }
        protected void GrdRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id;
            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = ((TextBox)(row.Cells[2].Controls[0])).Text;
            // string b=((TextBox)(row.Cells[1]).Controls[0])).Text;
            int b = Convert.ToInt32(row.Cells[1].Text);

            // id = Convert.ToInt32(e.RowIndex);
            XDocument data = XDocument.Load(Constant.constantApppath+"/XmlDatasource/CashCrops.xml");
            XElement animalExisting = data.Root.Elements("CashCrops").Where(i => (int)i.Attribute("id") == b).FirstOrDefault();
            animalExisting.Attribute("name").Value = a;
            data.Save(Constant.constantApppath+"/XmlDataSource/CashCrops.xml");

        }
    }
}