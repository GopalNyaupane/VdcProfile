﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Handwash.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Handwash" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div>
        <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblnew" visible="false" runat="server" Text="Hand Wash" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtnew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddHandWash" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddHandWashClicked" runat="server" Text="Add Handwash Type" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Handwash Type " Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
        <asp:GridView ID="GridView1" runat="server" AutoPostBack="true" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" DataKeyNames="HANDWASH_ID" AllowSorting="True">
            <Columns>
                <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
                <asp:BoundField DataField="HANDWASH_ID" HeaderText="HANDWASH_ID" ReadOnly="True" SortExpression="HANDWASH_ID"></asp:BoundField>
                <asp:BoundField DataField="HANDWASH_NAME" HeaderText="HANDWASH_NAME" SortExpression="HANDWASH_NAME"></asp:BoundField>
            </Columns>
        </asp:GridView>

        <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_HANDWASH] WHERE [HANDWASH_ID] = @HANDWASH_ID" InsertCommand="INSERT INTO [COM_HANDWASH] ([HANDWASH_ID], [HANDWASH_NAME]) VALUES (@HANDWASH_ID, @HANDWASH_NAME)" SelectCommand="SELECT * FROM [COM_HANDWASH]" UpdateCommand="UPDATE [COM_HANDWASH] SET [HANDWASH_NAME] = @HANDWASH_NAME WHERE [HANDWASH_ID] = @HANDWASH_ID">
            <DeleteParameters>
                <asp:Parameter Name="HANDWASH_ID" Type="Int32"></asp:Parameter>
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="HANDWASH_ID" Type="Int32"></asp:Parameter>
                <asp:Parameter Name="HANDWASH_NAME" Type="String"></asp:Parameter>
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="HANDWASH_NAME" Type="String"></asp:Parameter>
                <asp:Parameter Name="HANDWASH_ID" Type="Int32"></asp:Parameter>
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
  

</asp:Content>
