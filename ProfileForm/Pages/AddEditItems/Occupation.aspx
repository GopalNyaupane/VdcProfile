﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Occupation.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Occupation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Occupation" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddOccupation" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddOccupationClicked" runat="server" Text="Add Occupation" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Occupation List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="OCCUPATION_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="OCCUPATION_ID" ReadOnly="true" HeaderText="OCCUPATION_ID" SortExpression="OCCUPATION_ID"></asp:BoundField>
            <asp:BoundField DataField="OCCUPATION_NAME" HeaderText="OCCUPATION_NAME" SortExpression="OCCUPATION_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_OCCUPATION] WHERE [OCCUPATION_ID] = @OCCUPATION_ID" InsertCommand="INSERT INTO [COM_OCCUPATION] ([OCCUPATION_ID], [OCCUPATION_NAME]) VALUES (@OCCUPATION_ID, @OCCUPATION_NAME)" SelectCommand="SELECT * FROM [COM_OCCUPATION]" UpdateCommand="UPDATE [COM_OCCUPATION] SET [OCCUPATION_NAME] = @OCCUPATION_NAME WHERE [OCCUPATION_ID] = @OCCUPATION_ID">
        <DeleteParameters>
            <asp:Parameter Name="OCCUPATION_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="OCCUPATION_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="OCCUPATION_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="OCCUPATION_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="OCCUPATION_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
