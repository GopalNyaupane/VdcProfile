﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Religion.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Religion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Religion Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddReligion" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddReligionClicked" runat="server" Text="Add Religion" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Religion List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="RELIGION_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="RELIGION_ID" ReadOnly="true" HeaderText="RELIGION_ID" SortExpression="RELIGION_ID"></asp:BoundField>
            <asp:BoundField DataField="RELIGION_NAME" HeaderText="RELIGION_NAME" SortExpression="RELIGION_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_RELIGION] WHERE [RELIGION_ID] = @RELIGION_ID" InsertCommand="INSERT INTO [COM_RELIGION] ([RELIGION_ID], [RELIGION_NAME]) VALUES (@RELIGION_ID, @RELIGION_NAME)" SelectCommand="SELECT * FROM [COM_RELIGION]" UpdateCommand="UPDATE [COM_RELIGION] SET [RELIGION_NAME] = @RELIGION_NAME WHERE [RELIGION_ID] = @RELIGION_ID">
        <DeleteParameters>
            <asp:Parameter Name="RELIGION_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="RELIGION_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="RELIGION_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="RELIGION_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="RELIGION_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
