﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChildPunishment.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.ChildPunishment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Child Punishment" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddPunishment" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnChildPunishmentClicked" runat="server" Text="Add Child Punishment Type" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Child Punishment List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="CHILD_PUNISHMENT_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="CHILD_PUNISHMENT_ID" ReadOnly="true" HeaderText="CHILD_PUNISHMENT_ID" SortExpression="CHILD_PUNISHMENT_ID"></asp:BoundField>
            <asp:BoundField DataField="CHILD_PUNISHMENT_NAME" HeaderText="CHILD_PUNISHMENT_NAME" SortExpression="CHILD_PUNISHMENT_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_CHILD_PUNISHMENT] WHERE [CHILD_PUNISHMENT_ID] = @CHILD_PUNISHMENT_ID" InsertCommand="INSERT INTO [COM_CHILD_PUNISHMENT] ([CHILD_PUNISHMENT_ID], [CHILD_PUNISHMENT_NAME]) VALUES (@CHILD_PUNISHMENT_ID, @CHILD_PUNISHMENT_NAME)" SelectCommand="SELECT * FROM [COM_CHILD_PUNISHMENT]" UpdateCommand="UPDATE [COM_CHILD_PUNISHMENT] SET [CHILD_PUNISHMENT_NAME] = @CHILD_PUNISHMENT_NAME WHERE [CHILD_PUNISHMENT_ID] = @CHILD_PUNISHMENT_ID">
        <DeleteParameters>
            <asp:Parameter Name="CHILD_PUNISHMENT_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="CHILD_PUNISHMENT_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="CHILD_PUNISHMENT_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="CHILD_PUNISHMENT_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="CHILD_PUNISHMENT_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
