﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MigrationReason.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.MigrationReason" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Migration Reason" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddMigrationReason" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddMigrationReasonClicked" runat="server" Text="Add Migration Reason" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Migration Reason List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" DataKeyNames="REASON_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="REASON_ID" ReadOnly="true" HeaderText="REASON_ID" SortExpression="REASON_ID"></asp:BoundField>
            <asp:BoundField DataField="REASON_NAME" HeaderText="REASON_NAME" SortExpression="REASON_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_MIGRATIONREASON] WHERE [REASON_ID] = @REASON_ID" InsertCommand="INSERT INTO [COM_MIGRATIONREASON] ([REASON_ID], [REASON_NAME]) VALUES (@REASON_ID, @REASON_NAME)" SelectCommand="SELECT * FROM [COM_MIGRATIONREASON]" UpdateCommand="UPDATE [COM_MIGRATIONREASON] SET [REASON_NAME] = @REASON_NAME WHERE [REASON_ID] = @REASON_ID">
        <DeleteParameters>
            <asp:Parameter Name="REASON_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="REASON_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="REASON_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="REASON_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="REASON_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
