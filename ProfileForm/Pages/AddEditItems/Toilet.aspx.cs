﻿using Common.AddEditItemBO;
using DataAccessLayer.AddEditItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace ProfileForm.Pages.AddEditItems
{
    public partial class Toilet : System.Web.UI.Page
    {
        ToiletBo obj = new ToiletBo();
        ToiletDAO objDao = new ToiletDAO();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                txtNew.Visible = true;
                lblNew.Visible = true;
            }
        }

        protected void btnToiletClicked(object sender, EventArgs e)
        {
            if (txtNew.Text != null && txtNew.Text != "")
            {
                obj.toiletName = txtNew.Text;
                objDao.InsertToilet(obj);
                XmlDocument doc = new XmlDocument();
                string path = Server.MapPath("../../XMLDataSource/Toilet.xml");
                doc.Load(path);
                XmlElement root = doc.DocumentElement;
                XmlElement Toilet = doc.CreateElement("Toilet");
                XmlAttribute id = doc.CreateAttribute("id");
                XmlAttribute name = doc.CreateAttribute("name");

                XDocument xdoc;
                xdoc = XDocument.Load(path);
                string aid = Convert.ToString((from b in xdoc.Descendants("Toilet") orderby (int)b.Attribute("id") descending select (int)b.Attribute("id")).FirstOrDefault() + 1);
                id.Value = aid;
                name.Value = txtNew.Text;
                root.AppendChild(Toilet);
                Toilet.Attributes.Append(id);
                Toilet.Attributes.Append(name);
                doc.Save(path);
                txtNew.Text = null;
                Response.Write("<script>alert('" + "Data Properly Inserted" + "')</script>");
               Response.Redirect(Constant.constantApppath+"/pages/Toilet.aspx");


            }
        }

        XDocument data;
        protected void GrdRowDeleting(object sender, GridViewDeleteEventArgs e)
        {


            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = row.Cells[1].Text;

            data = XDocument.Load(Constant.constantApppath+"/XmlDatasource/Toilet.xml");
            var person =
                from p in data.Descendants("Toilet")
                where p.Attribute("id").Value == a
                select p;
            person.Remove();
            //data.Root.Elements("Person").Where(i => (int)i.Attribute("id") == id).Remove();
            data.Save(Constant.constantApppath+"/XmlDatasource/Toilet.xml");
        }
        protected void GrdRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id;
            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = ((TextBox)(row.Cells[2].Controls[0])).Text;
            // string b=((TextBox)(row.Cells[1]).Controls[0])).Text;
            int b = Convert.ToInt32(row.Cells[1].Text);

            // id = Convert.ToInt32(e.RowIndex);
            XDocument data = XDocument.Load(Constant.constantApppath+"/XmlDatasource/Toilet.xml");
            XElement Toilet = data.Root.Elements("Animals").Where(i => (int)i.Attribute("id") == b).FirstOrDefault();
            Toilet.Attribute("name").Value = a;
            data.Save(Constant.constantApppath+"/XmlDataSource/Toilet.xml");

        }
    }
}