﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Common.AddEditItemBO;
using DataAccessLayer.AddEditItem;

namespace ProfileForm.Pages.AddEditItems
{
    public partial class Handwash : System.Web.UI.Page
    {
        private HandWashDAO objDao = new HandWashDAO();
        private HandWashBo obj = new HandWashBo();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack)
            {
                txtnew.Visible = true;
                lblnew.Visible = true;
            }
        }

        protected void btnAddHandWashClicked(object sender, EventArgs e)
        {
            if (txtnew.Text != null && txtnew.Text != "")
            {
                obj.handWashName = txtnew.Text;
                objDao.InsertHandWash(obj);
                XmlDocument doc = new XmlDocument();
                string path = Server.MapPath("../../XMLDataSource/Handwash.xml");
                doc.Load(path);
                XmlElement root = doc.DocumentElement;
                XmlElement Handwash = doc.CreateElement("Handwash");
                XmlAttribute id = doc.CreateAttribute("id");
                XmlAttribute name = doc.CreateAttribute("name");

                XDocument xdoc;
                xdoc = XDocument.Load(path);
                string aid =
                    Convert.ToString(
                        (from b in xdoc.Descendants("Handwash")
                            orderby (int) b.Attribute("id") descending
                            select (int) b.Attribute("id")).FirstOrDefault() + 1);
                id.Value = aid;
                name.Value = txtnew.Text;
                root.AppendChild(Handwash);
                Handwash.Attributes.Append(id);
                Handwash.Attributes.Append(name);
                doc.Save(path);
                txtnew.Text = null;
                Response.Write("<script>alert('" + "Data Properly Inserted" + "')</script>");
                Response.Redirect("Handwash.aspx");

            }
        }

        private XDocument data;

        protected void GrdRowDeleting(object sender, GridViewDeleteEventArgs e)
        {


            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = row.Cells[1].Text;

            data = XDocument.Load(Server.MapPath("../../XmlDatasource/Handwash.xml"));
            var person =
                from p in data.Descendants("Handwash")
                where p.Attribute("id").Value == a
                select p;
            person.Remove();
            //data.Root.Elements("Person").Where(i => (int)i.Attribute("id") == id).Remove();
            data.Save(Server.MapPath("../../XmlDatasource/Handwash.xml"));
        }

        protected void GrdRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id;
            GridViewRow row = GridView1.Rows[e.RowIndex];
            string a = ((TextBox) (row.Cells[2].Controls[0])).Text;
            // string b=((TextBox)(row.Cells[1]).Controls[0])).Text;
            int b = Convert.ToInt32(row.Cells[1].Text);

            // id = Convert.ToInt32(e.RowIndex);
            XDocument data = XDocument.Load(Server.MapPath("../../XmlDatasource/Handwash.xml"));
            XElement animalExisting =
                data.Root.Elements("Handwash").Where(i => (int) i.Attribute("id") == b).FirstOrDefault();
            animalExisting.Attribute("name").Value = a;
            data.Save(Server.MapPath("../../XmlDataSource/Handwash.xml"));

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}


