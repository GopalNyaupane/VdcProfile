﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FloorName.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.FloorName" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Floor Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnFloor" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddFloorClicked" runat="server" Text="Add Floor" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Floor List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="FLOOR_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="FLOOR_ID" ReadOnly="true" HeaderText="FLOOR_ID" SortExpression="FLOOR_ID"></asp:BoundField>
            <asp:BoundField DataField="FLOOR_NAME" HeaderText="FLOOR_NAME" SortExpression="FLOOR_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_FLOOR] WHERE [FLOOR_ID] = @FLOOR_ID" InsertCommand="INSERT INTO [COM_FLOOR] ([FLOOR_ID], [FLOOR_NAME]) VALUES (@FLOOR_ID, @FLOOR_NAME)" SelectCommand="SELECT * FROM [COM_FLOOR]" UpdateCommand="UPDATE [COM_FLOOR] SET [FLOOR_NAME] = @FLOOR_NAME WHERE [FLOOR_ID] = @FLOOR_ID">
        <DeleteParameters>
            <asp:Parameter Name="FLOOR_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="FLOOR_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="FLOOR_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="FLOOR_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="FLOOR_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
