﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Relation.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Relation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Relation Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddRelation" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddRelationClicked" runat="server" Text="Add Relation" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" Text="Relation List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" runat="server" DataKeyNames="RELATION_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="RELATION_ID" ReadOnly="true" HeaderText="RELATION_ID" SortExpression="RELATION_ID"></asp:BoundField>
            <asp:BoundField DataField="RELATION_NAME" HeaderText="RELATION_NAME" SortExpression="RELATION_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_RELATION] WHERE [RELATION_ID] = @RELATION_ID" InsertCommand="INSERT INTO [COM_RELATION] ([RELATION_ID], [RELATION_NAME]) VALUES (@RELATION_ID, @RELATION_NAME)" SelectCommand="SELECT * FROM [COM_RELATION]" UpdateCommand="UPDATE [COM_RELATION] SET [RELATION_NAME] = @RELATION_NAME WHERE [RELATION_ID] = @RELATION_ID">
        <DeleteParameters>
            <asp:Parameter Name="RELATION_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="RELATION_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="RELATION_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="RELATION_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="RELATION_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
