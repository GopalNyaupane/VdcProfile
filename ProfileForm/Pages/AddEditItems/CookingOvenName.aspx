﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CookingOvenName.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.CookingOvenName" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Cooking Oven Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddCookingOven" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddCookingOvenClicked" runat="server" Text="Add Cooking Oven" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Cooking Oven List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="OVEN_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="OVEN_ID" ReadOnly="true" HeaderText="OVEN_ID" SortExpression="OVEN_ID"></asp:BoundField>
            <asp:BoundField DataField="OVEN_NAME" HeaderText="OVEN_NAME" SortExpression="OVEN_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_COOKINGOVEN] WHERE [OVEN_ID] = @OVEN_ID" InsertCommand="INSERT INTO [COM_COOKINGOVEN] ([OVEN_ID], [OVEN_NAME]) VALUES (@OVEN_ID, @OVEN_NAME)" SelectCommand="SELECT * FROM [COM_COOKINGOVEN]" UpdateCommand="UPDATE [COM_COOKINGOVEN] SET [OVEN_NAME] = @OVEN_NAME WHERE [OVEN_ID] = @OVEN_ID">
        <DeleteParameters>
            <asp:Parameter Name="OVEN_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="OVEN_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="OVEN_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="OVEN_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="OVEN_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
