﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ElectricitySource.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.ElectricitySource" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Electricity Source" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddElectricitySource" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnElectricitySourceClicked" runat="server" Text="Add Electricity Source" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Electricity Source List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="ELECTRICITY_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="ELECTRICITY_ID" ReadOnly="true" HeaderText="ELECTRICITY_ID" SortExpression="ELECTRICITY_ID"></asp:BoundField>
            <asp:BoundField DataField="ELECTRICITY_NAME" HeaderText="ELECTRICITY_NAME" SortExpression="ELECTRICITY_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>



    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_ELECTRICITYSOURCE] WHERE [ELECTRICITY_ID] = @ELECTRICITY_ID" InsertCommand="INSERT INTO [COM_ELECTRICITYSOURCE] ([ELECTRICITY_ID], [ELECTRICITY_NAME]) VALUES (@ELECTRICITY_ID, @ELECTRICITY_NAME)" SelectCommand="SELECT * FROM [COM_ELECTRICITYSOURCE]" UpdateCommand="UPDATE [COM_ELECTRICITYSOURCE] SET [ELECTRICITY_NAME] = @ELECTRICITY_NAME WHERE [ELECTRICITY_ID] = @ELECTRICITY_ID">
        <DeleteParameters>
            <asp:Parameter Name="ELECTRICITY_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="ELECTRICITY_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="ELECTRICITY_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ELECTRICITY_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="ELECTRICITY_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
