﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Crops.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.Crops" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Crops Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddCrop" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddCropClicked" runat="server" Text="Add Crop" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Crop List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="CROP_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="CROP_ID" ReadOnly="true" HeaderText="CROP_ID" SortExpression="CROP_ID"></asp:BoundField>
            <asp:BoundField DataField="CROP_NAME" HeaderText="CROP_NAME" SortExpression="CROP_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_CROPS] WHERE [CROP_ID] = @CROP_ID" InsertCommand="INSERT INTO [COM_CROPS] ([CROP_ID], [CROP_NAME]) VALUES (@CROP_ID, @CROP_NAME)" SelectCommand="SELECT * FROM [COM_CROPS]" UpdateCommand="UPDATE [COM_CROPS] SET [CROP_NAME] = @CROP_NAME WHERE [CROP_ID] = @CROP_ID">
        <DeleteParameters>
            <asp:Parameter Name="CROP_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="CROP_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="CROP_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="CROP_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="CROP_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
