﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DiseaseName.aspx.cs" Inherits="ProfileForm.Pages.AddEditItems.DiseaseName" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-hover">
        <tr>
            <td>
                 <asp:Label ID="lblNew" visible="false" runat="server" Text="Disease Name" Font-Size="Larger" Font-Bold="true"></asp:Label>
            </td>
            <td>
                 <asp:TextBox ID="txtNew" Visible="false" runat="server" Width="400"></asp:TextBox>
            </td>
            <td>
                  <asp:Button ID="btnAddAnimal" CssClass="btn btn-primary" AutoPostBack="true"  OnClick="btnAddDiseaseClicked" runat="server" Text="Add Disease" />
            </td>
        </tr>
    </table>
  
    <asp:Label ID="Label1"  runat="server" Text="Disease List" Font-Size="Larger" Font-Bold="true"></asp:Label> <br />
    <asp:GridView ID="GridView1" OnRowDeleting="GrdRowDeleting" OnRowUpdating="GrdRowUpdating" runat="server" DataKeyNames="DISEASE_ID" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="156px" CssClass="table table-hover" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
            <asp:BoundField DataField="DISEASE_ID" ReadOnly="true" HeaderText="DISEASE_ID" SortExpression="DISEASE_ID"></asp:BoundField>
            <asp:BoundField DataField="DISEASE_NAME" HeaderText="DISEASE_NAME" SortExpression="DISEASE_NAME"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:constr %>' DeleteCommand="DELETE FROM [COM_DISEASE] WHERE [DISEASE_ID] = @DISEASE_ID" InsertCommand="INSERT INTO [COM_DISEASE] ([DISEASE_ID], [DISEASE_NAME]) VALUES (@DISEASE_ID, @DISEASE_NAME)" SelectCommand="SELECT * FROM [COM_DISEASE]" UpdateCommand="UPDATE [COM_DISEASE] SET [DISEASE_NAME] = @DISEASE_NAME WHERE [DISEASE_ID] = @DISEASE_ID">
        <DeleteParameters>
            <asp:Parameter Name="DISEASE_ID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="DISEASE_ID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="DISEASE_NAME" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="DISEASE_NAME" Type="String"></asp:Parameter>
            <asp:Parameter Name="DISEASE_ID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
