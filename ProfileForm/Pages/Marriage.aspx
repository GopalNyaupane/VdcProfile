﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Marriage.aspx.cs" Inherits="ProfileForm.Pages.Marriage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" />
    <script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>

    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');


        }
    </script>
    <table class="table table-striped">
        <tr>
            <td>२६.</td>
            <td>गएको ३ वर्षभित्र तपाईंको परिवारमा कसैको विवाह भएको भए सो को विवरण दिनुहोस् ।</td>
            <td></td>
        </tr>
        <tr>

            <td></td>
            <td></td>
            <td>
                <asp:Button runat="server" Text="AddAnother" ID="btnAddAnother" OnClick="btnAddAnother_Click" CssClass="btn btn-primary" />
            </td>


        </tr>
    </table>
    <asp:UpdatePanel ID="upnlMarrige" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddAnother" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table class="table table-hover">
                <tr>
                    <td>सि. नं.</td>
                    <td>विवाह गर्दाको उमेर कति वर्षको थियो  </td>
                    <td>लिङ्ग  </td>
                    <td></td>
                    <td></td>
                </tr>

                <asp:Panel runat="server" ID="pnlMarrige">
                    <asp:Repeater runat="server" ID="rptrMarriage">
                        <ItemTemplate>
                            <tr id="<%#Eval("MARITAL_INFO_ID") %>" class="div_row">
                                <td></td>

                                <td>
                                    <asp:TextBox runat="server" TextMode="Number" Text='<%#Eval("AGE") %>' ID="txtChildAge"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID") %>'>
                                        <asp:ListItem Value="0">पुरुष</asp:ListItem>
                                        <asp:ListItem Value="1">महिला</asp:ListItem>
                                        <asp:ListItem Value="2">अन्य</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                                    <asp:HiddenField runat="server" ID="fieldId" Value='<%#Eval("MARITAL_INFO_ID") %>' />
                                </td>
                                <td></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>

            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table class="table table-responsive">
        <tr>

            <td align="right">
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnNext_Click"
                    OnClientClick="return validatePage();" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary"
                    OnClick="btnUpdateMarriage_OnClick" Visible="False" />

            </td>
        </tr>
    </table>

</asp:Content>
