﻿using Common.Entity;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class TeenAgeHealth : System.Web.UI.Page
    {

        TeenAgeHealthBo objTeenAgeHealthBo = new TeenAgeHealthBo();
        TeenAgeHealthDao objTeenAgeHealthDao = new TeenAgeHealthDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    RpterTeenAgeHealth.DataBind();
                    rptrTeenFatality.DataBind();
                    rptrChildFatality.DataBind();
                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    Next.Visible = false;
                    Update.Visible = true;
                    LoadTeenAgeHealth(OwnerId);
                    LoadTeenAgeFatality(OwnerId);
                    LoadChildFatality(OwnerId);
                    LoadAdultHealth(OwnerId);
                    
                }
                else
                {
                    Update.Visible = false;
                    Next.Visible = true;
                    btnAddAnother.Enabled = false;
                    btnAddFatility.Enabled = false;
                    btnAddTeen.Enabled = false;
                    btnAddAnother1.Enabled = false;

                }
                if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
                }
                if (rdoIsDisease.SelectedIndex == 1)
                    btnAddAnother.Enabled = false;
               // else rdoIsDisease.Enabled = false;
                 if (rdoIsTeenFatality.SelectedIndex == 1)
                btnAddFatility.Enabled = false;
                 if (rdoIsChildFatality.SelectedIndex == 1)
                btnAddTeen.Enabled = false;
                if(rdoAdult.SelectedIndex==1)
                btnAddAnother1.Enabled = false;

            }




        }

        private void LoadAdultHealth(int OwnerId)
        {
            objTeenAgeHealthBo.OwnerId = OwnerId;
            DataTable dt=new DataTable();
            dt = objTeenAgeHealthDao.FetchAdultHealth(objTeenAgeHealthBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                rptrAdultHealth.DataSource = dt;
                rptrAdultHealth.DataBind();
                rdoAdult.SelectedValue = "0";
                btnAddAnother1.Enabled = true;


            }
            else btnAddAnother1.Enabled = false;
        }
        protected void rdoIsDisease_SIC(object sender, EventArgs e)
        {
            RpterTeenAgeHealth.DataSource = null;
            RpterTeenAgeHealth.DataBind();
            if (rdoIsDisease.SelectedIndex == 0)
            {

                btnAddAnother.Enabled = true;

            }
            else btnAddAnother.Enabled = false;
        }

        protected void rdoIsAdult_SIC(object sender, EventArgs e)
        {
            rptrAdultHealth.DataSource = null;
            rptrAdultHealth.DataBind();
            if (rdoAdult.SelectedIndex == 0)
            {
                btnAddAnother1.Enabled = true;
            }
            else btnAddAnother1.Enabled = false;
        }
        protected void rdoIsTeenFatality_SIC(object sender, EventArgs e)
        {
            rptrTeenFatality.DataSource = null;
            rptrTeenFatality.DataBind();
            if (rdoIsTeenFatality.SelectedIndex == 0)
            {

                btnAddFatility.Enabled = true;

            }
            else btnAddFatility.Enabled = false;
        }
        protected void rdoIsChildFatality_SIC(object sender, EventArgs e)
        {
            rptrChildFatality.DataSource = null;
            rptrChildFatality.DataBind();
            if (rdoIsChildFatality.SelectedIndex == 0)
            {

                btnAddTeen.Enabled = true;

            }
            else
            {
                btnAddTeen.Enabled = false;
                
            }
           
        }

        private void LoadChildFatality(int OwnerId)
        {
            objTeenAgeHealthBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objTeenAgeHealthDao.FetchChildFatality(objTeenAgeHealthBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                rptrChildFatality.DataSource = dt;
                rptrChildFatality.DataBind();

                rdoIsChildFatality.SelectedValue = "0";
                btnAddTeen.Enabled = true;

            }
            else btnAddTeen.Enabled = false;
        }
        private void LoadTeenAgeFatality(int OwnerId)
        {
            objTeenAgeHealthBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objTeenAgeHealthDao.FetchTeenAgeFatality(objTeenAgeHealthBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                rptrTeenFatality.DataSource = dt;
                rptrTeenFatality.DataBind();
                rdoIsTeenFatality.SelectedValue = "0";
                btnAddFatility.Enabled = true;

            }
            else btnAddFatility.Enabled = false;


        }
        private void LoadTeenAgeHealth(int OwnerId)
        {
            objTeenAgeHealthBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objTeenAgeHealthDao.FetchTeenAgeHealth(objTeenAgeHealthBo);

            if (dt != null && dt.Rows.Count > 0)
            {
                RpterTeenAgeHealth.DataSource = dt;
                RpterTeenAgeHealth.DataBind();
                rdoIsDisease.SelectedValue = "0";
                btnAddAnother.Enabled = true;

            }
            else btnAddAnother.Enabled = false;
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objTeenAgeHealthDao.DeleteOldRecord(OwnerId);
            InsertChildFatality();
            InsertTeenageFatality();
            InsertTeenageHealth();
            InsertAdultHealth();

           Response.Redirect(Constant.constantApppath+"/pages/ChildNutrition.aspx?Owner_Id=" + Session["Owner_Id"]);


        }

        protected void Next_Click(object sender, EventArgs e)
        {
            InsertChildFatality();
            InsertTeenageFatality();
            InsertTeenageHealth();
            InsertAdultHealth();
           Response.Redirect(Constant.constantApppath+"/pages/ChildNutrition.aspx");


        }

        private void InsertTeenageHealth()
        {
            List<TeenAgeHealthBo> objListTeenAgeHealthBo = new List<TeenAgeHealthBo>();
            foreach (RepeaterItem rptItem3 in RpterTeenAgeHealth.Items)
            {
                objTeenAgeHealthBo = new TeenAgeHealthBo();
                DropDownList ddlDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");

                if (ddlDisease.SelectedValue != "" && RdoSex.SelectedIndex >= 0)
                {
                    objTeenAgeHealthBo.DiseaseId = Convert.ToInt32(ddlDisease.SelectedValue);
                    objTeenAgeHealthBo.SexId = Convert.ToInt32(RdoSex.SelectedValue);
                    objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListTeenAgeHealthBo.Add(objTeenAgeHealthBo);

                }
            }
            objTeenAgeHealthDao.InsertTeenAgeHealth(objListTeenAgeHealthBo);
        }
        private void InsertTeenageFatality()
        {
            List<TeenAgeHealthBo> objListTeenAgeFatalityBo = new List<TeenAgeHealthBo>();
            foreach (RepeaterItem rptItem3 in rptrTeenFatality.Items)
            {
                objTeenAgeHealthBo = new TeenAgeHealthBo();
                DropDownList ddlDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");

                if (RdoSex.SelectedIndex >= 0 && ddlDisease.SelectedIndex>-1)
                {
                    objTeenAgeHealthBo.DiseaseId = Convert.ToInt32(ddlDisease.SelectedValue);
                    objTeenAgeHealthBo.SexId = Convert.ToInt32(RdoSex.SelectedValue);
                    objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListTeenAgeFatalityBo.Add(objTeenAgeHealthBo);
                }
            }
            objTeenAgeHealthDao.InsertTeenAgeFatality(objListTeenAgeFatalityBo);
        }
        private void InsertChildFatality()
        {
            List<TeenAgeHealthBo> objListChildFatalityBo = new List<TeenAgeHealthBo>();
            foreach (RepeaterItem rptItem3 in rptrChildFatality.Items)
            {
                objTeenAgeHealthBo = new TeenAgeHealthBo();
                DropDownList ddlDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");
                if (RdoSex.SelectedIndex >= 0 && ddlDisease.SelectedIndex>-1)
                {
                    objTeenAgeHealthBo.DeadReason = Convert.ToInt32(ddlDisease.SelectedIndex);
                    objTeenAgeHealthBo.SexId = Convert.ToInt32(RdoSex.SelectedValue);
                    objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListChildFatalityBo.Add(objTeenAgeHealthBo);
                }

            }
            objTeenAgeHealthDao.InsertChildFatality(objListChildFatalityBo);
        }
        private void InsertAdultHealth()
        {
            List<TeenAgeHealthBo> objListAdultHealthBo = new List<TeenAgeHealthBo>();
            foreach (RepeaterItem rptItem3 in rptrAdultHealth.Items)
            {
                objTeenAgeHealthBo = new TeenAgeHealthBo();
                DropDownList ddlDisease = (DropDownList)rptItem3.FindControl("ddlAdultDisease");
                RadioButtonList rdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");
                if (rdoSex.SelectedIndex >= 0 && ddlDisease.SelectedIndex>-1)
                {
                    objTeenAgeHealthBo.DiseaseId = Convert.ToInt32(ddlDisease.SelectedValue);
                    objTeenAgeHealthBo.SexId = Convert.ToInt32(rdoSex.SelectedValue);
                    objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListAdultHealthBo.Add(objTeenAgeHealthBo);
                }

            }
            objTeenAgeHealthDao.InsertAdultHealth(objListAdultHealthBo);
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
           Response.Redirect(Constant.constantApppath+"/pages/MaternalInfo.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
        private int child_count = -1;
        private int teen_count = -1;
        private int health_count = -1;
        private int adult_health_count = -1;
        protected void btnAddHealth_Click(object sender, EventArgs e)
        {
            health_count++;
            DataTable dtTeenAgeHealth = new DataTable();
            dtTeenAgeHealth.Clear();
            dtTeenAgeHealth.Columns.Add("SN");
            dtTeenAgeHealth.Columns.Add("DISEASE_ID");
            dtTeenAgeHealth.Columns.Add("SEX_ID");
            dtTeenAgeHealth.Columns.Add("TEENAGE_HEALTH_ID");

            HiddenField hidTeenAgeHealth = new HiddenField();
            DropDownList ddlDisease = new DropDownList();
            RadioButtonList rblSex = new RadioButtonList();

            foreach (RepeaterItem rptItem in RpterTeenAgeHealth.Items)
            {
                DataRow drA = dtTeenAgeHealth.NewRow();
                hidTeenAgeHealth = (HiddenField)rptItem.FindControl("hidTeenHealthId");
                ddlDisease = (DropDownList)rptItem.FindControl("ddlDisease");
                rblSex = (RadioButtonList)rptItem.FindControl("RdoSex");

                if(hidTeenAgeHealth.Value!="")
                drA["TEENAGE_HEALTH_ID"] = hidTeenAgeHealth.Value;
                drA["DISEASE_ID"] = ddlDisease.SelectedValue;
                drA["SEX_ID"] = rblSex.SelectedIndex;
                if (rblSex.SelectedIndex >= 0)
                    dtTeenAgeHealth.Rows.Add(drA);
            }
            DataRow drB = dtTeenAgeHealth.NewRow();

            drB["TEENAGE_HEALTH_ID"] = health_count;
            drB["DISEASE_ID"] = 0;
            drB["SEX_ID"] = 0;

            dtTeenAgeHealth.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            RpterTeenAgeHealth.DataSource = db;
            RpterTeenAgeHealth.DataBind();

            RpterTeenAgeHealth.DataSource = dtTeenAgeHealth;
            RpterTeenAgeHealth.DataBind();

            btnAddAnother.Enabled = true;
        }
        protected void btnAddFatility_Click(object sender, EventArgs e)
        {
            teen_count++;
            DataTable dtTeenFatility = new DataTable();
            dtTeenFatility.Clear();
            dtTeenFatility.Columns.Add("SN");
            dtTeenFatility.Columns.Add("DISEASE_ID");
            dtTeenFatility.Columns.Add("SEX_ID");
            dtTeenFatility.Columns.Add("TEENAGE_FATALITY_ID");

            HiddenField hidTeenAgeHealth = new HiddenField();
            DropDownList ddlDisease = new DropDownList();
            RadioButtonList rblSex = new RadioButtonList();

            foreach (RepeaterItem rptItem in rptrTeenFatality.Items)
            {
                DataRow drA = dtTeenFatility.NewRow();
                hidTeenAgeHealth = (HiddenField)rptItem.FindControl("hidTeenageFatalityId");
                ddlDisease = (DropDownList)rptItem.FindControl("ddlDisease");
                rblSex = (RadioButtonList)rptItem.FindControl("RdoSex");
                drA["TEENAGE_FATALITY_ID"] = hidTeenAgeHealth.Value;
                drA["DISEASE_ID"] = ddlDisease.SelectedValue;
                drA["SEX_ID"] = rblSex.SelectedIndex;
                if (rblSex.SelectedIndex >= 0)
                    dtTeenFatility.Rows.Add(drA);
            }
            DataRow drB = dtTeenFatility.NewRow();

            drB["TEENAGE_FATALITY_ID"] = teen_count;
            drB["DISEASE_ID"] = 0;
            drB["SEX_ID"] = 0;

            dtTeenFatility.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrTeenFatality.DataSource = db;
            rptrTeenFatality.DataBind();

            rptrTeenFatality.DataSource = dtTeenFatility;
            rptrTeenFatality.DataBind();

            btnAddFatility.Enabled = true;
        }
        protected void btnAddChild_Click(object sender, EventArgs e)
        {
            child_count++;
            DataTable dtChildFatility = new DataTable();
            dtChildFatility.Clear();
            dtChildFatility.Columns.Add("SN");
            dtChildFatility.Columns.Add("DEAD_REASON");
            dtChildFatility.Columns.Add("SEX_ID");
            dtChildFatility.Columns.Add("CHILD_FATALITY_ID");

            HiddenField hidTeenAgeHealth = new HiddenField();
            DropDownList ddlDisease = new DropDownList();
            RadioButtonList rblSex = new RadioButtonList();

            foreach (RepeaterItem rptItem in rptrChildFatality.Items)
            {
                DataRow drA = dtChildFatility.NewRow();
                hidTeenAgeHealth = (HiddenField)rptItem.FindControl("hidChildFatalityId");
                ddlDisease = (DropDownList)rptItem.FindControl("ddlDisease");
                rblSex = (RadioButtonList)rptItem.FindControl("RdoSex");

                drA["CHILD_FATALITY_ID"] = hidTeenAgeHealth.Value;
                drA["DEAD_REASON"] = ddlDisease.SelectedValue;
                if (rblSex.SelectedIndex > -1)
                    drA["SEX_ID"] = rblSex.SelectedIndex;
                if (rblSex.SelectedIndex >= 0)
                    dtChildFatility.Rows.Add(drA);
            }
            DataRow drB = dtChildFatility.NewRow();

            drB["CHILD_FATALITY_ID"] = child_count;
            drB["DEAD_REASON"] = 0;
            drB["SEX_ID"] = 0;

            dtChildFatility.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrChildFatality.DataSource = db;
            rptrChildFatality.DataBind();

            rptrChildFatality.DataSource = dtChildFatility;
            rptrChildFatality.DataBind();

            btnAddTeen.Enabled = true;
        }

        protected void btnAddAdultHealth_Click(object sender, EventArgs e)
        {
            adult_health_count++;
            DataTable dtAdultHealth = new DataTable();
            dtAdultHealth.Clear();
            dtAdultHealth.Columns.Add("SN");
            dtAdultHealth.Columns.Add("DISEASE_ID");
            dtAdultHealth.Columns.Add("SEX_ID");
            dtAdultHealth.Columns.Add("ADULT_HEALTH_ID");

            HiddenField hidAdultHealth = new HiddenField();
            DropDownList ddlDisease = new DropDownList();
            RadioButtonList rblSex = new RadioButtonList();

            foreach (RepeaterItem rptItem in rptrAdultHealth.Items)
            {
                DataRow drA = dtAdultHealth.NewRow();
                hidAdultHealth = (HiddenField)rptItem.FindControl("hidAdultHealthId");
                ddlDisease = (DropDownList)rptItem.FindControl("ddlAdultDisease");
                rblSex = (RadioButtonList)rptItem.FindControl("RdoSex");

                drA["ADULT_HEALTH_ID"] = hidAdultHealth.Value;
                drA["DISEASE_ID"] = ddlDisease.SelectedValue;
                if (rblSex.SelectedIndex > -1)
                    drA["SEX_ID"] = rblSex.SelectedIndex;
                if (rblSex.SelectedIndex >= 0)
                    dtAdultHealth.Rows.Add(drA);
            }
            DataRow drB = dtAdultHealth.NewRow();

            drB["ADULT_HEALTH_ID"] = adult_health_count;
            drB["DISEASE_ID"] = 0;
            drB["SEX_ID"] = 0;

            dtAdultHealth.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrAdultHealth.DataSource = db;
            rptrAdultHealth.DataBind();

            rptrAdultHealth.DataSource = dtAdultHealth;
            rptrAdultHealth.DataBind();

            btnAddTeen.Enabled = true;
        }
    }
}