﻿using Common.Entity;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class IncomeExpenditure : System.Web.UI.Page
    {
        string str = null;
        int i = 0;
        IncomeExpenditureBo objBo = new IncomeExpenditureBo();
        IncomeExpenditureDao objDal = new IncomeExpenditureDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                   // Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    btnNext.Visible = false;
                    btnUpdate.Visible = true;
                    LoadIncomeSource(OwnerId);
                    LoadAnnualExpenditure(OwnerId);
                    LoadAgroIncome(OwnerId);
                    LoadIsLoan(OwnerId);
                    LoadLoanPurpose(OwnerId);
                }


            }
            else
            {
                btnNext.Visible = true;
                btnUpdate.Visible = false;
            }

            if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
            {
               Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
            }

        }

        private void LoadLoanPurpose(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchLoanPurpose(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidLoanDetailsId.Value = dt.Rows[0]["LOAN_DETAIL_ID"].ToString();
                if (Convert.ToInt32(dt.Rows[0]["LOAN_DONOR_TYPE_ID"].ToString())>-1)
                rbdInstitution.SelectedValue = dt.Rows[0]["LOAN_DONOR_TYPE_ID"].ToString();
               //int[] ab= dt.Rows[0]["LOAN_PURPOSE_TYPE_ID"].GetHashCode();
                string a = Convert.ToString(dt.Rows[0]["LOAN_PURPOSE_TYPE_ID"].ToString());

                if (a != null && a != "")
                {
                    string[] array = a.Split(',');



                    foreach (var str in array)
                    {
                        // if (Convert.ToInt32(array[i]) == 0) 
                        int i = Convert.ToInt32(str);
                        chkLoanUse.Items[i].Selected = true;

                    }
                }
                
                

            }
        }

        private void LoadIsLoan(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchIsLoan(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidLoanId.Value = dt.Rows[0]["LOAN_ID"].ToString();

                if (Convert.ToInt32(dt.Rows[0]["IS_LOAN"].ToString())>-1)
                rbdLoan.SelectedValue = dt.Rows[0]["IS_LOAN"].ToString();
            } 
        }

        private void LoadAgroIncome(int OwnerId)
        {

            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchAgroIncome(objBo);
            if(dt!=null && dt.Rows.Count>0)
            {
                hidAgroIncomeId.Value = dt.Rows[0]["ARGO_INCOME_ID"].ToString();
                if (Convert.ToInt32(dt.Rows[0]["INCOME_COVER_TIME_ID"].ToString())>-1)
                rbdAgriTime.SelectedValue = dt.Rows[0]["INCOME_COVER_TIME_ID"].ToString();
            }
        }

        private void LoadAnnualExpenditure(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchAnnualExpenditure(objBo);
            if(dt!=null && dt.Rows.Count>0)
            {
                txtFooding.Text = dt.Rows[0]["FOODING"].ToString();
                txtClothing.Text = dt.Rows[0]["CLOTHING"].ToString();
                txtEducation.Text = dt.Rows[0]["EDUCATION"].ToString();
                txtHealth.Text = dt.Rows[0]["HEALTH"].ToString();
                txtFestivity.Text = dt.Rows[0]["FESTIVITY"].ToString();
                txtPaymentCharge.Text = dt.Rows[0]["PAYMENT_CHARGE"].ToString();
                txtOthersExpenditure.Text = dt.Rows[0]["OTHERS"].ToString();
                hidAnnualExpenditureId.Value = dt.Rows[0]["ANNUAL_EXPENDITURE_ID"].ToString();
                

            }
        }

        private void LoadIncomeSource(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchIncomeSource(objBo);
            if(dt!=null  && dt.Rows.Count>0)
            {
                txtAgriculture.Text = dt.Rows[0]["AGRICULTURE"].ToString();
                txtBusiness.Text = dt.Rows[0]["BUSINESS"].ToString();
                txtJobs.Text = dt.Rows[0]["JOBS"].ToString();
                txtForeignEmployment.Text = dt.Rows[0]["FOREIGN_EMPLOYMENT"].ToString();
                txtWageLabour.Text = dt.Rows[0]["WAGE_LABOUR"].ToString();
                txtHouseRent.Text = dt.Rows[0]["HOUSE_RENT"].ToString();
                txtOthersIncome.Text = dt.Rows[0]["OTHERS"].ToString();
                hidIncomeSourceId.Value = dt.Rows[0]["INCOME_SOURCE_ID"].ToString();
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            //saving annual income
           
                //Session["Owner_ID"] = 99;
                objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                objBo.Agriculture = Convert.ToInt32(txtAgriculture.Text);
                objBo.Business = Convert.ToInt32(txtBusiness.Text);
                objBo.Jobs = Convert.ToInt32(txtJobs.Text);
                objBo.ForeignEmployment = Convert.ToInt32(txtForeignEmployment.Text);
                objBo.WageLabour = Convert.ToInt32(txtWageLabour.Text);
                objBo.HouseRent = Convert.ToInt32(txtHouseRent.Text);
                objBo.OthersIncome = Convert.ToInt32(txtOthersIncome.Text);

                int i = objDal.SaveAnnualIncome(objBo);
            //Annual Expenditure
           
               // Session["Owner_ID"] = 99;
                objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                objBo.Fooding = Convert.ToInt32(txtFooding.Text);
                objBo.Clothing = Convert.ToInt32(txtClothing.Text);
                objBo.Education = Convert.ToInt32(txtEducation.Text);
                objBo.Health = Convert.ToInt32(txtHealth.Text);
                objBo.Festivity = Convert.ToInt32(txtFestivity.Text);
                objBo.PaymentCharge = Convert.ToInt32(txtPaymentCharge.Text);
                objBo.OthersExpenditure = Convert.ToInt32(txtOthersExpenditure.Text);

                int result = objDal.SaveAnnualExpenditure(objBo);          


            //Agro Income
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            if (rbdAgriTime.SelectedIndex > -1)
                objBo.IncomeCoverTimeId = Convert.ToInt32(rbdAgriTime.SelectedValue);
            else objBo.IncomeCoverTimeId = -1;
            objDal.SaveAgroIncome(objBo);

            //Is Loan Taken
            //objBo.OwnerId = Session["Owner_Id"].GetHashCode();
            if (rbdLoan.SelectedIndex > -1)
                objBo.IsLoan = Convert.ToInt32(rbdLoan.SelectedValue);
            else objBo.IsLoan = -1;
            objDal.SaveIsLoanInfo(objBo);

            ////Loan Donor
            //objBo.OwnerId = Session["Owner_Id"].GetHashCode();
            if (rbdInstitution.SelectedIndex > -1)
                objBo.LoanDonorTypeId = Convert.ToInt32(rbdInstitution.SelectedValue);
            else objBo.LoanDonorTypeId = -1;

            for (i = 0; i < chkLoanUse.Items.Count; i++)
            {
                if (chkLoanUse.Items[i].Selected)
                {
                    if (str == null)
                    {
                        str = chkLoanUse.Items[i].Value;
                    }
                    else
                        str +=","+chkLoanUse.Items[i].Value;


                }
            }
            
            objBo.LoanPurposeTypeId = str;
            objDal.SaveLoanDonor(objBo);
            
               Response.Redirect(Constant.constantApppath+"/pages/AgroProducts.aspx");
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //Session["Owner_ID"] = 99;
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.Fooding = Convert.ToInt32(txtFooding.Text);
            objBo.Clothing = Convert.ToInt32(txtClothing.Text);
            objBo.Education = Convert.ToInt32(txtEducation.Text);
            objBo.Health = Convert.ToInt32(txtHealth.Text);
            objBo.Festivity = Convert.ToInt32(txtFestivity.Text);
            objBo.PaymentCharge = Convert.ToInt32(txtPaymentCharge.Text);
            objBo.OthersExpenditure = Convert.ToInt32(txtOthersExpenditure.Text);
            objBo.AnnualExpenditureId =Convert.ToInt32(hidAnnualExpenditureId.Value);

            int result = objDal.UpdateAnnualExpenditure(objBo);

            objBo.AgroIncomeId = Convert.ToInt32(hidAgroIncomeId.Value);
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            if (rbdAgriTime.SelectedIndex > -1)
                objBo.IncomeCoverTimeId = Convert.ToInt32(rbdAgriTime.SelectedValue);
            else objBo.IncomeCoverTimeId = -1;
           
            objDal.UpdateAgroIncome(objBo);


            //Session["Owner_ID"] = 99;
            objBo.OwnerId = Convert.ToInt32(Session["Owner_ID"]);
            objBo.Agriculture = Convert.ToInt32(txtAgriculture.Text);
            objBo.Business = Convert.ToInt32(txtBusiness.Text);
            objBo.Jobs = Convert.ToInt32(txtJobs.Text);
            objBo.ForeignEmployment = Convert.ToInt32(txtForeignEmployment.Text);
            objBo.WageLabour = Convert.ToInt32(txtWageLabour.Text);
            objBo.HouseRent = Convert.ToInt32(txtHouseRent.Text);
            objBo.OthersIncome = Convert.ToInt32(txtOthersIncome.Text);
            objBo.IncomeSourceId = Convert.ToInt32(hidIncomeSourceId.Value);

            int i = objDal.UpdateAnnualIncome(objBo);

            //Is Loan Taken
            objBo.OwnerId = Convert.ToInt32(Session["Owner_ID"]);
            objBo.LoanId = Convert.ToInt32(hidLoanId.Value);
            if (rbdLoan.SelectedIndex > -1)
                objBo.IsLoan = Convert.ToInt32(rbdLoan.SelectedValue);
            else objBo.IsLoan = -1;          

            objDal.UpdateIsLoanInfo(objBo);

            objBo.LoanDetailsId = Convert.ToInt32(hidLoanDetailsId.Value);
            objBo.OwnerId = Convert.ToInt32(Session["Owner_ID"]);
            if (rbdInstitution.SelectedIndex > -1)
                objBo.LoanDonorTypeId = Convert.ToInt32(rbdInstitution.SelectedValue);
            else objBo.LoanDonorTypeId = -1;
            for (i = 0; i < chkLoanUse.Items.Count; i++)
            {
                if (chkLoanUse.Items[i].Selected)
                {
                    if (str == null)
                    {
                        str = chkLoanUse.Items[i].Value;
                    }
                    else
                        str += "," + chkLoanUse.Items[i].Value;


                }
            }
            

            objBo.LoanPurposeTypeId = str;
            objDal.UpdateLoanDonor(objBo);
           Response.Redirect(Constant.constantApppath+"/pages/AgroProducts.aspx?Owner_Id="+Session["Owner_Id"]);

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
           Response.Redirect(Constant.constantApppath+"/pages/MemberInfo.aspx?Owner_Id="+Session["Owner_Id"]);
        }

        protected void rbdLoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbdLoan.SelectedIndex == 0)
            {
                rbdInstitution.Enabled = true;
            }
            else
            {
                rbdInstitution.Enabled = false;
                rbdInstitution.SelectedIndex = -1;
            }
        }


    }
}