﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer.Pages;

namespace ProfileForm.Pages
{
    public partial class Info : System.Web.UI.Page
    {
        InfoDao objInfoDao = new InfoDao();
        InfoBo objInfoBo = new InfoBo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateVDC();
                PopulateToleBasti(objInfoBo.VdcId,objInfoBo.WardNum);
                if (Request.QueryString["Info_Id"] != null)
                {
                    Session["Info_Id"] = Request.QueryString["Info_Id"];
                    int Info_Id = Convert.ToInt32(Session["Info_Id"].ToString());

                    BindData(Info_Id);
                    btnNext.Enabled = false;
                    btnNext.Visible = false;
                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;
                    btnUpdateBasti.Enabled = true;
                    btnUpdateBasti.Visible = true;
                    btnBasti.Enabled = false;
                    btnBasti.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnUpdateBasti.Visible = false;
                }
                if (Session["UserId"] == null || Convert.ToInt32(Session["UserId"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/Login.aspx");
                }
                if (Request.QueryString["infoId"] != null)
                {
                    int info_id = Convert.ToInt32(Request.QueryString["infoId"]);
                    BindData(info_id);
                    btnUpdateBasti.Visible = true;
                    btnBasti.Visible = false;
                    btnUpdate.Visible = true;
                    btnNext.Visible = false;
                }

            }
        }



        private void BindData(int Info_Id)
        {
            objInfoBo.VillageId = Info_Id;
            DataTable dt = new DataTable();
            dt = objInfoDao.FetchVillageInfo(objInfoBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                int vdcId = Convert.ToInt32(dt.Rows[0]["VDC_ID"]);
                int wardId = Convert.ToInt32(dt.Rows[0]["WARD_ID"]);

                ddlVDC.SelectedValue = dt.Rows[0]["VDC_ID"].ToString();
                ddlWoda.SelectedValue = dt.Rows[0]["WARD_NUM"].ToString();
                PopulateToleBasti(vdcId,wardId);
                ddlToleBastiName.SelectedValue = dt.Rows[0]["BASTI_ID"].ToString();
                txtToleBasti.Text = dt.Rows[0]["VILLAGE_NAME"].ToString();
                txtHouseNo.Text = dt.Rows[0]["HOUSE_NO"].ToString();
                txtFamilySn.Text = dt.Rows[0]["FAMILY_SN"].ToString();

            }
        }

        private void PopulateVDC()
        {
            DataTable dt = objInfoDao.PopulateMyagdiVdc();

            ddlVDC.DataSource = dt;
            ddlVDC.DataTextField = "VDC_NAME_NEP";
            ddlVDC.DataValueField = "VDC_ID";
            ddlVDC.DataBind();
            ddlVDC.Items.Insert(0, "-गाविस छान्नुहोस्-");
        }
        private void PopulateToleBasti(int vdcId, int wardId)
        {
            DataTable dt = objInfoDao.PopulateToleBasti(vdcId,wardId);
            ddlToleBastiName.DataSource = dt;
            ddlToleBastiName.DataTextField = "VILLAGE_NAME";
            ddlToleBastiName.DataValueField = "BASTI_ID";
            ddlToleBastiName.DataBind();
            ddlToleBastiName.Items.Insert(0, "Select Tole absti");
        }


        int vdc_code = 0;
        protected void btnNext_Click(object sender, EventArgs e)
        {
            objInfoBo = new InfoBo();

            objInfoBo.VdcId = int.Parse(ddlVDC.SelectedValue);
            objInfoBo.WardNum = int.Parse(ddlWoda.SelectedValue);
            for (int j = 0; j <= objInfoBo.WardNum; j++)
            {
                vdc_code = objInfoBo.WardNum * 100;
            }
            objInfoBo.HouseNo = vdc_code + txtHouseNo.Text;
            objInfoBo.FamilySn = txtFamilySn.Text;
            objInfoBo.BastiId = Convert.ToInt32(ddlToleBastiName.SelectedValue);
            // objInfoBo.VillageName = txtToleBasti.Text;
            int i = objInfoDao.InsertVillageInfo(objInfoBo); //i मा InfoId आउँछ  

            if (i > 0)
            {
                Session["InfoId"] = i;
                Session["WardNo"] = ddlWoda.SelectedValue;
                Session["GharNo"] = txtHouseNo.Text;
                Session["VDC"] = ddlVDC.SelectedValue;
               Response.Redirect(Constant.constantApppath+"/pages/Section1Introduction.aspx?infoId=" + Session["InfoId"].ToString());
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            objInfoBo = new InfoBo();
            objInfoBo.VillageId = Convert.ToInt32(Session["Info_Id"]);
            if (Request.QueryString["infoId"] != null)
            {
                objInfoBo.VillageId = Convert.ToInt32(Session["infoId"]);
            }


            objInfoBo.VdcId = int.Parse(ddlVDC.SelectedValue);
            objInfoBo.WardNum = int.Parse(ddlWoda.SelectedValue);
            for (int j = 0; j <= objInfoBo.WardNum; j++)
            {
                vdc_code = objInfoBo.WardNum * 100;
            }
            objInfoBo.HouseNo = txtHouseNo.Text;
            objInfoBo.FamilySn = txtFamilySn.Text;
            objInfoBo.BastiId = Convert.ToInt32(ddlToleBastiName.SelectedValue);
            int i = objInfoDao.UpdateVillageInfo(objInfoBo); //i मा InfoId आउँछ  
            if (i > 0)
            {
                /*Session["InfoId"] = i;
                Session["WardNo"] = ddlWoda.SelectedValue;
                Session["GharNo"] = txtHouseNo.Text;
                Session["VDC"] = ddlVDC.SelectedValue;*/
                if (Request.QueryString["infoId"] != null)
                {
                    string info_id = Request.QueryString["infoId"];
                   Response.Redirect(Constant.constantApppath+"/pages/Section1Introduction.aspx?infoId=" + info_id);

                }
                Response.Redirect(Constant.constantApppath+"/pages/Section1Introduction.aspx?Info_ID=" + Session["Info_Id"].ToString());
            }

        }

        protected void ddlWARD_SelectedIndexChanged(object sender, EventArgs e)
        {
            int vdcId = Convert.ToInt32(ddlVDC.SelectedValue);
            int wardID = Convert.ToInt32(ddlWoda.SelectedValue);
            PopulateToleBasti(vdcId,wardID);
        }

        protected void btnBasti_Click(object sender, EventArgs e)
        {
            objInfoBo.VillageName = txtToleBasti.Text;

            objInfoBo.VdcId = Convert.ToInt32(ddlVDC.SelectedValue);
            objInfoBo.WardNum = Convert.ToInt32(ddlWoda.SelectedValue);
            if (txtToleBasti.Text != null)
            {
                objInfoDao.InsertBasti(objInfoBo);
            }
            PopulateToleBasti(objInfoBo.VdcId,objInfoBo.WardNum);
        }

        protected void btnUpdateBasti_Click(object sender, EventArgs e)
        {
            objInfoBo = new InfoBo();
            int basti_id = Convert.ToInt32(ddlToleBastiName.SelectedValue);
            objInfoBo.BastiId = Convert.ToInt32(ddlToleBastiName.SelectedValue);
            objInfoBo.VillageName = txtToleBasti.Text;
            objInfoDao.UpdateBasti(objInfoBo);
            int vdc_id = Convert.ToInt32(ddlVDC.SelectedValue);
            int wardId = Convert.ToInt32(ddlWoda.SelectedValue);
            PopulateToleBasti(vdc_id,wardId);
            ddlToleBastiName.SelectedValue = basti_id.ToString();
        }



    }
}