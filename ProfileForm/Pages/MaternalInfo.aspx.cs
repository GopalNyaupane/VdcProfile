﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;

namespace ProfileForm.Pages
{
    public partial class MaternalInfo : System.Web.UI.Page
    {
        MaternalInfoBo objMtrlInfo = null;
        MaternalInfoDao objMaternl = new MaternalInfoDao();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Request.QueryString["Owner_Id"] != null)
                {

                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    BindRepeaterPregenentWomen(ownerId);
                    BindChildHivInf(ownerId);

                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;

                }

                if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
                }
            }
        }

        private void BindChildHivInf(int ownerId)
        {
            objMtrlInfo = new MaternalInfoBo();
            objMtrlInfo.OwnerID = ownerId;
            DataTable dt = new DataTable();
            dt = objMaternl.FetchChildHivInfo(objMtrlInfo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidHiv.Value = dt.Rows[0]["CHILD_HIV_INFECTION_ID"].ToString();
                if (Convert.ToInt32(dt.Rows[0]["HIV_INFECTION"].ToString()) > -1)
                    rdoHIV.SelectedValue = dt.Rows[0]["HIV_INFECTION"].ToString();
                if (Convert.ToInt32(dt.Rows[0]["ARV"].ToString()) > -1)
                    RadioButtonList1.SelectedValue = dt.Rows[0]["ARV"].ToString();
            }

        }

        private void BindRepeaterPregenentWomen(int ownerId)
        {
            objMtrlInfo = new MaternalInfoBo();
            objMtrlInfo.OwnerID = ownerId;
            DataTable dt = new DataTable();
            dt = objMaternl.FetchMaterialInfo(objMtrlInfo);

            RpterPregnentWomen.DataSource = null;
            RpterPregnentWomen.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                RpterPregnentWomen.DataSource = dt;
                RpterPregnentWomen.DataBind();
            }


        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            //Session["Owner_ID"] = "99";
            //            List<MaternalInfoBo> objListMtrlInfo = new List<MaternalInfoBo>();

            InsertPregnentWomens();
            objMtrlInfo = new MaternalInfoBo();
            objMtrlInfo.OwnerID = Convert.ToInt32(Session["Owner_Id"]);
            if (rdoHIV.SelectedIndex > -1)
                objMtrlInfo.HivInfection = Convert.ToInt32(rdoHIV.SelectedValue);
            else objMtrlInfo.HivInfection = -1;

            if (RadioButtonList1.SelectedIndex > -1)
                objMtrlInfo.Arv = Convert.ToInt32(RadioButtonList1.SelectedValue);
            else objMtrlInfo.Arv = -1;

            objMaternl.InsertMaternalHiv(objMtrlInfo);
           Response.Redirect(Constant.constantApppath+"/pages/TeenAgeHealth.aspx");


        }
        private void InsertPregnentWomens()
        {
            foreach (RepeaterItem rptr in RpterPregnentWomen.Items)
            {
                objMtrlInfo = new MaternalInfoBo();

                TextBox txtPregnancyTest = (TextBox)rptr.FindControl("txtPregnancyTest");
                TextBox txtIronTablet = (TextBox)rptr.FindControl("txtIronTablet");
                TextBox txtTTVaccine = (TextBox)rptr.FindControl("txtTTVaccine");
                TextBox txtInfantHealthCheck = (TextBox)rptr.FindControl("txtInfantHealthCheck");
                DropDownList ddlMaternityAddress = (DropDownList)rptr.FindControl("ddlMaternityAddress");
                RadioButtonList RdoCondition = (RadioButtonList)rptr.FindControl("RdoCondition");
                if (txtPregnancyTest.Text != "")
                {
                    objMtrlInfo.OwnerID = Convert.ToInt32(Session["Owner_Id"]);
                    objMtrlInfo.PregTest = Convert.ToInt32(txtPregnancyTest.Text);
                    objMtrlInfo.IronTablet = Convert.ToInt32(txtIronTablet.Text);
                    objMtrlInfo.TtVaccine = Convert.ToInt32(txtTTVaccine.Text);
                    if (RdoCondition.SelectedIndex > -1)
                        objMtrlInfo.Maternity = Convert.ToInt32(RdoCondition.SelectedValue);
                    else objMtrlInfo.Maternity = -1;

                    objMtrlInfo.MaternalAddressId = Convert.ToInt32(ddlMaternityAddress.SelectedValue);
                    objMtrlInfo.InfantHlthChk = Convert.ToInt32(txtInfantHealthCheck.Text);

                }
                objMaternl.InsertMaternalInfo(objMtrlInfo);
            }

        }
        private int count = -1;
        //private void BindMaternal(DataTable dtMaternal, int membercount)
        //{
        //    DataTable dtAddPregnencyInfo = new DataTable();
        //    dtAddPregnencyInfo.Clear();
        //    dtAddPregnencyInfo.Columns.Add("SN");
        //    dtAddPregnencyInfo.Columns.Add("PREGNANCY_TEST");
        //    dtAddPregnencyInfo.Columns.Add("IRON_TABLET");
        //    dtAddPregnencyInfo.Columns.Add("TT_VACCINE");
        //    dtAddPregnencyInfo.Columns.Add("MATERNITY");
        //    dtAddPregnencyInfo.Columns.Add("MATERNAL_ADDRESS_ID");
        //    dtAddPregnencyInfo.Columns.Add("INFANT_HEALTH_CHECK");
        //    dtAddPregnencyInfo.Columns.Add("MATERNAL_HEALTH_ID");

        //    int j = 0;
        //    if (dtMaternal.Rows.Count == 0)
        //    {

        //        for (int i = 0; i < membercount; i++)
        //        {
        //            DataRow _dtRow = dtAddPregnencyInfo.NewRow();
        //            _dtRow["SN"] = i + 1;
        //            _dtRow["PREGNANCY_TEST"] = 0;
        //            _dtRow["IRON_TABLET"] = 0;
        //            _dtRow["TT_VACCINE"] = 0;
        //            _dtRow["MATERNITY"] = 0;
        //            _dtRow["MATERNAL_ADDRESS_ID"] = 0;
        //            _dtRow["INFANT_HEALTH_CHECK"] = 0;
        //            _dtRow["MATERNAL_HEALTH_ID"] = 0;


        //            dtAddPregnencyInfo.Rows.Add(_dtRow);
        //        }

        //    }
        //    else
        //    {
        //        for (int i = 0; i < dtMaternal.Rows.Count; i++)
        //        {
        //            DataRow _dtRow = dtAddPregnencyInfo.NewRow();
        //            /*_dtRow["SN"] = i + 1;
        //            _dtRow["Name"] = dtAbroad.Rows[i]["away_name"].ToString();
        //            _dtRow["Age"] = dtAbroad.Rows[i]["away_age"].ToString();
        //            _dtRow["Relation"] = dtAbroad.Rows[i]["away_reason"].ToString();
        //            _dtRow["Sex"] = dtAbroad.Rows[i]["away_gender"].ToString();
        //           */

        //            dtAddPregnencyInfo.Rows.Add(_dtRow);
        //        }
        //    }
        //    RpterPregnentWomen.DataSource = dtAddPregnencyInfo;
        //    RpterPregnentWomen.DataBind();
        //}

        protected void btnAddAnother_OnClick(object sender, EventArgs e)
        {
            count++;
            DataTable dtAddPregnencyInfo = new DataTable();
            dtAddPregnencyInfo.Clear();
            dtAddPregnencyInfo.Columns.Add("SN");
            dtAddPregnencyInfo.Columns.Add("PREGNANCY_TEST");
            dtAddPregnencyInfo.Columns.Add("IRON_TABLET");
            dtAddPregnencyInfo.Columns.Add("TT_VACCINE");
            dtAddPregnencyInfo.Columns.Add("MATERNITY");
            dtAddPregnencyInfo.Columns.Add("MATERNAL_ADDRESS_ID");
            dtAddPregnencyInfo.Columns.Add("INFANT_HEALTH_CHECK");
            dtAddPregnencyInfo.Columns.Add("MATERNAL_HEALTH_ID");

            HiddenField hidHealth = new HiddenField();
            TextBox txtPTest = new TextBox();
            TextBox txtTablet = new TextBox();
            TextBox txtTT = new TextBox();
            RadioButtonList rblMat = new RadioButtonList();
            DropDownList ddlAdd = new DropDownList();
            TextBox txtInfant = new TextBox();

            foreach (RepeaterItem rptItem in RpterPregnentWomen.Items)
            {
                DataRow drA = dtAddPregnencyInfo.NewRow();
                hidHealth = (HiddenField)rptItem.FindControl("fieldId");
                txtPTest = (TextBox)rptItem.FindControl("txtPregnancyTest");
                txtTablet = (TextBox)rptItem.FindControl("txtIronTablet");
                txtTT = (TextBox)rptItem.FindControl("txtTTVaccine");
                rblMat = (RadioButtonList)rptItem.FindControl("RdoCondition");
                ddlAdd = (DropDownList)rptItem.FindControl("ddlMaternityAddress");
                txtInfant = (TextBox)rptItem.FindControl("txtInfantHealthCheck");

                drA["MATERNAL_HEALTH_ID"] = hidHealth.Value;
                drA["PREGNANCY_TEST"] = txtPTest.Text;
                drA["IRON_TABLET"] = txtTablet.Text;
                drA["TT_VACCINE"] = txtTT.Text;
                drA["INFANT_HEALTH_CHECK"] = txtInfant.Text;
                drA["MATERNAL_ADDRESS_ID"] = ddlAdd.SelectedValue;
                drA["MATERNITY"] = rblMat.SelectedIndex;

                if (rblMat.SelectedIndex >= 0)

                    dtAddPregnencyInfo.Rows.Add(drA);
            }
            DataRow drB = dtAddPregnencyInfo.NewRow();

            drB["MATERNAL_HEALTH_ID"] = count;
            drB["PREGNANCY_TEST"] = 0;
            drB["IRON_TABLET"] = 0;
            drB["TT_VACCINE"] = 0;
            drB["INFANT_HEALTH_CHECK"] = 0;
            drB["MATERNAL_ADDRESS_ID"] = 0;
            drB["MATERNITY"] = 0;

            dtAddPregnencyInfo.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            RpterPregnentWomen.DataSource = db;
            RpterPregnentWomen.DataBind();

            RpterPregnentWomen.DataSource = dtAddPregnencyInfo;
            RpterPregnentWomen.DataBind();


        }
        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objMaternl.DeleteOldRecord(OwnerId);
            InsertPregnentWomens();
            objMtrlInfo = new MaternalInfoBo();
            objMtrlInfo.OwnerID = OwnerId;
            objMtrlInfo.hiddenId = Convert.ToInt32(hidHiv.Value);
            if (rdoHIV.SelectedIndex > -1)
                objMtrlInfo.HivInfection = Convert.ToInt32(rdoHIV.SelectedValue);
            else objMtrlInfo.HivInfection = -1;
            if (RadioButtonList1.SelectedIndex > -1)
            {
                objMtrlInfo.Arv = Convert.ToInt32(RadioButtonList1.SelectedValue);
            }
            else
            {
                objMtrlInfo.Arv = -1;
            }


            objMaternl.UpdateMaternalHiv(objMtrlInfo);
           Response.Redirect(Constant.constantApppath+"/pages/TeenAgeHealth.aspx?Owner_Id=" + Session["Owner_Id"]);

        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
           Response.Redirect(Constant.constantApppath+"/pages/ChildInfo.aspx?Owner_Id=" + Session["Owner_Id"]);
        }

        protected void rdoHIV_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (rdoHIV.SelectedValue == "1")
            {
                RadioButtonList1.SelectedIndex = -1;
                RadioButtonList1.Enabled = false;
            }
            else RadioButtonList1.Enabled = true;
        }

    
    }
}