﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class ChildNutrition : System.Web.UI.Page
    {
        ChildNutritionBo objBo = new ChildNutritionBo();
        ChildNutritionDao objDao = new ChildNutritionDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                rptrChildWeight.DataBind();
                if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadWeightDetails(Owner_Id);
                    LoadNutritionDetails(Owner_Id);

                    btnUpdate.Visible = true;
                    btnNext.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                    btnAddAnother.Enabled = false;
                }

                if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
                }
            }
            if(rblChildWeight.SelectedIndex==1)
            btnAddAnother.Enabled = false;
        }

      
        public void LoadNutritionDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataSet ds = new DataSet();
            ds = objDao.Fetch_Child_Birth_Nutrition(objBo);
            DataTable dtNutrition = null;
            DataTable dtBC = null;
           
            if (ds != null && ds.Tables.Count > 0)
            {
                dtNutrition = ds.Tables[0];
                dtBC = ds.Tables[1];
                if (dtBC != null && dtBC.Rows.Count > 0)
                {
                   
                    hidBirth.Value = dtBC.Rows[0]["BIRTH_CERTIFICATE_ID"].ToString();
                    hidBirth1.Value = dtBC.Rows[1]["BIRTH_CERTIFICATE_ID"].ToString();
                    hidBirth2.Value = dtBC.Rows[2]["BIRTH_CERTIFICATE_ID"].ToString();
                    hidBirth3.Value = dtBC.Rows[3]["BIRTH_CERTIFICATE_ID"].ToString();
                    hidBirth4.Value = dtBC.Rows[4]["BIRTH_CERTIFICATE_ID"].ToString();
                    hidBirth5.Value = dtBC.Rows[5]["BIRTH_CERTIFICATE_ID"].ToString();

                    txtBoysCertificateUnder5.Text = dtBC.Rows[0]["BIRTH_CERTIFICATE"].ToString();
                    txtBoysNoCertificateUnder5.Text = dtBC.Rows[0]["NO_BIRTH_CERTIFICATE"].ToString();
                    txtGirlsCertificateUnder5.Text = dtBC.Rows[1]["BIRTH_CERTIFICATE"].ToString();
                    txtGirlsNoCertificateUnder5.Text = dtBC.Rows[1]["NO_BIRTH_CERTIFICATE"].ToString();
                    txtOtherCertificateUnder5.Text = dtBC.Rows[2]["BIRTH_CERTIFICATE"].ToString();
                    txtOtherNoCertificateUnder5.Text = dtBC.Rows[2]["NO_BIRTH_CERTIFICATE"].ToString();
                    txtBoysCertificate.Text = dtBC.Rows[3]["BIRTH_CERTIFICATE"].ToString();
                    txtBoysNoCertificate.Text = dtBC.Rows[3]["NO_BIRTH_CERTIFICATE"].ToString();
                    txtGirlsCertificate.Text = dtBC.Rows[4]["BIRTH_CERTIFICATE"].ToString();
                    txtGirlsNoCertificate.Text = dtBC.Rows[4]["NO_BIRTH_CERTIFICATE"].ToString();
                    txtOtherCertificate.Text = dtBC.Rows[5]["BIRTH_CERTIFICATE"].ToString();
                    txtOtherNoCertificate.Text = dtBC.Rows[5]["NO_BIRTH_CERTIFICATE"].ToString();
                }
                if (dtNutrition != null && dtNutrition.Rows.Count > 0)
                {
                    hidNutrition.Value = dtNutrition.Rows[0]["CHILD_NUTRITION_ID"].ToString();
                    if(Convert.ToInt32(dtNutrition.Rows[0]["IO_SALT"].ToString())>-1)
                    rblSalt.SelectedValue = dtNutrition.Rows[0]["IO_SALT"].ToString();

                    if(Convert.ToInt32(dtNutrition.Rows[0]["NUTRITIOUS_FOOD"].ToString())>-1)
                    rblNutrition.SelectedValue = dtNutrition.Rows[0]["NUTRITIOUS_FOOD"].ToString();
                    if(Convert.ToInt32(dtNutrition.Rows[0]["CHILD_WEIGHT"].ToString())>-1)
                    rblChildWeight.SelectedValue = dtNutrition.Rows[0]["CHILD_WEIGHT"].ToString();
                }
            }
            else
            {
                dtNutrition = null;
                dtBC = null;               
              
            }



        }

        private void LoadWeightDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_Child_Weight(objBo);

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrChildWeight.DataSource = dt;
                rptrChildWeight.DataBind();
                rblChildWeight.SelectedValue ="0";
                btnAddAnother.Enabled = true;

            }
        }

        private int count = -1;
        protected void btnAddAnother_OnClick(object sender, EventArgs e)
        {
            count++;
            DataTable dtChildWeight = new DataTable();
            dtChildWeight.Clear();
            dtChildWeight.Columns.Add("CHILD_WEIGHT_ID");
            dtChildWeight.Columns.Add("CHILD_AGE");
            dtChildWeight.Columns.Add("CHILD_WEIGHT");
            dtChildWeight.Columns.Add("MALNUTRITION_ID");

            HiddenField hidWeight = new HiddenField();
            TextBox txtAge = new TextBox();
            TextBox txtWeight = new TextBox();
            RadioButtonList rblMal = new RadioButtonList();
           
            foreach (RepeaterItem rptItem in rptrChildWeight.Items)
            {
                DataRow drA = dtChildWeight.NewRow();
                hidWeight = (HiddenField)rptItem.FindControl("hidWeight");
                txtAge = (TextBox)rptItem.FindControl("txtChildAge");
                txtWeight = (TextBox)rptItem.FindControl("txtChildWeight");
                rblMal = (RadioButtonList)rptItem.FindControl("rdlMalNutrition");


                drA["CHILD_WEIGHT_ID"] = hidWeight.Value;
                drA["CHILD_AGE"] = txtAge.Text;
                drA["CHILD_WEIGHT"] = txtWeight.Text;
                drA["MALNUTRITION_ID"] = rblMal.SelectedIndex;

                if (txtAge.Text != "" && txtWeight.Text != "" && rblMal.SelectedIndex>=0)

                dtChildWeight.Rows.Add(drA);
            }
            DataRow drB = dtChildWeight.NewRow();

            drB["CHILD_WEIGHT_ID"] = count;
            drB["CHILD_AGE"] = 0;
            drB["CHILD_WEIGHT"] = 0;
            drB["MALNUTRITION_ID"] = 0;
           
            dtChildWeight.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrChildWeight.DataSource = db;
            rptrChildWeight.DataBind();

            rptrChildWeight.DataSource = dtChildWeight;
            rptrChildWeight.DataBind();

            btnAddAnother.Enabled = true;
        }

        protected void rbl_SIC(object sender, EventArgs e)
        {
            rptrChildWeight.DataSource = null;
            rptrChildWeight.DataBind();
            if (rblChildWeight.SelectedItem.Value == "0")
            {
                
                btnAddAnother.Enabled = true;

            }
        }


        protected void btnNext_Click(object sender,EventArgs e)
        {
            InsertChildWeight();
           
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            if (rblSalt.SelectedIndex > -1)
            {
                objBo.salt = Convert.ToInt32(rblSalt.SelectedValue);
            }
            else
                objBo.salt = -1;
            if (rblNutrition.SelectedIndex > -1)
            {
                objBo.nutritionFood = Convert.ToInt32(rblNutrition.SelectedValue);
            }
            else objBo.nutritionFood = -1;

            if (rblChildWeight.SelectedIndex > -1)
            {
                objBo.childWeightInfo = Convert.ToInt32(rblChildWeight.SelectedValue);
            }
            else objBo.childWeightInfo = -1;

                int cni = objDao.Child_Nutrition_Insert(objBo);
            

            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.ageGroup = 0;
            objBo.birthCertificate = Convert.ToInt32(txtBoysCertificateUnder5.Text);
            objBo.noBirthCertificate = Convert.ToInt32(txtBoysNoCertificateUnder5.Text);
            objBo.sex = 0;
            
           int boy = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 0;
            objBo.birthCertificate = Convert.ToInt32(txtGirlsCertificateUnder5.Text);
            objBo.noBirthCertificate = Convert.ToInt32(txtGirlsNoCertificateUnder5.Text);
            objBo.sex = 1;          
           
            int girl = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 0;
            objBo.noBirthCertificate = Convert.ToInt32(txtOtherNoCertificateUnder5.Text);
            objBo.birthCertificate = Convert.ToInt32(txtOtherCertificateUnder5.Text);
            objBo.sex = 2;
           
            int other = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 1;
            objBo.birthCertificate = Convert.ToInt32(txtBoysCertificate.Text);
            objBo.noBirthCertificate = Convert.ToInt32(txtBoysNoCertificate.Text);
            objBo.sex = 0;

           
            int boy1 = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 1;
            objBo.birthCertificate = Convert.ToInt32(txtGirlsCertificate.Text);
            objBo.noBirthCertificate = Convert.ToInt32(txtGirlsNoCertificate.Text);
            objBo.sex = 1;

            
            int girl1 = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 1;
            objBo.noBirthCertificate = Convert.ToInt32(txtOtherNoCertificate.Text);
            objBo.birthCertificate = Convert.ToInt32(txtOtherCertificate.Text);
            objBo.sex = 2;
           
            int other1 = objDao.Birth_Certificate_Insert(objBo);            

               Response.Redirect(Constant.constantApppath+"/pages/Marriage.aspx?OwnerId="+Session["Owner_Id"]);           
           
           
        }

        private void InsertChildWeight()
        {
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            List<ChildNutritionBo> objList = new List<ChildNutritionBo>();
            foreach (RepeaterItem rptItem in rptrChildWeight.Items)
            {
                objBo = new ChildNutritionBo();
                TextBox txtChildAge = (TextBox)rptItem.FindControl("txtChildAge");
                TextBox txtChildWeight = (TextBox)rptItem.FindControl("txtChildWeight");
                RadioButtonList rdlMalNutrition = (RadioButtonList)rptItem.FindControl("rdlMalNutrition");

                if (txtChildAge.Text != "")
                {
                    objBo.age = Convert.ToInt32(txtChildAge.Text);
                    objBo.weight = Convert.ToInt32(txtChildWeight.Text);
                    objBo.malNutrition = Convert.ToInt32(rdlMalNutrition.SelectedValue);
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    objList.Add(objBo);

                }
            }
            objDao.Child_Weight_Insert(objList);

        }
        
        protected void btnUpdate_Click(object sender, EventArgs e)
        {

           // Session["Owner_Id"] = 10;
            int ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objDao.DeleteOldRecord(ownerId);
            InsertChildWeight();


                objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                objBo.hidden3 = Convert.ToInt32(hidBirth.Value);
                objBo.ageGroup = 0;
                objBo.birthCertificate = Convert.ToInt32(txtBoysCertificateUnder5.Text);
                objBo.noBirthCertificate = Convert.ToInt32(txtBoysNoCertificateUnder5.Text);
                objBo.sex = 0;

                int boy = objDao.Update_Child_Birth_Nutrition(objBo);

                objBo.hidden3 = Convert.ToInt32(hidBirth1.Value);
                objBo.ageGroup = 0;
                objBo.birthCertificate = Convert.ToInt32(txtGirlsCertificateUnder5.Text);
                objBo.noBirthCertificate = Convert.ToInt32(txtGirlsNoCertificateUnder5.Text);
                objBo.sex = 1;

                int girl = objDao.Update_Child_Birth_Nutrition(objBo);

                objBo.hidden3 = Convert.ToInt32(hidBirth2.Value);
                objBo.ageGroup = 0;
                objBo.noBirthCertificate = Convert.ToInt32(txtOtherNoCertificateUnder5.Text);
                objBo.birthCertificate = Convert.ToInt32(txtOtherCertificateUnder5.Text);
                objBo.sex = 2;

                int other = objDao.Update_Child_Birth_Nutrition(objBo);

                objBo.hidden3 = Convert.ToInt32(hidBirth3.Value);
                objBo.ageGroup = 1;
                objBo.birthCertificate = Convert.ToInt32(txtBoysCertificate.Text);
                objBo.noBirthCertificate = Convert.ToInt32(txtBoysNoCertificate.Text);
                objBo.sex = 0;


                int boy1 = objDao.Update_Child_Birth_Nutrition(objBo);

                objBo.hidden3 = Convert.ToInt32(hidBirth4.Value);
                objBo.ageGroup = 1;
                objBo.birthCertificate = Convert.ToInt32(txtGirlsCertificate.Text);
                objBo.noBirthCertificate = Convert.ToInt32(txtGirlsNoCertificate.Text);
                objBo.sex = 1;


                int girl1 = objDao.Update_Child_Birth_Nutrition(objBo);
               
                objBo.hidden3 = Convert.ToInt32(hidBirth5.Value);
                objBo.ageGroup = 1;
                objBo.noBirthCertificate = Convert.ToInt32(txtOtherNoCertificate.Text);
                objBo.birthCertificate = Convert.ToInt32(txtOtherCertificate.Text);
                objBo.sex = 2;


                objBo.hidden2 = Convert.ToInt32(hidNutrition.Value);
            if(rblSalt.SelectedIndex>-1)
                objBo.salt = Convert.ToInt32(rblSalt.SelectedValue);
            if(rblNutrition.SelectedIndex>-1)
                objBo.nutritionFood = Convert.ToInt32(rblNutrition.SelectedValue);
            if(rblChildWeight.SelectedIndex>-1)
                objBo.childWeightInfo = Convert.ToInt32(rblChildWeight.SelectedValue);

                int other1 = objDao.Update_Child_Birth_Nutrition(objBo);


                if (other1 > 0)
            {
               Response.Redirect(Constant.constantApppath+"/pages/Marriage.aspx?Owner_Id="+Session["Owner_Id"]);
            }

           

          

        }


        protected void btnBack_Click(object sender, EventArgs e)
        {
           Response.Redirect(Constant.constantApppath+"/pages/TeenAgeHealth.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
    }
}