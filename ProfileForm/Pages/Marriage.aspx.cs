﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;

namespace ProfileForm.Pages
{
    public partial class Marriage : System.Web.UI.Page
    {
        MarriageBo objMrgInfo = null;
        MarriageDao objMarriage = new MarriageDao();



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptrMarriage.DataBind();
                if (Request.QueryString["Owner_Id"] != null)
                {
                    Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                    BindMaritalInfo(OwnerId);

                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;
                }

                if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
                }


            }


        }

        private void BindMaritalInfo(int ownerId)
        {
            objMrgInfo = new MarriageBo();
            objMrgInfo.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objMarriage.FetchMaritalInfo(objMrgInfo);

            rptrMarriage.DataSource = null;
            rptrMarriage.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrMarriage.DataSource = dt;
                rptrMarriage.DataBind();
            }

        }
        private int count;
        protected void btnAddAnother_Click(object sender, EventArgs e)
        {
            count++;
            DataTable dtMaritalStatus = new DataTable();
            dtMaritalStatus.Clear();
            dtMaritalStatus.Columns.Add("AGE");
            dtMaritalStatus.Columns.Add("SEX_ID");
            dtMaritalStatus.Columns.Add("MARITAL_INFO_ID");

            HiddenField hidMarital = new HiddenField();
            TextBox txtAge = new TextBox();
            RadioButtonList sexId = new RadioButtonList();

            foreach (RepeaterItem rptItem in rptrMarriage.Items)
            {
                DataRow drA = dtMaritalStatus.NewRow();
                hidMarital = (HiddenField)rptItem.FindControl("fieldId");
                txtAge = (TextBox)rptItem.FindControl("txtChildAge");
                sexId = (RadioButtonList)rptItem.FindControl("RdoSex");

                drA["MARITAL_INFO_ID"] = hidMarital.Value;
                drA["AGE"] = txtAge.Text;
                drA["SEX_ID"] = sexId.SelectedIndex;
                if (sexId.SelectedIndex >= 0)
                    dtMaritalStatus.Rows.Add(drA);
            }
            DataRow drB = dtMaritalStatus.NewRow();

            drB["MARITAL_INFO_ID"] = count;
            drB["AGE"] = 0;
            drB["SEX_ID"] = 0;


            dtMaritalStatus.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrMarriage.DataSource = db;
            rptrMarriage.DataBind();

            rptrMarriage.DataSource = dtMaritalStatus;
            rptrMarriage.DataBind();


        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            //Session["Owner_ID"] = "7";
            InsertMarrigeInfo();
           Response.Redirect(Constant.constantApppath+"/pages/ChildLabour.aspx");

        }

        private void InsertMarrigeInfo()
        {
            foreach (RepeaterItem rptr in rptrMarriage.Items)
            {
                objMrgInfo = new MarriageBo();

                TextBox txtChildAge = (TextBox)rptr.FindControl("txtChildAge");
                RadioButtonList RdoSex = (RadioButtonList)rptr.FindControl("RdoSex");
                if (txtChildAge.Text != "")
                {
                    if (Session["Owner_Id"].ToString() != null)
                    {
                        objMrgInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                    }

                    objMrgInfo.MarriageAge = Convert.ToInt32(txtChildAge.Text);
                    if (RdoSex.SelectedIndex > -1)
                        objMrgInfo.SexId = Convert.ToInt32(RdoSex.SelectedValue);
                    else objMrgInfo.SexId = -1;


                }
                objMarriage.InsertMrgInfo(objMrgInfo);
            }
        }

        protected void btnUpdateMarriage_OnClick(object sender, EventArgs e)
        {
            int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objMarriage.DeleteOldRecord(OwnerId);
            InsertMarrigeInfo();
           Response.Redirect(Constant.constantApppath+"/pages/ChildLabour.aspx?Owner_Id=" + Session["Owner_Id"]);

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {

           Response.Redirect(Constant.constantApppath+"/pages/ChildNutrition.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
    }
}