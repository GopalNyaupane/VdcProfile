﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MemberInfo.aspx.cs" Inherits="ProfileForm.Pages.MemberInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:ScriptManager ID="ScriptManager" runat ="server" />
     <script type="text/javascript" src="../Scripts/jquery-1.4.1-vsdoc.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">

        function validatePage() {
            var check = false;
            $("#MainContent_btnUpdate").val('Processing...');
           
        }
    </script>
     
    <table class="table table-striped">        
        
         <tr>
                <td>६४.</td>
                <td>तपाईंको परिवारका सदस्यहरु कुन कुन पेशा (रोजगारी) मा संलग्न हुनुहुन्छ ?</td>
                <td> </td>
                <td></td>
            </tr>
        </table>
      <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddMemberOccupation" EventName="Click" />
        </Triggers>
        <ContentTemplate>
        <table class="table table-striped">   
            <tr>
                <td></td>
                <td> पेशा (रोजगारी)
              </td>
                  <td>
                     उमेर
                    <%-- --%>
                  </td>
                  <td>
                      <table class="table table-bordered">
                          <td> पुरुष </td>
                        <td>महिला </td>
                          
                      </table>

                                 </td>
            </tr>
            
                <asp:Panel runat="server" ID="pnlMarrige">
                <asp:Repeater ID="rptrFamilyMemberOccupation" runat="server" >
                                   
                                    <ItemTemplate>
                                        <tr id="<%#Eval("OCCUPATION_ID") %>" class="div_row">
                                            <td></td>
                                            
                                            <td>
                                                 <asp:DropDownList ID="ddlOccupation" runat="server" EnableViewState="true" DataSourceID="XmlDataSource_Occupation" DataTextField="name" SelectedValue='<%#Eval("OCCUPATION_TYPE_ID") %>' 
                        DataValueField="id">
                       </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Occupation" runat="server" 
                        DataFile="~/XMLDataSource/Occupation.xml"></asp:XmlDataSource>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlAgeGroup" runat="server" EnableViewState="true" RepeatDirection=" Vertical" SelectedValue='<%#Eval("AGE_GROUP_ID") %>'>
                        <asp:ListItem Value="0">१६-१८</asp:ListItem>
                        <asp:ListItem Value="1">१९-२४ </asp:ListItem>
                         <asp:ListItem Value="2">२५-४०</asp:ListItem>
                        <asp:ListItem Value="3">४१-६० </asp:ListItem>
                        <asp:ListItem Value="4">६०‌‌+ </asp:ListItem>
                        
                        </asp:DropDownList>
                                            </td>
                                            <td>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td><asp:TextBox ID="txtMaleNo" EnableViewState="true" runat="server" TextMode="Number" Text='<%#Eval("MALE_NO") %>'></asp:TextBox></td>
                                                        <td><asp:TextBox ID="txtFemaleNo" EnableViewState="true" runat="server" TextMode="Number" Text='<%#Eval("FEMALE_NO") %>'> </asp:TextBox></td>
                                                       
                                            </tr>
                                    </table>                                             
                                </td>
                                    <td>
                                            <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn  btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                                                             
                                        <asp:HiddenField ID="hidOccupationId" runat="server" Value='<%#Eval("OCCUPATION_ID") %>'>
                                        </asp:HiddenField>
                                    </td>                                            
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
           </asp:Panel>
            
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td align="right"><asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="btnAddMemberOccupation" OnClick="btnAddMemberOccupation_OnClick" /></td>
            </tr>
        </table>
 </ContentTemplate>
  </asp:UpdatePanel>

   <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddAbroad" EventName="Click" />
        </Triggers>
        <ContentTemplate>
    <table Float="right" width="100%">
        <tr>
            <td>६५.</td>
            <td>तपाईको परिवारको कुनै सदस्य जिल्ला बाहिर वा बिदेस जानुभएको छ?

            </td>
            
            <td>
                <br />
            </td>
            <td>
                <asp:RadioButtonList ID="RdoAbroad" runat="server" RepeatDirection="Horizontal"
                    AutoPostBack="True" OnSelectedIndexChanged="RdoAbroad_OnSelectedIndexChanged">
                    <asp:ListItem Value="0">छ</asp:ListItem>
                    <asp:ListItem Value="1">छैन</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            
        </tr>
        <tr>
            <td colspan="5"></td>
        </tr>
        </table>
     
      <table class="table table-bordered table-striped">
               
            <tr>
                <td class="style1">
                  &nbsp;क्रम सं</td>
                <td class="style1">
                    गएको सदस्यको नाम</td>
                <td class="style1">
                    उमेर</td>
                <td class="style1">
                    लिङ्ग</td>
                <td class="style1">
                    गएको ठाउँ/देश</td>
                <td class="style1">
                    जानुको कारण</td>
            </tr>
    
            <asp:Panel runat="server" ID="pnlAbroad">
           <asp:Repeater ID="rpterAbroad" runat="server" >
               <ItemTemplate>
                   
           <tr id="<%#Eval("OUT_MIGRATION_DETAIL_ID") %>" class="div_row">
                <td>
<%--                    <asp:Label ID="lblSN" runat="server" Text='<%#Eval("SN") %>'/></td>--%>
                <td>
                    <asp:TextBox ID="txtAbName" runat="server" Text='<%#Eval("NAME") %>'></asp:TextBox></td>
                <td>
                   <asp:TextBox ID="txtAbAge" runat="server" Text='<%#Eval("AGE") %>' TextMode="Number"></asp:TextBox></td>
              
                <td>
                   <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID") %>'>
                                       <asp:ListItem Value="0">पुरुष</asp:ListItem>
                        <asp:ListItem Value="1">महिला</asp:ListItem>
                                </asp:RadioButtonList></td>
                <td>
                    <asp:TextBox ID="txtAbCountry" runat="server" Text='<%#Eval("MIGRATED_PLACE") %>'></asp:TextBox></td>
                <td>
                     <asp:TextBox ID="txtAbReason" runat="server" Text='<%#Eval("OUT_MIGRATION_REASON") %>'></asp:TextBox></td>
                    <td>
                      <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
               
                    <asp:HiddenField ID="hidMigrationId" runat="server" Value='<%#Eval("OUT_MIGRATION_DETAIL_ID") %>'>
                    </asp:HiddenField>
                </td>
               
            </tr>  
            </ItemTemplate>
            </asp:Repeater>
         </asp:Panel>
          <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
                    <td align="right">
                        <asp:Button ID="btnAddAbroad" runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary"
                            OnClick="btnAddAbroad_OnClick"/>
                    </td>
                </tr>
         </table>
 </ContentTemplate>
          </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddHandicapped" EventName="Click" />
        </Triggers>
        <ContentTemplate>
   <table class="table table-bordered table-striped">       
         <tr>
             <td>६६.</td>
             <td>
                 तपाईको परिवारमा १८ वर्षभन्दा कम उमेरका शारीरिक तथा मानसिक हिसाबले अपांगता भएका सदस्यहरु भएमा तलको विवरण दिनुहोस् ।
             </td>
             <td></td>
             <td></td>
         </tr>

         <tr>
             <td></td>
             <td> <b>अपाङ्गताको अवस्था</b>
                   </td>
                    <td>  बालक</td>
                    <td> बालिका</td>
                  
         </tr>
         
             <asp:Panel runat="server" ID="Panel1">
             <asp:Repeater ID="rptrHandicapped" runat="server">
                                   
                <ItemTemplate>
                    <tr id="<%#Eval("DISABLED_ID") %>" class="div_row">
                        <td></td>
                                             
                        <td>
                            <asp:DropDownList ID="ddlHandicapped" runat="server" DataSourceID="XmlDataSource_handicapped" DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DISABLED_TYPE_ID") %>' >
                        </asp:DropDownList>
                            <asp:XmlDataSource ID="XmlDataSource_handicapped" runat="server" 
                                DataFile="~/XMLDataSource/Handicapped.xml">
                        </asp:XmlDataSource>
                        </td>                                            
                            <td><asp:TextBox ID="txtBoyNo" runat="server" TextMode="Number" Text='<%#Eval("MALE_CHILD_COUNT") %>'></asp:TextBox></td>
                            <td><asp:TextBox ID="txtGirlNo" runat="server" TextMode="Number" Text='<%#Eval("FEMALE_CHILD_COUNT") %>'> </asp:TextBox></td>
                        <td>
                                <asp:Button ID="btnRemove" runat="server" Text="X" CssClass="btn  btn-danger" OnClientClick="$(this).parents('.div_row').remove(); rolesCounter--;" EnableViewState="true" />
                           
                            <asp:HiddenField ID="hidDisableId" runat="server" Value='<%#Eval("DISABLED_ID") %>'>
                            </asp:HiddenField>
                        </td>    
                                            
                    </tr>
                </ItemTemplate>
               </asp:Repeater>
                 </asp:Panel>
        
         <tr>
             <td></td>
             <td></td>
             <td></td>
              <td align="right"><br /><asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="btnAddHandicapped" OnClick="btnAddHandicapped_OnClick"/></td>
         </tr>
       </table>
            </ContentTemplate>
          </asp:UpdatePanel>

    <table class="table table-responsive" >
         
           <tr>
               
                <td align="right">
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                   <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" 
                        onclick="btnNext_Click" OnClientClick="return validatePage();"/>     
                   <asp:Button ID="btnUpdateMemberInfo" runat="server" Text="Update" CssClass="btn btn-primary"
                       OnClick="btnUpdateMemberInfo_OnClick" OnClientClick="return validatePage();" Visible="False" />
                </td>

            </tr>
    </table>
</asp:Content>
