﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer.Pages;
using Common.Entity;

namespace ProfileForm.Pages
{
    public partial class ListApplicants : System.Web.UI.Page
    {
     
        ListApplicantsDAO listAppDao = new ListApplicantsDAO();
        OwnerDao objDao = new OwnerDao();
        OwnerBo objBo = new OwnerBo();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                BindListview();
                if (Session["UserId"] == null || Convert.ToInt32(Session["UserId"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"//Login.aspx");
                }
            }
        }
        ListApplicantsDAO obj = new ListApplicantsDAO();
        private void BindListview()
        {
            DataTable dt = null;
            dt = obj.FetchData();
            ViewState["TabeApplicant"] = dt;
            if (dt != null && dt.Rows.Count > 0)
            {
                lvAppList.DataSource = dt;
                lvAppList.DataBind();
            }

        }
        protected void lvAppList_LayoutCreated(object sender, EventArgs e)
        {
            Label lblInfo = (Label)lvAppList.FindControl("lblInfo");
            lblInfo.Text = "Info ID";
            Label lblVDC = (Label)lvAppList.FindControl("lblVDC");
            lblVDC.Text = "गाविस / नगरपालिका";
            Label lblWard = (Label)lvAppList.FindControl("lblWard");
            lblWard.Text = "वडा नं.";
            Label lblOwner = (Label)lvAppList.FindControl("lblOwner");
            lblOwner.Text = "घरमुलीको नाम";
            Label lblHouseNo = (Label)lvAppList.FindControl("lblHouseNo");
            lblHouseNo.Text = "घर नं.";
            Label lblTole = (Label)lvAppList.FindControl("lblTole");
            lblTole.Text = "टोलको नाम";
           
        }

        protected void BtnEdit_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                Session["InfoId"] = e.CommandArgument.ToString();
                DataTable dt = (DataTable)ViewState["TabeApplicant"];
                string strDataExpression = "InfoId = " + int.Parse(Session["InfoId"].ToString());
                DataRow[] filteredRows‍‍ = new DataRow[1000000];
                filteredRows‍‍ = dt.Select(strDataExpression);
                DataTable dtTemp = null;
                dtTemp = filteredRows‍‍.CopyToDataTable();
                Session["WardNo"] = dtTemp.Rows[0]["Ward"].ToString();
                Session["Tole"] = dtTemp.Rows[0]["Tole"].ToString();
                Session["GharMuli"] = dtTemp.Rows[0]["Owner"].ToString();
                Session["GharNo"] = dtTemp.Rows[0]["House"].ToString();
                Session["VDC"] = dtTemp.Rows[0]["Vdc"].ToString();
               Response.Redirect(Constant.constantApppath+"/pages/Info.aspx?Info_Id=" + Session["InfoId"]);
            }
            if (e.CommandName == "delete")
            {
                Session["InfoId"] = e.CommandArgument.ToString();
                DataTable dt = new DataTable();
                dt = (DataTable)ViewState["TabeApplicant"];
                string strDataExpression = "InfoId = " + int.Parse(Session["InfoId"].ToString());
                DataRow[] filteredRows‍‍ = new DataRow[1000000];
                filteredRows‍‍ = dt.Select(strDataExpression);
                DataTable dtTemp = null;
                dtTemp = filteredRows‍‍.CopyToDataTable();
                int ownerId = Convert.ToInt32(dtTemp.Rows[0]["OwnerId"].ToString());

                listAppDao.DeleteData(ownerId);
                // Response.Write("<script>alert('Data Deleted Successfully')</script>");
               Response.Redirect(Constant.constantApppath+"/pages/ListApplicants.aspx");
            }
               //report generation
               if(e.CommandName=="report")
            {
               Session["InfoId"] = e.CommandArgument.ToString();
               DataTable dt = new DataTable();
               dt = (DataTable)ViewState["TabeApplicant"];
               string strDataExpression = "InfoId = " + int.Parse(Session["InfoId"].ToString());
               DataRow[] filteredRows‍‍ = new DataRow[1000000];
               filteredRows‍‍ = dt.Select(strDataExpression);
               DataTable dtTemp = null;
               dtTemp = filteredRows‍‍.CopyToDataTable();
               int ownerId =Convert.ToInt32(dtTemp.Rows[0]["OwnerId"].ToString());                       
              Response.Redirect(Constant.constantApppath+"/pages/../ReportViewer/DetailsReportViewer.aspx?owner_id="+ownerId);
           }
           

        }
        protected void lvApps_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }
        protected void lvAppList_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = objDao.FetchIncomplete();
            if (dt.Rows.Count > 0 && dt != null)
            {

           
            for (int j=0; j< dt.Rows.Count; j++)
            {
                int OwnerId = Convert.ToInt32(dt.Rows[j]["OWNER_ID"]);
                listAppDao.DeleteData(OwnerId);

            }
            //int ownerId=dt.Rows[j]["OWNER_ID];

            }
           Response.Redirect(Constant.constantApppath+"/pages/ListApplicants.aspx");
        }

    }
}