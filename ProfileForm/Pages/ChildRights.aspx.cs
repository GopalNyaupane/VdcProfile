﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class ChildRights : System.Web.UI.Page
    {
        ChildRightBo objBo = new ChildRightBo();
        ChildRightDao objDao = new ChildRightDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadDetails(Owner_Id);
                    btnUpdate.Visible = true;
                    btnNext.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                }
                if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
                {
                   Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
                }
            }

           
           // rblSexDescriminatonType.Items[1].Enabled = false;
            //rblSexDescriminatonType.Items[2].Enabled = false;
            //rblSexDescriminatonType.Items[3].Enabled = false;
            //rblSexDescriminatonType.Items[4].Enabled = false;
           

        }

        public void LoadDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataSet ds = new DataSet();
            ds = objDao.Fetch_ChildRights(objBo);
            DataTable dtChildAbuse = null;
            DataTable dtChildDrugAbuse = null;
            DataTable dtSex = null;
            DataTable dtViolence = null;
            DataTable dtHCP = null;
            DataTable dtShelter = null;
            DataTable dtSCP = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dtChildAbuse = ds.Tables[0];
                dtChildDrugAbuse = ds.Tables[1];
                dtSex = ds.Tables[2];
                dtViolence = ds.Tables[3];
                dtHCP = ds.Tables[4];
                dtShelter = ds.Tables[5];
                dtSCP = ds.Tables[6];

                if (dtChildAbuse != null && dtChildAbuse.Rows.Count > 0)
                {
                    hidChildAbuse.Value = dtChildAbuse.Rows[0]["CHILD_ABUSE_ID"].ToString();
                    if (Convert.ToInt32(dtChildAbuse.Rows[0]["IS_SEX_EXPLOITATION"].ToString()) > -1)
                        rblSexExp.SelectedValue = dtChildAbuse.Rows[0]["IS_SEX_EXPLOITATION"].ToString();
                    if (Convert.ToInt32(dtChildAbuse.Rows[0]["IS_CHILD_ORG"].ToString()) > -1)
                        rblOrg.SelectedIndex = Convert.ToInt32(dtChildAbuse.Rows[0]["IS_CHILD_ORG"].ToString());
                    if (Convert.ToInt32(dtChildAbuse.Rows[0]["IS_STREET_CHILD"].ToString()) > -1)
                        rblStreet.SelectedIndex = Convert.ToInt32(dtChildAbuse.Rows[0]["IS_STREET_CHILD"].ToString());
                    if (Convert.ToInt32(dtChildAbuse.Rows[0]["IS_CHILD_RIGHTS"].ToString()) > -1)
                        rblRights.SelectedIndex = Convert.ToInt32(dtChildAbuse.Rows[0]["IS_CHILD_RIGHTS"].ToString());
                }
                if (dtChildDrugAbuse != null && dtChildDrugAbuse.Rows.Count > 0)
                {
                    hidDrug.Value = dtChildDrugAbuse.Rows[0]["CHILD_DRUG_ABUSE_ID"].ToString();
                    hidDrug1.Value = dtChildDrugAbuse.Rows[1]["CHILD_DRUG_ABUSE_ID"].ToString();
                    hidDrug2.Value = dtChildDrugAbuse.Rows[2]["CHILD_DRUG_ABUSE_ID"].ToString();

                    txtBoysSmoke.Text = dtChildDrugAbuse.Rows[0]["SMOKING"].ToString();
                    txtBoysAddicted.Text = dtChildDrugAbuse.Rows[0]["CANNABIS"].ToString();
                    txtBoysNon.Text = dtChildDrugAbuse.Rows[0]["NON_ADDICTION"].ToString();
                    txtGirlsSmoke.Text = dtChildDrugAbuse.Rows[1]["SMOKING"].ToString();
                    txtGirlsAddicted.Text = dtChildDrugAbuse.Rows[1]["CANNABIS"].ToString();
                    txtGirlsNon.Text = dtChildDrugAbuse.Rows[1]["NON_ADDICTION"].ToString();
                    txtOtherSmoke.Text = dtChildDrugAbuse.Rows[2]["SMOKING"].ToString();
                    txtOtherAddicted.Text = dtChildDrugAbuse.Rows[2]["CANNABIS"].ToString();
                    txtOtherNon.Text = dtChildDrugAbuse.Rows[2]["NON_ADDICTION"].ToString();

                }
                if (dtHCP != null && dtHCP.Rows.Count > 0)
                {
                    hidChildExp.Value = dtHCP.Rows[0]["CHILD_PUNISHMENT_ID"].ToString();
                    if (Convert.ToInt32(dtHCP.Rows[0]["PUNISMENT_TYPE_ID"].ToString()) > -1)
                        rblHomeChild.SelectedValue = dtHCP.Rows[0]["PUNISMENT_TYPE_ID"].ToString();
                }
                if (dtSCP != null && dtSCP.Rows.Count > 0)
                {
                    hidSPunishment.Value = dtSCP.Rows[0]["SCHOOL_CHILD_PUNISHMENT_ID"].ToString();
                    if (Convert.ToInt32(dtSCP.Rows[0]["PUNISHMENT_TYPE_ID"].ToString()) > -1)
                        rblSchoolPunishment.SelectedValue = dtSCP.Rows[0]["PUNISHMENT_TYPE_ID"].ToString();
                }
                if (dtSex != null && dtSex.Rows.Count > 0)
                {
                    hidSex.Value = dtSex.Rows[0]["SEX_DESCRIMINATION_ID"].ToString();
                    if (Convert.ToInt32(dtSex.Rows[0]["IS_SEX_DESCRIMINATION"].ToString()) > -1)
                        rblSexDescrimination.SelectedValue = dtSex.Rows[0]["IS_SEX_DESCRIMINATION"].ToString();
                    if (Convert.ToInt32(dtSex.Rows[0]["SEX_DESCRIMINATION_TYPE"].ToString()) > -1)
                        rblSexDescriminatonType.SelectedValue = dtSex.Rows[0]["SEX_DESCRIMINATION_TYPE"].ToString();
                }
                if (dtShelter != null && dtShelter.Rows.Count > 0)
                {
                    hidShelter.Value = dtShelter.Rows[0]["HOME_SHELTER_ID"].ToString();
                    if (Convert.ToInt32(dtShelter.Rows[0]["ANNUAL_CHILD_CLOTHING"].ToString()) > -1)
                        rblClothing.SelectedValue = dtShelter.Rows[0]["ANNUAL_CHILD_CLOTHING"].ToString();
                    if (Convert.ToInt32(dtShelter.Rows[0]["DIFF_ROOM"].ToString()) > -1)
                        rblDiffRoom.SelectedValue = dtShelter.Rows[0]["DIFF_ROOM"].ToString();
                    if (Convert.ToInt32(dtShelter.Rows[0]["SEPERATE_HOME_SHED"].ToString()) > -1)
                        rblHomeShed.SelectedValue = dtShelter.Rows[0]["SEPERATE_HOME_SHED"].ToString();
                }
                if (dtViolence != null && dtViolence.Rows.Count > 0)
                {
                    hidViolence.Value = dtViolence.Rows[0]["CHILD_WOMEN_VIOLENCE_ID"].ToString();
                    if (Convert.ToInt32(dtViolence.Rows[0]["VIOLENCE_TYPE_ID"].ToString()) > -1)
                        rblChildViolence.SelectedValue = dtViolence.Rows[0]["VIOLENCE_TYPE_ID"].ToString();
                }
            }
            else
            {
                dtChildAbuse = null;
                dtChildDrugAbuse = null;
                dtSex = null;
                dtViolence = null;
                dtHCP = null;
                dtShelter = null;
                dtSCP = null;
            }



        }
        protected void btnNext_Click(object sender, EventArgs e)
        {

            //For Child abuse 
            if (rblSexExp.SelectedIndex > -1)
            {
                objBo.sexExp = Convert.ToInt32(rblSexExp.SelectedValue);
            }
            else objBo.sexExp = -1;
            if (rblOrg.SelectedIndex > -1)
            {
                objBo.orgId = Convert.ToInt32(rblOrg.SelectedValue);
            }
            else objBo.orgId = -1;
            if (rblStreet.SelectedIndex > -1)
            {
                objBo.streetId = Convert.ToInt32(rblStreet.SelectedValue);
            }
            else objBo.streetId = -1;
            if (rblRights.SelectedIndex > -1)
            {
                objBo.rightsId = Convert.ToInt32(rblRights.SelectedValue);
            }
            else objBo.rightsId = -1;
            if (rblSexExp.SelectedIndex > -1)
            {
                objBo.childExploitationId = Convert.ToInt32(rblSexExp.SelectedValue);
            }
            else objBo.childExploitationId = -1;
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            int cai = objDao.Child_Abuse_Insert(objBo);

            //For  Child Sex desc    
            if (rblSexDescrimination.SelectedIndex > -1)
            {
                objBo.sexDescriminationId = Convert.ToInt32(rblSexDescrimination.SelectedValue);
            }
            else objBo.sexDescriminationId = -1;
            if (rblSexDescriminatonType.SelectedIndex > -1)
            {
                objBo.sexDescriminationTypeId = Convert.ToInt32(rblSexDescriminatonType.SelectedValue);
            }
            else objBo.sexDescriminationTypeId = -1;
            int csd = objDao.Child_Sex_Desc_Insert(objBo);

            //for child women violence
            if (rblChildViolence.SelectedIndex > -1)
            {
                objBo.violenceTypeId = Convert.ToInt32(rblChildViolence.SelectedValue);
            }
            else objBo.violenceTypeId = -1;
            int cwv = objDao.Child_Women_Violence_Insert(objBo);

            //For Home Child Punishment
            if (rblHomeChild.SelectedIndex > -1)
                objBo.punishmentTypeId = Convert.ToInt32(rblHomeChild.SelectedValue);
            else objBo.punishmentTypeId = -1;
            int hcp = objDao.Home_Child_Punishment_Insert(objBo);

            //For Home Shelter
            if (rblClothing.SelectedIndex > -1)
            {
                objBo.clothingId = Convert.ToInt32(rblClothing.SelectedValue);
            }
            else
                objBo.clothingId = -1;
            if (rblDiffRoom.SelectedIndex > -1)
            {
                objBo.diffRoomId = Convert.ToInt32(rblDiffRoom.SelectedValue);
            }
            else objBo.diffRoomId = -1;
            if (rblHomeShed.SelectedIndex > -1)
            {
                objBo.homeShedId = Convert.ToInt32(rblHomeShed.SelectedValue);
            }
            else
                objBo.homeShedId = -1;
            int hs = objDao.Home_Shelter_Insert(objBo);


            //For school punishment
            if (rblSchoolPunishment.SelectedIndex > -1)
            {

                objBo.punishment = Convert.ToInt32(rblSchoolPunishment.SelectedValue);
            }
            else
                objBo.punishment = -1;

            int sp = objDao.School_Child_Punishment_Insert(objBo);





            //for Boys
            objBo.smoking = Convert.ToInt32(txtBoysSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtBoysAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtBoysNon.Text);
            objBo.sexId = 0;

            int cdab = objDao.Child_Drug_Abuse_Insert(objBo);


            //For Girls
            objBo.smoking = Convert.ToInt32(txtGirlsSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtGirlsAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtGirlsNon.Text);
            objBo.sexId = 1;

            int cdag = objDao.Child_Drug_Abuse_Insert(objBo);

            //For Others
            objBo.smoking = Convert.ToInt32(txtOtherSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtOtherAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtOtherNon.Text);
            objBo.sexId = 2;

            int cda = objDao.Child_Drug_Abuse_Insert(objBo);

            if (cda > 0)
            {
               Response.Redirect(Constant.constantApppath+"/pages/EducationSection.aspx");
            }



        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            objBo.hiddenId1 = Convert.ToInt32(hidChildExp.Value);
            objBo.hiddenId2 = Convert.ToInt32(hidSex.Value);
            objBo.hiddenId3 = Convert.ToInt32(hidSPunishment.Value);
            objBo.hiddenId4 = Convert.ToInt32(hidViolence.Value);
            objBo.hiddenId5 = Convert.ToInt32(hidChildAbuse.Value);

            objBo.hiddenId7 = Convert.ToInt32(hidShelter.Value);


            objBo.hiddenId6 = Convert.ToInt32(hidDrug.Value);

            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.sexId = 0;
            objBo.smoking = Convert.ToInt32(txtBoysSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtBoysAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtBoysNon.Text);

            int m = objDao.Update_ChildRights(objBo);
            objBo.hiddenId6 = Convert.ToInt32(hidDrug1.Value);
            objBo.sexId = 1;
            objBo.smoking = Convert.ToInt32(txtGirlsSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtGirlsAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtGirlsNon.Text);
            int f = objDao.Update_ChildRights(objBo);

            objBo.hiddenId6 = Convert.ToInt32(hidDrug2.Value);
            objBo.sexId = 2;
            objBo.smoking = Convert.ToInt32(txtOtherSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtOtherAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtOtherNon.Text);
            int o = objDao.Update_ChildRights(objBo);
            if (rblHomeChild.SelectedIndex > -1)
                objBo.punishmentTypeId = Convert.ToInt32(rblHomeChild.SelectedValue);
            if (rblSexExp.SelectedIndex > -1)
                objBo.sexExp = Convert.ToInt32(rblSexExp.SelectedValue);
            if (rblOrg.SelectedIndex > -1)
                objBo.orgId = Convert.ToInt32(rblOrg.SelectedValue);
            if (rblStreet.SelectedIndex > -1)
                objBo.streetId = Convert.ToInt32(rblStreet.SelectedValue);
            if (rblRights.SelectedIndex > -1)
                objBo.rightsId = Convert.ToInt32(rblRights.SelectedValue);
            if (rblSexDescrimination.SelectedIndex > -1)
                objBo.sexDescriminationId = Convert.ToInt32(rblSexDescrimination.SelectedValue);
            if (rblSexDescriminatonType.SelectedIndex > -1)
                objBo.sexDescriminationTypeId = Convert.ToInt32(rblSexDescriminatonType.SelectedValue);
            else objBo.sexDescriminationTypeId = -1;
            if (rblChildViolence.SelectedIndex > -1)
                objBo.violenceTypeId = Convert.ToInt32(rblChildViolence.SelectedValue);
            if (rblSchoolPunishment.SelectedIndex > -1)
                objBo.punishment = Convert.ToInt32(rblSchoolPunishment.SelectedValue);
            if (rblClothing.SelectedIndex > -1)
                objBo.clothingId = Convert.ToInt32(rblClothing.SelectedValue);
            if (rblDiffRoom.SelectedIndex > -1)
                objBo.diffRoomId = Convert.ToInt32(rblDiffRoom.SelectedValue);
            if (rblHomeShed.SelectedIndex > -1)
                objBo.homeShedId = Convert.ToInt32(rblHomeShed.SelectedValue);

            int i = objDao.Update_ChildRights(objBo);
            if (i > 0)
            {
               Response.Redirect(Constant.constantApppath+"/pages/EducationSection.aspx?Owner_Id=" + Session["Owner_Id"]);
            }

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
           Response.Redirect(Constant.constantApppath+"/pages/Childlabour.aspx?Owner_Id=" + Session["Owner_ID"]);
        }

        protected void rblSexDescrimination_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            if (rblSexDescrimination.SelectedIndex == 1)
            {
                rblSexDescriminatonType.DataSource = null;
                rblSexDescriminatonType.DataBind();
                rblSexDescriminatonType.Enabled = false;
            }
            else rblSexDescriminatonType.Enabled = true;
        }
    }
}