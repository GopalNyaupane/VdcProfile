﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class EducationSection2 : System.Web.UI.Page
    {
        EducationSection2Bo objBo = new EducationSection2Bo();
        EducationSectionDao objDao = new EducationSectionDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadEducationDetails(Owner_Id);
                    LoadUnadmittedDetails(Owner_Id);
                    LoadCPDetails(Owner_Id);
                    LoadCMDetails(Owner_Id);

                   

                    btnUpdate.Visible = true;
                    btnNext.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                }
               if (Session["Owner_Id"] == null || Convert.ToInt32(Session["Owner_Id"]) < 0)
               {
                  Response.Redirect(Constant.constantApppath+"/pages/Info.aspx");
               }
            }
       
           
        }

        private void LoadCMDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_Committee_Membership(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidMembership.Value = dt.Rows[0]["COMMITTE_MEMBERSHIP_ID"].ToString();
                hidMembership1.Value = dt.Rows[1]["COMMITTE_MEMBERSHIP_ID"].ToString();
                txtBoyLocal.Text = dt.Rows[0]["LOCAL_BODY"].ToString();
                txtBoyWard.Text = dt.Rows[0]["WARD_CITIZEN_FORUM"].ToString();
                txtBoyMgmt.Text = dt.Rows[0]["SCHOOL_HLTH_MGMT"].ToString();
                txtBoyProtect.Text = dt.Rows[0]["CHILD_PRO_COM"].ToString();
                txtBoyChildClub.Text = dt.Rows[0]["CHILD_CLUB"].ToString();

                txtGirlLocal.Text = dt.Rows[1]["LOCAL_BODY"].ToString();
                txtGirlWard.Text = dt.Rows[1]["WARD_CITIZEN_FORUM"].ToString();
                txtGirlMgmt.Text = dt.Rows[1]["SCHOOL_HLTH_MGMT"].ToString();
                txtGirlProtect.Text = dt.Rows[1]["CHILD_PRO_COM"].ToString();
                txtGirlChildClub.Text = dt.Rows[1]["CHILD_CLUB"].ToString();
              
            }
        }

        private void LoadCPDetails(int Owner_Id)
        {
             objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_Club_Participation(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidClubParticipation.Value = dt.Rows[0]["CLUB_PARTICIPATION_ID"].ToString();
                hidClubParticipation1.Value = dt.Rows[1]["CLUB_PARTICIPATION_ID"].ToString();
                hidClubParticipation2.Value = dt.Rows[2]["CLUB_PARTICIPATION_ID"].ToString();
                hidClubParticipation3.Value = dt.Rows[3]["CLUB_PARTICIPATION_ID"].ToString();

                txtBoySchool.Text = dt.Rows[0]["MALE_CHILD_COUNT"].ToString();
                txtBoySocial.Text = dt.Rows[1]["MALE_CHILD_COUNT"].ToString();
                txtBoyClub.Text = dt.Rows[2]["MALE_CHILD_COUNT"].ToString();
                txtBoyNotAd.Text = dt.Rows[3]["MALE_CHILD_COUNT"].ToString();

                txtGirlSchool.Text = dt.Rows[0]["FEMALE_CHILD_COUNT"].ToString();
                txtGirlSocial.Text = dt.Rows[1]["FEMALE_CHILD_COUNT"].ToString();
                txtGirlClub.Text = dt.Rows[2]["FEMALE_CHILD_COUNT"].ToString();
                txtGirlNotAd.Text = dt.Rows[3]["FEMALE_CHILD_COUNT"].ToString();
            }
            
        }

        private void LoadUnadmittedDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_School_Unadmitted(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidUnadmitted.Value = dt.Rows[0]["UNADMITTED_ID"].ToString();
                hidUnadmitted1.Value = dt.Rows[1]["UNADMITTED_ID"].ToString();
                hidUnadmitted2.Value = dt.Rows[2]["UNADMITTED_ID"].ToString();
                hidUnadmitted3.Value = dt.Rows[3]["UNADMITTED_ID"].ToString();
                hidUnadmitted4.Value = dt.Rows[4]["UNADMITTED_ID"].ToString();
                hidUnadmitted5.Value = dt.Rows[5]["UNADMITTED_ID"].ToString();

                txtBoyNormal1.Text = dt.Rows[0]["NORMAL"].ToString();
                txtGirlNormal1.Text = dt.Rows[1]["NORMAL"].ToString();
                txtBoyNormal2.Text = dt.Rows[2]["NORMAL"].ToString();
                txtGirlNormal2.Text = dt.Rows[3]["NORMAL"].ToString();
                txtBoyNormal3.Text = dt.Rows[4]["NORMAL"].ToString();
                txtGirlNormal3.Text = dt.Rows[5]["NORMAL"].ToString();

                txtBoyAbNormal1.Text = dt.Rows[0]["ABNORMAL"].ToString();
                txtGirlAbNormal1.Text = dt.Rows[1]["ABNORMAL"].ToString();
                txtBoyAbNormal2.Text = dt.Rows[2]["ABNORMAL"].ToString();
                txtGirlAbNormal2.Text = dt.Rows[3]["ABNORMAL"].ToString();
                txtBoyAbNormal3.Text = dt.Rows[4]["ABNORMAL"].ToString();
                txtGirlAbNormal3.Text = dt.Rows[5]["ABNORMAL"].ToString();             

            }
        }

        private void LoadEducationDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataSet ds = new DataSet();
            ds = objDao.Fetch_Education(objBo);
            DataTable dtCD = null;
            DataTable dtLOM = null;
            DataTable dtSD = null;
            DataTable dtUE = null;

            if (ds != null && ds.Tables.Count > 0)
            {
                dtCD = ds.Tables[0];
                dtLOM = ds.Tables[1];
                dtSD = ds.Tables[2];
                dtUE = ds.Tables[3];

                if (dtCD != null && dtCD.Rows.Count > 0)
                {
                    hidDiscussion.Value = dtCD.Rows[0]["CHILD_DISCUSSION_ID"].ToString();
                    if (Convert.ToInt32(dtCD.Rows[0]["IS_DISCUSSION"].ToString())>-1)
                    rblChildDiscussion.SelectedValue = dtCD.Rows[0]["IS_DISCUSSION"].ToString();
                }
                if (dtLOM != null && dtLOM.Rows.Count > 0)
                {
                   hidLocalOrg.Value = dtLOM.Rows[0]["LOCAL_ORG_MEMBERSHIP_ID"].ToString();
                   if (Convert.ToInt32(dtLOM.Rows[0]["ORG_TYPE_ID"].ToString())>-1)
                   rblMembership.SelectedValue = dtLOM.Rows[0]["ORG_TYPE_ID"].ToString();
                   if (Convert.ToInt32(dtLOM.Rows[0]["PARA_LEGAL_REASON_ID"].ToString())>-1)
                   rblParaLegal.SelectedValue = dtLOM.Rows[0]["PARA_LEGAL_REASON_ID"].ToString();
                   if (Convert.ToInt32(dtLOM.Rows[0]["IS_PARA_LEGAL_WORK_SATISFYING"].ToString())>-1)
                   rblWorkSatisfy.SelectedValue = dtLOM.Rows[0]["IS_PARA_LEGAL_WORK_SATISFYING"].ToString();

                }
                if (dtSD != null && dtSD.Rows.Count > 0)
                {
                    hidDiscontinuation.Value = dtSD.Rows[0]["SCHOOL_DISCONTINUED_ID"].ToString();
                    hidDiscontinuation1.Value = dtSD.Rows[1]["SCHOOL_DISCONTINUED_ID"].ToString();
                    txtBoyDiscontinued.Text = dtSD.Rows[0]["DISCONTINUED_NO"].ToString();
                    txtBoyReason.Text = dtSD.Rows[0]["DISCONTINUED_REASON"].ToString();
                    txtGirlDiscontinued.Text = dtSD.Rows[1]["DISCONTINUED_NO"].ToString();
                    txtGirlReason.Text = dtSD.Rows[1]["DISCONTINUED_REASON"].ToString();
                }
                if (dtUE != null && dtUE.Rows.Count > 0)
                {
                    hidUnofficial.Value = dtUE.Rows[0]["UNOFFICIAL_ID"].ToString();
                    txtBoyUnEdCount.Text = dtUE.Rows[0]["MALE"].ToString();
                    txtGirlUnEdCount.Text = dtUE.Rows[0]["FEMALE"].ToString();
                }

            }
        else
        {
            dtCD = null;
            dtLOM = null;
            dtSD = null;
            dtUE = null;


        }

        }

        public void btnNext_Click(object sender,EventArgs e)
        {
            //Starting code for table School_unadmitted
          // Session["Owner_Id"] = 10;
           
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.ageGroupId = 0;
            objBo.sexId = 0;
            objBo.normal = Convert.ToInt32( txtBoyNormal1.Text);
            objBo.abNormal = Convert.ToInt32(txtBoyAbNormal1.Text);
           

            int sub1 = objDao.School_Unadmitted_Insert(objBo);

            objBo.ageGroupId = 0;
            objBo.sexId = 1;
            objBo.normal = Convert.ToInt32(txtGirlNormal1.Text);
            objBo.abNormal = Convert.ToInt32(txtGirlAbNormal1.Text);

            int sug1 = objDao.School_Unadmitted_Insert(objBo);

            objBo.ageGroupId = 1;
            objBo.sexId = 0;
            objBo.normal = Convert.ToInt32(txtBoyNormal2.Text);
            objBo.abNormal = Convert.ToInt32(txtBoyAbNormal2.Text);

            int sub2 = objDao.School_Unadmitted_Insert(objBo);

            objBo.ageGroupId = 1;
            objBo.sexId = 1;
            objBo.normal = Convert.ToInt32(txtGirlNormal2.Text);
            objBo.abNormal = Convert.ToInt32(txtGirlAbNormal2.Text);

            int sug2 = objDao.School_Unadmitted_Insert(objBo);
           

            objBo.ageGroupId = 2;
            objBo.sexId = 0;
            objBo.normal = Convert.ToInt32(txtBoyNormal3.Text);
            objBo.abNormal = Convert.ToInt32(txtBoyAbNormal3.Text);

            int sub3 = objDao.School_Unadmitted_Insert(objBo);

            objBo.ageGroupId = 2;
            objBo.sexId = 1;
            objBo.normal = Convert.ToInt32(txtGirlNormal3.Text);
            objBo.abNormal = Convert.ToInt32(txtGirlAbNormal3.Text);

            int sug3 = objDao.School_Unadmitted_Insert(objBo);

          
         //starting code for Table SchoolDiscontinuation

            objBo.sexId = 0;
            objBo.discontinuedNo = Convert.ToInt32(txtBoyDiscontinued.Text);
            objBo.reason = txtBoyReason.Text;
            int sdg = objDao.Insert_Discontinuation(objBo);    
 
            objBo.sexId = 1;
            objBo.discontinuedNo = Convert.ToInt32(txtGirlDiscontinued.Text);
            objBo.reason = txtGirlReason.Text;

            int sdg1 = objDao.Insert_Discontinuation(objBo);        
           

            //starting code for table_unofficial Education
            objBo.male = Convert.ToInt32(txtBoyUnEdCount.Text);
            objBo.female = Convert.ToInt32(txtGirlUnEdCount.Text);

            //Inserting code for Child Discussion
            if (rblChildDiscussion.SelectedIndex > -1)
            {
                objBo.isDiscussion = Convert.ToInt32(rblChildDiscussion.SelectedValue);
            }
            else objBo.isDiscussion = -1;

            //InStarting code for OrgMembership
            if(rblMembership.SelectedIndex>-1)
            {
            objBo.orgTypeId = Convert.ToInt32(rblMembership.SelectedValue);
            }
            else objBo.orgTypeId=-1;

            objBo.isParaLegalCom = 1;
            if (rblParaLegal.SelectedIndex > -1)
            {
                objBo.paraLegalReason = Convert.ToInt32(rblParaLegal.SelectedValue);
            }
            else
                objBo.paraLegalReason = -1;
            if (rblWorkSatisfy.SelectedIndex > -1)
            {
                objBo.workSatisfy = Convert.ToInt32(rblWorkSatisfy.SelectedValue);
            }
            else objBo.workSatisfy = -1;

            int sdb = objDao.Insert_Org_Edu_Dis(objBo);

           
            

            //Starting code for club_participation
           
            objBo.clubTypeId = 0;
            objBo.maleChildCount = Convert.ToInt32(txtBoySchool.Text);
            objBo.femaleChildCount = Convert.ToInt32(txtGirlSchool.Text);

            int cp0 = objDao.Club_Participation_Insert(objBo);

            objBo.clubTypeId = 1;
            objBo.maleChildCount = Convert.ToInt32(txtBoySocial.Text);
            objBo.femaleChildCount = Convert.ToInt32(txtGirlSocial.Text);

            int cp1 = objDao.Club_Participation_Insert(objBo);

            objBo.clubTypeId = 2;
            objBo.maleChildCount = Convert.ToInt32(txtBoyClub.Text);
            objBo.femaleChildCount = Convert.ToInt32(txtGirlClub.Text);

            int cp2 = objDao.Club_Participation_Insert(objBo);

            objBo.clubTypeId = 3;
            objBo.maleChildCount = Convert.ToInt32(txtBoyNotAd.Text);
            objBo.femaleChildCount = Convert.ToInt32(txtGirlNotAd.Text);           
            int cp3 = objDao.Club_Participation_Insert(objBo);


            //Starting code for COMMITTEE MEMBERSHIP
            objBo.sexId = 0;
            objBo.localBody = Convert.ToInt32(txtBoyLocal.Text);
            objBo.wardForum = Convert.ToInt32(txtBoyWard.Text);
            objBo.schoolMgmt = Convert.ToInt32(txtBoyMgmt.Text);
            objBo.childProCom = Convert.ToInt32(txtBoyProtect.Text);
            objBo.childClub = Convert.ToInt32(txtBoyChildClub.Text);

            int cm1 = objDao.Committee_Membership_Insert(objBo);

            objBo.sexId = 1;
            objBo.localBody = Convert.ToInt32(txtGirlLocal.Text);
            objBo.wardForum = Convert.ToInt32(txtGirlWard.Text);
            objBo.schoolMgmt = Convert.ToInt32(txtGirlMgmt.Text);
            objBo.childProCom = Convert.ToInt32(txtGirlProtect.Text);
            objBo.childClub = Convert.ToInt32(txtGirlChildClub.Text);

            int cm2 = objDao.Committee_Membership_Insert(objBo);                
           

           
               Response.Redirect(Constant.constantApppath+"/pages/TrainingOccupation.aspx");   





    }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            //Session["Owner_Id"] = 10;
            objBo.hidUnadmitted = Convert.ToInt32(hidUnadmitted.Value);
            objBo.ageGroupId = 0;
            objBo.sexId = 0;
           
            objBo.normal = Convert.ToInt32(txtBoyNormal1.Text);
            objBo.abNormal = Convert.ToInt32(txtBoyAbNormal1.Text);
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            int sub1 = objDao.Update_School_Unadmitted(objBo);

            objBo.ageGroupId = 0;
            objBo.sexId = 1;
            objBo.hidUnadmitted = Convert.ToInt32(hidUnadmitted1.Value);
            objBo.normal = Convert.ToInt32(txtGirlNormal1.Text);
            objBo.abNormal = Convert.ToInt32(txtGirlAbNormal1.Text);

            int sug1 = objDao.Update_School_Unadmitted(objBo);

            objBo.ageGroupId = 1;
            objBo.sexId = 0;
            objBo.hidUnadmitted = Convert.ToInt32(hidUnadmitted2.Value);
            objBo.normal = Convert.ToInt32(txtBoyNormal2.Text);
            objBo.abNormal = Convert.ToInt32(txtBoyAbNormal2.Text);

            int sub2 = objDao.Update_School_Unadmitted(objBo);

            objBo.ageGroupId = 1;
            objBo.sexId = 1;
            objBo.hidUnadmitted = Convert.ToInt32(hidUnadmitted3.Value);
            objBo.normal = Convert.ToInt32(txtGirlNormal2.Text);
            objBo.abNormal = Convert.ToInt32(txtGirlAbNormal2.Text);

            int sug2 = objDao.Update_School_Unadmitted(objBo);


            objBo.ageGroupId = 2;
            objBo.sexId = 0;
            objBo.hidUnadmitted = Convert.ToInt32(hidUnadmitted4.Value);
            objBo.normal = Convert.ToInt32(txtBoyNormal3.Text);
            objBo.abNormal = Convert.ToInt32(txtBoyAbNormal3.Text);

            int sub3 = objDao.Update_School_Unadmitted(objBo);

            objBo.ageGroupId = 2;
            objBo.sexId = 1;
            objBo.hidUnadmitted = Convert.ToInt32(hidUnadmitted5.Value);
            objBo.normal = Convert.ToInt32(txtGirlNormal3.Text);
            objBo.abNormal = Convert.ToInt32(txtGirlAbNormal3.Text);

            int sug3 = objDao.Update_School_Unadmitted(objBo);


            //starting code for Table SchoolDiscontinuation

            objBo.sexId = 0;
            objBo.hidDiscontinuation = Convert.ToInt32(hidDiscontinuation.Value);
            objBo.discontinuedNo = Convert.ToInt32(txtBoyDiscontinued.Text);
            objBo.reason = txtBoyReason.Text;
            int sdb1 = objDao.Update_Discontinuation(objBo);

            objBo.sexId = 1;
            objBo.hidDiscontinuation = Convert.ToInt32(hidDiscontinuation1.Value);
            objBo.discontinuedNo = Convert.ToInt32(txtGirlDiscontinued.Text);
            objBo.reason = txtGirlReason.Text;
            int sdb2 = objDao.Update_Discontinuation(objBo);

            //starting code for table_unofficial Education
            objBo.hidUnofficial = Convert.ToInt32(hidUnofficial.Value);
            objBo.male = Convert.ToInt32(txtBoyUnEdCount.Text);
            objBo.female = Convert.ToInt32(txtGirlUnEdCount.Text);

            //Inserting code for Child Discussion
            objBo.hidDiscussion = Convert.ToInt32(hidDiscussion.Value);
            if(rblChildDiscussion.SelectedIndex>-1)
            objBo.isDiscussion = Convert.ToInt32(rblChildDiscussion.SelectedValue);

            //InStarting code for OrgMembership
            objBo.hidLocalOrg = Convert.ToInt32(hidLocalOrg.Value);
            if(rblMembership.SelectedIndex>-1)
            objBo.orgTypeId = Convert.ToInt32(rblMembership.SelectedValue);
            objBo.isParaLegalCom = 1;
            if(rblParaLegal.SelectedIndex>-1)
            objBo.paraLegalReason = Convert.ToInt32(rblParaLegal.SelectedValue);
            if(rblWorkSatisfy.SelectedIndex>-1)
            objBo.workSatisfy = Convert.ToInt32(rblWorkSatisfy.SelectedValue);
            int sdb = objDao.Update_Org_Edu_Dis(objBo);
           
            

         
            //Starting code for club_participation

            objBo.clubTypeId = 0;
            objBo.hidClubParticipation = Convert.ToInt32(hidClubParticipation.Value);
            objBo.maleChildCount = Convert.ToInt32(txtBoySchool.Text);
            objBo.femaleChildCount = Convert.ToInt32(txtGirlSchool.Text);

            int cp0 = objDao.Update_Club_Participation(objBo);
            objBo.clubTypeId = 1;
            objBo.hidClubParticipation = Convert.ToInt32(hidClubParticipation1.Value);
            objBo.maleChildCount = Convert.ToInt32(txtBoySocial.Text);
            objBo.femaleChildCount = Convert.ToInt32(txtGirlSocial.Text);

            int cp1 = objDao.Update_Club_Participation(objBo);

            objBo.clubTypeId = 2;
            objBo.hidClubParticipation = Convert.ToInt32(hidClubParticipation2.Value);
            objBo.maleChildCount = Convert.ToInt32(txtBoyClub.Text);
            objBo.femaleChildCount = Convert.ToInt32(txtGirlClub.Text);

            int cp2 = objDao.Update_Club_Participation(objBo);

            objBo.clubTypeId = 3;
            objBo.hidClubParticipation = Convert.ToInt32(hidClubParticipation3.Value);
            objBo.maleChildCount = Convert.ToInt32(txtBoyNotAd.Text);
            objBo.femaleChildCount = Convert.ToInt32(txtGirlNotAd.Text);
            int cp3 = objDao.Update_Club_Participation(objBo);


            //Starting code for COMMITTEE MEMBERSHIP
            objBo.sexId = 0;
            objBo.hidMembership = Convert.ToInt32(hidMembership.Value);
            objBo.localBody = Convert.ToInt32(txtBoyLocal.Text);
            objBo.wardForum = Convert.ToInt32(txtBoyWard.Text);
            objBo.schoolMgmt = Convert.ToInt32(txtBoyMgmt.Text);
            objBo.childProCom = Convert.ToInt32(txtBoyProtect.Text);
            objBo.childClub = Convert.ToInt32(txtBoyChildClub.Text);

            int cm1 = objDao.Update_Committee_Membership(objBo);

            objBo.sexId = 1;
            objBo.hidMembership = Convert.ToInt32(hidMembership1.Value);
            objBo.localBody = Convert.ToInt32(txtGirlLocal.Text);
            objBo.wardForum = Convert.ToInt32(txtGirlWard.Text);
            objBo.schoolMgmt = Convert.ToInt32(txtGirlMgmt.Text);
            objBo.childProCom = Convert.ToInt32(txtGirlProtect.Text);
            objBo.childClub = Convert.ToInt32(txtGirlChildClub.Text);

            int cm2 = objDao.Update_Committee_Membership(objBo);




           Response.Redirect(Constant.constantApppath+"/pages/TrainingOccupation.aspx?Owner_Id="+Session["Owner_Id"]);
           





        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
           Response.Redirect(Constant.constantApppath+"/pages/EducationSection.aspx?Owner_Id="+Session["Owner_Id"]);
        }


    }
}