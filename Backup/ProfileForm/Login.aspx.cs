﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;
using Common;

namespace ProfileForm
{
    public partial class Login : System.Web.UI.Page
    {
        private LoginBo objBo = null;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        LoginDao obj
        {
            get { return new LoginDao(); }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            objBo = new LoginBo();
            objBo.Username = txtUsername.Text.Trim().ToLower();
            objBo.Password = txtPassword.Text.Trim();
            DataTable dt = obj.CheckUser(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                Session["UserId"] = dt.Rows[0]["USER_ID"].ToString();
               // Session["RoleId"] = dt.Rows[0]["RoleId"].ToString();
                Session["Username"] = dt.Rows[0]["USERNAME"].ToString();
                Response.Redirect("~/Pages/Info.aspx");
            }
            else
            {
                Response.Write("<script>alert('Wrong Username or Password')</script>");
            }

        }
    }
}