﻿using Common.Entity;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class TeenAgeHealth : System.Web.UI.Page
    {

        TeenAgeHealthBo objTeenAgeHealthBo= new TeenAgeHealthBo();
        TeenAgeHealthDao objTeenAgeHealthDao=new TeenAgeHealthDao();
        



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    Next.Visible = false;
                    Update.Visible = true;
                    LoadTeenAgeHealth(OwnerId);
                    LoadTeenAgeFatality(OwnerId);
                    LoadChildFatality(OwnerId);


                }
                else
                {


                    Update.Visible = false;
                    Next.Visible = true;

                }
            }

            

        }

        private void LoadChildFatality(int OwnerId)
        {
            objTeenAgeHealthBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
           dt= objTeenAgeHealthDao.FetchChildFatality(objTeenAgeHealthBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                rptrChildFatality.DataBind();
                rptrChildFatality.DataSource = dt;
                rdoIsChildFatality.SelectedIndex = 0;
                txtChildfatalityCount.Text = Convert.ToString(dt.Rows.Count);

            }
        }

        private void LoadTeenAgeFatality(int OwnerId)
        {
            objTeenAgeHealthBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objTeenAgeHealthDao.FetchTeenAgeFatality(objTeenAgeHealthBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                rptrTeenFatality.DataSource = dt;
                rptrTeenFatality.DataBind();
                rdoIsTeenFatality.SelectedIndex = 0;
                txtTeenFatalityCount.Text = Convert.ToString(dt.Rows.Count);
            }


        }

        private void LoadTeenAgeHealth(int OwnerId)
        {
            objTeenAgeHealthBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objTeenAgeHealthDao.FetchTeenAgeHealth(objTeenAgeHealthBo);

            if (dt != null && dt.Rows.Count > 0)
            {
                RpterTeenAgeHealth.DataSource = dt;
                RpterTeenAgeHealth.DataBind();
                rdoIsDisease.SelectedIndex = 0;
                txtTeenCount.Text = Convert.ToString(dt.Rows.Count);
               
            }
        }
        


        protected void RpterTeenAgeHealth_ItemCreated(object sender, RepeaterItemEventArgs e)
        {

        }

        private void BindTeenAgeHealth(DataTable dtHealth, int membercount)
        {
            DataTable dtTeenAgeHealth = new DataTable();
            dtTeenAgeHealth.Clear();
            dtTeenAgeHealth.Columns.Add("SN");
            dtTeenAgeHealth.Columns.Add("DISEASE_ID");
            dtTeenAgeHealth.Columns.Add("SEX_ID");
            dtTeenAgeHealth.Columns.Add("TEENAGE_HEALTH_ID");


            int j = 0;
            if (dtHealth.Rows.Count == 0)
            {

                for (int i = 0; i < membercount; i++)
                {
                    DataRow _dtRow = dtTeenAgeHealth.NewRow();
                    _dtRow["SN"] = i + 1;
                    _dtRow["DISEASE_ID"] = 0;
                    _dtRow["SEX_ID"] = 0;
                    _dtRow["TEENAGE_HEALTH_ID"] = 0;




                    dtTeenAgeHealth.Rows.Add(_dtRow);
                }

            }
            else
            {
                for (int i = 0; i < dtHealth.Rows.Count; i++)
                {
                    DataRow _dtRow = dtTeenAgeHealth.NewRow();
                    /*_dtRow["SN"] = i + 1;
                    _dtRow["Name"] = dtAbroad.Rows[i]["away_name"].ToString();
                    _dtRow["Age"] = dtAbroad.Rows[i]["away_age"].ToString();
                    _dtRow["Relation"] = dtAbroad.Rows[i]["away_reason"].ToString();
                    _dtRow["Sex"] = dtAbroad.Rows[i]["away_gender"].ToString();
                   */

                    dtTeenAgeHealth.Rows.Add(_dtRow);
                }
            }
            RpterTeenAgeHealth.DataSource = dtTeenAgeHealth;
            RpterTeenAgeHealth.DataBind();
        }

        protected void btnAddHealthIssue_OnClick(object sender, EventArgs e)
        {

        }


        protected void btnOk_OnClick(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(txtTeenCount.Text);
            DataTable dtHealth = new DataTable();
            BindTeenAgeHealth(dtHealth, count);
            
        }




        protected void btnFatalityOk_OnClick(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(txtTeenFatalityCount.Text);
            DataTable dtHealth = new DataTable();
            BindTeenAgeFatality(dtHealth, count);
        }

        private void BindTeenAgeFatality(DataTable dtHealth, int count)
        {
            DataTable dtTeenAgeHealth = new DataTable();
            dtTeenAgeHealth.Clear();
            dtTeenAgeHealth.Columns.Add("SN");
            dtTeenAgeHealth.Columns.Add("DISEASE_ID");
            dtTeenAgeHealth.Columns.Add("SEX_ID");
            dtTeenAgeHealth.Columns.Add("TEENAGE_FATALITY_ID");
            


            int j = 0;
            if (dtHealth.Rows.Count == 0)
            {

                for (int i = 0; i < count; i++)
                {
                    DataRow _dtRow = dtTeenAgeHealth.NewRow();
                    _dtRow["SN"] = i + 1;
                    _dtRow["DISEASE_ID"] = 0;
                    _dtRow["SEX_ID"] = 0;
                    _dtRow["TEENAGE_FATALITY_ID"] = 0;




                    dtTeenAgeHealth.Rows.Add(_dtRow);
                }

            }
            else
            {
                for (int i = 0; i < dtHealth.Rows.Count; i++)
                {
                    DataRow _dtRow = dtTeenAgeHealth.NewRow();
                    /*_dtRow["SN"] = i + 1;
                    _dtRow["Name"] = dtAbroad.Rows[i]["away_name"].ToString();
                    _dtRow["Age"] = dtAbroad.Rows[i]["away_age"].ToString();
                    _dtRow["Relation"] = dtAbroad.Rows[i]["away_reason"].ToString();
                    _dtRow["Sex"] = dtAbroad.Rows[i]["away_gender"].ToString();
                   */

                    dtTeenAgeHealth.Rows.Add(_dtRow);
                }
            }
            rptrTeenFatality.DataSource = dtTeenAgeHealth;
            rptrTeenFatality.DataBind();
        }

        protected void btnChilDFatalityOk_OnClick(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(txtChildfatalityCount.Text);
            DataTable dtHealth = new DataTable();
            BindChildFatality(dtHealth, count);
        }

        private void BindChildFatality(DataTable dtHealth, int count)
        {
            DataTable dtTeenAgeHealth = new DataTable();
            dtTeenAgeHealth.Clear();
            dtTeenAgeHealth.Columns.Add("SN");
            dtTeenAgeHealth.Columns.Add("DEAD_REASON");
            dtTeenAgeHealth.Columns.Add("SEX_ID");
            dtTeenAgeHealth.Columns.Add("CHILD_FATALITY_ID");

            int j = 0;
            if (dtHealth.Rows.Count == 0)
            {

                for (int i = 0; i < count; i++)
                {
                    DataRow _dtRow = dtTeenAgeHealth.NewRow();
                    _dtRow["SN"] = i + 1;
                    _dtRow["DEAD_REASON"] = 0;
                    _dtRow["SEX_ID"] = 0;
                    _dtRow["CHILD_FATALITY_ID"] = 0;




                    dtTeenAgeHealth.Rows.Add(_dtRow);
                }

            }
            else
            {
                for (int i = 0; i < dtHealth.Rows.Count; i++)
                {
                    DataRow _dtRow = dtTeenAgeHealth.NewRow();
                    /*_dtRow["SN"] = i + 1;
                    _dtRow["Name"] = dtAbroad.Rows[i]["away_name"].ToString();
                    _dtRow["Age"] = dtAbroad.Rows[i]["away_age"].ToString();
                    _dtRow["Relation"] = dtAbroad.Rows[i]["away_reason"].ToString();
                    _dtRow["Sex"] = dtAbroad.Rows[i]["away_gender"].ToString();
                   */

                    dtTeenAgeHealth.Rows.Add(_dtRow);
                }
            }
            rptrChildFatality.DataSource = dtTeenAgeHealth;
            rptrChildFatality.DataBind();
        }
        protected void Update_Click(object sender, EventArgs e)
        {
            {

                List<TeenAgeHealthBo> objListTeenAgeHealthBo = new List<TeenAgeHealthBo>();
                foreach (RepeaterItem rptItem3 in RpterTeenAgeHealth.Items)
                {
                    objTeenAgeHealthBo = new TeenAgeHealthBo();
                    DropDownList txtDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                    RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");
                    HiddenField hidTeenageHealth = (HiddenField)rptItem3.FindControl("hidTeenageHealthId");

                    if (txtDisease.Text != "")
                    {
                        objTeenAgeHealthBo.TeenAgeHealthId = Convert.ToInt32(hidTeenageHealth.Value);
                        objTeenAgeHealthBo.DiseaseId = Convert.ToInt32(txtDisease.Text);
                        objTeenAgeHealthBo.SexId = RdoSex.SelectedIndex;
                        objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                        objListTeenAgeHealthBo.Add(objTeenAgeHealthBo);

                    }
                }
                objTeenAgeHealthDao.UpdateTeenAgeHealth(objListTeenAgeHealthBo);

            }
            {


                List<TeenAgeHealthBo> objListTeenAgeFatalityBo = new List<TeenAgeHealthBo>();
                foreach (RepeaterItem rptItem3 in rptrTeenFatality.Items)
                {
                    objTeenAgeHealthBo = new TeenAgeHealthBo();
                    DropDownList ddlDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                    RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");
                    HiddenField hidTeenageFatalityId = (HiddenField)rptItem3.FindControl("hidTeenagefatalityId");


                    objTeenAgeHealthBo.TeenageFatalityId = Convert.ToInt32(hidTeenageFatalityId.Value);
                    objTeenAgeHealthBo.DiseaseId = ddlDisease.SelectedIndex;
                    objTeenAgeHealthBo.SexId = RdoSex.SelectedIndex;
                    objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListTeenAgeFatalityBo.Add(objTeenAgeHealthBo);


                }
                objTeenAgeHealthDao.UpdateTeenAgeFatality(objListTeenAgeFatalityBo);

            }

            {
                List<TeenAgeHealthBo> objListChildFatalityBo = new List<TeenAgeHealthBo>();
                foreach (RepeaterItem rptItem3 in rptrChildFatality.Items)
                {
                    objTeenAgeHealthBo = new TeenAgeHealthBo();
                    DropDownList ddlDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                    RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");
                    HiddenField hidChildFatality = (HiddenField)rptItem3.FindControl("hidChildFatalityId");

                    objTeenAgeHealthBo.ChildFatalityId = Convert.ToInt32(hidChildFatality.Value);
                    objTeenAgeHealthBo.DeadReason = Convert.ToInt32(ddlDisease.SelectedIndex);
                    objTeenAgeHealthBo.SexId = RdoSex.SelectedIndex;
                    objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListChildFatalityBo.Add(objTeenAgeHealthBo);


                }
                objTeenAgeHealthDao.UpdateChildFatality(objListChildFatalityBo);
            }

            Response.Redirect("ChildNutrition.aspx?Owner_Id="+Session["Owner_Id"]);




        }

        protected void Next_Click(object sender, EventArgs e)
        {


            {

                List<TeenAgeHealthBo> objListTeenAgeHealthBo = new List<TeenAgeHealthBo>();
                foreach (RepeaterItem rptItem3 in RpterTeenAgeHealth.Items)
                {
                    objTeenAgeHealthBo = new TeenAgeHealthBo();
                    DropDownList txtDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                    RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");
                    

                    if (txtDisease.Text != "")
                    {
                        
                        objTeenAgeHealthBo.DiseaseId = Convert.ToInt32(txtDisease.Text);
                        objTeenAgeHealthBo.SexId = RdoSex.SelectedIndex;
                        objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                        objListTeenAgeHealthBo.Add(objTeenAgeHealthBo);

                    }
                }
                objTeenAgeHealthDao.InsertTeenAgeHealth(objListTeenAgeHealthBo);

            }








            {


                List<TeenAgeHealthBo> objListTeenAgeFatalityBo = new List<TeenAgeHealthBo>();
                foreach (RepeaterItem rptItem3 in rptrTeenFatality.Items)
                {
                    objTeenAgeHealthBo = new TeenAgeHealthBo();
                    DropDownList ddlDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                    RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");


                    objTeenAgeHealthBo.DiseaseId = ddlDisease.SelectedIndex;
                    objTeenAgeHealthBo.SexId = RdoSex.SelectedIndex;
                    objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListTeenAgeFatalityBo.Add(objTeenAgeHealthBo);


                }
                objTeenAgeHealthDao.InsertTeenAgeFatality(objListTeenAgeFatalityBo);

            }

            {
                List<TeenAgeHealthBo> objListChildFatalityBo = new List<TeenAgeHealthBo>();
                foreach (RepeaterItem rptItem3 in rptrChildFatality.Items)
                {
                    objTeenAgeHealthBo = new TeenAgeHealthBo();
                    DropDownList ddlDisease = (DropDownList)rptItem3.FindControl("ddlDisease");
                    RadioButtonList RdoSex = (RadioButtonList)rptItem3.FindControl("RdoSex");

                    objTeenAgeHealthBo.DeadReason = Convert.ToInt32(ddlDisease.SelectedIndex);
                    objTeenAgeHealthBo.SexId = RdoSex.SelectedIndex;
                    objTeenAgeHealthBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    objListChildFatalityBo.Add(objTeenAgeHealthBo);


                }
                objTeenAgeHealthDao.InsertChildFatality(objListChildFatalityBo);
            }




            Response.Redirect("ChildNutrition.aspx");
          


        }


        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("MaternalInfo.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
    }
}