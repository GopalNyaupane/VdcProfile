﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;

namespace ProfileForm.Pages
{
    public partial class EducationSection : System.Web.UI.Page
    {
        EducationInfoBo objEducationInfo = null;
        EducationInfoDao objEdinfoDal = new EducationInfoDao();

        protected void Page_Load(object sender, EventArgs e)
        {
           if(!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int ownerId = Convert.ToInt32(Session["Owner_Id"].ToString());

                    BindUnofficialSchooling(ownerId);
                    BindFamilyEdu(ownerId);
                    BindChildSchool(ownerId);
                    BindSchoolReachTime(ownerId);
                    BindSchoolStatus(ownerId);

                    btnNext.Enabled = false;
                    btnNext.Visible = false;
                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;

                }
            }
        }

        private void BindSchoolStatus(int ownerId)
        {
            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objEdinfoDal.FetchSchoolStatus(objEducationInfo);

            if (dt != null && dt.Rows.Count > 0)
            {
                rdoFrndlyToiletPresnt.SelectedIndex = Convert.ToInt32(dt.Rows[0]["CHILD_FRIENDLY_TOILET"].ToString());
                rdoSpecEduForDisable.SelectedIndex = Convert.ToInt32(dt.Rows[0]["SPC_EDU_FOR_DISABLED"].ToString());
            }
        }

        private void BindChildSchool(int ownerId)
        {
             objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objEdinfoDal.FetchChildSchool(objEducationInfo);

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dt.Rows[i]["SEX_ID"].ToString()) == 0)
                    {
                        if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 0)
                        {
                            txtPraBoy.Text = dt.Rows[i]["PRIMARY_CHILD_CENTER"].ToString();
                            txtPraVdcSchBoy.Text = dt.Rows[i]["VDC_SCHOOL"].ToString(); 
                            txtPraOutVdcSchBoy.Text = dt.Rows[i]["NON_VDC_SCHOOL"].ToString();
                        }
                        if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 1)
                        {
                            txtNiBoy.Text = dt.Rows[i]["PRIMARY_CHILD_CENTER"].ToString();
                            txtNiVdcSchBoy.Text = dt.Rows[i]["VDC_SCHOOL"].ToString();
                            txtNiOutVdcSchBoy.Text = dt.Rows[i]["NON_VDC_SCHOOL"].ToString();
                        }
                        if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 2)
                        {
                            txtMabiBoy.Text = dt.Rows[i]["PRIMARY_CHILD_CENTER"].ToString();
                            txtMabiVdcSchBoy.Text = dt.Rows[i]["VDC_SCHOOL"].ToString();
                            txtMabiOutVdcSchBoy.Text = dt.Rows[i]["NON_VDC_SCHOOL"].ToString();
                        }
                        if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 3)
                        {
                            txtHghrBoy.Text = dt.Rows[i]["PRIMARY_CHILD_CENTER"].ToString();
                            txtHghrVdcBoy.Text = dt.Rows[i]["VDC_SCHOOL"].ToString();
                            txtHghrOutVdcBoy.Text = dt.Rows[i]["NON_VDC_SCHOOL"].ToString();
                        }
                        
                    }

                    if (Convert.ToInt32(dt.Rows[i]["SEX_ID"].ToString()) == 1)
                    {
                        if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 0)
                        {
                            txtPraGirl.Text = dt.Rows[i]["PRIMARY_CHILD_CENTER"].ToString();
                            txtPraVdcSchGirl.Text = dt.Rows[i]["VDC_SCHOOL"].ToString();
                            txtPraOutVdcSchGirl.Text = dt.Rows[i]["NON_VDC_SCHOOL"].ToString();
                        }
                        if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 1)
                        {
                            txtNiGirl.Text = dt.Rows[i]["PRIMARY_CHILD_CENTER"].ToString();
                            txtNiVdcSchGirl.Text = dt.Rows[i]["VDC_SCHOOL"].ToString();
                            txtNiOutVdcSchGirl.Text = dt.Rows[i]["NON_VDC_SCHOOL"].ToString();
                        }
                        if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 2)
                        {
                            txtMabiGirl.Text = dt.Rows[i]["PRIMARY_CHILD_CENTER"].ToString();
                            txtMabiVdcSchGirl.Text = dt.Rows[i]["VDC_SCHOOL"].ToString();
                            txtMabiOutVdcSchGirl.Text = dt.Rows[i]["NON_VDC_SCHOOL"].ToString();
                        }
                        if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 3)
                        {
                            txtHghrGirl.Text = dt.Rows[i]["PRIMARY_CHILD_CENTER"].ToString();
                            txtHghrVdcGirl.Text = dt.Rows[i]["VDC_SCHOOL"].ToString();
                            txtHghrOutVdcGirl.Text = dt.Rows[i]["NON_VDC_SCHOOL"].ToString();
                        }
                    }
                }
            }
            
        }

        private void BindSchoolReachTime(int ownerId)
        {
            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objEdinfoDal.FetchSchoolReachTime(objEducationInfo);

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 0)
                    {
                        rdoTimToRchPra.SelectedIndex = Convert.ToInt32(dt.Rows[i]["DURATION_ID"].ToString());

                    }
                    if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 1)
                    {
                        rdoTimToRchNima.SelectedIndex = Convert.ToInt32(dt.Rows[i]["DURATION_ID"].ToString());

                    }
                    if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 2)
                    {
                        rdoTimToRchMabi.SelectedIndex = Convert.ToInt32(dt.Rows[i]["DURATION_ID"].ToString());

                    }
                    if (Convert.ToInt32(dt.Rows[i]["LVL_ID"].ToString()) == 3)
                    {
                        rdoTimToRchHigher.SelectedIndex = Convert.ToInt32(dt.Rows[i]["DURATION_ID"].ToString());

                    }
                }

            }
        }

        private void BindFamilyEdu(int ownerId)
        {
            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objEdinfoDal.FetchFamilyEdu(objEducationInfo);

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dt.Rows[i]["SEX_ID"].ToString()) == 0)
                        {
                            if (Convert.ToInt32(dt.Rows[i]["AGE_ID"].ToString()) == 0)
                            {
                                txtIletratMaleG0.Text = dt.Rows[i]["ILLITERATE"].ToString();
                                txtLtrtMaleG0.Text = dt.Rows[i]["LITERATE"].ToString();
                                txtSlcPsMaleG0.Text = dt.Rows[i]["SLC_PASSED"].ToString();
                                txtPlsPsMaleG0.Text = dt.Rows[i]["PROF_CERT_LVL"].ToString();
                                txtBchlrMaleG0.Text =  dt.Rows[i]["BACHELOR_LVL"].ToString();
                                txtMsPsMaleG0.Text =  dt.Rows[i]["MASTERS_LVL"].ToString();
                                txtPhdPsMaleG0.Text = dt.Rows[i]["PHD"].ToString();
                            }
                        
                             if (Convert.ToInt32(dt.Rows[i]["AGE_ID"].ToString()) == 1)
                            {
                                txtIletratMaleG1.Text = dt.Rows[i]["ILLITERATE"].ToString();
                                txtLtrtMaleG1.Text = dt.Rows[i]["LITERATE"].ToString();
                                txtSlcPsMaleG1.Text = dt.Rows[i]["SLC_PASSED"].ToString();
                                txtPlsPsMaleG1.Text = dt.Rows[i]["PROF_CERT_LVL"].ToString();
                                txtBchlrMaleG1.Text =  dt.Rows[i]["BACHELOR_LVL"].ToString();
                                txtMsPsMaleG1.Text =  dt.Rows[i]["MASTERS_LVL"].ToString();
                                txtPhdPsMaleG1.Text = dt.Rows[i]["PHD"].ToString();
                            }
                             if (Convert.ToInt32(dt.Rows[i]["AGE_ID"].ToString()) == 2)
                                {
                                    txtIletratMaleG2.Text = dt.Rows[i]["ILLITERATE"].ToString();
                                    txtLtrtMaleG2.Text = dt.Rows[i]["LITERATE"].ToString();
                                    txtSlcPsMaleG2.Text = dt.Rows[i]["SLC_PASSED"].ToString();
                                    txtPlsPsMaleG2.Text = dt.Rows[i]["PROF_CERT_LVL"].ToString();
                                    txtBchlrMaleG2.Text = dt.Rows[i]["BACHELOR_LVL"].ToString();
                                    txtMsPsMaleG2.Text = dt.Rows[i]["MASTERS_LVL"].ToString();
                                    txtPhdPsMaleG2.Text = dt.Rows[i]["PHD"].ToString();
                                }
                             if (Convert.ToInt32(dt.Rows[i]["AGE_ID"].ToString()) == 3)
                                {
                                    txtIletratMaleG3.Text = dt.Rows[i]["ILLITERATE"].ToString();
                                    txtLtrtMaleG3.Text = dt.Rows[i]["LITERATE"].ToString();
                                    txtSlcPsMaleG3.Text = dt.Rows[i]["SLC_PASSED"].ToString();
                                    txtPlsPsMaleG3.Text = dt.Rows[i]["PROF_CERT_LVL"].ToString();
                                    txtBchlrMaleG3.Text = dt.Rows[i]["BACHELOR_LVL"].ToString();
                                    txtMsPsMaleG3.Text = dt.Rows[i]["MASTERS_LVL"].ToString();
                                    txtPhdPsMaleG3.Text = dt.Rows[i]["PHD"].ToString();
                                }

                        }

                    if (Convert.ToInt32(dt.Rows[i]["SEX_ID"].ToString()) == 1)
                    {
                        if (Convert.ToInt32(dt.Rows[i]["AGE_ID"].ToString()) == 0)
                        {
                            txtIletratFemaleG0.Text = dt.Rows[i]["ILLITERATE"].ToString();
                            txtLtrtFemaleG0.Text = dt.Rows[i]["LITERATE"].ToString();
                            txtSlcPsFemaleG0.Text = dt.Rows[i]["SLC_PASSED"].ToString();
                            txtPlsPsFemaleG0.Text = dt.Rows[i]["PROF_CERT_LVL"].ToString();
                            txtBchlrFemaleG0.Text = dt.Rows[i]["BACHELOR_LVL"].ToString();
                            txtMsPsFemaleG0.Text = dt.Rows[i]["MASTERS_LVL"].ToString();
                            txtPhdPsFemaleG0.Text = dt.Rows[i]["PHD"].ToString();
                        }

                        if (Convert.ToInt32(dt.Rows[i]["AGE_ID"].ToString()) == 1)
                        {
                            txtIletratFemaleG1.Text = dt.Rows[i]["ILLITERATE"].ToString();
                            txtLtrtFemaleG1.Text = dt.Rows[i]["LITERATE"].ToString();
                            txtSlcPsFemaleG1.Text = dt.Rows[i]["SLC_PASSED"].ToString();
                            txtPlsPsFemaleG1.Text = dt.Rows[i]["PROF_CERT_LVL"].ToString();
                            txtBchlrFemaleG1.Text = dt.Rows[i]["BACHELOR_LVL"].ToString();
                            txtMsPsFemaleG1.Text = dt.Rows[i]["MASTERS_LVL"].ToString();
                            txtPhdPsFemaleG1.Text = dt.Rows[i]["PHD"].ToString();
                        }
                        if (Convert.ToInt32(dt.Rows[i]["AGE_ID"].ToString()) == 2)
                        {
                            txtIletratFemaleG2.Text = dt.Rows[i]["ILLITERATE"].ToString();
                            txtLtrtFemaleG2.Text = dt.Rows[i]["LITERATE"].ToString();
                            txtSlcPsFemaleG2.Text = dt.Rows[i]["SLC_PASSED"].ToString();
                            txtPlsPsFemaleG2.Text = dt.Rows[i]["PROF_CERT_LVL"].ToString();
                            txtBchlrFemaleG2.Text = dt.Rows[i]["BACHELOR_LVL"].ToString();
                            txtMsPsFemaleG2.Text = dt.Rows[i]["MASTERS_LVL"].ToString();
                            txtPhdPsFemaleG2.Text = dt.Rows[i]["PHD"].ToString();
                        }
                        if (Convert.ToInt32(dt.Rows[i]["AGE_ID"].ToString()) == 3)
                        {
                            txtIletratFemaleG3.Text = dt.Rows[i]["ILLITERATE"].ToString();
                            txtLtrtFemaleG3.Text = dt.Rows[i]["LITERATE"].ToString();
                            txtSlcPsFemaleG3.Text = dt.Rows[i]["SLC_PASSED"].ToString();
                            txtPlsPsFemaleG3.Text = dt.Rows[i]["PROF_CERT_LVL"].ToString();
                            txtBchlrFemaleG3.Text = dt.Rows[i]["BACHELOR_LVL"].ToString();
                            txtMsPsFemaleG3.Text = dt.Rows[i]["MASTERS_LVL"].ToString();
                            txtPhdPsFemaleG3.Text = dt.Rows[i]["PHD"].ToString();
                        }

                    }
                }
            }
           

        }

        private void BindUnofficialSchooling(int ownerId)
        {
            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objEdinfoDal.FetchUnoffSchooling(objEducationInfo);

            if (dt != null && dt.Rows.Count > 0)
            {
                txtBoyCnt.Text = dt.Rows[0]["MALE_CHILD_COUNT"].ToString();
                txtGrlCnt.Text = dt.Rows[0]["FEMALE_CHILD_COUNT"].ToString();
                txtOthrCnt.Text = dt.Rows[0]["OTHER_COUNT"].ToString();
            }

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            objEducationInfo = new EducationInfoBo();
            int value;
            //Session["Owner_Id"] = 99;
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());

            objEducationInfo.BoyCount = Convert.ToInt32(txtBoyCnt.Text);
            objEducationInfo.GirlCount = Convert.ToInt32(txtGrlCnt.Text);
            objEducationInfo.OtherCount = Convert.ToInt32(txtOthrCnt.Text);
            value = objEdinfoDal.InsertUnofficialSchool(objEducationInfo);

            
            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0; //For Male
                objEducationInfo.AgeId = 0;
                objEducationInfo.Literate = Convert.ToInt32(txtLtrtMaleG0.Text);
                objEducationInfo.Illeterate = Convert.ToInt32(txtIletratMaleG0.Text);
                objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsMaleG0.Text);
                objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsMaleG0.Text);
                objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrMaleG0.Text);
                objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsMaleG0.Text);
                objEducationInfo.PhdPass =  Convert.ToInt32(txtPhdPsMaleG0.Text);
                value = objEdinfoDal.InsertFamilyEduInfo(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 0;
                objEducationInfo.AgeId = 1;
                objEducationInfo.Literate = Convert.ToInt32(txtLtrtMaleG1.Text);
                objEducationInfo.Illeterate = Convert.ToInt32(txtIletratMaleG1.Text);
                objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsMaleG1.Text);
                objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsMaleG1.Text);
                objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrMaleG1.Text);
                objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsMaleG1.Text);
                objEducationInfo.PhdPass =  Convert.ToInt32(txtPhdPsMaleG1.Text);
                value = objEdinfoDal.InsertFamilyEduInfo(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 0;
                objEducationInfo.AgeId = 2;
                objEducationInfo.Literate = Convert.ToInt32(txtLtrtMaleG2.Text);
                objEducationInfo.Illeterate = Convert.ToInt32(txtIletratMaleG2.Text);
                objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsMaleG2.Text);
                objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsMaleG2.Text);
                objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrMaleG2.Text);
                objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsMaleG2.Text);
                objEducationInfo.PhdPass =  Convert.ToInt32(txtPhdPsMaleG2.Text);
                value = objEdinfoDal.InsertFamilyEduInfo(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 0;
                objEducationInfo.AgeId = 3;
                objEducationInfo.Literate = Convert.ToInt32(txtLtrtMaleG3.Text);
                objEducationInfo.Illeterate = Convert.ToInt32(txtIletratMaleG3.Text);
                objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsMaleG3.Text);
                objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsMaleG3.Text);
                objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrMaleG3.Text);
                objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsMaleG3.Text);
                objEducationInfo.PhdPass =  Convert.ToInt32(txtPhdPsMaleG3.Text);
                value = objEdinfoDal.InsertFamilyEduInfo(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;  //For Female
                objEducationInfo.AgeId = 0;
                objEducationInfo.Literate = Convert.ToInt32(txtLtrtFemaleG0.Text);
                objEducationInfo.Illeterate = Convert.ToInt32(txtIletratFemaleG0.Text);
                objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsFemaleG0.Text);
                objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsFemaleG0.Text);
                objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrFemaleG0.Text);
                objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsFemaleG0.Text);
                objEducationInfo.PhdPass =  Convert.ToInt32(txtPhdPsFemaleG0.Text);
                value = objEdinfoDal.InsertFamilyEduInfo(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 1; 
                objEducationInfo.AgeId = 1;
                objEducationInfo.Literate = Convert.ToInt32(txtLtrtFemaleG1.Text);
                objEducationInfo.Illeterate = Convert.ToInt32(txtIletratFemaleG1.Text);
                objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsFemaleG1.Text);
                objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsFemaleG1.Text);
                objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrFemaleG1.Text);
                objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsFemaleG1.Text);
                objEducationInfo.PhdPass =  Convert.ToInt32(txtPhdPsFemaleG1.Text);
                value = objEdinfoDal.InsertFamilyEduInfo(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 1; 
                objEducationInfo.AgeId = 2;
                objEducationInfo.Literate = Convert.ToInt32(txtLtrtFemaleG2.Text);
                objEducationInfo.Illeterate = Convert.ToInt32(txtIletratFemaleG2.Text);
                objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsFemaleG2.Text);
                objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsFemaleG2.Text);
                objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrFemaleG2.Text);
                objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsFemaleG2.Text);
                objEducationInfo.PhdPass =  Convert.ToInt32(txtPhdPsFemaleG2.Text);
                value = objEdinfoDal.InsertFamilyEduInfo(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 1; 
                objEducationInfo.AgeId = 3;
                objEducationInfo.Literate = Convert.ToInt32(txtLtrtFemaleG3.Text);
                objEducationInfo.Illeterate = Convert.ToInt32(txtIletratFemaleG3.Text);
                objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsFemaleG3.Text);
                objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsFemaleG3.Text);
                objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrFemaleG3.Text);
                objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsFemaleG3.Text);
                objEducationInfo.PhdPass =  Convert.ToInt32(txtPhdPsFemaleG3.Text);
                value = objEdinfoDal.InsertFamilyEduInfo(objEducationInfo);


                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0;  //For Boys
                objEducationInfo.LvlId = 0;
                objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtPraBoy.Text);
                objEducationInfo.VdcSchool = Convert.ToInt32(txtPraVdcSchBoy.Text);
                objEducationInfo.NonVdcSchool = Convert.ToInt32(txtPraOutVdcSchBoy.Text);
                value = objEdinfoDal.InsertChildSchool(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 0;
                objEducationInfo.LvlId = 1;
                objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtNiBoy.Text);
                objEducationInfo.VdcSchool = Convert.ToInt32(txtNiVdcSchBoy.Text);
                objEducationInfo.NonVdcSchool = Convert.ToInt32(txtNiOutVdcSchBoy.Text);
                value = objEdinfoDal.InsertChildSchool(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 0;
                objEducationInfo.LvlId = 2;
                objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtMabiBoy.Text);
                objEducationInfo.VdcSchool = Convert.ToInt32(txtMabiVdcSchBoy.Text);
                objEducationInfo.NonVdcSchool = Convert.ToInt32(txtMabiOutVdcSchBoy.Text);
                value = objEdinfoDal.InsertChildSchool(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 0;
                objEducationInfo.LvlId = 3;
                objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtHghrBoy.Text);
                objEducationInfo.VdcSchool = Convert.ToInt32(txtHghrVdcBoy.Text);
                objEducationInfo.NonVdcSchool = Convert.ToInt32(txtHghrOutVdcBoy.Text);
                value = objEdinfoDal.InsertChildSchool(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;  //For Girls
                objEducationInfo.LvlId = 0;
                objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtPraGirl.Text);
                objEducationInfo.VdcSchool = Convert.ToInt32(txtPraVdcSchGirl.Text);
                objEducationInfo.NonVdcSchool = Convert.ToInt32(txtPraOutVdcSchGirl.Text);
                value = objEdinfoDal.InsertChildSchool(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 1;
                objEducationInfo.LvlId = 1;
                objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtNiGirl.Text);
                objEducationInfo.VdcSchool = Convert.ToInt32(txtNiVdcSchGirl.Text);
                objEducationInfo.NonVdcSchool = Convert.ToInt32(txtNiOutVdcSchGirl.Text);
                value = objEdinfoDal.InsertChildSchool(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 1;
                objEducationInfo.LvlId = 2;
                objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtMabiGirl.Text);
                objEducationInfo.VdcSchool = Convert.ToInt32(txtMabiVdcSchGirl.Text);
                objEducationInfo.NonVdcSchool = Convert.ToInt32(txtMabiOutVdcSchGirl.Text);
                value = objEdinfoDal.InsertChildSchool(objEducationInfo);

                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objEducationInfo.SexId = 1;
                objEducationInfo.LvlId = 3;
                objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtHghrGirl.Text);
                objEducationInfo.VdcSchool = Convert.ToInt32(txtHghrVdcGirl.Text);
                objEducationInfo.NonVdcSchool = Convert.ToInt32(txtHghrOutVdcGirl.Text);
                value = objEdinfoDal.InsertChildSchool(objEducationInfo);


                objEducationInfo = new EducationInfoBo();
                objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.LvlId = 0;
                objEducationInfo.Duration = rdoTimToRchPra.SelectedIndex;
                value = objEdinfoDal.InsertTimeToSchool(objEducationInfo);
            objEducationInfo.LvlId = 1;
            objEducationInfo.Duration = rdoTimToRchNima.SelectedIndex; 
            value = objEdinfoDal.InsertTimeToSchool(objEducationInfo);
            objEducationInfo.LvlId = 2;
                objEducationInfo.Duration = rdoTimToRchMabi.SelectedIndex;
                value = objEdinfoDal.InsertTimeToSchool(objEducationInfo);
            objEducationInfo.LvlId = 3;
                objEducationInfo.Duration = rdoTimToRchHigher.SelectedIndex;
            value = objEdinfoDal.InsertTimeToSchool(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SpcEduForDisabled = rdoSpecEduForDisable.SelectedIndex;
            objEducationInfo.ChldFrndlyTlt = rdoFrndlyToiletPresnt.SelectedIndex;
            value = objEdinfoDal.InsertSchoolStatus(objEducationInfo);

            Response.Redirect("EducationSection2.aspx");
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            objEducationInfo = new EducationInfoBo();
            UpdateUnOffSchool(objEducationInfo);
            objEducationInfo = new EducationInfoBo();
            UpdateFamilyEdu(objEducationInfo);
            objEducationInfo = new EducationInfoBo();
            UpdateChildSchool(objEducationInfo);
            objEducationInfo = new EducationInfoBo();
            UpdateTimeToSchool(objEducationInfo);
            objEducationInfo = new EducationInfoBo();
            UpdateSchoolStatus(objEducationInfo);
            Response.Redirect("EducationSection2.aspx?Owner_Id="+Session["Owner_Id"]);
        }

        private void UpdateSchoolStatus(EducationInfoBo objEducationInfo)
        {
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SpcEduForDisabled = rdoSpecEduForDisable.SelectedIndex;
            objEducationInfo.ChldFrndlyTlt = rdoFrndlyToiletPresnt.SelectedIndex;
            objEdinfoDal.UpdateSchoolStatus(objEducationInfo);
        }

        private void UpdateTimeToSchool(EducationInfoBo objEducationInfo)
        {
            int value;
            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.LvlId = 0;
            objEducationInfo.Duration = rdoTimToRchPra.SelectedIndex;
            value = objEdinfoDal.UpdateTimeToSchool(objEducationInfo);
            objEducationInfo.LvlId = 1;
            objEducationInfo.Duration = rdoTimToRchNima.SelectedIndex;
            value = objEdinfoDal.UpdateTimeToSchool(objEducationInfo);
            objEducationInfo.LvlId = 2;
            objEducationInfo.Duration = rdoTimToRchMabi.SelectedIndex;
            value = objEdinfoDal.UpdateTimeToSchool(objEducationInfo);
            objEducationInfo.LvlId = 3;
            objEducationInfo.Duration = rdoTimToRchHigher.SelectedIndex;
            value = objEdinfoDal.UpdateTimeToSchool(objEducationInfo);
        }

        private void UpdateChildSchool(EducationInfoBo objEducationInfo)
        {
            int value;
            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0;  //For Boys
            objEducationInfo.LvlId = 0;
            objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtPraBoy.Text);
            objEducationInfo.VdcSchool = Convert.ToInt32(txtPraVdcSchBoy.Text);
            objEducationInfo.NonVdcSchool = Convert.ToInt32(txtPraOutVdcSchBoy.Text);
            value = objEdinfoDal.UpdateChildSchool(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0;
            objEducationInfo.LvlId = 1;
            objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtNiBoy.Text);
            objEducationInfo.VdcSchool = Convert.ToInt32(txtNiVdcSchBoy.Text);
            objEducationInfo.NonVdcSchool = Convert.ToInt32(txtNiOutVdcSchBoy.Text);
            value = objEdinfoDal.UpdateChildSchool(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0;
            objEducationInfo.LvlId = 2;
            objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtMabiBoy.Text);
            objEducationInfo.VdcSchool = Convert.ToInt32(txtMabiVdcSchBoy.Text);
            objEducationInfo.NonVdcSchool = Convert.ToInt32(txtMabiOutVdcSchBoy.Text);
            value = objEdinfoDal.UpdateChildSchool(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0;
            objEducationInfo.LvlId = 3;
            objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtHghrBoy.Text);
            objEducationInfo.VdcSchool = Convert.ToInt32(txtHghrVdcBoy.Text);
            objEducationInfo.NonVdcSchool = Convert.ToInt32(txtHghrOutVdcBoy.Text);
            value = objEdinfoDal.UpdateChildSchool(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;  //For Girls
            objEducationInfo.LvlId = 0;
            objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtPraGirl.Text);
            objEducationInfo.VdcSchool = Convert.ToInt32(txtPraVdcSchGirl.Text);
            objEducationInfo.NonVdcSchool = Convert.ToInt32(txtPraOutVdcSchGirl.Text);
            value = objEdinfoDal.UpdateChildSchool(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;
            objEducationInfo.LvlId = 1;
            objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtNiGirl.Text);
            objEducationInfo.VdcSchool = Convert.ToInt32(txtNiVdcSchGirl.Text);
            objEducationInfo.NonVdcSchool = Convert.ToInt32(txtNiOutVdcSchGirl.Text);
            value = objEdinfoDal.UpdateChildSchool(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;
            objEducationInfo.LvlId = 2;
            objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtMabiGirl.Text);
            objEducationInfo.VdcSchool = Convert.ToInt32(txtMabiVdcSchGirl.Text);
            objEducationInfo.NonVdcSchool = Convert.ToInt32(txtMabiOutVdcSchGirl.Text);
            value = objEdinfoDal.UpdateChildSchool(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;
            objEducationInfo.LvlId = 3;
            objEducationInfo.PrimaryChildCenter = Convert.ToInt32(txtHghrGirl.Text);
            objEducationInfo.VdcSchool = Convert.ToInt32(txtHghrVdcGirl.Text);
            objEducationInfo.NonVdcSchool = Convert.ToInt32(txtHghrOutVdcGirl.Text);
            value = objEdinfoDal.UpdateChildSchool(objEducationInfo);
        }

        private void UpdateFamilyEdu(EducationInfoBo objEducationInfo)
        {
            int value;

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0; //For Male
            objEducationInfo.AgeId = 0;
            objEducationInfo.Literate = Convert.ToInt32(txtLtrtMaleG0.Text);
            objEducationInfo.Illeterate = Convert.ToInt32(txtIletratMaleG0.Text);
            objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsMaleG0.Text);
            objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsMaleG0.Text);
            objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrMaleG0.Text);
            objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsMaleG0.Text);
            objEducationInfo.PhdPass = Convert.ToInt32(txtPhdPsMaleG0.Text);
            value = objEdinfoDal.UpdateFamilyEduInfo(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0;
            objEducationInfo.AgeId = 1;
            objEducationInfo.Literate = Convert.ToInt32(txtLtrtMaleG1.Text);
            objEducationInfo.Illeterate = Convert.ToInt32(txtIletratMaleG1.Text);
            objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsMaleG1.Text);
            objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsMaleG1.Text);
            objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrMaleG1.Text);
            objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsMaleG1.Text);
            objEducationInfo.PhdPass = Convert.ToInt32(txtPhdPsMaleG1.Text);
            value = objEdinfoDal.UpdateFamilyEduInfo(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0;
            objEducationInfo.AgeId = 2;
            objEducationInfo.Literate = Convert.ToInt32(txtLtrtMaleG2.Text);
            objEducationInfo.Illeterate = Convert.ToInt32(txtIletratMaleG2.Text);
            objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsMaleG2.Text);
            objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsMaleG2.Text);
            objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrMaleG2.Text);
            objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsMaleG2.Text);
            objEducationInfo.PhdPass = Convert.ToInt32(txtPhdPsMaleG2.Text);
            value = objEdinfoDal.UpdateFamilyEduInfo(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 0;
            objEducationInfo.AgeId = 3;
            objEducationInfo.Literate = Convert.ToInt32(txtLtrtMaleG3.Text);
            objEducationInfo.Illeterate = Convert.ToInt32(txtIletratMaleG3.Text);
            objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsMaleG3.Text);
            objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsMaleG3.Text);
            objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrMaleG3.Text);
            objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsMaleG3.Text);
            objEducationInfo.PhdPass = Convert.ToInt32(txtPhdPsMaleG3.Text);
            value = objEdinfoDal.UpdateFamilyEduInfo(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;  //For Female
            objEducationInfo.AgeId = 0;
            objEducationInfo.Literate = Convert.ToInt32(txtLtrtFemaleG0.Text);
            objEducationInfo.Illeterate = Convert.ToInt32(txtIletratFemaleG0.Text);
            objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsFemaleG0.Text);
            objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsFemaleG0.Text);
            objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrFemaleG0.Text);
            objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsFemaleG0.Text);
            objEducationInfo.PhdPass = Convert.ToInt32(txtPhdPsFemaleG0.Text);
            value = objEdinfoDal.UpdateFamilyEduInfo(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;
            objEducationInfo.AgeId = 1;
            objEducationInfo.Literate = Convert.ToInt32(txtLtrtFemaleG1.Text);
            objEducationInfo.Illeterate = Convert.ToInt32(txtIletratFemaleG1.Text);
            objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsFemaleG1.Text);
            objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsFemaleG1.Text);
            objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrFemaleG1.Text);
            objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsFemaleG1.Text);
            objEducationInfo.PhdPass = Convert.ToInt32(txtPhdPsFemaleG1.Text);
            value = objEdinfoDal.UpdateFamilyEduInfo(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;
            objEducationInfo.AgeId = 2;
            objEducationInfo.Literate = Convert.ToInt32(txtLtrtFemaleG2.Text);
            objEducationInfo.Illeterate = Convert.ToInt32(txtIletratFemaleG2.Text);
            objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsFemaleG2.Text);
            objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsFemaleG2.Text);
            objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrFemaleG2.Text);
            objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsFemaleG2.Text);
            objEducationInfo.PhdPass = Convert.ToInt32(txtPhdPsFemaleG2.Text);
            value = objEdinfoDal.UpdateFamilyEduInfo(objEducationInfo);

            objEducationInfo = new EducationInfoBo();
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.SexId = 1;
            objEducationInfo.AgeId = 3;
            objEducationInfo.Literate = Convert.ToInt32(txtLtrtFemaleG3.Text);
            objEducationInfo.Illeterate = Convert.ToInt32(txtIletratFemaleG3.Text);
            objEducationInfo.SlcPassed = Convert.ToInt32(txtSlcPsFemaleG3.Text);
            objEducationInfo.ProfCertLvl = Convert.ToInt32(txtPlsPsFemaleG3.Text);
            objEducationInfo.BachelorLvl = Convert.ToInt32(txtBchlrFemaleG3.Text);
            objEducationInfo.MasterLvl = Convert.ToInt32(txtMsPsFemaleG3.Text);
            objEducationInfo.PhdPass = Convert.ToInt32(txtPhdPsFemaleG3.Text);
            value = objEdinfoDal.UpdateFamilyEduInfo(objEducationInfo);


        }

        private void UpdateUnOffSchool(EducationInfoBo objEducationInfo)
        {
            objEducationInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objEducationInfo.BoyCount = Convert.ToInt32(txtBoyCnt.Text);
            objEducationInfo.GirlCount = Convert.ToInt32(txtGrlCnt.Text);
            objEducationInfo.OtherCount = Convert.ToInt32(txtOthrCnt.Text);

            objEdinfoDal.UpdateUnoffSchool(objEducationInfo);
        }
    }
}