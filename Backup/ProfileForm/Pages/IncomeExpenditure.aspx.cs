﻿using Common.Entity;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class IncomeExpenditure : System.Web.UI.Page
    {
        string str = null;
        int i = 0;
        IncomeExpenditureBo objBo = new IncomeExpenditureBo();
        IncomeExpenditureDao objDal = new IncomeExpenditureDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                   // Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    btnNext.Visible = false;
                    btnUpdate.Visible = true;
                    LoadIncomeSource(OwnerId);
                    LoadAnnualExpenditure(OwnerId);
                    LoadAgroIncome(OwnerId);
                    LoadIsLoan(OwnerId);
                    LoadLoanPurpose(OwnerId);
                }


            }
            else
            {
                btnNext.Visible = true;
                btnUpdate.Visible = false;
            }

        }

        private void LoadLoanPurpose(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchLoanPurpose(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidLoanDetailsId.Value = dt.Rows[0]["LOAN_DETAIL_ID"].ToString();
                rbdInstitution.SelectedIndex = dt.Rows[0]["LOAN_DONOR_TYPE_ID"].GetHashCode();
               //int[] ab= dt.Rows[0]["LOAN_PURPOSE_TYPE_ID"].GetHashCode();
                string a = Convert.ToString(dt.Rows[0]["LOAN_PURPOSE_TYPE_ID"].ToString());
                 string[] array = a.Split(',');
                 //int[] array = stringArray[];

                 //chkLoanUse.SelectedIndex = array.


                 foreach (var str in array)
                 {
                    // if (Convert.ToInt32(array[i]) == 0) 
                     int i = Convert.ToInt32(str);
                     chkLoanUse.Items[i].Selected = true;
                    
                 }
                
                

            }
        }

        private void LoadIsLoan(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchIsLoan(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidLoanId.Value = dt.Rows[0]["LOAN_ID"].ToString();
               
                rbdLoan.SelectedIndex = dt.Rows[0]["IS_LOAN"].GetHashCode();
            } 
        }

        private void LoadAgroIncome(int OwnerId)
        {

            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchAgroIncome(objBo);
            if(dt!=null && dt.Rows.Count>0)
            {
                hidAgroIncomeId.Value = dt.Rows[0]["ARGO_INCOME_ID"].ToString();
                rbdAgriTime.SelectedIndex = dt.Rows[0]["INCOME_COVER_TIME_ID"].GetHashCode();
            }
        }

        private void LoadAnnualExpenditure(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchAnnualExpenditure(objBo);
            if(dt!=null && dt.Rows.Count>0)
            {
                txtFooding.Text = dt.Rows[0]["FOODING"].ToString();
                txtClothing.Text = dt.Rows[0]["CLOTHING"].ToString();
                txtEducation.Text = dt.Rows[0]["EDUCATION"].ToString();
                txtHealth.Text = dt.Rows[0]["HEALTH"].ToString();
                txtFestivity.Text = dt.Rows[0]["FESTIVITY"].ToString();
                txtPaymentCharge.Text = dt.Rows[0]["PAYMENT_CHARGE"].ToString();
                txtOthersExpenditure.Text = dt.Rows[0]["OTHERS"].ToString();
                hidAnnualExpenditureId.Value = dt.Rows[0]["ANNUAL_EXPENDITURE_ID"].ToString();
                

            }
        }

        private void LoadIncomeSource(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchIncomeSource(objBo);
            if(dt!=null  && dt.Rows.Count>0)
            {
                txtAgriculture.Text = dt.Rows[0]["AGRICULTURE"].ToString();
                txtBusiness.Text = dt.Rows[0]["BUSINESS"].ToString();
                txtJobs.Text = dt.Rows[0]["JOBS"].ToString();
                txtForeignEmployment.Text = dt.Rows[0]["FOREIGN_EMPLOYMENT"].ToString();
                txtWageLabour.Text = dt.Rows[0]["WAGE_LABOUR"].ToString();
                txtHouseRent.Text = dt.Rows[0]["HOUSE_RENT"].ToString();
                txtOthersIncome.Text = dt.Rows[0]["OTHERS"].ToString();
                hidIncomeSourceId.Value = dt.Rows[0]["INCOME_SOURCE_ID"].ToString();
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            //saving annual income
            try
            {
                //Session["Owner_ID"] = 99;
                objBo.OwnerId = Session["Owner_Id"].GetHashCode();
                objBo.Agriculture = Convert.ToInt32(txtAgriculture.Text);
                objBo.Business = Convert.ToInt32(txtBusiness.Text);
                objBo.Jobs = Convert.ToInt32(txtJobs.Text);
                objBo.ForeignEmployment = Convert.ToInt32(txtForeignEmployment.Text);
                objBo.WageLabour = Convert.ToInt32(txtWageLabour.Text);
                objBo.HouseRent = Convert.ToInt32(txtHouseRent.Text);
                objBo.OthersIncome = Convert.ToInt32(txtOthersIncome.Text);

                int i = objDal.SaveAnnualIncome(objBo);
            }catch(Exception ex)
            {
                
            }



            //Annual Expenditure
            try
            {
               // Session["Owner_ID"] = 99;
                objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                objBo.Fooding = Convert.ToInt32(txtFooding.Text);
                objBo.Clothing = Convert.ToInt32(txtClothing.Text);
                objBo.Education = Convert.ToInt32(txtEducation.Text);
                objBo.Health = Convert.ToInt32(txtHealth.Text);
                objBo.Festivity = Convert.ToInt32(txtFestivity.Text);
                objBo.PaymentCharge = Convert.ToInt32(txtPaymentCharge.Text);
                objBo.OthersExpenditure = Convert.ToInt32(txtOthersExpenditure.Text);

                int result = objDal.SaveAnnualExpenditure(objBo);
            }catch(Exception ex)
            {
                
            }


            //Agro Income
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.IncomeCoverTimeId = rbdAgriTime.SelectedIndex;
            objDal.SaveAgroIncome(objBo);

            //Is Loan Taken
            //objBo.OwnerId = Session["Owner_Id"].GetHashCode();
            objBo.IsLoan = rbdLoan.SelectedIndex;
            objDal.SaveIsLoanInfo(objBo);

            ////Loan Donor
            //objBo.OwnerId = Session["Owner_Id"].GetHashCode();
            objBo.LoanDonorTypeId = rbdInstitution.SelectedIndex;
            
            


            
            for (i = 0; i < chkLoanUse.Items.Count; i++)
            {
                if (chkLoanUse.Items[i].Selected)
                {
                    if (str == null)
                    {
                        str = chkLoanUse.Items[i].Value;
                    }
                    else
                        str +=","+chkLoanUse.Items[i].Value;


                }
            }
            
            objBo.LoanPurposeTypeId = str;
            objDal.SaveLoanDonor(objBo);
            
                Response.Redirect("AgroProducts.aspx");
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //Session["Owner_ID"] = 99;
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.Fooding = Convert.ToInt32(txtFooding.Text);
            objBo.Clothing = Convert.ToInt32(txtClothing.Text);
            objBo.Education = Convert.ToInt32(txtEducation.Text);
            objBo.Health = Convert.ToInt32(txtHealth.Text);
            objBo.Festivity = Convert.ToInt32(txtFestivity.Text);
            objBo.PaymentCharge = Convert.ToInt32(txtPaymentCharge.Text);
            objBo.OthersExpenditure = Convert.ToInt32(txtOthersExpenditure.Text);
            objBo.AnnualExpenditureId = Convert.ToInt32(hidAnnualExpenditureId.Value);

            int result = objDal.UpdateAnnualExpenditure(objBo);


            //objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.IncomeCoverTimeId = rbdAgriTime.SelectedIndex;
            objBo.AgroIncomeId = Convert.ToInt32(hidAgroIncomeId.Value);
            objDal.UpdateAgroIncome(objBo);


            //Session["Owner_ID"] = 99;
            //objBo.OwnerId = Convert.ToInt32(Session["Owner_ID"]);
            objBo.Agriculture = Convert.ToInt32(txtAgriculture.Text);
            objBo.Business = Convert.ToInt32(txtBusiness.Text);
            objBo.Jobs = Convert.ToInt32(txtJobs.Text);
            objBo.ForeignEmployment = Convert.ToInt32(txtForeignEmployment.Text);
            objBo.WageLabour = Convert.ToInt32(txtWageLabour.Text);
            objBo.HouseRent = Convert.ToInt32(txtHouseRent.Text);
            objBo.OthersIncome = Convert.ToInt32(txtOthersIncome.Text);
            objBo.IncomeSourceId = Convert.ToInt32(hidIncomeSourceId.Value);

            int i = objDal.UpdateAnnualIncome(objBo);

            //Is Loan Taken
            //objBo.OwnerId = Convert.ToInt32(Session["Owner_ID"]);
            objBo.IsLoan = rbdLoan.SelectedIndex;
            objBo.IsLoan = Convert.ToInt32(hidLoanId.Value);

            objDal.UpdateIsLoanInfo(objBo);



            //objBo.OwnerId = Convert.ToInt32(Session["Owner_ID"]);
            objBo.LoanDonorTypeId = rbdInstitution.SelectedIndex;
            objBo.LoanDetailsId = Convert.ToInt32(hidLoanDetailsId.Value);





            for (i = 0; i < chkLoanUse.Items.Count; i++)
            {
                if (chkLoanUse.Items[i].Selected)
                {
                    if (str == null)
                    {
                        str = chkLoanUse.Items[i].Value;
                    }
                    else
                        str += "," + chkLoanUse.Items[i].Value;


                }
            }
            

            objBo.LoanPurposeTypeId = str;
            objDal.UpdateLoanDonor(objBo);
            Response.Redirect("AgroProducts.aspx?Owner_Id="+Session["Owner_Id"]);

        }


    }
}