﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AgroProducts.aspx.cs" Inherits="ProfileForm.Pages.LastSection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-striped">
        <tr>
            <td>
                ७१.
            </td>
            <td>
                तपाईंको परिवारमा निम्न कार्यहरु प्रायः कसले गर्दछ ?
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                घर व्यवहारसम्बन्धी विषयमा निर्णय
            </td>
            <td>
                <asp:RadioButtonList ID="rdoHousHoldDecision" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">पुरुष </asp:ListItem>
                    <asp:ListItem Value="1"> महिला </asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                घरायसी काममा संलग्न
            </td>
            <td>
                <asp:RadioButtonList ID="rdoHouseHldInvolved" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">पुरुष </asp:ListItem>
                    <asp:ListItem Value="1"> महिला </asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                बैंकमा खाता संचालन
            </td>
            <td>
                <asp:RadioButtonList ID="rdoBankAcc" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">पुरुष </asp:ListItem>
                    <asp:ListItem Value="1"> महिला </asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                उपभोक्ता समितिमा सहभागिता
            </td>
            <td>
                <asp:RadioButtonList ID="rdoConsumerCom" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">पुरुष </asp:ListItem>
                    <asp:ListItem Value="1"> महिला </asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                विद्यालय व्यवस्थापन समितिको पदाधिकारीका रुपमा सहभागिता
            </td>
            <td>
                <asp:RadioButtonList ID="rdoSchoolMngmnt" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">पुरुष </asp:ListItem>
                    <asp:ListItem Value="1"> महिला </asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                उद्योग व्यापारमा सहभागिता
            </td>
            <td>
                <asp:RadioButtonList ID="rdoBusinessParticipation" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">पुरुष </asp:ListItem>
                    <asp:ListItem Value="1"> महिला </asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td>
            </td>
        </tr>
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <td>
                ७२.
            </td>
            <td>
                तपाईँको परिवारको वार्षिक रुपमा अन्नबाली उत्पादनको अवस्था कस्तो छ?
            </td>
            <td>
                उत्पादन
            </td>
            <td>
                Fiscal Year:
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table class="table table-bordered">
        <tr>
            <td>
            </td>
            <td>
                वाली/उत्पादन समूह
            </td>
            <td>
                क्षेत्रफल (हेक्टर)
            </td>
            <td>
                उत्पादन (क्विन्टल) वा रु. अन्न
            </td>
        </tr>
        <asp:Repeater ID="rptrCrops" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCrops" runat="server" DataSourceID="XmlDataSource_Crops"
                            DataTextField="name" SelectedValue='<%#Eval("CROPS_ID") %>' DataValueField="id">
                        </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Crops" runat="server" DataFile="~/XMLDataSource/Crops.xml">
                        </asp:XmlDataSource>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtArea" Text='<%# Eval("LAND_AREA") %>'> </asp:TextBox>
                    <td>
                        <asp:TextBox runat="server" ID="txtIncome" Text='<%# Eval("INCOME") %>'>
                        </asp:TextBox>
                    </td>
                    <td>
                       
                    </td>
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="btnAddMemberOccupation"
                    OnClick="btnAddCropsIncome_OnClick" />
            </td>
        </tr>
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <td>
            </td>
            <td>
                तपाईँको परिवारको बार्षीक तरकारी जन्य(प्याज/काउली/लसुन/च्याउ/अदुवा) उत्पादनको अवस्था
                कस्तो छ?
            </td>
            <td>
                उत्पादन
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <table class="table table-bordered">
        <tr>
            <td>
            </td>
            <td>
                वाली/उत्पादन समूह
            </td>
            <td>
                क्षेत्रफल (हेक्टर)
            </td>
            <td>
                उत्पादन (क्विन्टल) वा रु. अन्न
            </td>
        </tr>
        <asp:Repeater ID="rptrVeg" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlName" runat="server" DataSourceID="XmlDataSource_Vegetables"
                            DataTextField="name" SelectedValue='<%#Eval("VEG_ID") %>' DataValueField="id">
                        </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Vegetables" runat="server" DataFile="~/XMLDataSource/Vegetables.xml">
                        </asp:XmlDataSource>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtArea" Text='<%# Eval("LAND_AREA") %>'> </asp:TextBox>
                    <td>
                        <asp:TextBox runat="server" ID="txtIncome" Text='<%# Eval("INCOME") %>'>
                        </asp:TextBox>
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hidVegIncId" Value='<%# Eval("VEG_INCOME_ID") %>'>
                        </asp:HiddenField>
                    </td>
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="Button2"
                    OnClick="btnAddVegetablesIncome_OnClick" />
            </td>
        </tr>
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <td>
            </td>
            <td>
                तपाईँको परिवारको वार्षिक रुपमा नगदे बालीको उत्पादनको अवस्था कस्तो छ?
            </td>
            <td>
                उत्पादन
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <table class="table table-bordered">
        <tr>
            <td>
            </td>
            <td>
                वाली/उत्पादन समूह
            </td>
            <td>
                क्षेत्रफल (हेक्टर)
            </td>
            <td>
                उत्पादन (क्विन्टल) वा रु. अन्न
            </td>
        </tr>
        <asp:Repeater ID="rptrCashCrops" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlName" runat="server" DataSourceID="XmlDataSource_CashCrops"
                            DataTextField="name" SelectedValue='<%#Eval("CASHCROPS_ID") %>' DataValueField="id">
                        </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_CashCrops" runat="server" DataFile="~/XMLDataSource/CashCrops.xml">
                        </asp:XmlDataSource>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtArea" Text='<%# Eval("LAND_AREA") %>'> </asp:TextBox>
                    <td>
                        <asp:TextBox runat="server" ID="txtIncome" Text='<%# Eval("INCOME") %>'>
                        </asp:TextBox>
                    </td> 
                    <td>
                        <asp:HiddenField runat="server" ID="hidCashCropIncId" Value='<%# Eval("CASHCROPS_INCOME_ID") %>'>
                        </asp:HiddenField>
                    </td> 
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="Button1"
                    OnClick="btnAddCashCropsIncome_OnClick" />
            </td>
        </tr>
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <td>
            </td>
            <td>
                तपाईँको परिवारको बार्षिक रुपमा फलफुल उत्पादनको अवस्था कस्तो छ?
            </td>
            <td>
                उत्पादन
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <table class="table table-bordered">
        <tr>
            <td>
            </td>
            <td>
                वाली/उत्पादन समूह
            </td>
            <td>
                क्षेत्रफल (हेक्टर)
            </td>
            <td>
                उत्पादन (क्विन्टल) वा रु. अन्न
            </td>
        </tr>
        <asp:Repeater ID="rptrFruits" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlName" runat="server" DataSourceID="XmlDataSource_Fruits"
                            DataTextField="name" SelectedValue='<%#Eval("FRUITS_ID") %>' DataValueField="id">
                        </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Fruits" runat="server" DataFile="~/XMLDataSource/Fruits.xml">
                        </asp:XmlDataSource>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtArea" Text='<%# Eval("LAND_AREA") %>'> </asp:TextBox>
                    <td>
                        <asp:TextBox runat="server" ID="txtIncome" Text='<%# Eval("INCOME") %>'>
                        </asp:TextBox>
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hidFruitsIncId" Value='<%# Eval("FRUITS_INCOME_ID") %>'>
                        </asp:HiddenField>
                    </td>
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="Button3"
                    OnClick="btnAddFruitsIncome_OnClick" />
            </td>
        </tr>
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <td>
            </td>
            <td>
                वाली/उत्पादन समूह
            </td>
            <td>
                क्षेत्रफल (हेक्टर)
            </td>
            <td>
                उत्पादन (क्विन्टल) वा रु. अन्न
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                घासे वाली डाले घाँस
            </td>
            <td>
                <asp:TextBox ID="txtGhaseLnd" runat="server" Text="0"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtGhaseIncome" runat="server" Text="0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                घासे वाली BHUI घाँस
            </td>
            <td>
                <asp:TextBox ID="txtBhuiLnd" runat="server" Text="0"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtBhuiIncome" runat="server" Text="0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                रेशम खेती
            </td>
            <td>
                <asp:TextBox ID="txtResamLnd" runat="server" Text="0"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtResamIncome" runat="server" Text="0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                जडिवुटी खेती खेती
            </td>
            <td>
                <asp:TextBox ID="txtJdbLnd" runat="server" Text="0"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtJdbIncome" runat="server" Text="0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                अन्य
            </td>
            <td>
                <asp:TextBox ID="txtOtherLnd" runat="server" Text="0"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtOtherIncome" runat="server" Text="0"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <td>
                ७३.
            </td>
            <td>
                परिवारको पशु धन तथा सोबाट हुने उत्पादन?
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <td>
                क्र. सं.
            </td>
            <td>
                किसिम
            </td>
            <td>
                संख्या
            </td>
            <td>
                उत्पादन
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            स्थानीय
                        </td>
                        <td>
                            उन्नत
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            दूध लि.
                        </td>
                        <td>
                            मासु केजी
                        </td>
                        <td>
                            उन केजी
                        </td>
                        <td>
                            अण्डा सख्या
                        </td>
                        <td>
                            मह केजी
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <asp:Repeater ID="rptrAnimalIncome" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlName" runat="server" DataSourceID="XmlDataSource_Animals"
                            DataTextField="name" SelectedValue='<%#Eval("ANIMALS_ID") %>' DataValueField="id">
                        </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Animals" runat="server" DataFile="~/XMLDataSource/Animals.xml">
                        </asp:XmlDataSource>
                    </td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="txtLocal" Text='<%# Eval("ANIMALS_LOCAL") %>' TextMode="Number"
                                        Width="50"> </asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtHybrid" Text='<%# Eval("ANIMALS_HYBRID") %>' TextMode="Number"
                                        Width="50">
                                    </asp:TextBox>
                                </td>
                                
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMilk" Text='<%# Eval("MILK_PRODUCTION") %>' TextMode="Number"
                                        Width="50"> </asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMeat" Text='<%# Eval("MEAT_PRODUCTION") %>' TextMode="Number"
                                        Width="50">
                                    </asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtWool" Text='<%# Eval("WOOL_PRODUCTION") %>' TextMode="Number"
                                        Width="50"> </asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtEgg" Text='<%# Eval("EGG_PRODUCTION") %>' TextMode="Number"
                                        Width="50">
                                    </asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtHoney" Text='<%# Eval("HONEY_PRODUCTION") %>'
                                        TextMode="Number" Width="50">
                                    </asp:TextBox>
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hidAnimalId" Value='<%# Eval("ANIMALS_INCOME_ID") %>'>
                                    </asp:HiddenField>
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="Button4"
                    OnClick="btnAddAnimalsIncome_OnClick" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" />
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnNext_OnClick"
                    OnClientClick="return validatePage();" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary"
                    OnClick="btnUpdate_OnClick" OnClientClick="return validatePage();" Visible="False" />
            </td>
        </tr>
    </table>
</asp:Content>
