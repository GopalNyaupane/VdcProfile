﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TeenAgeHealth.aspx.cs" Inherits="ProfileForm.Pages.TeenAgeHealth" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <table class="table table-bordered">
                 <tr>
                <td>१९.</td>
                <td> तपाईको परिवारको १८ वर्षभन्दा मुनिको सदस्यलाई कुनै दीर्घ रोग लागेको छ ? </td>
                <td>  <asp:RadioButtonList ID="rdoIsDisease" runat="server" RepeatDirection="Horizontal">
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                        
                                </asp:RadioButtonList></td>
                               

            </tr>
            <tr>
                <td> </td>
                <td> परिवारको १८ वर्षभन्दा मुनिको कति जना सदस्यलाई कुनै दीर्घ रोग लागेको छ? </td>
                <td><asp:TextBox runat="server" Text="0" TextMode="Number" ID ="txtTeenCount" Width="80"></asp:TextBox>
                <asp:Button runat="server" Text="Ok" ID="btnOk" OnClick="btnOk_OnClick"/></td>
            </tr>
            </table>
            <table class="table table-striped">
                 <tr>
                <td>सि. नं.</td>
                <td>  रोगको नाम </td>
                <td> लिङ्ग   </td>
               
                
               
                                

            </tr>
             <asp:Panel runat="server" ID="pnlTeenAgeHealth">
                <asp:Repeater runat="server" ID="RpterTeenAgeHealth" OnItemCreated="RpterTeenAgeHealth_ItemCreated">
                    <ItemTemplate>
                        <tr>
                            <td></td>
                            
                            <td>
                                
                            <asp:DropDownList ID="ddlDisease" runat="server" DataSourceID="XmlDataSource_Disease"
                                    DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DISEASE_ID")%>'>
                                </asp:DropDownList>
                                <asp:XmlDataSource ID="XmlDataSource_Disease" runat="server" DataFile="~/XMLDataSource/Disease.xml">
                                </asp:XmlDataSource>
                            </td>
                                
                                 <td>
                                <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID")%>'>
                                    <asp:ListItem Value="0">बालक </asp:ListItem>
                                    <asp:ListItem Value="1">बालिका</asp:ListItem>
                                    <asp:ListItem Value="2">तेस्रो लिंगी</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            
                           <td>
                                <asp:HiddenField runat="server" ID="hidTeenageHealthId" Value='<%#Eval("TEENAGE_HEALTH_ID") %>' />
                                
                            </td>
                            
                            
                            
                            
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </asp:Panel>
            </table>
            <table class="table table-striped">
                <tr>
                <td> २०.</td>
                <td> तपाईको परिवारमा दीर्घ रोगको कारण विगत १० वर्षमा १८ वर्ष भन्दामुनिका कुनै सदस्यको मृत्यु भएको छ ?</td>
                <td> <asp:RadioButtonList ID="rdoIsTeenFatality" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ")%>'>
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>
                                 <tr>
                <td> </td>
                <td> तपाईको परिवारमा दीर्घ रोगको कारण विगत १० वर्षमा १८ वर्ष भन्दामुनिका कति जना सदस्यको मृत्यु भएको छ? </td>
                <td><asp:TextBox runat="server" Text="0" TextMode="Number" ID ="txtTeenFatalityCount" Width="80"></asp:TextBox>
                <asp:Button runat="server" Text="Ok" ID="btnFatalityOk" OnClick="btnFatalityOk_OnClick"/></td>
            </tr>

            </tr>
           
            </table>
            
             <table class="table table-striped">
                 <tr>
                <td>सि. नं.</td>
                <td>  मृत्यु गराउने रोगको नाम </td>
                <td> लिङ्ग   </td>
               
                
               
                                

            </tr>
             <asp:Panel runat="server" ID="Panel1">
                <asp:Repeater runat="server" ID="rptrTeenFatality">
                    <ItemTemplate>
                        <tr>
                            <td></td>
                            
                            <td>
                                
                            <asp:DropDownList ID="ddlDisease" runat="server" DataSourceID="XmlDataSource_Disease"
                                    DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DISEASE_ID")%>'>
                                </asp:DropDownList>
                                <asp:XmlDataSource ID="XmlDataSource_Disease" runat="server" DataFile="~/XMLDataSource/Disease.xml">
                                </asp:XmlDataSource>
                            </td>
                                
                                 <td>
                                <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID")%>'>
                                    <asp:ListItem Value="0">बालक </asp:ListItem>
                                    <asp:ListItem Value="1">बालिका</asp:ListItem>
                                    <asp:ListItem Value="2">तेस्रो लिंगी</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <asp:HiddenField runat="server" ID="hidTeenageFatalityId" Value='<%#Eval("TEENAGE_FATALITY_ID") %>'/>
                            </td>
                            
                           
                            
                            
                            
                            
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </asp:Panel>
            </table>
            
            <table class="table table-striped">
            
             <tr>
                <td> २१.</td>
                <td> बितेको १ वर्षभित्र तपाईंको परिवारमा ५ वर्षमुनिका कुनै बालबालिकाको मृत्यु भएको छ ?</td>
                <td> <asp:RadioButtonList ID="rdoIsChildFatality" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ")%>'>
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>

            </tr>
             <tr>
                <td> </td>
                <td> बितेको १ वर्षभित्र तपाईंको परिवारमा ५ वर्षमुनिका कति जना बालबालिकाको मृत्यु भएको छ? </td>
                <td><asp:TextBox runat="server" Text="0" TextMode="Number" ID ="txtChildfatalityCount" Width="80"></asp:TextBox>
                <asp:Button runat="server" Text="Ok" ID="btnChilDFatalityOk" OnClick="btnChilDFatalityOk_OnClick"/></td>
            </tr>
           
            </table>
             
            
             <table class="table table-striped">
                 <tr>
                <td>सि. नं.</td>
                <td>  मृत्यु गराउने रोगको नाम </td>
                <td> लिङ्ग   </td>
               
                
               
                                

            </tr>
             <asp:Panel runat="server" ID="Panel2">
                <asp:Repeater runat="server" ID="rptrChildFatality">
                    <ItemTemplate>
                        <tr>
                            <td></td>

                         
                            <td>
                                <asp:DropDownList ID="ddlDisease" runat="server" DataSourceID="XmlDataSource_Disease"
                                    DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DEAD_REASON")%>'>
                                </asp:DropDownList>
                                <asp:XmlDataSource ID="XmlDataSource_Disease" runat="server" DataFile="~/XMLDataSource/Disease.xml">
                                </asp:XmlDataSource>
                            
                            </td>
                                
                                 <td>
                                <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID")%>'>
                                    <asp:ListItem Value="0">बालक </asp:ListItem>
                                    <asp:ListItem Value="1">बालिका</asp:ListItem>
                                    <asp:ListItem Value="2">तेस्रो लिंगी</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <asp:HiddenField runat="server" ID="hidChildFatalityId" Value='<%#Eval("CHILD_FATALITY_ID") %>'/>
                            </td>
                            
                           
                            
                            
                            
                            
                        </tr>
                      
                        
                    </ItemTemplate>
                </asp:Repeater>
                </asp:Panel>
            </table>
            
    <table  class="table table-hover">
                        <tr>
                            <td>

                            </td>
                            <td>
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                            </td>
                            <td>
                                <asp:Button ID="Next" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="Next_Click"/>
                            </td>
                            <td>
                                <asp:Button ID="Update" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="Update_Click"/>
                            </td>
                        </tr>
                            </table>
</asp:Content>

