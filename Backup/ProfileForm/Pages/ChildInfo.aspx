﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChildInfo.aspx.cs" Inherits="ProfileForm.Pages.ChildInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../js/demo/dashboard-demo.js" type="text/javascript"></script>
    
    <%--<script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>--%>

     <div>
     Child Health Related
        <table class="table table-striped">
            <tr>
                <td>
                    १२. </td>
                <td>
                   बालबालिका बिरामी पर्दा तुरुन्त उपचार गर्ने आर्थिक क्षमता छ ?</td>
                
                <td>
                    <asp:RadioButtonList ID="rdoSex" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>

                    </asp:RadioButtonList>
                    
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hidChildInfo" Value='<%#Eval("CHILD_HEALTH_INFO")%>' />

                </td>
                
            </tr>
            </table>

            <table class="table table-bordered table-striped">
             <tr>
                <td colspan="3">
                    १३. 
                  <b>वितेको २ बर्षभित्र जन्मेका बालबालिकाको विवरण </b></td>
                
                </tr>
           
            
            <tr>
                <td>
                    </td>
                <td>
                    <b> बालक संख्या</b>
                </td>
                <td>
                  <b> बालिका संख्या</b></td>
                
               </tr>

                <tr>
                
                <td>
                     जीवित जन्मेका शिशुको संख्या कनि छ?</td>
                
                <td>
                   
                    <asp:TextBox ID="txtAliveBornMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtAliveBornFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                    जन्मने वितिक्कै बेगुति दुध खुवाएको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtBreastFeedBornTimeMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtBreastFeedBornTimeFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                    जन्मने वितिक्कै बेगुति दुध नखुवाएको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtNotBreastFeedBornTimeMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtNotBreastFeedBornTimeFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                    ६ महिनासम्म आमाको दुध मात्र खुवाएको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtBreastFeedOnlySixMonthMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtBreastFeedOnlySixMonthFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                   ६ महिनासम्म आमाको दुध र अन्य खानेकुरा पनि खुवाएको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtBreastFeedPlusOtherMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtBreastFeedPlusOtherFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
               <tr>
                
                <td>
                  २ महिना वा सो भन्दा कम मात्र आमाको दुध खुवाएको संख्या </td>
                
                <td>
                   
                    <asp:TextBox ID="txtBreastFeedLessTwoMonthMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtBreastFeedLessTwoMonthFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
               
               </table>
               

               <table class="table table-bordered table-striped">
              <tr>
                <td>
                    १४. </td>
                <td>
                  <b>तपाईंलाई बालरोग तथा खोपहरुको बिषयमा जानकारी छ ? </b></td>
                 <td>
                   <asp:RadioButtonList ID="rdoDisease" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                       
                    </asp:RadioButtonList>
                    
                </td>
                  <td>
                                         <asp:HiddenField runat="server" ID="hidChildBreastFeedId" Value='<%#Eval("CHILD_RECORD_ID")%>' />

                                     </td>
                </tr>
               </table>
               
               <table class="table table-bordered table-striped">
             <tr>
                <td colspan="3">
                    १५. 
                  <b>एक वर्षमुनिका बालबालिकालाई खोप लगाएको विवरण</b></td>
                
                </tr>
           
            
            <tr>
                <td>
                    </td>
                <td>
                    <b> बालक संख्या</b>
                </td>
                <td>
                  <b> बालिका संख्या</b></td>
                
               </tr>

                <tr>
                
                <td>
                     बि.सि.जी. (BCG) लगाएको संख्या</td>
                    
                <td>
                   
                    <asp:TextBox ID="txtBcgMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>


                   <td>
                    <asp:TextBox ID="txtBcgFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                     </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hidChildBreastFeedFemaleId" Value='<%#Eval("CHILD_RECORD_ID")%>' />

                    </td>


                
               </tr>
                <tr>
                
                <td>
                    डि.पि.टि १ (DPT1) लगाएको</td>
                
                <td>
                   
                    <asp:TextBox ID="txtDpt1Male" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtDpt1Female" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                   डि.पि.टि २ लगाएको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtDpt2Male" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtDpt2Female" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                  डि.पि.टि ३ लगाएको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtDpt3Male" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtDpt3Female" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
               <tr>
                
                <td>
                 दादुरा\रुबेला बिरुद्धको खोप लगाएको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtDadhuraMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtDadhuraFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                 हेपटाईटिस वी बिरुद्धको खोप लगाएको संख्या</td>
                    
                
                
                <td>
                   
                    <asp:TextBox ID="txtHbMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtHbFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
               
               </table>
               
               
                <table class="table table-bordered table-striped">
             <tr>
                <td colspan="3">
                    १६. 
                  <b>पाँच वर्षमुनिका बालबालिकालाई वर्षको २ पटक भिटामिन ए, जुकाको औषधी र पोलियो थोपा खुवाएको विवरण</b></td>
                </tr>
           
            
            <tr>
                <td>
                    </td>
                <td>
                    <b> बालक संख्या</b>
                </td>
                <td>
                  <b> बालिका संख्या</b></td>
                
               </tr>

                <tr>
                
                <td>
                    भिटामिन ए खुवाएका बालबालिकाको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtVitaminaMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtVitaminaFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                    जुकाको औषधी खुवाएका बालबालिकाको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtWormDrugsMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtWormDrugsFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
               </tr>
                <tr>
                
                <td>
                   पोलियो थोपा खुवाएका बालबालिकाको संख्या</td>
                
                <td>
                   
                    <asp:TextBox ID="txtPolioVaccineMale" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                   <td>
                    <asp:TextBox ID="txtPolioVaccineFemale" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                </td>
                    <td>
                                         <asp:HiddenField runat="server" ID="hidChildVaccinne" Value='<%#Eval("CHILD_VACCINE_ID")%>' />

                                     </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hidChildVaccineFemaleId" Value='<%#Eval("CHILD_VACCINE_ID")%>' />

                    </td>
               </tr>
                
               <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" 
                        onclick="btnUpdate_Click" OnClientClick="return validatePage();"/>
                   <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" 
                        onclick="btnNext_Click" OnClientClick="return validatePage();"/>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
               </table>
               
               
               

    </div>
</asp:Content>
