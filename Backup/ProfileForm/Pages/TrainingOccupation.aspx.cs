﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class TrainingOccupation : System.Web.UI.Page
    {
        TrainingOccupationBo objBo = new TrainingOccupationBo();
        TrainingOccupationDao objDao = new TrainingOccupationDao();
        //DataTable dtA = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Redirect("IncomeExpenditure.aspx");
            if (!IsPostBack)
            {
                //PopulateBudgetHead();               
                rptrFamilyAwarenessTraining.DataBind();               
                rptrEconomicTraining.DataBind();

                cblTeleSource.DataBind();
                if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadAwarenessDetails(Owner_Id);
                    LoadEconomicDetails(Owner_Id);
                    LoadOccupationDetails(Owner_Id);

                    btnUpdate.Visible = true;
                    btnNext.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                }
            }
        }

        private void LoadOccupationDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataSet ds = new DataSet();
            ds = objDao.Fetch_Occupation(objBo);
            DataTable dtOccu = null;
            DataTable dtMig = null;
            DataTable dtComm = null;
            DataTable dtStatus = null;
            DataTable dtReason = null;
           

            if (ds != null && ds.Tables.Count > 0)
            {
                dtOccu = ds.Tables[0];
                dtMig = ds.Tables[1];
                dtComm= ds.Tables[2];
                dtStatus = ds.Tables[3];
                dtReason = ds.Tables[4];


                hidOccupation.Value = dtOccu.Rows[0]["FAMILY_MAIN_OCCUPATION_ID"].ToString();
                rblOccupation.SelectedValue = dtOccu.Rows[0]["OCCUPATION_TYPE_ID"].ToString();

                hidShelter.Value = dtMig.Rows[0]["MIGRATION_ID"].ToString();
                rblPreShleter.SelectedValue = dtMig.Rows[0]["IS_MIGRATION"].ToString();

               
                hidComm.Value = dtComm.Rows[0]["COMMUNICATION_ID"].ToString();

                int[] selected = new int[10];

                selected[0] = Convert.ToInt32(dtComm.Rows[0]["IS_RADIO"].ToString());
                selected[1] = Convert.ToInt32(dtComm.Rows[0]["IS_TV"].ToString());
                selected[2] = Convert.ToInt32(dtComm.Rows[0]["IS_TELEPHONE"].ToString());
                selected[3] = Convert.ToInt32(dtComm.Rows[0]["IS_MOBILE"].ToString());
                selected[4] = Convert.ToInt32(dtComm.Rows[0]["IS_INTERNET"].ToString());
                selected[5] = Convert.ToInt32(dtComm.Rows[0]["IS_NEWSPAPER"].ToString());
                selected[6] = Convert.ToInt32(dtComm.Rows[0]["OTHERS"].ToString());
                

                for (int i = 0; i < cblTeleSource.Items.Count;i++ )
                {
                    if(selected[i]== 0)
                    {
                        cblTeleSource.Items[i].Selected  = true;

                    }
                }


                hidReason.Value = dtReason.Rows[0]["MIGRATION_REASON_ID"].ToString();
                rblMigrationReason.SelectedValue = dtReason.Rows[0]["REASON_TYPE_ID"].ToString();

                hidStatus.Value = dtStatus.Rows[0]["HOUSE_STATUS_ID"].ToString();
                rblRoof.SelectedValue = dtStatus.Rows[0]["ROOF_MATERIAL_ID"].ToString();
                rblLightSource.SelectedValue = dtStatus.Rows[0]["ELECTRICITY_SOURCE_ID"].ToString();
                rblFuel.SelectedValue = dtStatus.Rows[0]["FUEL_SOURCE_ID"].ToString();
                rblStove.SelectedValue = dtStatus.Rows[0]["STOVE_TYPE_ID"].ToString();

                //private int selectedindex[];                          

            }
            else
            {
                dtOccu = null;
                dtMig = null;
                dtComm = null;
                dtStatus = null;
                dtReason = null;

            }

        }

        private void LoadEconomicDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_Family_Economic(objBo);

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrEconomicTraining .DataSource = dt;
                rptrEconomicTraining.DataBind();

            }
        }
      
        private void LoadAwarenessDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_Family_Awareness(objBo);

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrFamilyAwarenessTraining .DataSource = dt;
                rptrFamilyAwarenessTraining.DataBind();

            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
           // Session["Owner_Id"] = 10;
            //inserting data into FAmily_awarness table
            List<TrainingOccupationBo> objList = new List<TrainingOccupationBo>();
            foreach (RepeaterItem rptItem in rptrFamilyAwarenessTraining.Items)
            {
                objBo = new TrainingOccupationBo();
                TextBox txtTrainingName = (TextBox)rptItem.FindControl("txtTrainingName");
                TextBox txtMaleNo = (TextBox)rptItem.FindControl("txtMaleNo");
                TextBox txtFemaleNo = (TextBox)rptItem.FindControl("txtFemaleNo");
                RadioButtonList rblDuration = (RadioButtonList)rptItem.FindControl("rblDuration");

                if (txtMaleNo.Text != "")
                {
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    objBo.trainingName = txtTrainingName.Text;
                    objBo.maleNo = Convert.ToInt32(txtMaleNo.Text);
                    objBo.femaleNo = Convert.ToInt32(txtFemaleNo.Text);
                    objBo.durationId = rblDuration.SelectedIndex;

                    objList.Add(objBo);

                }
            }
                objDao.Family_Awareness_Insert(objList);

            

            //Inserting code for table familly_economic
            List<TrainingOccupationBo> objEco = new List<TrainingOccupationBo>();
            foreach (RepeaterItem rItem in rptrEconomicTraining.Items)
            {
                objBo = new TrainingOccupationBo();

                TextBox ecoTrainingName = (TextBox)rItem.FindControl("txtEcoTrainingName");
                TextBox ecoMaleNo = (TextBox)rItem.FindControl("txtEcoMaleNo");
                TextBox ecoFemaleNo = (TextBox)rItem.FindControl("txtEcoFemaleNo");
                RadioButtonList rblEcoDuration = (RadioButtonList)rItem.FindControl("rblEcoDuration");


                if (ecoMaleNo.Text != "")
                {
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    objBo.ecoTrainingName = ecoTrainingName.Text;
                    objBo.ecoMaleNo = Convert.ToInt32(ecoMaleNo.Text);
                    objBo.ecoFemaleNo = Convert.ToInt32(ecoFemaleNo.Text);
                    objBo.ecoDurationId = rblEcoDuration.SelectedIndex;

                    objEco.Add(objBo);

                }
            }
                objDao.Family_Economic_Insert(objEco);

            

            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.occupationTypeId = rblOccupation.SelectedIndex;
           
            objBo.isMigration = rblPreShleter.SelectedIndex;

            objBo.reasonTypeId = rblMigrationReason.SelectedIndex;
            objBo.roofMaterialId = rblRoof.SelectedIndex;
            objBo.electricitySourceId = rblLightSource.SelectedIndex;
            objBo.stoveTypeId = rblStove.SelectedIndex;
            objBo.fuelSourceId = rblFuel.SelectedIndex;

            //Dada inserting for cheeckBoxList 
            objBo.isRadio = 1;
            objBo.isTV = 1;
            objBo.isTelePhone = 1;
            objBo.isMobile = 1;
            objBo.isInternet = 1;
            objBo.isNews = 1;
            objBo.others = 1;

            if (cblTeleSource.Items[0].Selected)
             {
                 objBo.isRadio = 0;  
             }                   
                
             if(cblTeleSource.Items[1].Selected)
             {
                     objBo.isTV = 0;
             }
                   
             if(cblTeleSource.Items[2].Selected)
             {
                       objBo.isTelePhone = 0;
             }
             if(cblTeleSource.Items[3].Selected)  
              {
                      objBo.isMobile = 0;
              }
              if(cblTeleSource.Items[4].Selected)  
              {
                      objBo.isInternet = 0;
              }
                if(cblTeleSource.Items[5].Selected)  
              {
                       objBo.isNews = 0;
              }
                 if(cblTeleSource.Items[6].Selected)  
              {
                        objBo.others = 0;
              }
                
                   
            int l = objDao.Occupation_Insert(objBo);

            if (l > 0)
            {
                Response.Redirect("MemberInfo.aspx");
            }
 
        }

        protected void btnAddNewTraining_OnClick(object sender, EventArgs e)
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("FAMILY_AWARENESS_TRAINING_ID");
            dtA.Columns.Add("TRAINING_NAME");
            dtA.Columns.Add("DURATION_ID");
            dtA.Columns.Add("MALE_NO");
            dtA.Columns.Add("FEMALE_NO");
            HiddenField hidAwareness = new HiddenField();
            RadioButtonList rblDuration = new RadioButtonList();
            TextBox txtTrainingName = new TextBox();
            TextBox txtMaleNo = new TextBox();
            TextBox txtFemaleNo = new TextBox();

            foreach (RepeaterItem rptItem in rptrFamilyAwarenessTraining.Items)
            {
                DataRow drA = dtA.NewRow();
                hidAwareness = (HiddenField)rptItem.FindControl("hidAwareness");
                rblDuration = (RadioButtonList)rptItem.FindControl("rblDuration");
                txtTrainingName = (TextBox)rptItem.FindControl("txtTrainingName");
                txtMaleNo = (TextBox)rptItem.FindControl("txtMaleNo");
                txtFemaleNo = (TextBox)rptItem.FindControl("txtFemaleNo");

                drA["FAMILY_AWARENESS_TRAINING_ID"] = hidAwareness.Value;
                drA["DURATION_ID"] = rblDuration.SelectedIndex;
                drA["TRAINING_NAME"] = txtTrainingName.Text;
                drA["MALE_NO"] = Convert.ToInt32(txtMaleNo.Text);
                drA["FEMALE_NO"] = Convert.ToInt32(txtFemaleNo.Text);

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["FAMILY_AWARENESS_TRAINING_ID"] = null;
           drB["TRAINING_NAME"] = null;
            drB["DURATION_ID"] = 0;
            drB["MALE_NO"] = 0;
            drB["FEMALE_NO"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrFamilyAwarenessTraining.DataSource = db;
            rptrFamilyAwarenessTraining.DataBind();

            rptrFamilyAwarenessTraining.DataSource = dtA;
            rptrFamilyAwarenessTraining.DataBind();
        }

        protected void btnAddNewEconomicTraining_OnClick(object sender, EventArgs e)
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("FAMILY_ECONOMIC_TRAINING_ID");
            dtA.Columns.Add("TRAINING_NAME");
            dtA.Columns.Add("DURATION_ID");
            dtA.Columns.Add("MALE_NO");
            dtA.Columns.Add("FEMALE_NO");
            HiddenField hidEconomic = new HiddenField();
            RadioButtonList rblDuration = new RadioButtonList();
            TextBox txtTrainingName = new TextBox();
            TextBox txtMaleNo = new TextBox();
            TextBox txtFemaleNo = new TextBox();

            foreach (RepeaterItem rptItem in rptrEconomicTraining.Items)
            {
                DataRow drA = dtA.NewRow();
                hidEconomic = (HiddenField)rptItem.FindControl("hidEconomic");
                rblDuration = (RadioButtonList)rptItem.FindControl("rblEcoDuration");
                txtTrainingName = (TextBox)rptItem.FindControl("txtEcoTrainingName");
                txtMaleNo = (TextBox)rptItem.FindControl("txtEcoMaleNo");
                txtFemaleNo = (TextBox)rptItem.FindControl("txtEcoFemaleNo");

                drA["FAMILY_ECONOMIC_TRAINING_ID"] = hidEconomic.Value;
                drA["DURATION_ID"] = rblDuration.SelectedIndex;
                drA["TRAINING_NAME"] = txtTrainingName.Text;
                drA["MALE_NO"] = Convert.ToInt32 (txtMaleNo.Text);
                drA["FEMALE_NO"] = Convert.ToInt32(txtFemaleNo.Text);

               

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["FAMILY_ECONOMIC_TRAINING_ID"] = null;
            drB["TRAINING_NAME"] = null;
            drB["DURATION_ID"] = 0;
            drB["MALE_NO"] = 0;
            drB["FEMALE_NO"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrEconomicTraining.DataSource = db;
            rptrEconomicTraining.DataBind();

            rptrEconomicTraining.DataSource = dtA;
            rptrEconomicTraining.DataBind();
        }

        public void btnUpdate_Click(object sender, EventArgs e)
        {

           
             foreach (RepeaterItem rptItem in rptrFamilyAwarenessTraining .Items)
            {
                objBo = new TrainingOccupationBo();
                HiddenField hidAwareness = (HiddenField)rptItem.FindControl("hidAwareness");
                TextBox txtTrainingName = (TextBox)rptItem.FindControl("txtTrainingName");
                TextBox txtMaleNo = (TextBox)rptItem.FindControl("txtMaleNo");
                TextBox txtFemaleNo = (TextBox)rptItem.FindControl("txtFemaleNo");
                RadioButtonList rblDuration = (RadioButtonList)rptItem.FindControl("rblDuration");

               
                    objBo.hidAwareness = Convert.ToInt32(hidAwareness.Value);
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    objBo.trainingName = txtTrainingName.Text;
                    objBo.maleNo = Convert.ToInt32(txtMaleNo.Text);
                    objBo.femaleNo = Convert.ToInt32(txtFemaleNo.Text);
                    objBo.durationId = rblDuration.SelectedIndex;

                 objDao.Update_Family_Awareness(objBo);
                }

             foreach (RepeaterItem rptItem in rptrEconomicTraining.Items)
             {
                 objBo = new TrainingOccupationBo();
                 HiddenField hidEconomic = (HiddenField)rptItem.FindControl("hidEconomic");
                 TextBox txtTrainingName = (TextBox)rptItem.FindControl("txtEcoTrainingName");
                 TextBox txtMaleNo = (TextBox)rptItem.FindControl("txtEcoMaleNo");
                 TextBox txtFemaleNo = (TextBox)rptItem.FindControl("txtEcoFemaleNo");
                 RadioButtonList rblDuration = (RadioButtonList)rptItem.FindControl("rblEcoDuration");

                
                 objBo.hidEconomic = Convert.ToInt32(hidEconomic.Value);
                 objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                 objBo.ecoTrainingName = txtTrainingName.Text;
                 objBo.ecoMaleNo = Convert.ToInt32(txtMaleNo.Text);
                 objBo.ecoFemaleNo = Convert.ToInt32(txtFemaleNo.Text);
                 objBo.ecoDurationId = rblDuration.SelectedIndex;

                 objDao.Update_Family_Economic(objBo);
             }

            
             objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.hidOccupation =Convert.ToInt32( hidOccupation.Value);
            objBo.occupationTypeId = rblOccupation.SelectedIndex;
           
             objBo.hidMigration  =Convert.ToInt32(hidShelter.Value);
            objBo.isMigration = rblPreShleter.SelectedIndex;

             objBo.hidReason  =Convert.ToInt32(hidReason.Value);
            objBo.reasonTypeId = rblMigrationReason.SelectedIndex;

             objBo.hidStatus =Convert.ToInt32(hidStatus.Value);
            objBo.roofMaterialId = rblRoof.SelectedIndex;
            objBo.electricitySourceId = rblLightSource.SelectedIndex;
            objBo.stoveTypeId = rblStove.SelectedIndex;
            objBo.fuelSourceId = rblFuel.SelectedIndex;

            objBo.hidComm  =Convert.ToInt32(hidComm.Value);

           objBo.isRadio = 1;
            objBo.isTV = 1;
            objBo.isTelePhone = 1;
            objBo.isMobile = 1;
            objBo.isInternet = 1;
            objBo.isNews = 1;
            objBo.others = 1;

            if (cblTeleSource.Items[0].Selected)
             {
                 objBo.isRadio = 0;  
             }                   
                
             if(cblTeleSource.Items[1].Selected)
             {
                     objBo.isTV = 0;
             }
                   
             if(cblTeleSource.Items[2].Selected)
             {
                       objBo.isTelePhone = 0;
             }
             if(cblTeleSource.Items[3].Selected)  
              {
                      objBo.isMobile = 0;
              }
              if(cblTeleSource.Items[4].Selected)  
              {
                      objBo.isInternet = 0;
              }
                if(cblTeleSource.Items[5].Selected)  
              {
                       objBo.isNews = 0;
              }
                 if(cblTeleSource.Items[6].Selected)  
              {
                        objBo.others = 0;
              }
            
            int j = objDao.Update_Occupation(objBo);

            if (j > 0)
            {
                Response.Redirect("MemberInfo.aspx?Owner_Id="+Session["Owner_Id"]);
            }
        }
       

    }
}