﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Info.aspx.cs" Inherits="ProfileForm.Pages.Info" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <script type="text/javascript">
       function validatePage() {
           var check = false;

           // Check only simple textbox for now
           if ($("#MainContent_ddlVDC").val() == '-गाविस छान्नुहोस्-') {
               alert("गाविस छान्नुहोस");
               check = false;
           }

           else if ($("#MainContent_ddlWoda").val() == '0') {
               alert("वडा छान्नुहोस");
               check = false;
           }
           else if ($("#MainContent_txtHouseNo").val() == '') {
               alert("घर नं. राख्नुहोस");
               check = false;
           } else {
               check = true;
           }

           if (check == false) {
               //alert('Missing data. Please complete');
               return false;
           }
           else {
               $("#MainContent_btnNext").val('Processing...');
               //                $("#ContentPlaceHolder1_btnNext").attr("disabled", true);
               return true;
           }
       }

    </script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <%--<script src="../Validation/Info.js" type="text/javascript"></script>--%>
    <div>
        <strong style="text-align: center">गाउँ पार्श्वचित्र निर्माण लागि तयार गरिएको घरधुरी
            सर्वेक्षण फाराम</strong><br />
        <%-- ID No.<br />
        <asp:TextBox ID="txtHomeID" runat="server" ></asp:TextBox>
        <br />--%>
        <table class="table" width="100%">
            <tr>
                <td width="10%">
                    १.
                </td>
                <td width="40%">
                    गाविस :
                </td>
                <td width="50%">
                    <asp:DropDownList ID="ddlVDC" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td width="10%">
                    २.
                </td>
                <td width="40%">
                    वडा नं. :
                </td>
                <td width="50%">
                    <asp:DropDownList ID="ddlWoda" runat="server">
                        <asp:ListItem Value="0">-वडा नं. छान्नुहोस्-</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                        <%--<asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                        <asp:ListItem Value="17">17</asp:ListItem>
                        <asp:ListItem Value="18">18</asp:ListItem>
                        <asp:ListItem Value="19">19</asp:ListItem>
                        <asp:ListItem Value="20">20</asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
            </tr>
             <tr>
                <td>
                    ३.</td>
                <td>
                    टोल बस्तीको नाम :</td>
                <td>
                    <asp:TextBox ID="txtToleBasti" runat="server"></asp:TextBox>
                </td>
            </tr>
          <%--  <tr>
                <td>
                    ४.</td>
                <td>
                    घरमुली को नाम :</td>
                <td>
                    <asp:TextBox ID="txtGharmuli" runat="server"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td>
                    ४.
                </td>
                <td>
                    घर नं.:
                </td>
                <td>
                    <asp:TextBox ID="txtHouseNo" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;<asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary"
                        OnClick="btnNext_Click" OnClientClick="return validatePage();" />
                        &nbsp;<asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary"
                        OnClick="btnUpdate_Click" OnClientClick="return validatePage();" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
