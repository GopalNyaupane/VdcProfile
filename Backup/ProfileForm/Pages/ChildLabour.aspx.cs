﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class ChildLabour : System.Web.UI.Page
    {
        ChildLabourBo objBo = new ChildLabourBo();
        ChildLabourDao objDao = new ChildLabourDao();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                //PopulateBudgetHead();              
                
                rptrChildLabour.DataBind();
                
                if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadLabourDetails(Owner_Id);
                    LoadLabourExpDetails(Owner_Id);

                    btnUpdate.Visible = true;
                    btnNext.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                }
            }
            btnAddAnother.Enabled = false;
            
          
         }


       
       protected void btnAddAnother_Click(object sender, EventArgs e)
        {
            DataTable dtChildLabour = new DataTable();
            dtChildLabour.Clear();
            dtChildLabour.Columns.Add("CHILD_LABOUR_ID");
            dtChildLabour.Columns.Add("AGE");
            dtChildLabour.Columns.Add("SEX_ID");
            dtChildLabour.Columns.Add("LABOUR_TYPE_ID");
            dtChildLabour.Columns.Add("WORK_HOUR");
            dtChildLabour.Columns.Add("WORK_YEAR");
            dtChildLabour.Columns.Add("YEARLY_INCOME");
            dtChildLabour.Columns.Add("ANNUAL_VISIT");
            dtChildLabour.Columns.Add("IS_SCHOOL");

            HiddenField hidLabour = new HiddenField();
            TextBox txtAge = new TextBox();
            RadioButtonList rblSex = new RadioButtonList();
            RadioButtonList rblLabourType = new RadioButtonList();
            TextBox txtWorkHour = new TextBox();
            TextBox txtWorkYear = new TextBox();
            TextBox txtYearlyIncome = new TextBox();
            TextBox txtVisit = new TextBox();
            RadioButtonList rblIsSchool = new RadioButtonList();

             foreach (RepeaterItem rptItem in rptrChildLabour.Items)
            {
                DataRow drA = dtChildLabour.NewRow();
                hidLabour = (HiddenField)rptItem.FindControl("hidLabour");
                txtAge = (TextBox)rptItem.FindControl("txtAge");
                rblSex = (RadioButtonList)rptItem.FindControl("rblSex");
                rblLabourType  = (RadioButtonList)rptItem.FindControl("rblLabourType");
                txtWorkHour = (TextBox)rptItem.FindControl("txtWorkHour");
                txtWorkYear = (TextBox)rptItem.FindControl("txtWorkYear");
                txtYearlyIncome = (TextBox)rptItem.FindControl("txtYearlyIncome");
                txtVisit = (TextBox)rptItem.FindControl("txtVisit");
                rblIsSchool = (RadioButtonList)rptItem.FindControl("rblIsSchool");

                drA["CHILD_LABOUR_ID"] = hidLabour.Value;
                drA["AGE"] = txtAge.Text;
                drA["SEX_ID"] = rblSex.SelectedIndex;
                drA["LABOUR_TYPE_ID"] = rblLabourType.SelectedIndex;
                drA["WORK_HOUR"] = txtWorkHour.Text;
                drA["WORK_YEAR"] = txtWorkYear.Text;
                drA["YEARLY_INCOME"] = txtYearlyIncome.Text;
                drA["ANNUAL_VISIT"] = txtVisit.Text;
                drA["IS_SCHOOL"] = rblLabourType.SelectedIndex;

               
               
                dtChildLabour.Rows.Add(drA);
            }
            DataRow drB = dtChildLabour.NewRow();

            drB["CHILD_LABOUR_ID"] = 0;
            drB["AGE"] = null;
            drB["SEX_ID"] = 0;
            drB["LABOUR_TYPE_ID"] = 0;
            drB["WORK_HOUR"] = null;
            drB["WORK_YEAR"] = null;
            drB["YEARLY_INCOME"] = null;
            drB["ANNUAL_VISIT"] = null;
            drB["IS_SCHOOL"] = 0;
            dtChildLabour.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrChildLabour.DataSource = db;
            rptrChildLabour.DataBind();

            rptrChildLabour.DataSource = dtChildLabour;
            rptrChildLabour.DataBind();

            btnAddAnother.Enabled = true;

 
        }
       

      
        protected void btnNext_Click(object sender, EventArgs e)
        {
            //For Repeater inserting
            //Session["Owner_Id"] = 11;
             List<ChildLabourBo> objList = new List<ChildLabourBo>();
            foreach (RepeaterItem rptItem in rptrChildLabour.Items )
            {
                 objBo = new ChildLabourBo();
                 HiddenField hidLabour = (HiddenField)rptItem.FindControl("hidLabour");
               TextBox txtAge = (TextBox)rptItem.FindControl("txtAge");
              RadioButtonList  rblSex = (RadioButtonList)rptItem.FindControl("rblSex");
               RadioButtonList rblLabourType  = (RadioButtonList)rptItem.FindControl("rblLabourType");
               TextBox txtWorkHour = (TextBox)rptItem.FindControl("txtWorkHour");
               TextBox txtWorkYear = (TextBox)rptItem.FindControl("txtWorkYear");
               TextBox txtYearlyIncome = (TextBox)rptItem.FindControl("txtYearlyIncome");
              TextBox  txtVisit = (TextBox)rptItem.FindControl("txtVisit");
             RadioButtonList rblIsSchool = (RadioButtonList)rptItem.FindControl("rblIsSchool");

             if (txtWorkHour.Text != "")
                {
                    objBo.hiddenId1 = Convert.ToInt32(hidLabour.Value);
                    objBo.childLabour = rblChildLabour.SelectedIndex;
                    objBo.age = Convert.ToInt32(txtAge.Text);
                    objBo.sexId = rblSex.SelectedIndex;
                    objBo.labourType = rblLabourType.SelectedIndex;
                    objBo.workHour = Convert.ToInt32(txtWorkHour.Text);
                    objBo.workYear = Convert.ToInt32(txtWorkYear.Text);
                    objBo.yearlyIncome = Convert.ToInt32(txtYearlyIncome.Text);
                    objBo.annualVisit = Convert.ToInt32(txtVisit.Text);
                    objBo.isSchool = rblIsSchool.SelectedIndex;
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    objList.Add(objBo);
                }
            }
              
                    objDao.Child_Labour_Insert(objList);

            
               //For Table Child_labour_exploitation          


            objBo.labourExploitationType = rblLabourProblem.SelectedIndex;
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            int exp = objDao.Child_Labour_Exploitation(objBo);


            if (exp > 0)
            {
                Response.Redirect("ChildRights.aspx");
            }

            
            
            
        }

        protected void rblChildLabour_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblChildLabour.SelectedItem.Value == "0")
            {

                btnAddAnother.Enabled = true;
                             
            }
        }

        public void LoadLabourDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable() ;
            dt = objDao.Fetch_ChildLabour(objBo);            
           
            if (dt != null && dt.Rows.Count > 0)
            {
                rptrChildLabour.DataSource = dt;
                rptrChildLabour.DataBind();              

            }
        }

        public void LoadLabourExpDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_ChildLabour_Exp(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidLabourExp.Value = dt.Rows[0]["LABOUR_EXPLOITATION_ID"].ToString();
                rblLabourProblem.SelectedValue = dt.Rows[0]["LABOUR_EXPLOITATION_TYPE_ID"].ToString();

            }

 


        }

        public void btnUpdate_Click(object sender, EventArgs e)
        {
             
            foreach (RepeaterItem rptItem in rptrChildLabour.Items)
            {
                objBo = new ChildLabourBo();
                HiddenField hid = (HiddenField)rptItem.FindControl("hidLabour");
                TextBox txtAge = (TextBox)rptItem.FindControl("txtAge");
                RadioButtonList rblSex = (RadioButtonList)rptItem.FindControl("rblSex");
                RadioButtonList rblLabourType = (RadioButtonList)rptItem.FindControl("rblLabourType");
                TextBox txtWorkHour = (TextBox)rptItem.FindControl("txtWorkHour");
                TextBox txtWorkYear = (TextBox)rptItem.FindControl("txtWorkYear");
                TextBox txtYearlyIncome = (TextBox)rptItem.FindControl("txtYearlyIncome");
                TextBox txtVisit = (TextBox)rptItem.FindControl("txtVisit");
                RadioButtonList rblIsSchool = (RadioButtonList)rptItem.FindControl("rblIsSchool");

                    objBo.hiddenId1 = Convert.ToInt32(hid.Value);
                    objBo.age = Convert.ToInt32(txtAge.Text);
                    objBo.sexId = rblSex.SelectedIndex;
                    objBo.labourType = rblLabourType.SelectedIndex;
                    objBo.workHour = Convert.ToInt32(txtWorkHour.Text);
                    objBo.workYear = Convert.ToInt32(txtWorkYear.Text);
                    objBo.yearlyIncome = Convert.ToInt32(txtYearlyIncome.Text);
                    objBo.annualVisit = Convert.ToInt32(txtVisit.Text);
                    objBo.isSchool = rblIsSchool.SelectedIndex;
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);

                    objDao.Update_ChildLabour(objBo);
                }
           

           

            objBo.hiddenId2 = Convert.ToInt32(hidLabourExp.Value); 
            objBo.labourExploitationType = rblLabourProblem.SelectedIndex;
            int i = objDao.Update_Child_Labour_Exp(objBo); 
            
            if(i>0)
            {
                Response.Redirect("ChildRights.aspx?Owner_Id="+Session["Owner_Id"]);
            }

        }


        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Marriage.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
    }
}