﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListApplicants.aspx.cs" Inherits="ProfileForm.Pages.ListApplicants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div>
        <table class="table table-bordered table-striped">
            <tr>
                <td>
                    घरको सुची
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GrdList" CssClass="table table-bordered table-striped" 
                        runat="server" AutoGenerateColumns="False" 
                        onrowcommand="GrdList_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="InfoId" HeaderText="InfoId" Visible="False"/>
                            <asp:BoundField DataField="VDC" HeaderText="गाविस / नगरपालिका" />
                            <asp:BoundField DataField="Ward" HeaderText="वडा नं." />
                            <asp:BoundField DataField="Owner" HeaderText="घरमुलीको नाम" />
                            <asp:BoundField DataField="House" HeaderText="घर नं." />
                            <asp:BoundField DataField="Tole" HeaderText="टोलको नाम" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkView" runat="server" CommandName="Edit" CommandArgument='<%#Eval("InfoId") %>'>Edit</asp:LinkButton>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("InfoId") %>'>Delete</asp:LinkButton>
                                </ItemTemplate>
                              </asp:TemplateField>
                        </Columns>
                        
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
