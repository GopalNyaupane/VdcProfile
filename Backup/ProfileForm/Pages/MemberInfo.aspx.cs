﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;

namespace ProfileForm.Pages
{
    public partial class MemberInfo : System.Web.UI.Page
    {
        MemberInfoBo objMmbr = null;
        MemberInfoDao objMmbrDal = new MemberInfoDao();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];

                    int ownerId = Convert.ToInt32(Session["Owner_Id"].ToString());

                    BindRepeaterOccupation(ownerId);
                    BindMigration(ownerId);
                    BindRptrMigrationDetailInfo(ownerId);
                    BindRptrDisabilityInfo(ownerId);        

                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdateMemberInfo.Enabled = true;
                    btnUpdateMemberInfo.Visible = true;
                }
            }
        }

        private void BindMigration(int ownerId)
        {
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = ownerId;

            DataTable dt = new DataTable();
            dt = objMmbrDal.FetchMigration(objMmbr);

            if (dt != null && dt.Rows.Count > 0)
            {
                RdoAbroad.SelectedIndex = Convert.ToInt32(dt.Rows[0]["IS_OUT_MIGRATION"]);
            }
        }

        private void BindRepeaterOccupation(int ownerId)
        {
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = ownerId;

            DataTable dt = new DataTable();
            dt = objMmbrDal.FetchOccupation(objMmbr);

            

            rptrFamilyMemberOccupation.DataSource = null;
            rptrFamilyMemberOccupation.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {

                rptrFamilyMemberOccupation.DataSource = dt;
                rptrFamilyMemberOccupation.DataBind();
            }
          
        }

        private void BindRptrMigrationDetailInfo(int ownerId)
        {
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = ownerId;

            DataTable dt = new DataTable();
            dt = objMmbrDal.FetchMigrationDetail(objMmbr);

            rpterAbroad.DataSource = null;
            rpterAbroad.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rpterAbroad.DataSource = dt;
                rpterAbroad.DataBind();
            }

        }

        private void BindRptrDisabilityInfo(int ownerId)
        {
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = ownerId;

            DataTable dt = new DataTable();
            dt = objMmbrDal.FetchDisability(objMmbr);

            rptrHandicapped.DataSource = null;
            rptrHandicapped.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrHandicapped.DataSource = dt;
                rptrHandicapped.DataBind();
            }
        }

        

        protected void btnNext_Click(object sender, EventArgs e)
        {
           // Session["Owner_ID"] = "99";
            int val;

            foreach (RepeaterItem rptr in rptrFamilyMemberOccupation.Items)
            {
                objMmbr = new MemberInfoBo();

                DropDownList ddlOccupation = (DropDownList) rptr.FindControl("ddlOccupation");
                DropDownList ddlAgeGroup = (DropDownList) rptr.FindControl("ddlAgeGroup");
                TextBox txtMaleNo = (TextBox) rptr.FindControl("txtMaleNo");
                TextBox txtFemaleNo = (TextBox) rptr.FindControl("txtFemaleNo");

                objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objMmbr.OccupationType = ddlOccupation.SelectedIndex;
                objMmbr.AgeGroup = ddlAgeGroup.SelectedIndex;
                objMmbr.MaleNum = Convert.ToInt32(txtMaleNo.Text);
                objMmbr.FemaleNum = Convert.ToInt32(txtFemaleNo.Text);

                val = objMmbrDal.InsertOccupation(objMmbr);


            }
    
            objMmbr = new MemberInfoBo();
            objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objMmbr.IsOutMigrant = RdoAbroad.SelectedIndex;

            val = objMmbrDal.InsertMigration(objMmbr);


            foreach (RepeaterItem rptr in rpterAbroad.Items)
            {
                objMmbr = new MemberInfoBo();

                TextBox txtAbName = (TextBox) rptr.FindControl("txtAbName");
                TextBox txtAbAge = (TextBox) rptr.FindControl("txtAbAge");
                RadioButtonList RdoSex = (RadioButtonList) rptr.FindControl("RdoSex");
                TextBox txtAbCountry = (TextBox) rptr.FindControl("txtAbCountry");
                TextBox txtAbReason = (TextBox) rptr.FindControl("txtAbReason");

                objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objMmbr.Name = txtAbName.Text;
                objMmbr.Age = Convert.ToInt32(txtAbAge.Text);
                objMmbr.SexId = RdoSex.SelectedIndex;
                objMmbr.MigratedPlace = txtAbCountry.Text;
                objMmbr.OutReason = txtAbReason.Text;

               val = objMmbrDal.InsertMigrationDeatil(objMmbr);

            }


            foreach (RepeaterItem rptr in rptrHandicapped.Items)
            {
                objMmbr = new MemberInfoBo();

                DropDownList ddlHandddlHandicapped = (DropDownList) rptr.FindControl("ddlHandicapped");
                TextBox txtBoyNo = (TextBox) rptr.FindControl("txtBoyNo");
                TextBox txtGirlNo = (TextBox) rptr.FindControl("txtGirlNo");

                objMmbr.OwnerId = Convert.ToInt32(Session["Owner_ID"].ToString());
                objMmbr.DisableType = ddlHandddlHandicapped.SelectedIndex;
                objMmbr.MaleChildCount = Convert.ToInt32(txtBoyNo.Text);
                objMmbr.FemaleChildCount = Convert.ToInt32(txtGirlNo.Text);

                val = objMmbrDal.InsertDisableInfo(objMmbr);

            }

            Response.Redirect("IncomeExpenditure.aspx");


            }





            
        

        protected void btnAddMemberOccupation_OnClick(object sender, EventArgs e)
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("OCCUPATION_ID");
            dtA.Columns.Add("OCCUPATION_TYPE_ID");
            dtA.Columns.Add("AGE_GROUP_ID");
            dtA.Columns.Add("MALE_NO");
            dtA.Columns.Add("FEMALE_NO");

           DropDownList ddlOccupation = new DropDownList();
           DropDownList ddlAgeGroup = new DropDownList();
           
            TextBox txtMaleNo = new TextBox();
            TextBox txtFemaleNo = new TextBox();

            foreach (RepeaterItem rptItem in rptrFamilyMemberOccupation.Items)
            {
                DataRow drA = dtA.NewRow();
                ddlAgeGroup = (DropDownList)rptItem.FindControl("ddlAgeGroup");
                ddlOccupation = (DropDownList)rptItem.FindControl("ddlOccupation");
                txtMaleNo = (TextBox)rptItem.FindControl("txtMaleNo");
                txtFemaleNo = (TextBox)rptItem.FindControl("txtFemaleNo");
                drA["OCCUPATION_TYPE_ID"] = ddlOccupation.SelectedIndex;
                drA["AGE_GROUP_ID"] = ddlAgeGroup.SelectedIndex;
                drA["MALE_NO"] = txtMaleNo.Text;
                drA["FEMALE_NO"] = txtFemaleNo.Text;

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["OCCUPATION_TYPE_ID"] = 0;
            drB["AGE_GROUP_ID"] = 0;
            drB["MALE_NO"] = 0;
            drB["FEMALE_NO"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrFamilyMemberOccupation.DataSource = db;
            rptrFamilyMemberOccupation.DataBind();

            rptrFamilyMemberOccupation.DataSource = dtA;
            rptrFamilyMemberOccupation.DataBind();
        }


        public void AddOutMigrationDetails()
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
//            dtA.Columns.Add("SN");
            dtA.Columns.Add("OUT_MIGRATION_DETAIL_ID");
            dtA.Columns.Add("NAME");
            dtA.Columns.Add("AGE");
            dtA.Columns.Add("SEX_ID");
            dtA.Columns.Add("MIGRATED_PLACE");
            dtA.Columns.Add("OUT_MIGRATION_REASON");

           // DropDownList ddlOccupation = new DropDownList();
           RadioButtonList RdoSex = new RadioButtonList();
            TextBox txtAbName = new TextBox();
            TextBox txtAbAge = new TextBox();
            TextBox txtAbCountry = new TextBox();
            TextBox txtAbReason = new TextBox();

            foreach(RepeaterItem rptItem in rpterAbroad.Items)
            {
                DataRow drA = dtA.NewRow();

                RdoSex = (RadioButtonList)rptItem.FindControl("RdoSex");
                txtAbName = (TextBox)rptItem.FindControl("txtAbName");
                txtAbAge = (TextBox)rptItem.FindControl("txtAbAge");
                txtAbCountry = (TextBox)rptItem.FindControl("txtAbCountry");
                txtAbReason = (TextBox)rptItem.FindControl("txtAbReason");
                drA["SEX_ID"] = RdoSex.SelectedIndex;
                drA["NAME"] = txtAbName.Text;
                drA["AGE"] = txtAbAge.Text;
                drA["MIGRATED_PLACE"] = txtAbCountry.Text;
                drA["OUT_MIGRATION_REASON"] = txtAbReason.Text;
               

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["SEX_ID"] = 0;
            drB["NAME"] = null;
            drB["AGE"] = null;
            drB["MIGRATED_PLACE"] = null;
            drB["OUT_MIGRATION_REASON"] = null;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rpterAbroad.DataSource = db;
            rpterAbroad.DataBind();

            rpterAbroad.DataSource = dtA;
            rpterAbroad.DataBind();
        }


       

        protected void btnAddAbroad_OnClick(object sender, EventArgs e)
        {
            AddOutMigrationDetails();
        }

       
        protected void RdoAbroad_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            rpterAbroad.DataSource = null;
            rpterAbroad.DataBind();
            // if(RdoAbroad.SelectedIndex<1)
            //AddOutMigrationDetails();
        }

       

        protected void btnAddHandicapped_OnClick(object sender, EventArgs e)
        {
             DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("SN");
           
            dtA.Columns.Add("DISABLED_TYPE_ID");
            dtA.Columns.Add("MALE_CHILD_COUNT");
            dtA.Columns.Add("FEMALE_CHILD_COUNT");
            dtA.Columns.Add("DISABLED_ID");

           DropDownList ddlHandicapped = new DropDownList();
          
            TextBox txtMaleNo = new TextBox();
            TextBox txtFemaleNo = new TextBox();
            HiddenField hidDisableId = new HiddenField();
            foreach(RepeaterItem rptItem in rptrHandicapped.Items)
            {
                DataRow drA = dtA.NewRow();

                ddlHandicapped = (DropDownList)rptItem.FindControl("ddlHandicapped");
               txtMaleNo = (TextBox)rptItem.FindControl("txtMaleNo");
                txtFemaleNo = (TextBox)rptItem.FindControl("txtFemaleNo");
                hidDisableId = (HiddenField) rptItem.FindControl("hidDisableId");
                drA["DISABLED_TYPE_ID"] = ddlHandicapped.SelectedIndex;
                drA["MALE_CHILD_COUNT"] = txtMaleNo.Text;
                drA["FEMALE_CHILD_COUNT"] = txtFemaleNo.Text;
                drA["DISABLED_ID"] = hidDisableId.Value;
               
               

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["DISABLED_TYPE_ID"] = 0;
           
            drB["MALE_CHILD_COUNT"] = 0;
            drB["FEMALE_CHILD_COUNT"] = 0;
            drB["DISABLED_ID"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrHandicapped.DataSource = db;
            rptrHandicapped.DataBind();

            rptrHandicapped.DataSource = dtA;
            rptrHandicapped.DataBind();
        }

        protected void btnUpdateMemberInfo_OnClick(object sender, EventArgs e)
        {
            objMmbr = new MemberInfoBo();
            UpdateOccupation(objMmbr);
            objMmbr = new MemberInfoBo();
            UpdateMigration(objMmbr);
            objMmbr = new MemberInfoBo();
            UpdateMigrationDetail(objMmbr);
            objMmbr = new MemberInfoBo();
            UpdateDisability(objMmbr);
            Response.Redirect("IncomeExpenditure.aspx?Owner_Id="+Session["Owner_Id"]);
        }

        private void UpdateDisability(MemberInfoBo objMmbr)
        {
            foreach (RepeaterItem rptr in rptrHandicapped.Items)
            {

                DropDownList ddlHandicapped = (DropDownList) rptr.FindControl("ddlHandicapped");
                TextBox txtBoyNo = (TextBox)rptr.FindControl("txtBoyNo");
                TextBox txtGirlNo = (TextBox)rptr.FindControl("txtGirlNo");
                HiddenField hidDisableId = (HiddenField)rptr.FindControl("hidDisableId");


                objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objMmbr.userId = Convert.ToInt32(hidDisableId.Value);

                objMmbr.DisableType = ddlHandicapped.SelectedIndex;
                objMmbr.MaleChildCount = Convert.ToInt32(txtBoyNo.Text);
                objMmbr.FemaleChildCount = Convert.ToInt32(txtGirlNo.Text);
               


                objMmbrDal.UpdateDisabilityData(objMmbr);

            }
        }

        private void UpdateMigration(MemberInfoBo objMmbr)
        {
            objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objMmbr.IsOutMigrant = RdoAbroad.SelectedIndex;
            objMmbrDal.UpdateIsOutMigrationData(objMmbr);
        }

        private void UpdateMigrationDetail(MemberInfoBo objMmbr)
        {
            foreach (RepeaterItem rptr in rpterAbroad.Items)
            {

                TextBox txtAbName = (TextBox) rptr.FindControl("txtAbName");
                TextBox txtAbAge = (TextBox)rptr.FindControl("txtAbAge");
                RadioButtonList RdoSex = (RadioButtonList)rptr.FindControl("RdoSex");
                TextBox txtAbCountry = (TextBox) rptr.FindControl("txtAbCountry");
                TextBox txtAbReason = (TextBox)rptr.FindControl("txtAbReason");
                HiddenField hidMigrationId = (HiddenField) rptr.FindControl("hidMigrationId");
        

                objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objMmbr.userId = Convert.ToInt32(hidMigrationId.Value);
                objMmbr.Name = txtAbName.Text;
                objMmbr.Age = Convert.ToInt32(txtAbAge.Text);
                objMmbr.SexId = RdoSex.SelectedIndex;
                objMmbr.MigratedPlace = txtAbCountry.Text;
                objMmbr.OutReason = txtAbReason.Text;

                
                objMmbrDal.UpdateMigrationDetailData(objMmbr);

            }
        }

        private void UpdateOccupation(MemberInfoBo objMmbr)
        {

            foreach (RepeaterItem rptr in rptrFamilyMemberOccupation.Items)
            {

                DropDownList ddlOccupation = (DropDownList) rptr.FindControl("ddlOccupation");
                DropDownList ddlAgeGroup = (DropDownList)rptr.FindControl("ddlAgeGroup");
                TextBox txtMaleNo = (TextBox) rptr.FindControl("txtMaleNo");
                TextBox txtFemaleNo = (TextBox) rptr.FindControl("txtFemaleNo");
                HiddenField hidOccupationId = (HiddenField) rptr.FindControl("hidOccupationId");

                objMmbr.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objMmbr.userId = Convert.ToInt32(hidOccupationId.Value);
                objMmbr.OccupationType = ddlOccupation.SelectedIndex;
                objMmbr.MaleNum = Convert.ToInt32(txtMaleNo.Text);
                objMmbr.FemaleNum = Convert.ToInt32(txtFemaleNo.Text);
                objMmbr.AgeGroup = ddlAgeGroup.SelectedIndex;

                objMmbrDal.UpdateOccupationData(objMmbr);

            }


        }
    }
    
}
