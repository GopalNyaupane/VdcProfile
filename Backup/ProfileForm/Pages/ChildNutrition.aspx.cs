﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class ChildNutrition : System.Web.UI.Page
    {
        ChildNutritionBo objBo = new ChildNutritionBo();
        ChildNutritionDao objDao = new ChildNutritionDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptrChildWeight.DataBind();
                if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadWeightDetails(Owner_Id);
                    LoadNutritionDetails(Owner_Id);

                    btnUpdate.Visible = true;
                    btnNext.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                }
            }
        }

      
        public void LoadNutritionDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataSet ds = new DataSet();
            ds = objDao.Fetch_Child_Birth_Nutrition(objBo);
            DataTable dtNutrition = null;
            DataTable dtBC = null;
           
            if (ds != null && ds.Tables.Count > 0)
            {
                dtNutrition = ds.Tables[0];
                dtBC = ds.Tables[1];

                hidNutrition.Value = dtNutrition.Rows[0]["CHILD_NUTRITION_ID"].ToString();
                hidBirth.Value = dtBC.Rows[0]["BIRTH_CERTIFICATE_ID"].ToString();
                hidBirth1.Value = dtBC.Rows[1]["BIRTH_CERTIFICATE_ID"].ToString();
                hidBirth2.Value = dtBC.Rows[2]["BIRTH_CERTIFICATE_ID"].ToString();
                hidBirth3.Value = dtBC.Rows[3]["BIRTH_CERTIFICATE_ID"].ToString();
                hidBirth4.Value = dtBC.Rows[4]["BIRTH_CERTIFICATE_ID"].ToString();
                hidBirth5.Value = dtBC.Rows[5]["BIRTH_CERTIFICATE_ID"].ToString();

                txtBoysCertificateUnder5.Text = dtBC.Rows[0]["BIRTH_CERTIFICATE"].ToString();
                txtBoysNoCertificateUnder5.Text = dtBC.Rows[0]["NO_BIRTH_CERTIFICATE"].ToString();
                txtGirlsCertificateUnder5.Text = dtBC.Rows[1]["BIRTH_CERTIFICATE"].ToString();
                txtGirlsNoCertificateUnder5.Text = dtBC.Rows[1]["NO_BIRTH_CERTIFICATE"].ToString();
                txtOtherCertificateUnder5.Text = dtBC.Rows[2]["BIRTH_CERTIFICATE"].ToString();
                txtOtherNoCertificateUnder5.Text = dtBC.Rows[2]["NO_BIRTH_CERTIFICATE"].ToString();
                txtBoysCertificate.Text = dtBC.Rows[3]["BIRTH_CERTIFICATE"].ToString();
                txtBoysNoCertificate.Text = dtBC.Rows[3]["NO_BIRTH_CERTIFICATE"].ToString();
                txtGirlsCertificate.Text = dtBC.Rows[4]["BIRTH_CERTIFICATE"].ToString();
                txtGirlsNoCertificate.Text = dtBC.Rows[4]["NO_BIRTH_CERTIFICATE"].ToString();
                txtOtherCertificate.Text = dtBC.Rows[5]["BIRTH_CERTIFICATE"].ToString();
                txtOtherNoCertificate.Text = dtBC.Rows[5]["NO_BIRTH_CERTIFICATE"].ToString();

                rblSalt.SelectedValue = dtNutrition.Rows[0]["IO_SALT"].ToString();
                rblNutrition.SelectedValue = dtNutrition.Rows[0]["NUTRITION_FOOD"].ToString();
                rblChildWeight.SelectedValue = dtNutrition.Rows[0]["CHILD_WEIGHT"].ToString();               

            }
            else
            {
                dtNutrition = null;
                dtBC = null;
               
              
            }



        }

        private void LoadWeightDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataTable dt = new DataTable();
            dt = objDao.Fetch_Child_Weight(objBo);

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrChildWeight.DataSource = dt;
                rptrChildWeight.DataBind();

            }
        }

        protected void btnOk_OnClick(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(txtChildCount.Text);
            DataTable dtHealth = new DataTable();
            BindChildWeight(dtHealth, count);
        }

        private void BindChildWeight(DataTable dtHealth, int count)
        {
            DataTable dtChildWeight = new DataTable();
            dtChildWeight.Clear();
            dtChildWeight.Columns.Add("CHILD_WEIGHT_ID");
            dtChildWeight.Columns.Add("CHILD_AGE");
            dtChildWeight.Columns.Add("CHILD_WEIGHT");
            dtChildWeight.Columns.Add("MALNUTRITION_ID");


            int j = 0;
            if (dtHealth.Rows.Count == 0)
            {

                for (int i = 0; i < count; i++)
                {
                    DataRow _dtRow = dtChildWeight.NewRow();
                    _dtRow["CHILD_WEIGHT_ID"] = i + 1;
                    _dtRow["CHILD_AGE"] = 0;
                    _dtRow["CHILD_WEIGHT"] = 0;
                    _dtRow["MALNUTRITION_ID"] = 0;

                    dtChildWeight.Rows.Add(_dtRow);
                }

            }
            else
            {
                for (int i = 0; i < dtHealth.Rows.Count; i++)
                {
                    DataRow _dtRow = dtChildWeight.NewRow();
                    /*_dtRow["SN"] = i + 1;
                    _dtRow["Name"] = dtAbroad.Rows[i]["away_name"].ToString();
                    _dtRow["Age"] = dtAbroad.Rows[i]["away_age"].ToString();
                    _dtRow["Relation"] = dtAbroad.Rows[i]["away_reason"].ToString();
                    _dtRow["Sex"] = dtAbroad.Rows[i]["away_gender"].ToString();
                   */

                    dtChildWeight.Rows.Add(_dtRow);
                }
            }
            rptrChildWeight.DataSource = dtChildWeight;
            rptrChildWeight.DataBind();
        }


        protected void btnNext_Click(object sender,EventArgs e)
        {
            //Session["Owner_Id"] = 10;
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
           
            objBo.salt = rblSalt.SelectedIndex;
            objBo.nutritionFood = rblNutrition.SelectedIndex;
            objBo.childWeightInfo = rblChildWeight.SelectedIndex;

            int cni = objDao.Child_Nutrition_Insert(objBo);
           
            List<ChildNutritionBo> objList = new List<ChildNutritionBo>();
            foreach (RepeaterItem rptItem in rptrChildWeight.Items )
            {
                objBo = new ChildNutritionBo();
                TextBox txtChildAge = (TextBox)rptItem.FindControl("txtChildAge");
                TextBox txtChildWeight = (TextBox)rptItem.FindControl("txtChildWeight");
                RadioButtonList rdlMalNutrition = (RadioButtonList)rptItem.FindControl("rdlMalNutrition");

                if(txtChildAge.Text !="")
                {
                    objBo.age = Convert.ToInt32(txtChildAge.Text);
                    objBo.weight = Convert.ToInt32(txtChildWeight.Text);
                    objBo.malNutrition = rdlMalNutrition.SelectedIndex;
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    objList.Add(objBo);

                }
            }
                objDao.Child_Weight_Insert(objList);


            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.ageGroup = 0;
            objBo.birthCertificate = Convert.ToInt32(txtBoysCertificateUnder5.Text);
            objBo.noBirthCertificate = Convert.ToInt32(txtBoysNoCertificateUnder5.Text);
            objBo.sex = 0;
            
           int boy = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 0;
            objBo.birthCertificate = Convert.ToInt32(txtGirlsCertificateUnder5.Text);
            objBo.noBirthCertificate = Convert.ToInt32(txtGirlsNoCertificateUnder5.Text);
            objBo.sex = 1;          
           
            int girl = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 0;
            objBo.noBirthCertificate = Convert.ToInt32(txtOtherNoCertificateUnder5.Text);
            objBo.birthCertificate = Convert.ToInt32(txtOtherCertificateUnder5.Text);
            objBo.sex = 2;
           
            int other = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 1;
            objBo.birthCertificate = Convert.ToInt32(txtBoysCertificate.Text);
            objBo.noBirthCertificate = Convert.ToInt32(txtBoysNoCertificate.Text);
            objBo.sex = 0;

           
            int boy1 = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 1;
            objBo.birthCertificate = Convert.ToInt32(txtGirlsCertificate.Text);
            objBo.noBirthCertificate = Convert.ToInt32(txtGirlsNoCertificate.Text);
            objBo.sex = 1;

            
            int girl1 = objDao.Birth_Certificate_Insert(objBo);

            objBo.ageGroup = 1;
            objBo.noBirthCertificate = Convert.ToInt32(txtOtherNoCertificate.Text);
            objBo.birthCertificate = Convert.ToInt32(txtOtherCertificate.Text);
            objBo.sex = 2;
           
            int other1 = objDao.Birth_Certificate_Insert(objBo);



           

            if (cni > 0)
            {

                //Response.Write("<Script> alert('Insert Data Properly')</Script>");

                Response.Redirect("Marriage.aspx");
            }
           
           
        }
        
        protected void btnUpdate_Click(object sender, EventArgs e)
        {

           // Session["Owner_Id"] = 10;
           
            foreach (RepeaterItem rptItem in rptrChildWeight .Items)
            {
                objBo = new ChildNutritionBo();
                HiddenField hid = (HiddenField)rptItem.FindControl("hidWeight");
                TextBox txtAge = (TextBox)rptItem.FindControl("txtChildAge");
                TextBox txtWeight = (TextBox)rptItem.FindControl("txtChildWeight");
                RadioButtonList rblMal = (RadioButtonList)rptItem.FindControl("rdlMalNutrition");

                    objBo.hidden1 = Convert.ToInt32(hid.Value);
                    objBo.age = Convert.ToInt32(txtAge.Text);
                    objBo.weight = Convert.ToInt32(txtWeight.Text);
                    objBo.malNutrition = rblMal.SelectedIndex;
                    objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);

                    objDao.Update_Child_Weight(objBo);
            }
          


                objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
                objBo.hidden3 = Convert.ToInt32(hidBirth.Value);
                objBo.ageGroup = 0;
                objBo.birthCertificate = Convert.ToInt32(txtBoysCertificateUnder5.Text);
                objBo.noBirthCertificate = Convert.ToInt32(txtBoysNoCertificateUnder5.Text);
                objBo.sex = 0;

                int boy = objDao.Update_Child_Birth_Nutrition(objBo);

                objBo.hidden3 = Convert.ToInt32(hidBirth1.Value);
                objBo.ageGroup = 0;
                objBo.birthCertificate = Convert.ToInt32(txtGirlsCertificateUnder5.Text);
                objBo.noBirthCertificate = Convert.ToInt32(txtGirlsNoCertificateUnder5.Text);
                objBo.sex = 1;

                int girl = objDao.Update_Child_Birth_Nutrition(objBo);

                objBo.hidden3 = Convert.ToInt32(hidBirth2.Value);
                objBo.ageGroup = 0;
                objBo.noBirthCertificate = Convert.ToInt32(txtOtherNoCertificateUnder5.Text);
                objBo.birthCertificate = Convert.ToInt32(txtOtherCertificateUnder5.Text);
                objBo.sex = 2;

                int other = objDao.Update_Child_Birth_Nutrition(objBo);

                objBo.hidden3 = Convert.ToInt32(hidBirth3.Value);
                objBo.ageGroup = 1;
                objBo.birthCertificate = Convert.ToInt32(txtBoysCertificate.Text);
                objBo.noBirthCertificate = Convert.ToInt32(txtBoysNoCertificate.Text);
                objBo.sex = 0;


                int boy1 = objDao.Update_Child_Birth_Nutrition(objBo);

                objBo.hidden3 = Convert.ToInt32(hidBirth4.Value);
                objBo.ageGroup = 1;
                objBo.birthCertificate = Convert.ToInt32(txtGirlsCertificate.Text);
                objBo.noBirthCertificate = Convert.ToInt32(txtGirlsNoCertificate.Text);
                objBo.sex = 1;


                int girl1 = objDao.Update_Child_Birth_Nutrition(objBo);
               
                objBo.hidden3 = Convert.ToInt32(hidBirth5.Value);
                objBo.ageGroup = 1;
                objBo.noBirthCertificate = Convert.ToInt32(txtOtherNoCertificate.Text);
                objBo.birthCertificate = Convert.ToInt32(txtOtherCertificate.Text);
                objBo.sex = 2;


                objBo.hidden2 = Convert.ToInt32(hidNutrition.Value);
                objBo.salt = rblSalt.SelectedIndex;
                objBo.nutritionFood = rblNutrition.SelectedIndex;
                objBo.childWeightInfo = rblChildWeight.SelectedIndex;

                int other1 = objDao.Update_Child_Birth_Nutrition(objBo);


                if (other1 > 0)
            {
                Response.Redirect("Marriage.aspx?Owner_Id="+Session["Owner_Id"]);
            }

           

          

        }


        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("TeenAgeHealth.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
    }
}