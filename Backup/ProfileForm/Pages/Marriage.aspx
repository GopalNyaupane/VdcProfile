﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Marriage.aspx.cs" Inherits="ProfileForm.Pages.Marriage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-striped">
   <tr>
                <td> २६.</td>
                <td> गएको ३ वर्षभित्र तपाईंको परिवारमा कसैको विवाह भएको भए सो को विवरण दिनुहोस् ।</td>
                <td> </td> </tr>
                <tr>
                    <tr>
                <td> </td>
                <td></td>
                <td> <b> संख्या ? </b>
                <asp:TextBox runat="server" Text="0" TextMode="Number" ID ="txtMarriageCount" Width="80"></asp:TextBox>
                <asp:Button runat="server" Text="Ok" ID="btnOk" OnClick="btnOk_OnClick" CssClass="btn btn-primary"/> </td>
            </tr>
                           
     </tr>
     </table>
      <table class="table table-hover">
                <tr>
                <td>सि. नं.</td>
                <td> विवाह गर्दाको उमेर कति वर्षको थियो  </td>
                <td>  लिङ्ग  </td>
                <td> </td>
                <td> </td>
               </tr>
               
             <asp:Panel runat="server" ID="pnlTeenAgeHealth">
                <asp:Repeater runat="server" ID="rptrMarriage">
                    <ItemTemplate>
                        <tr>
                            <td>
                                
                            </td>
                           
                            <td>
                                <asp:TextBox runat="server" TextMode="Number" Text='<%#Eval("AGE") %>' ID="txtChildAge"></asp:TextBox>
                            </td>
                           <td>
                                <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID") %>'>
                                    <asp:ListItem Value="0">पुरुष</asp:ListItem>
                                    <asp:ListItem Value="1">महिला</asp:ListItem>
                                    <asp:ListItem Value="2">अन्य</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <asp:HiddenField runat="server" ID="fieldId" Value='<%#Eval("MARITAL_INFO_ID") %>' />
                            </td>
                            <td></td>
                            </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </asp:Panel>
                <tr>
                <td>
                    &nbsp;
                </td>
                <td></td>
                <td></td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                    <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnNext_Click"
                        OnClientClick="return validatePage();" />
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary"
                        OnClick="btnUpdateMarriage_OnClick" Visible="False" />

                </td>
                
            </tr>
            </table>
           
</asp:Content>
