﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChildNutrition.aspx.cs" Inherits="ProfileForm.Pages.ChildNutrition" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
            <table class="table table-striped">
            
            <tr>
                <td> २२.</td>
                     
                <td> दुई बालबालिका चिन्ह भएको आयोडिनयुक्त नुन प्रयोग गर्ने गर्नुभएको छ ?</td>
                <td> <asp:RadioButtonList ID="rblSalt" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>
                <td><asp:HiddenField ID ="hidNutrition" runat ="server" Value ='<%#Eval("CHILD_NUTRITION_ID") %>' /></td>  
            </tr>
            <tr>
                <td> २३.</td>
                <td> बर्ष मुनिका बालबालिकाले पोषिलो खाना (दिनको कम्तिमा ३ पटक दुघ, लिटो, भात, फलफुल र सागसव्जी) खान पाउँछन् ?</td>
                <td> <asp:RadioButtonList ID="rblNutrition" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                                       <asp:ListItem Value="0">पाउँछन्</asp:ListItem>
                        <asp:ListItem Value="1">पाउँदैनन्</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>

            </tr>
            <tr>
                <td> २४.</td>
                <td> तपाईंको परिवारका १ वर्ष मुनिकाको मासिक र १ वर्ष भन्दा माथि ५ वर्ष सम्मका बालबालिकाहरुको त्रैमासिक रुपमा तौल लिने गरिएको छ ?</td>
                <td> <asp:RadioButtonList ID="rblChildWeight" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>
                            

            </tr>
            <tr>
                <td> </td>
                <td></td>
                <td> <b> संख्या ? </b>
                <asp:TextBox runat="server" Text="0" TextMode="Number" ID ="txtChildCount" Width="80"></asp:TextBox>
                <asp:Button runat="server" Text="Ok" ID="btnOk" OnClick="btnOk_OnClick" CssClass="btn btn-primary"/> </td>
            </tr>
            </table>
            
             
            <table class="table table-bordered">
                <tr>
                <td>सि. नं.</td>
                <td> उमेर (महिनामा) </td>
                <td>  तौल (वजन)  </td>
                <td> कुपोषण (कम तौल) छ?</td>
                <td> </td>
               </tr>
               
             <asp:Panel runat="server" ID="pnlTeenAgeHealth">
                <asp:Repeater runat="server" ID="rptrChildWeight">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hidWeight" runat="server" Value ='<%#Eval("CHILD_WEIGHT_ID") %>'></asp:HiddenField>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtChildAge" TextMode="Number" Text='<%#Eval("CHILD_AGE") %>' ></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox  runat="server" ID="txtChildWeight" TextMode="Number" Text='<%#Eval("CHILD_WEIGHT") %>'></asp:TextBox>
                            </td>
                            
                                
                                 <td>
                                <asp:RadioButtonList ID="rdlMalNutrition" runat="server"  RepeatDirection="Horizontal" SelectedValue='<%#Eval("MALNUTRITION_ID") %>'>
                                    <asp:ListItem Value="0">कुपोषण (कम तौल) </asp:ListItem>
                                    <asp:ListItem Value="1">पोषणयुक्त (ठिक तौल) </asp:ListItem>
                                   
                                </asp:RadioButtonList>
                            </td>
                                                    
                           
                            
                            
                            
                            
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </asp:Panel>
            </table>
            

            <table class="table table-striped">
             <tr>
                <td> २५.</td>
                <td> पाँच वर्षमुनिका र ६ वर्षदेखि १८ वर्षसम्मका बालबालिकाको जन्म दर्ता गराएको विवरण?</td>
                <td>  </td>
                             

            </tr>
            </table>
            <table class="table table-bordered">
                <tr>
                    <td> ५ वर्षमुनिका बालबालिका</td>
                    <td> ६ देखि १८ वर्षसम्मका बालबालिका </td>
                    
                </tr>
                <tr> <td>
                    <table class="table table-bordered">
                        <tr>
                              
                            <td>
                                जन्म दर्ता  गराएको संख्या 
                            </td>
                            <td>जन्म दर्ता  नगराएको संख्या </td>
                        </tr>
                        <tr>
                            <td>बालक<br />
                            <asp:TextBox runat="server" ID="txtBoysCertificateUnder5" TextMode="Number" Text="0"></asp:TextBox></td>
                             <td>बालक<br />
                            <asp:TextBox ID="txtBoysNoCertificateUnder5" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>

                        </tr>
                        <tr>
                            <td> बालिका <br />
                            <asp:TextBox ID="txtGirlsCertificateUnder5" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
                             <td> बालिका <br />
                            <asp:TextBox ID="txtGirlsNoCertificateUnder5" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>

                        </tr>
                        <tr>
                            <td>तेस्रो लिंगी <br />
                            <asp:TextBox ID="txtOtherCertificateUnder5" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
                             <td>तेस्रो लिंगी <br />
                            <asp:TextBox ID="txtOtherNoCertificateUnder5" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>

                        </tr>
                    </table>
                    </td>
                    <td>
                    <table class="table table-bordered">
                        <tr>
                            <td>
                                जन्म दर्ता  गराएको संख्या 
                            </td>
                            <td>जन्म दर्ता  नगराएको संख्या </td>
                        </tr>
                         <tr>
                            <td>बालक<br />
                            <asp:TextBox ID="txtBoysCertificate" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
                             <td>बालक<br />
                            <asp:TextBox ID="txtBoysNoCertificate" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>

                        </tr>
                        <tr>
                            <td> बालिका <br />
                            <asp:TextBox ID="txtGirlsCertificate" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
                             <td> बालिका <br />
                            <asp:TextBox ID="txtGirlsNoCertificate" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>

                        </tr>
                        <tr>
                            <td>तेस्रो लिंगी <br />
                            <asp:TextBox ID="txtOtherCertificate" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
                             <td>तेस्रो लिंगी <br />
                            <asp:TextBox ID="txtOtherNoCertificate" runat="server" TextMode="Number" Text="0"></asp:TextBox></td>
                            <td><asp:HiddenField ID ="hidBirth" runat ="server"  /></td>   
                             <td><asp:HiddenField ID ="hidBirth1" runat ="server"  /></td>   
                             <td><asp:HiddenField ID ="hidBirth2" runat ="server"  /></td>   
                             <td><asp:HiddenField ID ="hidBirth3" runat ="server"  /></td>   
                             <td><asp:HiddenField ID ="hidBirth4" runat ="server"  /></td>   
                             <td><asp:HiddenField ID ="hidBirth5" runat ="server"  /></td>     
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    
                </tr>
            </table>
            
            
            

            <table class="table table-striped">
             <%--<tr>
                <td> २६.</td>
                <td> गएको ३ वर्षभित्र तपाईंको परिवारमा कसैको विवाह भएको भए सो को विवरण दिनुहोस् ।</td>
                <td> </td> </tr>
                <tr>
                    <td></td>
                <td> <asp:RadioButtonList ID="RadioButtonList9" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("Sex") %>'>
                                       <asp:ListItem Value="1">पुरुष</asp:ListItem>
                        <asp:ListItem Value="2">महिला</asp:ListItem>
                         <asp:ListItem Value="3">अन्य</asp:ListItem>
                                </asp:RadioButtonList> &nbsp;   Age: <asp:TextBox runat="server"></asp:TextBox> </td>
                             <td><asp:Button runat="server" Text="Add" CssClass="btn btn-pinterest"/> </td>
                           
            </tr>--%>

            <tr>
                <td>
                    &nbsp;</td>
                    <td> </td>
                    <td></td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                   <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnNext_Click"/>
                   <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btnUpdate_Click"/>

                </td>
                
            </tr>
            </table>
</asp:Content>