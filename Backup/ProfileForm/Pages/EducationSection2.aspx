﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EducationSection2.aspx.cs" Inherits="ProfileForm.Pages.EducationSection2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<table class="table table-striped">
     <tr>
            <td>४८. </td>
            <td> विद्यालय भर्ना नभएका बालबालिकाको संख्या</td>
            <td></td>
            <td></td>
        </tr>
        </table>
        <table class="table table-bordered">
            <tr>
                <td>विवरण </td>
                <td> प्राथमिक उमेर समूह  (५–९ वर्ष) </td>
                <td> नि.मा.वि. उमेर समूह  (१०–१४ वर्ष)</td>
                <td> माध्यमिक उमेर समूह (१५ वर्ष) </td>
                <td><asp:HiddenField ID ="hidUnadmitted" runat ="server"  /></td>
                 <td><asp:HiddenField ID ="hidUnadmitted1" runat ="server"  /></td>
                 <td><asp:HiddenField ID ="hidUnadmitted2" runat ="server"  /></td>
                 <td><asp:HiddenField ID ="hidUnadmitted3" runat ="server"   /></td>
                 <td><asp:HiddenField ID ="hidUnadmitted4" runat ="server"  /></td>
                 <td><asp:HiddenField ID ="hidUnadmitted5" runat ="server"  /></td>
            </tr>
            <tr>
                <td> </td>
                <td>
                    <table class="table table-bordered">
                        <tr>
                            <td>बालक</td>
                            <td>वालिका </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table class="table table-bordered">
                        <tr>
                            <td>बालक</td>
                            <td>वालिका </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table class="table table-bordered">
                        <tr>
                            <td>बालक</td>
                            <td>वालिका </td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    सामान्य क्षमता
                </td>
                <td>
                     <table class="table table-bordered">
                        <tr>
                            <td><asp:TextBox  ID="txtBoyNormal1" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                           <td><asp:TextBox ID="txtGirlNormal1" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td>
                     <table class="table table-bordered">
                        <tr>
                            <td><asp:TextBox ID="txtBoyNormal2" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                           <td><asp:TextBox ID="txtGirlNormal2" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td>
                     <table class="table table-bordered">
                        <tr>
                            <td><asp:TextBox ID="txtBoyNormal3" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                           <td><asp:TextBox ID="txtGirlNormal3" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                      अपांगता भएका   
                </td>
                 <td>
                     <table class="table table-bordered">
                        <tr>
                            <td><asp:TextBox ID="txtBoyAbNormal1" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                           <td><asp:TextBox ID="txtGirlAbNormal1" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                 <td>
                     <table class="table table-bordered">
                        <tr>
                            <td><asp:TextBox ID="txtBoyAbNormal2" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                           <td><asp:TextBox ID="txtGirlAbNormal2" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                 <td>
                     <table class="table table-bordered">
                        <tr>
                            <td><asp:TextBox ID="txtBoyAbNormal3" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                           <td><asp:TextBox ID="txtGirlAbNormal3" runat="server" Text="0" TextMode="Number" Width="80"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table class="table table-striped">
        <tr>
            <td>४९.</td>
            <td>बीचैमा विद्यालय जान छोडेका ६ – १५ वर्ष उमेर समूहका बालबालिकाहरुका संख्या र कारण</td>
                            <td><asp:HiddenField ID ="hidDiscontinuation" runat ="server"  /></td>
            <td><asp:HiddenField ID ="hidDiscontinuation1" runat ="server"  /></td>

        </tr>
        <tr>
            <td></td>
            <td>बालक : <asp:TextBox ID="txtBoyDiscontinued" runat="server" TextMode="Number"  Text="0"></asp:TextBox></td> <td>विद्यालय जान छोड्नुको कारण: <br /> <asp:TextBox ID="txtBoyReason" runat="server" TextMode="MultiLine"></asp:TextBox></td> </tr>
            <tr><td></td><td>बलिका:<asp:TextBox ID="txtGirlDiscontinued" runat="server" TextMode="Number"  Text="0"></asp:TextBox></td> <td>विद्यालय जान छोड्नुको कारण: <br /> <asp:TextBox ID="txtGirlReason" runat="server" TextMode="MultiLine"></asp:TextBox></td> 
       
           
         
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>५०.</td>
            <td>अनौपचारिक शिक्षामा बाल (विद्यालय शिक्षाबाट वंचित (१० देखि १४ वर्ष सम्मका)) सहभागिता अनौपचारिक शिक्षा लिइरहेका संख्या</td>
            <td>बालक: <br /><asp:TextBox ID="txtBoyUnEdCount" runat="server" TextMode="Number" Text="0"></asp:TextBox> <br /> 
                बालिका: <br /><asp:TextBox ID="txtGirlUnEdCount" runat="server" TextMode="Number" Text="0"></asp:TextBox> </td>
                            <td><asp:HiddenField ID ="hidUnofficial" runat ="server"  Value ='<%#Eval("UNOFFICIAL_ID ") %>' /></td>

        </tr>
        <tr>
            <td>५१.</td>
            <td>परिवारमा बालबालिकासँग सम्बन्धित कुनै विषयमा निर्णय गर्दा उनीहरुलाई पनि सहभागी गराउने गर्नु भएको छ ?</td>
             <td> <asp:RadioButtonList ID="rblChildDiscussion" runat="server"  SelectedValue='<%#Eval(" ") %>'>
                                       <asp:ListItem Value="0">गराउने गरेको छ</asp:ListItem>
                        <asp:ListItem Value="1">गराउने गरेको छैन</asp:ListItem>                       
                        
                  </asp:RadioButtonList> </td>
             <td><asp:HiddenField ID ="hidDiscussion" runat ="server"  Value ='<%#Eval("CHILD_DISCUSSION_ID") %>' /></td>
        </tr>
        
        <tr>
            <td>५२.</td>
            <td>तपाईंको परिवारका बालबालिकाहरु बाल क्लव/संगठन आदिमा आबद्धता रहेका छन् ?</td>
            <td> </td>
           </tr>
        </table>
        <table class="table table-striped table-bordered">
        <tr>
            <td> लिङ्ग</td> <td>स्कुलमा आधारित</td><td>समुदायमा आधारित</td><td>श्रमिक बाल क्लव</td><td>आबद्ध नभएको</td>
        </tr>
        <tr>
            <td>बालक</td><td><asp:TextBox ID="txtBoySchool"  runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtBoySocial" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtBoyClub" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtBoyNotAd" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td>
        </tr>
        <tr>
            <td>बालिका</td><td><asp:TextBox ID="txtGirlSchool" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtGirlSocial" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtGirlClub" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtGirlNotAd" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td>
        <td><asp:HiddenField ID ="hidClubParticipation" runat ="server"  /></td>
             <td><asp:HiddenField ID ="hidClubParticipation1" runat ="server"  /></td>
            <td><asp:HiddenField ID ="hidClubParticipation2" runat ="server"  /></td>
            <td><asp:HiddenField ID ="hidClubParticipation3" runat ="server"  /></td>
             </tr>
    </table>
     <table>
     <tr>
         <td>५३.</td><td>तपाईका घरका १८ वर्षभन्दा कम उमेरका कतिजना बालबालिका देहायको निकायमा प्रतिनिधि वा सदस्य रहेका छन् ?</td>
     </tr>
    </table>
    <table class="table table-striped table-bordered">
        <tr>
            <td> लिङ्ग</td> <td>स्थानीय निकाय/योजना तर्जुमा वा कार्यान्वयन समिति</td><td>वडा नागरिक मञ्च</td><td>विद्यालय/स्वास्थ्य संस्था व्यवस्थापन समिति</td><td>बाल संरक्षण समिति</td><td>बाल समूह
वा बाल क्लव, बालमैत्री
स्थानीय शासन समिति
वा बाल संजाल</td>
        </tr>
        <tr>
            <td>बालक</td><td><asp:TextBox ID="txtBoyLocal" runat="server" Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtBoyWard" runat="server" Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtBoyMgmt" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtBoyProtect" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtBoyChildClub" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td>
        </tr>
        <tr>
            <td>बालिका</td><td><asp:TextBox ID="txtGirlLocal" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtGirlWard" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtGirlMgmt" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtGirlProtect" runat="server" Text="0" TextMode="Number"></asp:TextBox></td><td><asp:TextBox ID="txtGirlChildClub" runat="server"  Text="0" TextMode="Number"></asp:TextBox></td>
        <td><asp:HiddenField ID ="hidMembership" runat="server" /></td>
             <td><asp:HiddenField ID ="hidMembership1" runat ="server"   /></td>
             </tr>
    </table>
    <table class="table table-striped">
        <tr>
            <td>५४.</td>
            <td>तपाईको परिवारका कुनै सदस्य स्थानीय कुनै संघसंस्था वा समूहमा आवद्ध हुनुहुन्छ ?</td>
            <td><asp:RadioButtonList ID="rblMembership" runat="server" 
                        DataSourceID="XmlDataSource_Community" DataTextField="name" 
                        DataValueField="id">
                    </asp:RadioButtonList>
                    <asp:XmlDataSource ID="XmlDataSource_Community" runat="server" 
                        DataFile="~/XMLDataSource/Community.xml"></asp:XmlDataSource></td>
        </tr>
        <tr>
            <td></td>
            <td>तपाई कहिल्यै प्यारालिगल कमिटिमा जानु भएको छ ? यदि छ भने किन?</td>
            <td><asp:RadioButtonList ID="rblParaLegal" runat="server" 
                        DataSourceID="XmlDataSource_ParalegalCommunity" DataTextField="name" 
                        DataValueField="id">
                    </asp:RadioButtonList>
                    <asp:XmlDataSource ID="XmlDataSource_ParalegalCommunity" runat="server" 
                        DataFile="~/XMLDataSource/ParalegalCommunity.xml"></asp:XmlDataSource></td>
        </tr>
        <tr>
            <td></td>
            <td>यदि तपाईले कुनै उजुर गर्नु भएको भए प्यारालिगल कमिटिको काम कारवाहिप्रति तपाई सन्तुष्ट हुनहुन्छ?</td>
            <td><asp:RadioButtonList ID="rblWorkSatisfy" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem> </asp:RadioButtonList></td>
             <td><asp:HiddenField ID ="hidLocalOrg" runat ="server"  Value ='<%#Eval("LOCAL_ORG_MEMBERSHIP_ID") %>' /></td>
           </tr>
           <tr>
           
                <td>
                    &nbsp;</td>
                    <td> </td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary"/>
                   <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnNext_Click" 
                         OnClientClick="return validatePage();"/>
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary"  
                         OnClientClick="return validatePage();" OnClick ="btnUpdate_Click"/>
                </td>
               
                
            </tr>
                        </table>
</asp:Content>