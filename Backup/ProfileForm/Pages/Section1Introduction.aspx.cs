﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer.Pages;

namespace ProfileForm.Pages
{
    public partial class Section1Introduction : System.Web.UI.Page
    {

        OwnerDao objOwnerDao = new OwnerDao();
        OwnerBo objBo = new OwnerBo();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtWard.Text = Session["WardNo"].ToString();
                txtGharNo.Text = Session["GharNo"].ToString();
                txtVdcName.Text = Session["VDC"].ToString();
                if (Request.QueryString["Info_Id"] != null)
                {
                    Session["Info_Id"] = Request.QueryString["Info_Id"];
                    int Info_Id = Convert.ToInt32(Session["Info_Id"].ToString());

                    BindData(Info_Id);
                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;
                }
            }
        }

        private void BindData(int Info_Id)
        {
            DataTable dt = new DataTable();
            dt=objOwnerDao.FetchSection1(Info_Id);
            if (dt != null && dt.Rows.Count > 0)
            {
                
                txtGharMuli.Text = dt.Rows[0]["OWNER_NAME"].ToString();
                rdoSex.SelectedIndex = Convert.ToInt32(dt.Rows[0]["SEX_ID"].ToString());
                ddlCaste.SelectedIndex = Convert.ToInt32(dt.Rows[0]["CASTE_ID"].ToString());
                ddlLanguage.SelectedIndex = Convert.ToInt32(dt.Rows[0]["LANGUAGE_ID"].ToString());
                ddlReligion.SelectedIndex = Convert.ToInt32(dt.Rows[0]["RELIGION_ID"].ToString());
                Session["Owner_Id"] = dt.Rows[0]["OWNER_ID"].ToString();
            }
        }

      
        protected void btnNext_Click(object sender, EventArgs e)
        {
            objBo.VillageId = int.Parse(Session["InfoId"].ToString());
            objBo.OwnerName = txtGharMuli.Text;
            objBo.SexId = rdoSex.SelectedIndex;
            objBo.CasteId = ddlCaste.SelectedIndex;
            objBo.ReligionId = ddlReligion.SelectedIndex;
            objBo.LanguageId = ddlLanguage.SelectedIndex;
            int i = objOwnerDao.InsertSection1(objBo);
            if (i > 0) //i मा OwnerId आउँछ 
            {
                Session["Owner_Id"] = i;
                Response.Redirect("Section2FamilyInfo.aspx");
            }
            else
            {
                Response.Write("<Script>alert('Error')</script>");
            }
          
        }



        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Info.aspx?Info_Id=" +Session["Info_Id"]); 
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            objBo.VillageId = int.Parse(Session["InfoId"].ToString());
            objBo.OwnerName = txtGharMuli.Text;
            objBo.SexId = rdoSex.SelectedIndex;
            objBo.CasteId = ddlCaste.SelectedIndex;
            objBo.ReligionId = ddlReligion.SelectedIndex;
            objBo.LanguageId = ddlLanguage.SelectedIndex;
            int i = objOwnerDao.UpdateSection(objBo);
            if (i > 0) //i मा OwnerId आउँछ 
            {
                //Session["Owner_Id"] = i;
                Response.Redirect("Section2FamilyInfo.aspx?Owner_Id=" +Session["Owner_Id"]);
            }
            else
            {
                Response.Write("<Script>alert('Error')</script>");
            }
        }
    }
}