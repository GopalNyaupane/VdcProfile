﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccessLayer.Pages;

namespace ProfileForm.Pages
{
    public partial class ListApplicants : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }
        ListApplicantsDAO obj = new ListApplicantsDAO();
        private void BindGrid()
        {
            DataTable dt = null;
            dt = obj.FetchData();
            ViewState["TabeApplicant"] = dt;
            if (dt != null && dt.Rows.Count > 0)
            {
                GrdList.DataSource = dt;
                GrdList.DataBind();
            }

        }

        protected void GrdList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                Session["InfoId"] = e.CommandArgument.ToString();
                DataTable dt = (DataTable)ViewState["TabeApplicant"];
                string strDataExpression = "InfoId = " + int.Parse(Session["InfoId"].ToString());
                DataRow[] filteredRows‍‍ = new DataRow[1000000];
                filteredRows‍‍ = dt.Select(strDataExpression);
                DataTable dtTemp = null;
                dtTemp = filteredRows‍‍.CopyToDataTable();
                Session["WardNo"] = dtTemp.Rows[0]["Ward"].ToString();
                Session["Tole"] = dtTemp.Rows[0]["Tole"].ToString();
                Session["GharMuli"] = dtTemp.Rows[0]["Owner"].ToString();
                Session["GharNo"] = dtTemp.Rows[0]["House"].ToString();
                Session["VDC"] = dtTemp.Rows[0]["Vdc"].ToString();
                Response.Redirect("Info.aspx?Info_Id=" + Session["InfoId"]);
            }
        }
    }
}