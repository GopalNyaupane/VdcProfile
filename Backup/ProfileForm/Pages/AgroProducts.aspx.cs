﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;

namespace ProfileForm.Pages
{
    public partial class LastSection : System.Web.UI.Page
    {
        private AgroProdBo objAgroBl = null;
        private AgroProdDao objAgrDal = new AgroProdDao();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int ownerId = Convert.ToInt32(Session["Owner_Id"].ToString());

                    BindHouseHoldDecision(ownerId);
                    BindRptrCropIncome(ownerId);
                    BindRptrVegIncome(ownerId);
                    BindRptrCashCropIncome(ownerId);
                    BindRptrFruitIncome(ownerId);
                    BindOtherIncome(ownerId);
                    BindRptrAnimalIncome(ownerId);

                    btnNext.Enabled = false;
                    btnNext.Visible = false;
                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;

                }
            }
        }

        private void BindRptrAnimalIncome(int ownerId)
        {
            objAgroBl = new AgroProdBo();
            objAgroBl.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objAgrDal.FetchAnimalIncome(objAgroBl);


            rptrAnimalIncome.DataSource = null;
            rptrAnimalIncome.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrAnimalIncome.DataSource = dt;
                rptrAnimalIncome.DataBind();
            }
        }

        private void BindOtherIncome(int ownerId)
        {
            objAgroBl = new AgroProdBo();
            objAgroBl.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objAgrDal.FetchOtherIncome(objAgroBl);

            if (dt != null && dt.Rows.Count > 0)
            {
                txtGhaseLnd.Text = Convert.ToString(dt.Rows[0]["GRASS_DALEY_LAND"]);
                txtGhaseIncome.Text = Convert.ToString(dt.Rows[0]["GRASS_DALAY_INCOME"]);
                txtBhuiLnd.Text = Convert.ToString(dt.Rows[0]["GRASS_BHUI_LAND"]);
                txtBhuiIncome.Text = Convert.ToString(dt.Rows[0]["GRASS_BHUI_INCOME"]);
                txtResamLnd.Text = Convert.ToString(dt.Rows[0]["RESHAM_LAND"]);
                txtResamIncome.Text = Convert.ToString(dt.Rows[0]["RESHAM_INCOME"]);
                txtJdbLnd.Text = Convert.ToString(dt.Rows[0]["MEDICINE_LAND"]);
                txtJdbIncome.Text = Convert.ToString(dt.Rows[0]["MEDICINE_INCOME"]);
                txtOtherLnd.Text = Convert.ToString(dt.Rows[0]["OTHER_LAND"]);
                txtOtherIncome.Text = Convert.ToString(dt.Rows[0]["OTHER_LAND_INCOME"]);

            }


        }

        private void BindRptrFruitIncome(int ownerId)
        {
            objAgroBl = new AgroProdBo();
            objAgroBl.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objAgrDal.FetchFruitsIncome(objAgroBl);


            rptrFruits.DataSource = null;
            rptrFruits.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrFruits.DataSource = dt;
                rptrFruits.DataBind();
            }
        }

        private void BindRptrCashCropIncome(int ownerId)
        {
            objAgroBl = new AgroProdBo();
            objAgroBl.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objAgrDal.FetchCashCropIncome(objAgroBl);


            rptrCashCrops.DataSource = null;
            rptrCashCrops.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrCashCrops.DataSource = dt;
                rptrCashCrops.DataBind();
            }
        }

        private void BindRptrVegIncome(int ownerId)
        {
            objAgroBl = new AgroProdBo();
            objAgroBl.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objAgrDal.FetchVegIncome(objAgroBl);


            rptrVeg.DataSource = null;
            rptrVeg.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrVeg.DataSource = dt;
                rptrVeg.DataBind();
            }
        }

        private void BindRptrCropIncome(int ownerId)
        {
            objAgroBl = new AgroProdBo();
            objAgroBl.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objAgrDal.FetchCropIncome(objAgroBl);


            rptrCrops.DataSource = null;
            rptrCrops.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                string fyear = dt.Rows[0]["FISCAL_YEAR"].ToString();
                txtDate.Text = fyear;
                rptrCrops.DataSource = dt;
                rptrCrops.DataBind();
            }
        }


        private void BindHouseHoldDecision(int ownerId)
        {
            objAgroBl = new AgroProdBo();
            objAgroBl.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objAgrDal.FetchHouseHoldDecision(objAgroBl);

            if (dt != null && dt.Rows.Count > 0)
            {
                rdoHousHoldDecision.SelectedIndex = Convert.ToInt32(dt.Rows[0]["HOUSEHOLD_DECISION"]);
                rdoHouseHldInvolved.SelectedIndex = Convert.ToInt32(dt.Rows[0]["HOUSEHOLD_INVOLVED"]);
                rdoBankAcc.SelectedIndex = Convert.ToInt32(dt.Rows[0]["BANK_ACCOUNT"]);
                rdoConsumerCom.SelectedIndex = Convert.ToInt32(dt.Rows[0]["CONSUMER_COM"]);
                rdoSchoolMngmnt.SelectedIndex = Convert.ToInt32(dt.Rows[0]["SCHOOL_MGMT"]);
                rdoBusinessParticipation.SelectedIndex = Convert.ToInt32(dt.Rows[0]["BUSINESS_PARTICIPATION"]);
            }
        }

        protected void btnAddCropsIncome_OnClick(object sender, EventArgs e)
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("CROPS_ID");
            dtA.Columns.Add("CROPS_INCOME_ID");

            dtA.Columns.Add("LAND_AREA");
            dtA.Columns.Add("INCOME");


            DropDownList ddlName = new DropDownList();

            HiddenField hidCrop = new HiddenField();
            TextBox txtArea = new TextBox();
            TextBox txtIncome = new TextBox();

            foreach (RepeaterItem rptItem in rptrCrops.Items)
            {
                DataRow drA = dtA.NewRow();
                ddlName = (DropDownList) rptItem.FindControl("ddlCrops");
                hidCrop = (HiddenField) rptItem.FindControl("hidCropsIncId");
                txtArea = (TextBox) rptItem.FindControl("txtArea");
                txtIncome = (TextBox) rptItem.FindControl("txtIncome");

                drA["CROPS_ID"] = ddlName.SelectedValue;
                drA["CROPS_INCOME_ID"] = hidCrop.Value;
                drA["LAND_AREA"] = txtArea.Text;
                drA["INCOME"] = txtIncome.Text;

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["CROPS_ID"] = 0;
            drB["LAND_AREA"] = 0;
            drB["INCOME"] = 0;
            drB["CROPS_INCOME_ID"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrCrops.DataSource = db;
            rptrCrops.DataBind();

            rptrCrops.DataSource = dtA;
            rptrCrops.DataBind();
        }

        protected void btnAddCashCropsIncome_OnClick(object sender, EventArgs e)
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("CASHCROPS_ID");
            dtA.Columns.Add("CASHCROPS_INCOME_ID");
            dtA.Columns.Add("LAND_AREA");
            dtA.Columns.Add("INCOME");


            DropDownList ddlName = new DropDownList();
            HiddenField hidCash = new HiddenField();

            TextBox txtArea = new TextBox();
            TextBox txtIncome = new TextBox();

            foreach (RepeaterItem rptItem in rptrCashCrops.Items)
            {
                DataRow drA = dtA.NewRow();
                ddlName = (DropDownList) rptItem.FindControl("ddlName");
                hidCash = (HiddenField) rptItem.FindControl("hidCashCropIncId");
                txtArea = (TextBox) rptItem.FindControl("txtArea");
                txtIncome = (TextBox) rptItem.FindControl("txtIncome");

                drA["CASHCROPS_INCOME_ID"] = hidCash.Value;
                drA["CASHCROPS_ID"] = ddlName.SelectedIndex;
                drA["LAND_AREA"] = txtArea.Text;
                drA["INCOME"] = txtIncome.Text;

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["CASHCROPS_INCOME_ID"] = 0;
            drB["CASHCROPS_ID"] = 0;
            drB["LAND_AREA"] = 0;
            drB["INCOME"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrCashCrops.DataSource = db;
            rptrCashCrops.DataBind();

            rptrCashCrops.DataSource = dtA;
            rptrCashCrops.DataBind();
        }

        protected void btnAddVegetablesIncome_OnClick(object sender, EventArgs e)
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("VEG_INCOME_ID");
            dtA.Columns.Add("VEG_ID");
            dtA.Columns.Add("LAND_AREA");
            dtA.Columns.Add("INCOME");

            DropDownList ddlName = new DropDownList();
            TextBox txtArea = new TextBox();
            TextBox txtIncome = new TextBox();
            HiddenField hidVeg = new HiddenField();

            foreach (RepeaterItem rptItem in rptrVeg.Items)
            {
                DataRow drA = dtA.NewRow();
                ddlName = (DropDownList) rptItem.FindControl("ddlName");
                hidVeg = (HiddenField) rptItem.FindControl("hidVegIncId");
                txtArea = (TextBox) rptItem.FindControl("txtArea");
                txtIncome = (TextBox) rptItem.FindControl("txtIncome");

                drA["VEG_INCOME_ID"] = hidVeg.Value;
                drA["VEG_ID"] = ddlName.SelectedIndex;
                drA["LAND_AREA"] = txtArea.Text;
                drA["INCOME"] = txtIncome.Text;

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["VEG_ID"] = 0;
            drB["VEG_INCOME_ID"] = 0;
            drB["LAND_AREA"] = 0;
            drB["INCOME"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrVeg.DataSource = db;
            rptrVeg.DataBind();

            rptrVeg.DataSource = dtA;
            rptrVeg.DataBind();
        }

        protected void btnAddFruitsIncome_OnClick(object sender, EventArgs e)
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("FRUITS_INCOME_ID");
            dtA.Columns.Add("FRUITS_ID");
            dtA.Columns.Add("LAND_AREA");
            dtA.Columns.Add("INCOME");

            DropDownList ddlName = new DropDownList();
            HiddenField hidFruit = new HiddenField();
            TextBox txtArea = new TextBox();
            TextBox txtIncome = new TextBox();

            foreach (RepeaterItem rptItem in rptrFruits.Items)
            {
                DataRow drA = dtA.NewRow();
                ddlName = (DropDownList) rptItem.FindControl("ddlName");
                hidFruit = (HiddenField) rptItem.FindControl("hidFruitsIncId");
                txtArea = (TextBox) rptItem.FindControl("txtArea");
                txtIncome = (TextBox) rptItem.FindControl("txtIncome");

                drA["FRUITS_INCOME_ID"] = hidFruit.Value;
                drA["FRUITS_ID"] = ddlName.SelectedIndex;
                drA["LAND_AREA"] = txtArea.Text;
                drA["INCOME"] = txtIncome.Text;

                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["FRUITS_INCOME_ID"] = 0;
            drB["FRUITS_ID"] = 0;
            drB["LAND_AREA"] = 0;
            drB["INCOME"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrFruits.DataSource = db;
            rptrFruits.DataBind();

            rptrFruits.DataSource = dtA;
            rptrFruits.DataBind();
        }

        protected void btnAddAnimalsIncome_OnClick(object sender, EventArgs e)
        {
            DataTable dtA = new DataTable();
            dtA.Clear();
            dtA.Columns.Add("ANIMALS_ID");
            dtA.Columns.Add("ANIMALS_INCOME_ID");
            dtA.Columns.Add("ANIMALS_LOCAL");
            dtA.Columns.Add("ANIMALS_HYBRID");
            dtA.Columns.Add("MILK_PRODUCTION");
            dtA.Columns.Add("MEAT_PRODUCTION");
            dtA.Columns.Add("WOOL_PRODUCTION");
            dtA.Columns.Add("EGG_PRODUCTION");
            dtA.Columns.Add("HONEY_PRODUCTION");

            HiddenField hidAnimal = new HiddenField();
            DropDownList ddlName = new DropDownList();
            TextBox txtHybrid = new TextBox();
            TextBox txtLocal = new TextBox();
            TextBox txtMilk = new TextBox();
            TextBox txtMeat = new TextBox();
            TextBox txtWool = new TextBox();
            TextBox txtEgg = new TextBox();
            TextBox txtHoney = new TextBox();

            foreach (RepeaterItem rptItem in rptrAnimalIncome.Items)
            {
                DataRow drA = dtA.NewRow();
                ddlName = (DropDownList) rptItem.FindControl("ddlName");
                hidAnimal = (HiddenField) rptItem.FindControl("hidAnimalId");

                txtMilk = (TextBox) rptItem.FindControl("txtMilk");
                txtLocal = (TextBox) rptItem.FindControl("txtLocal");

                txtHybrid = (TextBox) rptItem.FindControl("txtHybrid");
                txtMeat = (TextBox) rptItem.FindControl("txtMeat");


                txtWool = (TextBox) rptItem.FindControl("txtWool");
                txtEgg = (TextBox) rptItem.FindControl("txtEgg");

                txtHoney = (TextBox) rptItem.FindControl("txtHoney");


                drA["ANIMALS_INCOME_ID"] = hidAnimal.Value;
                drA["ANIMALS_ID"] = ddlName.SelectedIndex;
                drA["ANIMALS_LOCAL"] = txtLocal.Text;
                drA["ANIMALS_HYBRID"] = txtHybrid.Text;
                drA["MILK_PRODUCTION"] = txtMilk.Text;
                drA["MEAT_PRODUCTION"] = txtMeat.Text;
                drA["WOOL_PRODUCTION"] = txtWool.Text;
                drA["EGG_PRODUCTION"] = txtEgg.Text;
                drA["HONEY_PRODUCTION"] = txtHoney.Text;



                dtA.Rows.Add(drA);
            }

            DataRow drB = dtA.NewRow();
            drB["ANIMALS_INCOME_ID"] = 0;
            drB["ANIMALS_ID"] = 0;
            drB["ANIMALS_LOCAL"] = 0;
            drB["ANIMALS_HYBRID"] = 0;
            drB["MILK_PRODUCTION"] = 0;
            drB["MEAT_PRODUCTION"] = 0;
            drB["WOOL_PRODUCTION"] = 0;
            drB["EGG_PRODUCTION"] = 0;
            drB["HONEY_PRODUCTION"] = 0;
            dtA.Rows.Add(drB);

            // to empty the repeater
            DataTable db = new DataTable();
            rptrAnimalIncome.DataSource = db;
            rptrAnimalIncome.DataBind();

            rptrAnimalIncome.DataSource = dtA;
            rptrAnimalIncome.DataBind();
        }

        protected void btnNext_OnClick(object sender, EventArgs e)
        {
            //Session["OwnerID"] = 99;
            int val;
            string date = txtDate.Text;

            objAgroBl = new AgroProdBo();

            objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objAgroBl.HousHoldDecision = rdoHousHoldDecision.SelectedIndex;
            objAgroBl.HouseHldInvolved = rdoHouseHldInvolved.SelectedIndex;
            objAgroBl.BankAcc = rdoBankAcc.SelectedIndex;
            objAgroBl.ConsumerCom = rdoConsumerCom.SelectedIndex;
            objAgroBl.SchoolMngmnt = rdoSchoolMngmnt.SelectedIndex;
            objAgroBl.BusinessParticipation = rdoBusinessParticipation.SelectedIndex;

            val = objAgrDal.InsertHouseHoldDecisions(objAgroBl); //Insert HouseHold Decisions


            foreach (RepeaterItem rptr in rptrCrops.Items)
            {
                objAgroBl = new AgroProdBo();

                DropDownList ddlCrops = (DropDownList) rptr.FindControl("ddlCrops");
                TextBox txtArea = (TextBox) rptr.FindControl("txtArea");
                TextBox txtIncome = (TextBox) rptr.FindControl("txtIncome");



                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.Date = date;
                objAgroBl.Income = txtIncome.Text;
                objAgroBl.CropGroup = ddlCrops.SelectedIndex;
                objAgroBl.LandArea = Convert.ToDecimal(txtArea.Text);

                val = objAgrDal.InsertCropIncomeDetail(objAgroBl); //Insert Crops Income Details

            }


            foreach (RepeaterItem rptr in rptrVeg.Items)
            {
                objAgroBl = new AgroProdBo();

                DropDownList ddlName = (DropDownList) rptr.FindControl("ddlName");
                TextBox txtArea = (TextBox) rptr.FindControl("txtArea");
                TextBox txtIncome = (TextBox) rptr.FindControl("txtIncome");


                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.Date = date;
                objAgroBl.VegIncome = txtIncome.Text;
                objAgroBl.VegId = ddlName.SelectedIndex;
                objAgroBl.VegLand = Convert.ToDecimal(txtArea.Text);

                val = objAgrDal.InsertVegetableIncomeDetail(objAgroBl); //Insert Vegetable Income Details

            }


            foreach (RepeaterItem rptr in rptrCashCrops.Items)
            {
                objAgroBl = new AgroProdBo();

                DropDownList ddlName = (DropDownList) rptr.FindControl("ddlName");
                TextBox txtArea = (TextBox) rptr.FindControl("txtArea");
                TextBox txtIncome = (TextBox) rptr.FindControl("txtIncome");


                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.Date = date;
                objAgroBl.CashCropIncome = txtIncome.Text;
                objAgroBl.CashCropId = ddlName.SelectedIndex;
                objAgroBl.CashCropLand = Convert.ToDecimal(txtArea.Text);

                val = objAgrDal.InsertCashCropIncomeDetail(objAgroBl); //Insert CashCrops Income Details

            }

            foreach (RepeaterItem rptr in rptrFruits.Items)
            {
                objAgroBl = new AgroProdBo();

                DropDownList ddlName = (DropDownList) rptr.FindControl("ddlName");
                TextBox txtArea = (TextBox) rptr.FindControl("txtArea");
                TextBox txtIncome = (TextBox) rptr.FindControl("txtIncome");


                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.Date = date;
                objAgroBl.FruitIncome = txtIncome.Text;
                objAgroBl.FruitId = ddlName.SelectedIndex;
                objAgroBl.FruitLand = Convert.ToDecimal(txtArea.Text);

                val = objAgrDal.InsertFruitsIncomeDetail(objAgroBl); //Insert Fruits Income Details

            }


            foreach (RepeaterItem rptr in rptrAnimalIncome.Items)
            {
                objAgroBl = new AgroProdBo();

                DropDownList ddlName = (DropDownList) rptr.FindControl("ddlName");
                TextBox txtLocal = (TextBox) rptr.FindControl("txtLocal");
                TextBox txtHybrid = (TextBox) rptr.FindControl("txtHybrid");
                TextBox txtMilk = (TextBox) rptr.FindControl("txtMilk");
                TextBox txtMeat = (TextBox) rptr.FindControl("txtMeat");
                TextBox txtWool = (TextBox) rptr.FindControl("txtWool");
                TextBox txtEgg = (TextBox) rptr.FindControl("txtEgg");
                TextBox txtHoney = (TextBox) rptr.FindControl("txtHoney");


                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.AnimalId = ddlName.SelectedIndex;
                objAgroBl.LocalAnimal = Convert.ToInt32(txtLocal.Text);
                objAgroBl.HybridAnimal = Convert.ToInt32(txtHybrid.Text);
                objAgroBl.MilkProd = Convert.ToDecimal(txtMilk.Text);
                objAgroBl.MeatProd = Convert.ToDecimal(txtMeat.Text);
                objAgroBl.WoolProd = Convert.ToDecimal(txtWool.Text);
                objAgroBl.EggProd = Convert.ToDecimal(txtEgg.Text);
                objAgroBl.HoneyProd = Convert.ToDecimal(txtHoney.Text);


                val = objAgrDal.InsertAnimalIncomeDetail(objAgroBl); //Insert Animal Income Details

            }

            objAgroBl = new AgroProdBo();

            objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objAgroBl.GhaseLnd = Convert.ToDecimal(txtGhaseLnd.Text);
            objAgroBl.GhaseIncome = Convert.ToDecimal(txtGhaseIncome.Text);
            objAgroBl.BhuiLnd = Convert.ToDecimal(txtBhuiLnd.Text);
            objAgroBl.BhuiIncome = Convert.ToDecimal(txtBhuiIncome.Text);
            objAgroBl.ResamLnd = Convert.ToDecimal(txtResamLnd.Text);
            objAgroBl.ResamIncome = Convert.ToDecimal(txtResamIncome.Text);
            objAgroBl.JdbLnd = Convert.ToDecimal(txtJdbLnd.Text);
            objAgroBl.JdbIncome = Convert.ToDecimal(txtJdbIncome.Text);
            objAgroBl.OtherLnd = Convert.ToDecimal(txtOtherLnd.Text);
            objAgroBl.OtherIncome = Convert.ToDecimal(txtOtherIncome.Text);

            val = objAgrDal.InsertOtherIncomeDetail(objAgroBl); //Insert Other Income Details

            Response.Redirect("DisasterReport.aspx");


        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            objAgroBl = new AgroProdBo();
            UpdateHouseholdDecision(objAgroBl);
            objAgroBl = new AgroProdBo();
            UpdateCropIncome(objAgroBl);
            objAgroBl = new AgroProdBo();
            UpdateVegIncome(objAgroBl);
            objAgroBl = new AgroProdBo();
            UpdateCashCropIncome(objAgroBl);
            objAgroBl = new AgroProdBo();
            UpdateFruitIncome(objAgroBl);
            objAgroBl = new AgroProdBo();
            UpdateOtherIncome(objAgroBl);
            objAgroBl = new AgroProdBo();
            UpdateAnimalIncome(objAgroBl);
            Response.Redirect("DisasterReport.aspx?Owner_Id="+Session["Owner_Id"]);
        }

        private void UpdateAnimalIncome(AgroProdBo objAgroBl)
        {
            foreach (RepeaterItem rptr in rptrAnimalIncome.Items)
            {

                DropDownList ddlName = (DropDownList) rptr.FindControl("ddlName");
                TextBox txtLocal = (TextBox) rptr.FindControl("txtLocal");
                TextBox txtHybrid = (TextBox) rptr.FindControl("txtHybrid");
                TextBox txtMilk = (TextBox) rptr.FindControl("txtMilk");
                TextBox txtMeat = (TextBox) rptr.FindControl("txtMeat");
                TextBox txtWool = (TextBox) rptr.FindControl("txtWool");
                TextBox txtEgg = (TextBox) rptr.FindControl("txtEgg");
                TextBox txtHoney = (TextBox) rptr.FindControl("txtHoney");
                HiddenField hidAnimalId = (HiddenField) rptr.FindControl("hidAnimalId");


                objAgroBl.UserId = Convert.ToInt32(hidAnimalId.Value);
                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.AnimalId = ddlName.SelectedIndex;
                objAgroBl.LocalAnimal = Convert.ToInt32(txtLocal.Text);
                objAgroBl.HybridAnimal = Convert.ToInt32(txtHybrid.Text);
                objAgroBl.MilkProd = Convert.ToDecimal(txtMilk.Text);
                objAgroBl.MeatProd = Convert.ToDecimal(txtMeat.Text);
                objAgroBl.WoolProd = Convert.ToDecimal(txtWool.Text);
                objAgroBl.EggProd = Convert.ToDecimal(txtEgg.Text);
                objAgroBl.HoneyProd = Convert.ToDecimal(txtHoney.Text);


                objAgrDal.UpdateAnimalIncomeDetail(objAgroBl);

            }
        }

        private void UpdateOtherIncome(AgroProdBo objAgroBl)
        {


            objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objAgroBl.GhaseLnd = Convert.ToDecimal(txtGhaseLnd.Text);
            objAgroBl.GhaseIncome = Convert.ToDecimal(txtGhaseIncome.Text);
            objAgroBl.BhuiLnd = Convert.ToDecimal(txtBhuiLnd.Text);
            objAgroBl.BhuiIncome = Convert.ToDecimal(txtBhuiIncome.Text);
            objAgroBl.ResamLnd = Convert.ToDecimal(txtResamLnd.Text);
            objAgroBl.ResamIncome = Convert.ToDecimal(txtResamIncome.Text);
            objAgroBl.JdbLnd = Convert.ToDecimal(txtJdbLnd.Text);
            objAgroBl.JdbIncome = Convert.ToDecimal(txtJdbIncome.Text);
            objAgroBl.OtherLnd = Convert.ToDecimal(txtOtherLnd.Text);
            objAgroBl.OtherIncome = Convert.ToDecimal(txtOtherIncome.Text);

            objAgrDal.UpdateOtherIncomeDetail(objAgroBl);
        }

        private void UpdateFruitIncome(AgroProdBo objAgroBl)
        {
            string date = txtDate.Text;
            foreach (RepeaterItem rptr in rptrFruits.Items)
            {

                DropDownList ddlName = (DropDownList) rptr.FindControl("ddlName");
                TextBox txtArea = (TextBox) rptr.FindControl("txtArea");
                TextBox txtIncome = (TextBox) rptr.FindControl("txtIncome");
                HiddenField hidFruitsIncId = (HiddenField) rptr.FindControl("hidFruitsIncId");


                objAgroBl.UserId = Convert.ToInt32(hidFruitsIncId.Value);
                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.Date = date;
                objAgroBl.FruitIncome = txtIncome.Text;
                objAgroBl.FruitId = ddlName.SelectedIndex;
                objAgroBl.FruitLand = Convert.ToDecimal(txtArea.Text);

                objAgrDal.UpdateFruitsIncomeDetail(objAgroBl);

            }
        }

        private void UpdateCashCropIncome(AgroProdBo objAgroBl)
        {
            string date = txtDate.Text;
            foreach (RepeaterItem rptr in rptrCashCrops.Items)
            {

                DropDownList ddlName = (DropDownList) rptr.FindControl("ddlName");
                TextBox txtArea = (TextBox) rptr.FindControl("txtArea");
                TextBox txtIncome = (TextBox) rptr.FindControl("txtIncome");
                HiddenField hidCashCropIncId = (HiddenField) rptr.FindControl("hidCashCropIncId");


                objAgroBl.UserId = Convert.ToInt32(hidCashCropIncId.Value);
                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.Date = date;
                objAgroBl.CashCropIncome = txtIncome.Text;
                objAgroBl.CashCropId = ddlName.SelectedIndex;
                objAgroBl.CashCropLand = Convert.ToDecimal(txtArea.Text);

                objAgrDal.UpdateCashCropIncomeDetail(objAgroBl);

            }
        }

        private void UpdateVegIncome(AgroProdBo objAgroBl)
        {
            string date = txtDate.Text;
            foreach (RepeaterItem rptr in rptrVeg.Items)
            {

                DropDownList ddlName = (DropDownList) rptr.FindControl("ddlName");
                TextBox txtArea = (TextBox) rptr.FindControl("txtArea");
                TextBox txtIncome = (TextBox) rptr.FindControl("txtIncome");
                HiddenField hidVegIncId = (HiddenField) rptr.FindControl("hidVegIncId");


                objAgroBl.UserId = Convert.ToInt32(hidVegIncId.Value);
                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.Date = date;
                objAgroBl.VegIncome = txtIncome.Text;
                objAgroBl.VegId = ddlName.SelectedIndex;
                objAgroBl.VegLand = Convert.ToDecimal(txtArea.Text);

                objAgrDal.UpdateVegetableIncomeDetail(objAgroBl);

            }
        }

        private void UpdateCropIncome(AgroProdBo objAgroBl)
        {
            string date = txtDate.Text;
            foreach (RepeaterItem rptr in rptrCrops.Items)
            {

                DropDownList ddlCrops = (DropDownList) rptr.FindControl("ddlCrops");
                TextBox txtArea = (TextBox) rptr.FindControl("txtArea");
                TextBox txtIncome = (TextBox) rptr.FindControl("txtIncome");
                HiddenField hidCropsIncId = (HiddenField) rptr.FindControl("hidCropsIncId");

                int a = Int32.Parse(hidCropsIncId.Value);

                objAgroBl.UserId = Convert.ToInt32(hidCropsIncId.Value);
                objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                objAgroBl.Date = date;
                objAgroBl.Income = txtIncome.Text;
                objAgroBl.CropGroup = ddlCrops.SelectedIndex;
                objAgroBl.LandArea = Convert.ToDecimal(txtArea.Text);

                objAgrDal.UpdateCropIncomeDetail(objAgroBl);

            }
        }

        private void UpdateHouseholdDecision(AgroProdBo objAgroBl)
        {
            string date = txtDate.Text;

            objAgroBl.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());
            objAgroBl.HousHoldDecision = rdoHousHoldDecision.SelectedIndex;
            objAgroBl.HouseHldInvolved = rdoHouseHldInvolved.SelectedIndex;
            objAgroBl.BankAcc = rdoBankAcc.SelectedIndex;
            objAgroBl.ConsumerCom = rdoConsumerCom.SelectedIndex;
            objAgroBl.SchoolMngmnt = rdoSchoolMngmnt.SelectedIndex;
            objAgroBl.BusinessParticipation = rdoBusinessParticipation.SelectedIndex;

            objAgrDal.UpdateHouseHoldDecisions(objAgroBl);
        }
    }
}