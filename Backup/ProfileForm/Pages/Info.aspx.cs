﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer.Pages;

namespace ProfileForm.Pages
{
    public partial class Info : System.Web.UI.Page
    {
        InfoDao objInfoDao = new InfoDao();
        InfoBo objInfoBo = new InfoBo();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateVDC();
                if (Request.QueryString["Info_Id"] != null)
                {
                    Session["Info_Id"] = Request.QueryString["Info_Id"];
                    int Info_Id = Convert.ToInt32(Session["Info_Id"].ToString());

                    BindData(Info_Id);
                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;
                }
                else
                btnUpdate.Visible = false;
            }
        }

        private void BindData(int Info_Id)
        {
            objInfoBo.VillageId = Info_Id;
            DataTable dt = new DataTable();
            dt = objInfoDao.FetchVillageInfo(objInfoBo);
            if(dt != null && dt.Rows.Count>0)
            {
                ddlVDC.SelectedValue = dt.Rows[0]["VDC_ID"].ToString();
                ddlWoda.SelectedIndex = Convert.ToInt32(dt.Rows[0]["WARD_NUM"].ToString());
                txtToleBasti.Text = dt.Rows[0]["VILLAGE_NAME"].ToString();
                txtHouseNo.Text = dt.Rows[0]["HOUSE_NO"].ToString();
            }
        }

        private void PopulateVDC()
        {
            DataTable dt = objInfoDao.PopulateMyagdiVdc();

            ddlVDC.DataSource = dt;
            ddlVDC.DataTextField = "VDC_NAME_NEP";
            ddlVDC.DataValueField = "VDC_ID";
            ddlVDC.DataBind();
            ddlVDC.Items.Insert(0, "-गाविस छान्नुहोस्-"); 
        }


        protected void btnNext_Click(object sender, EventArgs e)
        {
            objInfoBo = new InfoBo();
            objInfoBo.VdcId = int.Parse(ddlVDC.SelectedValue);
            objInfoBo.WardNum = int.Parse(ddlWoda.SelectedValue);
            objInfoBo.HouseNo = txtHouseNo.Text;
            objInfoBo.VillageName = txtToleBasti.Text;
            int i = objInfoDao.InsertVillageInfo(objInfoBo); //i मा InfoId आउँछ  
            if (i > 0)
            {
                Session["InfoId"] = i;
                Session["WardNo"] = ddlWoda.SelectedValue;
                Session["GharNo"] = txtHouseNo.Text;
                Session["VDC"] = ddlVDC.SelectedValue;
                Response.Redirect("Section1Introduction.aspx");
            }
           
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            objInfoBo = new InfoBo();
            objInfoBo.VillageId = Convert.ToInt32(Session["Info_Id"]);
            objInfoBo.VdcId = int.Parse(ddlVDC.SelectedValue);
            objInfoBo.WardNum = int.Parse(ddlWoda.SelectedValue);
            objInfoBo.HouseNo = txtHouseNo.Text;
            objInfoBo.VillageName = txtToleBasti.Text;
            int i = objInfoDao.UpdateVillageInfo(objInfoBo); //i मा InfoId आउँछ  
            if (i > 0)
            {
                /*Session["InfoId"] = i;
                Session["WardNo"] = ddlWoda.SelectedValue;
                Session["GharNo"] = txtHouseNo.Text;
                Session["VDC"] = ddlVDC.SelectedValue;*/
                Response.Redirect("Section1Introduction.aspx?Info_ID="+Session["Info_Id"]);
            }
           
        }
    }
}