﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;

namespace ProfileForm.Pages
{
    public partial class Marriage : System.Web.UI.Page
    {
        MarriageBo objMrgInfo = null;
        MarriageDao objMarriage = new MarriageDao(); 

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int ownerId = Convert.ToInt32(Session["Owner_Id"].ToString());
                    BindMaritalInfo(ownerId);

                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;
                }
            }

        }

        private void BindMaritalInfo(int ownerId)
        {
            objMrgInfo = new MarriageBo();
            objMrgInfo.OwnerId = ownerId;
            DataTable dt = new DataTable();
            dt = objMarriage.FetchMaritalInfo(objMrgInfo);

            txtMarriageCount.Text = dt.Rows.Count.ToString();

            rptrMarriage.DataSource = null;
            rptrMarriage.DataBind();

            if (dt != null && dt.Rows.Count > 0)
            {
                rptrMarriage.DataSource = dt;
                rptrMarriage.DataBind();
            }

        }

        protected void btnOk_OnClick(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(txtMarriageCount.Text);
            DataTable dtMarriage = new DataTable();
            BindMarriage(dtMarriage, count);
        }

        private void BindMarriage(DataTable dtMarriage, int count)
        {
            DataTable dtMaritalStatus = new DataTable();
            dtMaritalStatus.Clear();
            dtMaritalStatus.Columns.Add("AGE");
            dtMaritalStatus.Columns.Add("SEX_ID");
            dtMaritalStatus.Columns.Add("MARITAL_INFO_ID");
           


            int j = 0;
            if (dtMarriage.Rows.Count == 0)
            {

                for (int i = 0; i < count; i++)
                {
                    DataRow _dtRow = dtMaritalStatus.NewRow();
                    _dtRow["AGE"] = 0;
                    _dtRow["SEX_ID"] = 0;
                    _dtRow["MARITAL_INFO_ID"] = 0;
                    
                    dtMaritalStatus.Rows.Add(_dtRow);
                }

            }
            else
            {
                for (int i = 0; i < dtMarriage.Rows.Count; i++)
                {
                    DataRow _dtRow = dtMaritalStatus.NewRow();
                  _dtRow["AGE"] = dtMarriage.Rows[i]["AGE"].ToString();
                   _dtRow["SEX_ID"] = dtMarriage.Rows[i]["SEX_ID"].ToString();
                    _dtRow["MARITAL_INFO_ID"] = dtMarriage.Rows[i]["MARITAL_INFO_ID"].ToString(); ;
                    

                    dtMaritalStatus.Rows.Add(_dtRow);
                }
            }
            rptrMarriage.DataSource = dtMaritalStatus;
            rptrMarriage.DataBind();
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            //Session["Owner_ID"] = "99";
            foreach (RepeaterItem rptr in rptrMarriage.Items)
            {
                objMrgInfo = new MarriageBo();

                TextBox txtChildAge = (TextBox)rptr.FindControl("txtChildAge");
                RadioButtonList RdoSex = (RadioButtonList)rptr.FindControl("RdoSex");

                objMrgInfo.OwnerId = Convert.ToInt32(Session["Owner_ID"].ToString());
                objMrgInfo.MarriageAge = Convert.ToInt32(txtChildAge.Text);
                objMrgInfo.SexId = RdoSex.SelectedIndex;

                int val = objMarriage.InsertMrgInfo(objMrgInfo);

            }
                Response.Redirect("ChildLabour.aspx");

        }

        protected void btnUpdateMarriage_OnClick(object sender, EventArgs e)
        {
            foreach (RepeaterItem rptr in rptrMarriage.Items)
            {
                objMrgInfo = new MarriageBo();

                TextBox txtChildAge = (TextBox)rptr.FindControl("txtChildAge");
                RadioButtonList RdoSex = (RadioButtonList)rptr.FindControl("RdoSex");

                HiddenField fieldId = (HiddenField)rptr.FindControl("fieldId");


                objMrgInfo.userId = Convert.ToInt32(fieldId.Value);
                objMrgInfo.MarriageAge = Convert.ToInt32(txtChildAge.Text);
                objMrgInfo.SexId = RdoSex.SelectedIndex;
                objMrgInfo.OwnerId = Convert.ToInt32(Session["Owner_Id"].ToString());

                objMarriage.UpdateMarriageInfo(objMrgInfo);
                Response.Redirect("ChildLabour.aspx?Owner_Id="+Session["Owner_Id"]);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ChildNutrition.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
    }
}