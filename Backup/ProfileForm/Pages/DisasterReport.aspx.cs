﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class DisasterReport : System.Web.UI.Page
    {
        DisasterReportBo objBo = new DisasterReportBo();
        DisasterReportDao objDao = new DisasterReportDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadDisasterDetails(Owner_Id);
                  



                    btnUpdate.Visible = true;
                    btnNext.Visible = false;

                }
                else
                {
                    btnUpdate.Visible = false;
                    btnNext.Visible = true;
                }
            }
       
        }

        private void LoadDisasterDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataSet ds = new DataSet();
            ds = objDao.Fetch_Disaster(objBo);
            DataTable dtDA = null;
            DataTable dtDPL= null;
            DataTable dtIO = null;
           
            if (ds != null && ds.Tables.Count > 0)
            {
                dtDA = ds.Tables[0];
                dtDPL = ds.Tables[1];
                dtIO = ds.Tables[2];


                hidAffect.Value = dtDA.Rows[0]["DISASTER_AFFECT_ID"].ToString();
                hidLoss.Value = dtDPL.Rows[0]["DISASTER_LOSS_ID"].ToString();
                hidIO.Value = dtIO.Rows[0]["OFFICIAL_ID"].ToString();

                //For TBL_DISASTER_AFFECT

                txtFlood.Text = dtDA.Rows[0]["FLOOD"].ToString();
                txtLandslide.Text = dtDA.Rows[0]["LANDSLIDE"].ToString();
                txtHailstorm.Text = dtDA.Rows[0]["HAILSTORM"].ToString();
                txtStrom.Text = dtDA.Rows[0]["STORM"].ToString();
                txtFire.Text = dtDA.Rows[0]["FIRE"].ToString();
                txtEarthquake.Text = dtDA.Rows[0]["EARTHQUAKE"].ToString();
                txtDrought.Text = dtDA.Rows[0]["DROUGHT"].ToString();
                txtArtibristi.Text = dtDA.Rows[0]["ATIBRISTI"].ToString();
                txtInfluenza.Text = dtDA.Rows[0]["INFLUENZA"].ToString();
                txtInsecticide.Text = dtDA.Rows[0]["INSECTICIDE"].ToString();
                txtOther.Text = dtDA.Rows[0]["OTHER"].ToString();

                txtFloodLoss.Text = dtDPL.Rows[0]["FLOOD"].ToString();
                txtLandslideLoss.Text = dtDPL.Rows[0]["LANDSLIDE"].ToString();
                txtHailstormLoss.Text = dtDPL.Rows[0]["HAILSTORM"].ToString();
                txtStromLoss.Text = dtDPL.Rows[0]["STORM"].ToString();
                txtFireLoss.Text = dtDPL.Rows[0]["FIRE"].ToString();
                txtEarthquakeLoss.Text = dtDPL.Rows[0]["EARTHQUAKE"].ToString();
                txtDroughtLoss.Text = dtDPL.Rows[0]["DROUGHT"].ToString();
                txtArtibristiLoss.Text = dtDPL.Rows[0]["ATIBRISTI"].ToString();
                txtInfluenzaLoss.Text = dtDPL.Rows[0]["INFLUENZA"].ToString();
                txtInsecticideLoss.Text = dtDPL.Rows[0]["INSECTICIDE"].ToString();
                txtOtherLoss.Text = dtDPL.Rows[0]["OTHER"].ToString();

                txtProviderName.Text = dtIO.Rows[0]["INFORMATION_PROVIDER"].ToString();
                txtCollectorName.Text = dtIO.Rows[0]["INFORMATION_COLLECTOR"].ToString();
                txtValidaterName.Text = dtIO.Rows[0]["INFORMATION_VALIDATER"].ToString();
                txtDate.Text = dtIO.Rows[0]["DATE_TAKEN"].ToString();



            }
            else
            {
                dtDA = null;
                dtDPL = null;
                dtIO = null;
                

            }

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            Session["Owner_Id"] = 9;
            //Code For table TBL_DISASTER_AFFECT
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.flood = Convert.ToInt32( txtFlood.Text);
            objBo.landslide = Convert.ToInt32(txtLandslide.Text);
            objBo.hailStorm = Convert.ToInt32(txtHailstorm.Text);
            objBo.storm = Convert.ToInt32(txtStrom.Text);
            objBo.fire = Convert.ToInt32(txtFire.Text);
            objBo.earthquake = Convert.ToInt32(txtEarthquake.Text);
            objBo.drought = Convert.ToInt32(txtDrought.Text);
            objBo.artibristi  = Convert.ToInt32(txtArtibristi .Text);
            objBo.influenza = Convert.ToInt32(txtInfluenza.Text);
            objBo.insecticide = Convert.ToInt32(txtInsecticide.Text);
            objBo.other = Convert.ToInt32(txtOther.Text);

            //CODE FOR TBL_DISASTER_PHYSICAL_LOSS
            objBo.floodLoss = Convert.ToDecimal(txtFloodLoss.Text);
            objBo.landslideLoss = Convert.ToDecimal(txtLandslideLoss.Text);
            objBo.hailStormLoss = Convert.ToDecimal(txtHailstormLoss.Text);
            objBo.stormLoss = Convert.ToDecimal(txtStromLoss.Text);
            objBo.fireLoss = Convert.ToDecimal(txtFireLoss.Text);
            objBo.earthquakeLoss = Convert.ToDecimal(txtEarthquakeLoss.Text);
            objBo.droughtLoss = Convert.ToDecimal(txtDroughtLoss.Text);
            objBo.artibristiLoss = Convert.ToDecimal(txtArtibristiLoss.Text);
            objBo.influenzaLoss = Convert.ToDecimal(txtInfluenzaLoss.Text);
            objBo.insecticideLoss = Convert.ToDecimal(txtInsecticideLoss.Text);
            objBo.otherLoss = Convert.ToDecimal(txtOtherLoss.Text);

            //CODE FOR TBL_INFORMATION_OFFICIAL
            objBo.provider = txtProviderName.Text;
            objBo.collector = txtCollectorName.Text;
            objBo.validater = txtValidaterName.Text;
            objBo.dataTaken = Convert.ToDateTime(txtDate.Text);

            int i = objDao.Disaster_Insert(objBo);

            if(i>0)
            {
                Response.Redirect("ListApplicants.aspx");
            }          


            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            Session["Owner_Id"] = 9;
            objBo.hidAffect = Convert.ToInt32(hidAffect.Value);
            objBo.hidLoss  = Convert.ToInt32(hidLoss.Value);
            objBo.hidIO = Convert.ToInt32(hidIO.Value);

            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.flood = Convert.ToInt32(txtFlood.Text);
            objBo.landslide = Convert.ToInt32(txtLandslide.Text);
            objBo.hailStorm = Convert.ToInt32(txtHailstorm.Text);
            objBo.storm = Convert.ToInt32(txtStrom.Text);
            objBo.fire = Convert.ToInt32(txtFire.Text);
            objBo.earthquake = Convert.ToInt32(txtEarthquake.Text);
            objBo.drought = Convert.ToInt32(txtDrought.Text);
            objBo.artibristi = Convert.ToInt32(txtArtibristi.Text);
            objBo.influenza = Convert.ToInt32(txtInfluenza.Text);
            objBo.insecticide = Convert.ToInt32(txtInsecticide.Text);
            objBo.other = Convert.ToInt32(txtOther.Text);

            //CODE FOR TBL_DISASTER_PHYSICAL_LOSS
            objBo.floodLoss = Convert.ToDecimal(txtFloodLoss.Text);
            objBo.landslideLoss = Convert.ToDecimal(txtLandslideLoss.Text);
            objBo.hailStormLoss = Convert.ToDecimal(txtHailstormLoss.Text);
            objBo.stormLoss = Convert.ToDecimal(txtStromLoss.Text);
            objBo.fireLoss = Convert.ToDecimal(txtFireLoss.Text);
            objBo.earthquakeLoss = Convert.ToDecimal(txtEarthquakeLoss.Text);
            objBo.droughtLoss = Convert.ToDecimal(txtDroughtLoss.Text);
            objBo.artibristiLoss = Convert.ToDecimal(txtArtibristiLoss.Text);
            objBo.influenzaLoss = Convert.ToDecimal(txtInfluenzaLoss.Text);
            objBo.insecticideLoss = Convert.ToDecimal(txtInsecticideLoss.Text);
            objBo.otherLoss = Convert.ToDecimal(txtOtherLoss.Text);

            //CODE FOR TBL_INFORMATION_OFFICIAL
            objBo.provider = txtProviderName.Text;
            objBo.collector = txtCollectorName.Text;
            objBo.validater = txtValidaterName.Text;
            objBo.dataTaken = Convert.ToDateTime(txtDate.Text);

            int i = objDao.Disaster_Insert(objBo);

            if (i > 0)
            {
                Response.Write("<Script> alert('Data Updated Successfully')</Script>");
            }          

        }
    }
}