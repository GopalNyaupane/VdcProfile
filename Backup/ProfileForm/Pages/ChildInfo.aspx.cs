﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using Common.Entity;
using DataAccessLayer;


namespace ProfileForm.Pages
{
    public partial class ChildInfo : System.Web.UI.Page
    {
        
        ChildInfoBo objBo = new ChildInfoBo();
        ChildInfoDao objDal = new ChildInfoDao();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if(Request.QueryString["Owner_Id"]!=null)
                {
                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                    btnNext.Visible = false;
                    btnUpdate.Visible = true;
                    LoadChildInfo(OwnerId);
                    LoadChildBreastFeedInfo(OwnerId);
                    LoadChildVaccineInfo(OwnerId);

                }
                else
                {
                    btnNext.Visible = true;
                    btnUpdate.Visible = false;
                }
            }
           

        }

        private void LoadChildVaccineInfo(int OwnerId)
        {
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchChildVaccineInfo(objBo);
            if (dt!=null && dt.Rows.Count > 0)
            {
                hidChildVaccinne.Value = dt.Rows[0]["CHILD_VACCINE_ID"].ToString();
                txtVitaminaMale.Text = dt.Rows[0]["VITAMINA"].ToString();
                txtWormDrugsMale.Text = dt.Rows[0]["WORM_DRUGS"].ToString();
                txtPolioVaccineMale.Text = dt.Rows[0]["POLIO_VACCINE"].ToString();
               // rdoSex.SelectedIndex = dt.Rows[0]["SEX_ID"].GetHashCode();
                txtBcgMale.Text = dt.Rows[0]["BCG"].ToString();
                txtDpt1Male.Text = dt.Rows[0]["DPT1"].ToString();
                txtDpt2Male.Text = dt.Rows[0]["DPT2"].ToString();
                txtDpt3Male.Text = dt.Rows[0]["DPT3"].ToString();
                txtDadhuraMale.Text = dt.Rows[0]["DADHURA"].ToString();
                txtHbMale.Text = dt.Rows[0]["HB"].ToString();


                hidChildVaccineFemaleId.Value = dt.Rows[1]["CHILD_VACCINE_ID"].ToString();
                txtVitaminaFemale.Text = dt.Rows[1]["VITAMINA"].ToString();
                txtWormDrugsFemale.Text = dt.Rows[1]["WORM_DRUGS"].ToString();
                txtPolioVaccineFemale.Text = dt.Rows[1]["POLIO_VACCINE"].ToString();
               // rdoSex.SelectedIndex = dt.Rows[1]["SEX_ID"].GetHashCode();
                txtBcgFemale.Text = dt.Rows[1]["BCG"].ToString();
                txtDpt1Female.Text = dt.Rows[1]["DPT1"].ToString();
                txtDpt2Female.Text = dt.Rows[1]["DPT2"].ToString();
                txtDpt3Female.Text = dt.Rows[1]["DPT3"].ToString();
                txtDadhuraFemale.Text = dt.Rows[1]["DADHURA"].ToString();
                txtHbFemale.Text = dt.Rows[1]["HB"].ToString();


            }
           
        }

        private void LoadChildBreastFeedInfo(int OwnerId)
        {
           
            objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchChildBreasetFeedInfo(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidChildBreastFeedId.Value = dt.Rows[0]["CHILD_RECORD_ID"].ToString();
                txtAliveBornMale.Text = dt.Rows[0]["ALIVE_BORN_CHILD"].ToString();
                txtBreastFeedBornTimeMale.Text = dt.Rows[0]["BREAST_FEED_BORNTIME"].ToString();
                txtNotBreastFeedBornTimeMale.Text = dt.Rows[0]["NOT_BREAST_FEED_BORNTIME"].ToString();
                //rdoSex.SelectedIndex = dt.Rows[0]["SEX_ID"].GetHashCode();
                txtBreastFeedOnlySixMonthMale.Text = dt.Rows[0]["BREAST_FEED_ONLY_SIX_MONTH"].ToString();
                txtBreastFeedPlusOtherMale.Text = dt.Rows[0]["BREAST_FEED_PLUS_OTHER"].ToString();
                txtBreastFeedLessTwoMonthMale.Text = dt.Rows[0]["BREAST_FEED_LESS_TWO_MONTH"].ToString();




                hidChildBreastFeedFemaleId.Value = dt.Rows[1]["CHILD_RECORD_ID"].ToString();
                txtAliveBornFemale.Text = dt.Rows[1]["ALIVE_BORN_CHILD"].ToString();
                txtBreastFeedBornTimeFemale.Text = dt.Rows[1]["BREAST_FEED_BORNTIME"].ToString();
                txtNotBreastFeedBornTimeFemale.Text = dt.Rows[1]["NOT_BREAST_FEED_BORNTIME"].ToString();
              //  rdoSex.SelectedIndex = dt.Rows[1]["SEX_ID"].GetHashCode();
                txtBreastFeedPlusOtherFemale.Text = dt.Rows[1]["BREAST_FEED_PLUS_OTHER"].ToString();
                txtBreastFeedLessTwoMonthFemale.Text = dt.Rows[1]["BREAST_FEED_LESS_TWO_MONTH"].ToString();
                txtBreastFeedOnlySixMonthFemale.Text = dt.Rows[1]["BREAST_FEED_ONLY_SIX_MONTH"].ToString();
                

            }

        }

        private void LoadChildInfo(int OwnerId)
        {
             objBo.OwnerId = OwnerId;
            DataTable dt = new DataTable();
            dt = objDal.FetchChildInfo(objBo);
            if (dt != null && dt.Rows.Count > 0)
            {
                hidChildInfo.Value = dt.Rows[0]["CHILD_HEALTH_INFO"].ToString();
                rdoSex.SelectedIndex = Convert.ToInt32(dt.Rows[0]["IMMEDIATE_CHILD_CURE"].ToString());
                rdoDisease.SelectedIndex = Convert.ToInt32(dt.Rows[0]["CHILD_DISEASE_VACCINE"].ToString());


            }
            
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            //Session["Owner_ID"] = 99;

            objBo.ImmediateChildCure = rdoSex.SelectedIndex;
            objBo.ChildDiseaseVaccine = rdoDisease.SelectedIndex;
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            int i = objDal.SaveChildInfo(objBo);
            
            

            objBo.AliveBornChild = Convert.ToInt32(txtAliveBornMale.Text);
            objBo.BreastFeedBornTime = Convert.ToInt32(txtBreastFeedBornTimeMale.Text);
            objBo.NotBreastFeedBornTime = Convert.ToInt32(txtNotBreastFeedBornTimeMale.Text);
            objBo.BreastFeedOnlySixMonth = Convert.ToInt32(txtBreastFeedOnlySixMonthMale.Text);
            objBo.BreastFeedPlusOther = Convert.ToInt32(txtBreastFeedPlusOtherMale.Text);
            objBo.BreastFeedLessTwoMonth = Convert.ToInt32(txtBreastFeedLessTwoMonthMale.Text);
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            
                objBo.SexId = 0;
           
            int result = objDal.SaveChildBreastFeedInfo(objBo);



            objBo.Vitamina = Convert.ToInt32(txtVitaminaMale.Text);
            objBo.WormDrugs = Convert.ToInt32(txtWormDrugsMale.Text);
            objBo.PolioVaccine = Convert.ToInt32(txtPolioVaccineMale.Text);
            //objBo.SexId = Convert.ToInt32(txtSexIdMale.Text);
           // objBo.OwnerId = Convert.ToInt32(txtBreastFeedPlusOtherMale.Text);
            objBo.Bcg = Convert.ToInt32(txtBcgMale.Text);
            objBo.Dpt1 = Convert.ToInt32(txtDpt1Male.Text);
            objBo.Dpt2 = Convert.ToInt32(txtDpt2Male.Text);
            objBo.Dpt3 = Convert.ToInt32(txtDpt3Male.Text);
            objBo.Dadhura = Convert.ToInt32(txtDadhuraMale.Text);
            objBo.Hb = Convert.ToInt32(txtHbMale.Text);
            objBo.SexId = 0;
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            int results = objDal.SaveChildVaccineInfo(objBo);



            
            
            

            
            


                objBo.AliveBornChild = Convert.ToInt32(txtAliveBornFemale.Text);
                objBo.BreastFeedBornTime = Convert.ToInt32(txtBreastFeedBornTimeFemale.Text);
                objBo.NotBreastFeedBornTime = Convert.ToInt32(txtNotBreastFeedBornTimeFemale.Text);
                objBo.BreastFeedOnlySixMonth = Convert.ToInt32(txtBreastFeedOnlySixMonthFemale.Text);
                objBo.BreastFeedPlusOther = Convert.ToInt32(txtBreastFeedPlusOtherFemale.Text);
                objBo.BreastFeedLessTwoMonth = Convert.ToInt32(txtBreastFeedLessTwoMonthFemale.Text);
                objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                
                    objBo.SexId = 1;
                
                objDal.SaveChildBreastFeedInfo(objBo);







                objBo.Vitamina = Convert.ToInt32(txtVitaminaFemale.Text);
                objBo.WormDrugs = Convert.ToInt32(txtWormDrugsFemale.Text);
                objBo.PolioVaccine = Convert.ToInt32(txtPolioVaccineFemale.Text);
                //objBo.SexId = Convert.ToInt32(txtSexIdMale.Text);
                // objBo.OwnerId = Convert.ToInt32(txtBreastFeedPlusOtherMale.Text);
                objBo.Bcg = Convert.ToInt32(txtBcgFemale.Text);
                objBo.Dpt1 = Convert.ToInt32(txtDpt1Female.Text);
                objBo.Dpt2 = Convert.ToInt32(txtDpt2Female.Text);
                objBo.Dpt3 = Convert.ToInt32(txtDpt3Female.Text);
                objBo.Dadhura = Convert.ToInt32(txtDadhuraFemale.Text);
                objBo.Hb = Convert.ToInt32(txtHbFemale.Text);
                objBo.SexId= 1;
                objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
                int resultss = objDal.SaveChildVaccineInfo(objBo);
                if (resultss > 0)
                {
                    Response.Redirect("MaternalInfo.aspx");
                }
            }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //Session["Owner_ID"] = 99;

            objBo.ImmediateChildCure = rdoSex.SelectedIndex;
            objBo.ChildDiseaseVaccine = rdoDisease.SelectedIndex;
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.ChildHealthInfo = Convert.ToInt32(hidChildInfo.Value);
            int i = objDal.UpdateChildInfo(objBo);



            objBo.AliveBornChild = Convert.ToInt32(txtAliveBornMale.Text);
            objBo.BreastFeedBornTime = Convert.ToInt32(txtBreastFeedBornTimeMale.Text);
            objBo.NotBreastFeedBornTime = Convert.ToInt32(txtNotBreastFeedBornTimeMale.Text);
            objBo.BreastFeedOnlySixMonth = Convert.ToInt32(txtBreastFeedOnlySixMonthMale.Text);
            objBo.BreastFeedPlusOther = Convert.ToInt32(txtBreastFeedPlusOtherMale.Text);
            objBo.BreastFeedLessTwoMonth = Convert.ToInt32(txtBreastFeedLessTwoMonthMale.Text);
            objBo.ChildRecordId = Convert.ToInt32(hidChildBreastFeedId.Value);
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);

            objBo.SexId = 0;

            int result = objDal.UpdateChildBreastFeedInfo(objBo);



            objBo.Vitamina = Convert.ToInt32(txtVitaminaMale.Text);
            objBo.WormDrugs = Convert.ToInt32(txtWormDrugsMale.Text);
            objBo.PolioVaccine = Convert.ToInt32(txtPolioVaccineMale.Text);
            objBo.Bcg = Convert.ToInt32(txtBcgMale.Text);
            objBo.Dpt1 = Convert.ToInt32(txtDpt1Male.Text);
            objBo.Dpt2 = Convert.ToInt32(txtDpt2Male.Text);
            objBo.Dpt3 = Convert.ToInt32(txtDpt3Male.Text);
            objBo.Dadhura = Convert.ToInt32(txtDadhuraMale.Text);
            objBo.Hb = Convert.ToInt32(txtHbMale.Text);
            objBo.SexId = 0;
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.ChildVaccineId = Convert.ToInt32(hidChildVaccinne.Value);
            int results = objDal.UpdateChildVaccineInfo(objBo);


            objBo.AliveBornChild = Convert.ToInt32(txtAliveBornFemale.Text);
            objBo.BreastFeedBornTime = Convert.ToInt32(txtBreastFeedBornTimeFemale.Text);
            objBo.NotBreastFeedBornTime = Convert.ToInt32(txtNotBreastFeedBornTimeFemale.Text);
            objBo.BreastFeedOnlySixMonth = Convert.ToInt32(txtBreastFeedOnlySixMonthFemale.Text);
            objBo.BreastFeedPlusOther = Convert.ToInt32(txtBreastFeedPlusOtherFemale.Text);
            objBo.BreastFeedLessTwoMonth = Convert.ToInt32(txtBreastFeedLessTwoMonthFemale.Text);
            objBo.ChildRecordId = Convert.ToInt32(hidChildBreastFeedFemaleId.Value);
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);

            objBo.SexId = 1;

            objDal.UpdateChildBreastFeedInfo(objBo);







            objBo.Vitamina = Convert.ToInt32(txtVitaminaFemale.Text);
            objBo.WormDrugs = Convert.ToInt32(txtWormDrugsFemale.Text);
            objBo.PolioVaccine = Convert.ToInt32(txtPolioVaccineFemale.Text);
            //objBo.SexId = Convert.ToInt32(txtSexIdMale.Text);
            // objBo.OwnerId = Convert.ToInt32(txtBreastFeedPlusOtherMale.Text);
            objBo.Bcg = Convert.ToInt32(txtBcgFemale.Text);
            objBo.Dpt1 = Convert.ToInt32(txtDpt1Female.Text);
            objBo.Dpt2 = Convert.ToInt32(txtDpt2Female.Text);
            objBo.Dpt3 = Convert.ToInt32(txtDpt3Female.Text);
            objBo.Dadhura = Convert.ToInt32(txtDadhuraFemale.Text);
            objBo.Hb = Convert.ToInt32(txtHbFemale.Text);
            objBo.SexId = 1;
            objBo.OwnerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.ChildVaccineId = -1;
            objBo.ChildVaccineId = Convert.ToInt32(hidChildVaccineFemaleId.Value);
            int resultss = objDal.UpdateChildVaccineInfo(objBo);
            if (resultss > 0)
            {
                Response.Redirect("MaternalInfo.aspx?Owner_Id=" +Session["Owner_Id"]);
            }




        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Section2FamilyInfo.aspx?Owner_Id=" + Session["Owner_Id"]);
        }
    }
    }
