﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Entity;
using DataAccessLayer;

namespace ProfileForm.Pages
{
    public partial class MaternalInfo : System.Web.UI.Page
    {
        MaternalInfoBo objMtrlInfo = null;
        MaternalInfoDao objMaternl = new MaternalInfoDao();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {

                    //Session["Owner_Id"] = Request.QueryString["Owner_Id"];
                    int ownerId = Convert.ToInt32(Session["Owner_Id"]);
                    BindRepeaterPregenentWomen(ownerId);
                    BindChildHivInf(ownerId);

                    btnNext.Enabled = false;
                    btnNext.Visible = false;

                    btnUpdate.Enabled = true;
                    btnUpdate.Visible = true;

                 

                }
            }
        }

        private void BindChildHivInf(int ownerId)
        {
            objMtrlInfo = new MaternalInfoBo();
            objMtrlInfo.OwnerID = ownerId;
            DataTable dt = new DataTable();
            dt = objMaternl.FetchChildHivInfo(objMtrlInfo);





            if (dt != null && dt.Rows.Count > 0)
            {
                rdoHIV.SelectedIndex = Convert.ToInt32(dt.Rows[0]["HIV_INFECTION"].ToString());
                RadioButtonList1.SelectedIndex = Convert.ToInt32(dt.Rows[0]["ARV"].ToString());
            }

        }

        private void BindRepeaterPregenentWomen(int ownerId)
        {
            objMtrlInfo = new MaternalInfoBo();
            objMtrlInfo.OwnerID = ownerId;
            DataTable dt = new DataTable();
            dt = objMaternl.FetchMaterialInfo(objMtrlInfo);

            txtPregnantWomen.Text = dt.Rows.Count.ToString();

            RpterPregnentWomen.DataSource = null;
            RpterPregnentWomen.DataBind();

            if (dt!= null && dt.Rows.Count > 0)
            {
                RpterPregnentWomen.DataSource = dt;
                RpterPregnentWomen.DataBind();
            }


        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            //Session["Owner_ID"] = "99";
            //            List<MaternalInfoBo> objListMtrlInfo = new List<MaternalInfoBo>();
            foreach (RepeaterItem rptr in RpterPregnentWomen.Items)
            {
                objMtrlInfo = new MaternalInfoBo();

                TextBox txtPregnancyTest = (TextBox)rptr.FindControl("txtPregnancyTest");
                TextBox txtIronTablet = (TextBox)rptr.FindControl("txtIronTablet");
                TextBox txtTTVaccine = (TextBox)rptr.FindControl("txtTTVaccine");
                TextBox txtInfantHealthCheck = (TextBox)rptr.FindControl("txtInfantHealthCheck");
                DropDownList ddlMaternityAddress = (DropDownList)rptr.FindControl("ddlMaternityAddress");
                RadioButtonList RdoCondition = (RadioButtonList)rptr.FindControl("RdoCondition");

                objMtrlInfo.OwnerID = Convert.ToInt32(Session["Owner_Id"].ToString());
                objMtrlInfo.PregTest = Convert.ToInt32(txtPregnancyTest.Text);
                objMtrlInfo.IronTablet = Convert.ToInt32(txtIronTablet.Text);
                objMtrlInfo.TtVaccine = Convert.ToInt32(txtTTVaccine.Text);
                objMtrlInfo.Maternity = RdoCondition.SelectedIndex;
                if (ddlMaternityAddress.SelectedIndex > 0)
                    objMtrlInfo.MaternalAddressId = ddlMaternityAddress.SelectedIndex;
                objMtrlInfo.InfantHlthChk = Convert.ToInt32(txtInfantHealthCheck.Text);


                objMaternl.InsertMaternalInfo(objMtrlInfo);

            }


            objMtrlInfo = new MaternalInfoBo();
            objMtrlInfo.OwnerID = Convert.ToInt32(Session["Owner_Id"].ToString());
            objMtrlInfo.HivInfection = rdoHIV.SelectedIndex;
            objMtrlInfo.Arv = RadioButtonList1.SelectedIndex;

            objMaternl.InsertMaternalHiv(objMtrlInfo);




            Response.Redirect("TeenAgeHealth.aspx");    


        }

        private int pregnencycount = 0;

        protected void btnOk_OnClick(object sender, EventArgs e)
        {

            pregnencycount = Convert.ToInt32(txtPregnantWomen.Text);
            DataTable dtMaternal = new DataTable();
            BindMaternal(dtMaternal, pregnencycount);

        }


        protected void RpterPregnentWomen_ItemCreated(object sender, RepeaterItemEventArgs e)
        {

        }

        private void BindMaternal(DataTable dtMaternal, int membercount)
        {
            DataTable dtAddPregnencyInfo = new DataTable();
            dtAddPregnencyInfo.Clear();
            dtAddPregnencyInfo.Columns.Add("SN");
            dtAddPregnencyInfo.Columns.Add("PREGNANCY_TEST");
            dtAddPregnencyInfo.Columns.Add("IRON_TABLET");
            dtAddPregnencyInfo.Columns.Add("TT_VACCINE");
            dtAddPregnencyInfo.Columns.Add("MATERNITY");
            dtAddPregnencyInfo.Columns.Add("MATERNAL_ADDRESS_ID");
            dtAddPregnencyInfo.Columns.Add("INFANT_HEALTH_CHECK");
            dtAddPregnencyInfo.Columns.Add("MATERNAL_HEALTH_ID");

            int j = 0;
            if (dtMaternal.Rows.Count == 0)
            {

                for (int i = 0; i < membercount; i++)
                {
                    DataRow _dtRow = dtAddPregnencyInfo.NewRow();
                    _dtRow["SN"] = i + 1;
                    _dtRow["PREGNANCY_TEST"] = 0;
                    _dtRow["IRON_TABLET"] = 0;
                    _dtRow["TT_VACCINE"] = 0;
                    _dtRow["MATERNITY"] = 0;
                    _dtRow["MATERNAL_ADDRESS_ID"] = 0;
                    _dtRow["INFANT_HEALTH_CHECK"] = 0;
                    _dtRow["MATERNAL_HEALTH_ID"] = 0;


                    dtAddPregnencyInfo.Rows.Add(_dtRow);
                }

            }
            else
            {
                for (int i = 0; i < dtMaternal.Rows.Count; i++)
                {
                    DataRow _dtRow = dtAddPregnencyInfo.NewRow();
                    /*_dtRow["SN"] = i + 1;
                    _dtRow["Name"] = dtAbroad.Rows[i]["away_name"].ToString();
                    _dtRow["Age"] = dtAbroad.Rows[i]["away_age"].ToString();
                    _dtRow["Relation"] = dtAbroad.Rows[i]["away_reason"].ToString();
                    _dtRow["Sex"] = dtAbroad.Rows[i]["away_gender"].ToString();
                   */

                    dtAddPregnencyInfo.Rows.Add(_dtRow);
                }
            }
            RpterPregnentWomen.DataSource = dtAddPregnencyInfo;
            RpterPregnentWomen.DataBind();
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {


            foreach (RepeaterItem rptr in RpterPregnentWomen.Items)
            {
                objMtrlInfo = new MaternalInfoBo();

                TextBox txtPregnancyTest = (TextBox)rptr.FindControl("txtPregnancyTest");
                TextBox txtIronTablet = (TextBox)rptr.FindControl("txtIronTablet");
                TextBox txtTTVaccine = (TextBox)rptr.FindControl("txtTTVaccine");
                TextBox txtInfantHealthCheck = (TextBox)rptr.FindControl("txtInfantHealthCheck");
                DropDownList ddlMaternityAddress = (DropDownList)rptr.FindControl("ddlMaternityAddress");
                RadioButtonList RdoCondition = (RadioButtonList)rptr.FindControl("RdoCondition");
                HiddenField fieldId = (HiddenField)rptr.FindControl("fieldId");

                objMtrlInfo.MainID = Convert.ToInt32(fieldId.Value);
                objMtrlInfo.OwnerID = Convert.ToInt32(Session["Owner_Id"].ToString());
                objMtrlInfo.PregTest = Convert.ToInt32(txtPregnancyTest.Text);
                objMtrlInfo.IronTablet = Convert.ToInt32(txtIronTablet.Text);
                objMtrlInfo.TtVaccine = Convert.ToInt32(txtTTVaccine.Text);
                objMtrlInfo.Maternity = RdoCondition.SelectedIndex;
                if (ddlMaternityAddress.SelectedIndex > 0)
                    objMtrlInfo.MaternalAddressId = ddlMaternityAddress.SelectedIndex;
                objMtrlInfo.InfantHlthChk = Convert.ToInt32(txtInfantHealthCheck.Text);


                objMaternl.UpdateMaternalInfo(objMtrlInfo);

            }


            objMtrlInfo = new MaternalInfoBo();

            objMtrlInfo.OwnerID = Convert.ToInt32(Session["Owner_Id"].ToString());
            objMtrlInfo.HivInfection = rdoHIV.SelectedIndex;
            objMtrlInfo.Arv = RadioButtonList1.SelectedIndex;

            objMaternl.UpdateMaternalHiv(objMtrlInfo);
            Response.Redirect("TeenAgeHealth.aspx?Owner_Id="+Session["Owner_Id"]);    

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("MaternalInfo.aspx?Owner_Id=" + Session["Owner_Id"]); 
        }
    }
}