﻿using Common.Entity;
using DataAccessLayer.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileForm.Pages
{
    public partial class ChildRights : System.Web.UI.Page
    {
        ChildRightBo objBo = new ChildRightBo();
        ChildRightDao objDao = new ChildRightDao();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Request.QueryString["Owner_Id"] != null)
                {
                    int Owner_Id = Convert.ToInt32(Session["Owner_Id"]);
                    LoadDetails(Owner_Id);
                    btnUpdate.Visible = true;
                    btnNext.Visible = false;
                    
                 }
                else
                {
                     btnUpdate.Visible = false;
                      btnNext.Visible = true;
                }
               
            }
           
        }

        public void LoadDetails(int Owner_Id)
        {
            objBo.ownerId = Owner_Id;
            DataSet ds = new DataSet();
            ds = objDao.Fetch_ChildRights(objBo);
            DataTable dtChildAbuse = null;
            DataTable dtChildDrugAbuse = null;
            DataTable dtSex = null;
            DataTable dtViolence = null;
            DataTable dtHCP = null;
            DataTable dtShelter = null;
            DataTable dtSCP = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dtChildAbuse = ds.Tables[0];
                dtChildDrugAbuse = ds.Tables[1];
                dtSex = ds.Tables[2];
                dtViolence = ds.Tables[3];
                dtHCP = ds.Tables[4];
                dtShelter = ds.Tables[5];
                dtSCP = ds.Tables[6];

                hidChildExp.Value = dtHCP.Rows[0]["CHILD_PUNISHMENT_ID"].ToString();
                hidSex.Value = dtSex.Rows[0]["SEX_DESCRIMINATION_ID"].ToString();
                hidSPunishment.Value = dtSCP.Rows[0]["SCHOOL_CHILD_PUNISHMENT_ID"].ToString();
                hidViolence.Value = dtViolence.Rows[0]["CHILD_WOMEN_VIOLENCE_ID"].ToString();
                hidChildAbuse.Value = dtChildAbuse.Rows[0]["CHILD_ABUSE_ID"].ToString() ;
                hidDrug.Value = dtChildDrugAbuse.Rows[0]["CHILD_DRUG_ABUSE_ID"].ToString();
                hidDrug1.Value = dtChildDrugAbuse.Rows[1]["CHILD_DRUG_ABUSE_ID"].ToString();
                hidDrug2.Value = dtChildDrugAbuse.Rows[2]["CHILD_DRUG_ABUSE_ID"].ToString();
                hidShelter.Value = dtShelter.Rows[0]["HOME_SHELTER_ID"].ToString();



                rblHomeChild.SelectedIndex = Convert.ToInt32(dtHCP.Rows[0]["PUNISHMENT_TYPE_ID"].ToString());
               
                rblSexDescrimination.SelectedIndex = Convert.ToInt32(dtSex.Rows[0]["IS_SEX_DESCRIMINATION"].ToString());
                rblSexDescriminatonType.SelectedIndex = Convert.ToInt32(dtSex.Rows[0]["SEX_DESCRIMINATION_TYPE"].ToString());
               
                rblSchoolPunishment.SelectedIndex = Convert.ToInt32(dtSCP.Rows[0]["PUNISHMENT_TYPE_ID"].ToString());
               
                rblChildViolence.SelectedIndex = Convert.ToInt32(dtViolence.Rows[0]["VIOLENCE_TYPE_ID"].ToString());

                rblSexExp.SelectedIndex = Convert.ToInt32(dtChildAbuse.Rows[0]["IS_SEX_EXPLOITATION"].ToString());
                
                rblOrg.SelectedIndex = Convert.ToInt32(dtChildAbuse.Rows[0]["IS_CHILD_ORG"].ToString());
                rblStreet.SelectedIndex = Convert.ToInt32(dtChildAbuse.Rows[0]["IS_STREET_CHILD"].ToString());
                rblRights.SelectedIndex = Convert.ToInt32(dtChildAbuse.Rows[0]["IS_CHILD_RIGHTS"].ToString());
                
                rblClothing.SelectedIndex = Convert.ToInt32(dtShelter.Rows[0]["ANNUAL_CHILD_CLOTHING"].ToString());
                rblDiffRoom.SelectedIndex = Convert.ToInt32(dtShelter.Rows[0]["DIFF_ROOM"].ToString());
                rblHomeShed.SelectedIndex = Convert.ToInt32(dtShelter.Rows[0]["SEPERATE_HOME_SHED"].ToString());
                

                txtBoysSmoke.Text = dtChildDrugAbuse.Rows[0]["SMOKING"].ToString();
                txtBoysAddicted.Text = dtChildDrugAbuse.Rows[0]["CANNABIS"].ToString();
                txtBoysNon.Text = dtChildDrugAbuse.Rows[0]["NON_ADDICTION"].ToString();
                txtGirlsSmoke.Text = dtChildDrugAbuse.Rows[1]["SMOKING"].ToString();
                txtGirlsAddicted.Text = dtChildDrugAbuse.Rows[1]["CANNABIS"].ToString();
                txtGirlsNon.Text = dtChildDrugAbuse.Rows[1]["NON_ADDICTION"].ToString();
                txtOtherSmoke.Text = dtChildDrugAbuse.Rows[2]["SMOKING"].ToString();
                txtOtherAddicted.Text = dtChildDrugAbuse.Rows[2]["CANNABIS"].ToString();
                txtOtherNon.Text = dtChildDrugAbuse.Rows[2]["NON_ADDICTION"].ToString();

               
                
            }
            else
            {
                dtChildAbuse = null;
                dtChildDrugAbuse = null;
                dtSex = null;
                dtViolence = null;
                dtHCP = null;
                dtShelter = null;
                dtSCP = null;
            }

           
               
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {

          //  Session["Owner_Id"] = 9;

           //For Child abuse 
            objBo.sexExp = rblSexExp.SelectedIndex;
            objBo.orgId = rblOrg.SelectedIndex;
            objBo.streetId = rblStreet.SelectedIndex;
            objBo.rightsId = rblRights.SelectedIndex;
            objBo.childExploitationId = rblSexExp.SelectedIndex;
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            int cai = objDao.Child_Abuse_Insert(objBo);
            
            //For  Child Sex desc       
            objBo.sexDescriminationId = rblSexDescrimination.SelectedIndex;
            objBo.sexDescriminationTypeId = rblSexDescriminatonType.SelectedIndex;
           
            int csd = objDao.Child_Sex_Desc_Insert(objBo);

            //for child women violence
            objBo.violenceTypeId = rblChildViolence.SelectedIndex;
           int cwv = objDao.Child_Women_Violence_Insert(objBo);

            //For Home Child Punishment

            objBo.punishmentTypeId = rblHomeChild.SelectedIndex;
             
              int hcp = objDao.Home_Child_Punishment_Insert(objBo);

            //For Home Shelter
              objBo.clothingId = rblClothing.SelectedIndex;
              objBo.diffRoomId = rblDiffRoom.SelectedIndex;
              objBo.homeShedId = rblHomeShed.SelectedIndex;
             
              int hs = objDao.Home_Shelter_Insert(objBo);

           
            //For school punishment
              objBo.punishment = rblSchoolPunishment.SelectedIndex;
             
              int sp = objDao.School_Child_Punishment_Insert(objBo);
                      




            //for Boys
            objBo.smoking =  Convert.ToInt32(txtBoysSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtBoysAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtBoysNon.Text);
            objBo.sexId = 0;
           
            int cdab = objDao.Child_Drug_Abuse_Insert(objBo);


            //For Girls
            objBo.smoking = Convert.ToInt32(txtGirlsSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtGirlsAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtGirlsNon.Text);
            objBo.sexId = 1;
           
            int cdag = objDao.Child_Drug_Abuse_Insert(objBo);

            //For Others
            objBo.smoking = Convert.ToInt32(txtOtherSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtOtherAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtOtherNon.Text);
            objBo.sexId = 2;
            
            int cda = objDao.Child_Drug_Abuse_Insert(objBo);

            if (cda > 0)
        {
            Response.Redirect("EducationSection.aspx");
        }          

            

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            Session["Owner_Id"] = 9;
           
            objBo.hiddenId1 = Convert.ToInt32(hidChildExp.Value);           
            objBo.hiddenId2 = Convert.ToInt32(hidSex .Value);
            objBo.hiddenId3 = Convert.ToInt32(hidSPunishment.Value);
            objBo.hiddenId4 = Convert.ToInt32(hidViolence.Value);           
            objBo.hiddenId5 = Convert.ToInt32(hidChildAbuse.Value);
           
            objBo.hiddenId7 = Convert.ToInt32(hidShelter.Value);


            objBo.hiddenId6 = Convert.ToInt32(hidDrug.Value);
           
            objBo.ownerId = Convert.ToInt32(Session["Owner_Id"]);
            objBo.sexId = 0;
            objBo.smoking = Convert.ToInt32(txtBoysSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtBoysAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtBoysNon.Text);

            int m = objDao.Update_ChildRights(objBo);
            objBo.hiddenId6 = Convert.ToInt32(hidDrug1.Value);
            objBo.sexId = 1;
            objBo.smoking = Convert.ToInt32(txtGirlsSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtGirlsAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtGirlsNon.Text);
            int f = objDao.Update_ChildRights(objBo);

            objBo.hiddenId6 = Convert.ToInt32(hidDrug2.Value);
            objBo.sexId = 2;
            objBo.smoking = Convert.ToInt32(txtOtherSmoke.Text);
            objBo.cannabis = Convert.ToInt32(txtOtherAddicted.Text);
            objBo.nonAddiction = Convert.ToInt32(txtOtherNon.Text);
            int o = objDao.Update_ChildRights(objBo);

            objBo.sexExp = rblSexExp.SelectedIndex;
            objBo.orgId = rblOrg.SelectedIndex;
            objBo.streetId = rblStreet.SelectedIndex;
            objBo.rightsId = rblRights.SelectedIndex;
           
            

            objBo.sexDescriminationId = rblSexDescrimination.SelectedIndex;
            objBo.sexDescriminationTypeId = rblSexDescriminatonType.SelectedIndex;

            objBo.violenceTypeId = rblChildViolence .SelectedIndex;

            objBo.punishmentTypeId = rblSchoolPunishment.SelectedIndex;

            objBo.clothingId = rblClothing.SelectedIndex;
            objBo.diffRoomId = rblDiffRoom.SelectedIndex;
            objBo.homeShedId = rblHomeShed.SelectedIndex;

            int i = objDao.Update_ChildRights(objBo);
            if(i>0)
            {
                Response.Redirect("EducationSection.aspx?Owner_Id="+Session["Owner_Id"]);
            }

           

          

        }
    }
}