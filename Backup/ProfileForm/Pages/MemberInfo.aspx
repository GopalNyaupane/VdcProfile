﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MemberInfo.aspx.cs" Inherits="ProfileForm.Pages.MemberInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-striped">
        
        
         <tr>
                <td>६४.</td>
                <td>तपाईंको परिवारका सदस्यहरु कुन कुन पेशा (रोजगारी) मा संलग्न हुनुहुन्छ ?</td>
                <td> </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td> पेशा (रोजगारी)
              </td>
                  <td>
                     उमेर
                    <%-- --%>
                  </td>
                  <td>
                      <table class="table table-bordered">
                          <td> पुरुष </td>
                        <td>महिला </td>
                          
                      </table>

                                 </td>
            </tr>
            <tr>
                <asp:Repeater ID="rptrFamilyMemberOccupation" runat="server">
                                   
                                    <ItemTemplate>
                                        <tr>
                                            <td></td>
                                            
                                            <td>
                                                 <asp:DropDownList ID="ddlOccupation" runat="server" DataSourceID="XmlDataSource_Occupation" DataTextField="name" SelectedValue='<%#Eval("OCCUPATION_TYPE_ID") %>' 
                        DataValueField="id">
                       </asp:DropDownList>
                        <asp:XmlDataSource ID="XmlDataSource_Occupation" runat="server" 
                        DataFile="~/XMLDataSource/Occupation.xml"></asp:XmlDataSource>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlAgeGroup" runat="server" RepeatDirection=" Vertical" SelectedValue='<%#Eval("AGE_GROUP_ID") %>'>
                        <asp:ListItem Value="0">१६-१८</asp:ListItem>
                        <asp:ListItem Value="1">१९-२४ </asp:ListItem>
                         <asp:ListItem Value="2">२५-४०</asp:ListItem>
                        <asp:ListItem Value="3">४१-६० </asp:ListItem>
                        <asp:ListItem Value="4">६०‌‌+ </asp:ListItem>
                        
                  </asp:DropDownList>
                                            </td>
                                            <td>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td><asp:TextBox ID="txtMaleNo" runat="server" TextMode="Number" Text='<%#Eval("MALE_NO") %>'></asp:TextBox></td>
                                                        <td><asp:TextBox ID="txtFemaleNo" runat="server" TextMode="Number" Text='<%#Eval("FEMALE_NO") %>'> </asp:TextBox></td>
                                                        <td>
                                                            <asp:HiddenField ID="hidOccupationId" runat="server" Value='<%#Eval("OCCUPATION_ID") %>'>
                                                            </asp:HiddenField>
                                                        </td>
                                                    </tr>
                                                </table>
                                             
                                            </td>
                                            
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>

            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="btnAddMemberOccupation" OnClick="btnAddMemberOccupation_OnClick" /></td>
            </tr>
             <tr>
                <td>
                     ६५.</td>
                <td>
                    तपाईको परिवारको कुनै सदस्य जिल्ला बाहिर वा बिदेस जानुभएको छ?</td>
                <td>
                     <asp:RadioButtonList ID="RdoAbroad" runat="server" RepeatDirection="Horizontal" 
                          AutoPostBack="True" OnSelectedIndexChanged="RdoAbroad_OnSelectedIndexChanged">
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                                </asp:RadioButtonList>
                                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <br />
                    </td>
            </tr>
             <tr>
                <td colspan="5">
            <table class="table table-bordered table-striped">
               
            <tr>
                <td class="style1">
                  &nbsp;क्रम सं</td>
                <td class="style1">
                    गएको सदस्यको नाम</td>
                <td class="style1">
                    उमेर</td>
                <td class="style1">
                    लिङ्ग</td>
                <td class="style1">
                    गएको ठाउँ/देश</td>
                <td class="style1">
                    जानुको कारण</td>
            </tr>
    
            <asp:Panel runat="server" ID="pnlAbroad">
           <asp:Repeater ID="rpterAbroad" runat="server">
               <ItemTemplate>
                   
           <tr>
                <td>
<%--                    <asp:Label ID="lblSN" runat="server" Text='<%#Eval("SN") %>'/></td>--%>
                <td>
                    <asp:TextBox ID="txtAbName" runat="server" Text='<%#Eval("NAME") %>'></asp:TextBox></td>
                <td>
                   <asp:TextBox ID="txtAbAge" runat="server" Text='<%#Eval("AGE") %>' TextMode="Number"></asp:TextBox></td>
              
                <td>
                   <asp:RadioButtonList ID="RdoSex" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("SEX_ID") %>'>
                                       <asp:ListItem Value="0">पुरुष</asp:ListItem>
                        <asp:ListItem Value="1">महिला</asp:ListItem>
                                </asp:RadioButtonList></td>
                <td>
                    <asp:TextBox ID="txtAbCountry" runat="server" Text='<%#Eval("MIGRATED_PLACE") %>'></asp:TextBox></td>
                <td>
                     <asp:TextBox ID="txtAbReason" runat="server" Text='<%#Eval("OUT_MIGRATION_REASON") %>'></asp:TextBox></td> 
                <td>
                    <asp:HiddenField ID="hidMigrationId" runat="server" Value='<%#Eval("OUT_MIGRATION_DETAIL_ID") %>'>
                    </asp:HiddenField>
                </td>
               
            </tr>  
            </ItemTemplate>
            </asp:Repeater>
         </asp:Panel>
          <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
                    <td colspan="5">
                        <asp:Button ID="btnAddAbroad" runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary"
                            OnClick="btnAddAbroad_OnClick"/>
                    </td>
                </tr>
         </table>
         </td>
         </tr>
         <tr>
             <td>६६.</td>
             <td>
                 तपाईको परिवारमा १८ वर्षभन्दा कम उमेरका शारीरिक तथा मानसिक हिसाबले अपांगता भएका सदस्यहरु भएमा तलको विवरण दिनुहोस् ।
             </td>
             <td></td>
             <td></td>
         </tr>
         <tr>
             <td></td>
             <td> <b>अपाङ्गताको अवस्था</b>
                   </td>
                    <td>  बालक</td>
                    <td> बालिका</td>
                  
         </tr>
         <tr>
             <asp:Repeater ID="rptrHandicapped" runat="server">
                                   
                                    <ItemTemplate>
                                        <tr>
                                            <td></td>
                                             
                                            <td>
                                                <asp:DropDownList ID="ddlHandicapped" runat="server" DataSourceID="XmlDataSource_handicapped" DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("DISABLED_TYPE_ID") %>' >
                    </asp:DropDownList>
                     <asp:XmlDataSource ID="XmlDataSource_handicapped" runat="server" 
                         DataFile="~/XMLDataSource/Handicapped.xml">
                    </asp:XmlDataSource>
                                            </td>
                                            
                                                        <td><asp:TextBox ID="txtBoyNo" runat="server" TextMode="Number" Text='<%#Eval("MALE_CHILD_COUNT") %>'></asp:TextBox></td>
                                                        <td><asp:TextBox ID="txtGirlNo" runat="server" TextMode="Number" Text='<%#Eval("FEMALE_CHILD_COUNT") %>'> </asp:TextBox></td>
                                                        <td>
                                                            <asp:HiddenField ID="hidDisableId" runat="server" Value='<%#Eval("DISABLED_ID") %>'>
                                                            </asp:HiddenField>
                                                        </td>    
                                            
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
         </tr>
         <tr>
             <td></td>
             <td></td>
             <td></td>
              <td><br /><asp:Button runat="server" Text="थप्नुहोस्" CssClass="btn btn-primary" ID="btnAddHandicapped" OnClick="btnAddHandicapped_OnClick"/></td>
         </tr>
         <tr>
             <td></td>
             <td></td>
             <td></td>
             <td></td>
         </tr>
           <tr>
                <td>
                    &nbsp;</td>
                    <td> </td>
                <td colspan="3">
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary"/>
                   <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" 
                        onclick="btnNext_Click" OnClientClick="return validatePage();"/>     
                   <asp:Button ID="btnUpdateMemberInfo" runat="server" Text="Update" CssClass="btn btn-primary"
                       OnClick="btnUpdateMemberInfo_OnClick" OnClientClick="return validatePage();" Visible="False" />
                </td>
<%--               --%>
            </tr>
    </table>
</asp:Content>
