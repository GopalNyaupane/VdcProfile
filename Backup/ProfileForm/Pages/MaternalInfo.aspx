﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MaternalInfo.aspx.cs" Inherits="ProfileForm.Pages.MaternalInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="table table-striped">
        <tr>
            <td>
                १७.
            </td>
            <td>
                बितेको १ वर्षभित्रको तपाईको परिवारमा रहेका गर्भवती महिलाको संख्या कति छ?
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:TextBox ID= "txtPregnantWomen" runat="server" TextMode="Number" Text="0"></asp:TextBox>
                <asp:Button runat="server" Text="Ok" ID="btnOk" OnClick="btnOk_OnClick" CssClass="btn btn-primary" />
            </td>
            <td>
            </td>
        </tr>
    </table>
    <table class="table table-striped">
        <tr>
            <td>
                बितेको १ वर्षभित्रको तपाईको परिवारमा रहेका गर्भवती महिलाको स्वास्थ्य अवस्था बारे
                जानकारी
            </td>
        </tr>
    </table>
    <table class="table table-bordered">
        <tr>
            <td>
                <strong>गर्भवती सि. नं.</strong>
            </td>
            <td>
                <b>गर्भवती परीक्षण गरेको पटक (संख्या)</b>
            </td>
            <td>
                <b>आइरन चक्की को प्रयोग (चक्कि संख्या) </b>
            </td>
            <td>
                <b>टि. टि. खोप लगाएको संख्या </b>
            </td>
            <td>
                <b>प्रसूति भए नभएको</b>
            </td>
            <td>
                <b>प्रसूति कहाँ भएको</b>
            </td>
            <td>
                <b>नवजात शिशुको जाँच गराएको संख्या </b>
            </td>
        </tr>
        <asp:Panel runat="server" ID="pnlPregnentWomen">
            <asp:Repeater runat="server" ID="RpterPregnentWomen" OnItemCreated="RpterPregnentWomen_ItemCreated">
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblSN" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPregnancyTest" runat="server" Text='<%#Eval("PREGNANCY_TEST") %>'
                                Width="80" TextMode="Number"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtIronTablet" runat="server" Text='<%#Eval("IRON_TABLET") %>' Width="80"
                                TextMode="Number"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtTTVaccine" runat="server" Text='<%#Eval("TT_VACCINE") %>' Width="80"
                                TextMode="Number"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="RdoCondition" runat="server" RepeatDirection="Vertical"
                                SelectedValue='<%#Eval("MATERNITY") %>'>
                                <asp:ListItem Value="0">भएको</asp:ListItem>
                                <asp:ListItem Value="1">नभएको</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlMaternityAddress" runat="server" DataSourceID="XmlDataSource_MaternityAddress"
                                DataTextField="name" DataValueField="id" SelectedValue='<%#Eval("MATERNAL_ADDRESS_ID") %>'>
                            </asp:DropDownList>
                            <asp:XmlDataSource ID="XmlDataSource_MaternityAddress" runat="server" DataFile="~/XMLDataSource/MaternityAddress.xml">
                            </asp:XmlDataSource>
                        </td>
                        <td>
                            <asp:TextBox ID="txtInfantHealthCheck" runat="server" Text='<%#Eval("INFANT_HEALTH_CHECK") %>'
                                Width="80" TextMode="Number"></asp:TextBox>
                        </td>
                        <td>   
                            <asp:HiddenField ID="fieldId" runat="server" Value='<%#Eval("MATERNAL_HEALTH_ID") %>'
                              ></asp:HiddenField>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>
    </table>
    <table class="table table-striped">
        <tr>
            <td>
                १८.
            </td>
            <td>
                तपाईको परिवारमा एच्आईभी संक्रमित आमावाट जन्मेका बालवालिका छन्?
            </td>
            <td>
                <asp:RadioButtonList ID="rdoHIV" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छन्</asp:ListItem>
                    <asp:ListItem Value="1">छैनन्</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                यदि छन् भने उनीहरुले ARV Prophylaxis पाएका छन् ?
            </td>
            <td>
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                    SelectedValue='<%#Eval(" ") %>'>
                    <asp:ListItem Value="0">छन्</asp:ListItem>
                    <asp:ListItem Value="1">छैनन्</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <%-- <table class="table-striped">
            
            <tr>
                <td> २२.</td>
                <td> दुई बालबालिका चिन्ह भएको आयोडिनयुक्त नुन प्रयोग गर्ने गर्नुभएको छ ?</td>
                <td> <asp:RadioButtonList ID="RadioButtonList6" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>

            </tr>
            <tr>
                <td> २३.</td>
                <td> बर्ष मुनिका बालबालिकाले पोषिलो खाना (दिनको कम्तिमा ३ पटक दुघ, लिटो, भात, फलफुल र सागसव्जी) खान पाउँछन् ?</td>
                <td> <asp:RadioButtonList ID="RadioButtonList7" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                                       <asp:ListItem Value="0">पाउँछन्</asp:ListItem>
                        <asp:ListItem Value="1">पाउँदैनन्</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>

            </tr>
            <tr>
                <td> २४.</td>
                <td> तपाईंको परिवारका १ वर्ष मुनिकाको मासिक र १ वर्ष भन्दा माथि ५ वर्ष सम्मका बालबालिकाहरुको त्रैमासिक रुपमा तौल लिने गरिएको छ ?</td>
                <td> <asp:RadioButtonList ID="RadioButtonList8" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval(" ") %>'>
                                       <asp:ListItem Value="0">छ</asp:ListItem>
                        <asp:ListItem Value="1">छैन</asp:ListItem>
                        
                                </asp:RadioButtonList> </td>
                              $1$  Display list of family member less than 5 years old and record weights#1#

            </tr>
             <tr>
                <td> २५.</td>
                <td> पाँच वर्षमुनिका र ६ वर्षदेखि १८ वर्षसम्मका बालबालिकाको जन्म दर्ता गराएको विवरण?</td>
                <td>  $1$  Display list of family member less than 18 years old and record weights#1# Age Yes No </td>
                             

            </tr>
             <tr>
                <td> २६.</td>
                <td> गएको ३ वर्षभित्र तपाईंको परिवारमा कसैको विवाह भएको भए सो को विवरण दिनुहोस् ।</td>
                <td> </td> </tr>
                <tr>
                    <td></td>
                <td> <asp:RadioButtonList ID="RadioButtonList9" runat="server" RepeatDirection="Horizontal" SelectedValue='<%#Eval("Sex") %>'>
                                       <asp:ListItem Value="1">पुरुष</asp:ListItem>
                        <asp:ListItem Value="2">महिला</asp:ListItem>
                         <asp:ListItem Value="3">अन्य</asp:ListItem>
                                </asp:RadioButtonList> &nbsp;   Age: <asp:TextBox runat="server"></asp:TextBox> </td>
                             <td><asp:Button runat="server" Text="Add" CssClass="btn btn-pinterest"/> </td>
                            $1$ or list all family member>age 5 and record the married age (marriage in last 3 years)#1#
            </tr> --%>
    <table>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="btnBack_Click"/>
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary" OnClick="btnNext_Click"
                    OnClientClick="return validatePage();" />      
                <asp:Button ID="btnUpdate" CssClass="btn btn-primary" Text="Update" runat="server"
                    OnClick="btnUpdate_OnClick" Visible="False" />
            </td>
        </tr>
    </table>
</asp:Content>

