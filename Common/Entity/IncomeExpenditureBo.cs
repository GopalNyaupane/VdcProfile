﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class IncomeExpenditureBo
    {
        public int AgroIncomeId { get; set; }
        public int OwnerId { get; set; }
        public int IncomeCoverTimeId { get; set; }
        public int AnnualExpenditureId { get; set; }
        public int Fooding { get; set; }
        public int Clothing { get; set; }
        public int Education { get; set; }
        public int Health { get; set; }
        public int Festivity { get; set; }
        public int PaymentCharge { get; set; }
        public int OthersExpenditure { get; set; }
        public int IncomeSourceId { get; set; }
        public int Agriculture { get; set; }
        public int Business { get; set; }
        public int Jobs { get; set; }
        public int ForeignEmployment { get; set; }
        public int WageLabour { get; set; }
        public int HouseRent { get; set; }
        public int OthersIncome { get; set; }
        public int IsLoan { get; set; }
        public int LoanDonorTypeId { get; set; }
        public string LoanPurposeTypeId { get; set; }

        public int LoanDetailsId { get; set; }
      

        public object LoanId { get; set; }
        
    }
}
