﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class OwnerBo
    {
        public int VillageId { get; set; }
        public string OwnerName { get; set; }
        public int SexId { get; set; }
        public int CasteId { get; set; }
        public int ReligionId { get; set; }
        public int LanguageId { get; set; }
        public int IsComplete { get; set; }
        public int OwnerId { get; set; }
    }
}
