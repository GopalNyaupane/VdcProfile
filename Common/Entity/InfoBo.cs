﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class InfoBo
    {
        public int VillageId { get; set; }
        public int VdcId { get; set; }
        public int WardNum { get; set; }
        public string HouseNo { get; set; }
        public string FamilySn { get; set; }
        public string VillageName { get; set; }
        public int BastiId { get; set; }
        public int IsComplete { get; set; }
        public int OwnerId { get; set; }
    }

}
