﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
   public  class ChildLabourBo
    {
       public int ownerId { get; set; }
       public int childLabour { get; set; }
       public int hiddenId1 { get; set; }
       public int hiddenId2 { get; set; }
       public int age { get; set; }
       public int sexId { get; set; }
       public int labourType { get; set; }
       public int workHour { get; set; }
       public int workYear { get; set; }
       public int yearlyIncome { get; set; }
       public int annualVisit { get; set; }
       public int isSchool { get; set; }
       public int labourExploitationType { get; set; }

    }
}
