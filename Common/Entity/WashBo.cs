﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class WashBo
    {
        public int WashId { get; set; }
        public int OwnerId { get; set; }
        public int WaterSourceId { get; set; }
        public int WaterFetchTimeId { get; set; }
        public int ToiletStatusId { get; set; }
        public int HealthCheckUpId { get; set; }
        public int HandWashId { get; set; }
        public string HealthPostDistance { get; set; }
        public string HealthPostTime { get; set; }
        public string HospitalDistance { get; set; }
        public string HospitalTime { get; set; }
        
    }
}
