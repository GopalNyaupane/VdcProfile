﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class FamilyInfoBo
    {
        public int FamilyCountId { get; set; }
        public int OwnerId { get; set; }
        public string Name { get; set; }
        public int RelationTypeId { get; set; }
        public int Age { get; set; }
        public int SexId { get; set; }
        public string Remarks { get; set; }


    }
}
