﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
   public class AgroProdBo
    {
       public int UserId { get; set; }

       public int OwnerId { get; set; }
       public int HousHoldDecision { get; set; }
       public int HouseHldInvolved { get; set; }
       public int BankAcc { get; set; }
       public int ConsumerCom { get; set; }
       public int SchoolMngmnt { get; set; }
       public int BusinessParticipation { get; set; }

       public String Date { get; set; }
       public int CropGroup { get; set; }
       public decimal LandArea { get; set; }
       public String Income { get; set; }

       public int VegId { get; set; }
       public decimal VegLand { get; set; }
       public String VegIncome { get; set; }
       
       public int CashCropId { get; set; }
       public decimal CashCropLand { get; set; }
       public String CashCropIncome { get; set; }

       public int FruitId { get; set; }
       public decimal FruitLand { get; set; }
       public String FruitIncome { get; set; }


       public int AnimalId { get; set; }
       public int LocalAnimal { get; set; }
       public int HybridAnimal { get; set; }
       public decimal MilkProd { get; set; }
       public decimal EggProd { get; set; }
       public decimal MeatProd { get; set; }
       public decimal WoolProd { get; set; }
       public decimal HoneyProd { get; set; }

       public decimal GhaseLnd { get; set; }
       public decimal GhaseIncome { get; set; }
       public decimal BhuiIncome { get; set; }
       public decimal BhuiLnd { get; set; }
       public decimal ResamLnd { get; set; }
       public decimal ResamIncome { get; set; }
       public decimal JdbIncome { get; set; }
       public decimal JdbLnd { get; set; }
       public decimal OtherLnd { get; set; }
       public decimal OtherIncome { get; set; }
      
 
    }
}
