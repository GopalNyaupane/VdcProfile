﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class ChildInfoBo
    {
        public int ChildHealthInfo { get; set; }
        public int OwnerId { get; set; }
        public int ImmediateChildCure { get; set; }
        public int ChildDiseaseVaccine{ get; set; }
        public int ChildRecordId { get; set; }
        public int SexId { get; set; }
        public int AliveBornChild { get; set; }
        public int BreastFeedBornTime { get; set; }
        public int BreastFeedOnlySixMonth { get; set; }
        public int NotBreastFeedBornTime { get; set; }
        public int BreastFeedPlusOther { get; set; }
        public int BreastFeedLessTwoMonth { get; set; }
        public int ChildVaccineId { get; set; }
        public int Vitamina { get; set; }
        public int WormDrugs { get; set; }
        public int PolioVaccine { get; set; }
        public int Bcg { get; set; }
        public int Dpt1 { get; set; }

        public int Dpt2 { get; set; }
        public int Dpt3 { get; set; }
        public int Dadhura { get; set; }
        public int Hb { get; set; }





    }
}
