﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
   public  class TrainingOccupationBo
    {
       public int firewoodId { get; set; }
       public int homeType { get; set; }
       public int ownerId { get; set; }
       public int hidAwareness { get; set; }
       public int hidEconomic { get; set; }
       public int hidOccupation { get; set; }
       public int hidMigration { get; set; }
       public int hidStatus { get; set; }
       public int hidComm { get; set; }
       public int hidReason { get; set; }
       public string trainingName { get; set; }
       public int maleNo { get; set; }
       public int femaleNo { get; set; }
       public int durationId { get; set; }
       public string ecoTrainingName { get; set; }
       public int ecoMaleNo { get; set; }
       public int ecoFemaleNo { get; set; }
       public int ecoDurationId { get; set; }
       public int occupationTypeId { get; set; }
       public int isMigration { get; set; }
       public int migrationId { get; set; }
       public int reasonTypeId { get; set; }
       public int communicationId { get; set; }
       public int electricitySourceId { get; set; }
       public int fuelSourceId { get; set; }
       public int stoveTypeId { get; set; }
       public int roofMaterialId { get; set; }
       public int isRadio { get; set; }
       public int isTV { get; set; }
       public int isTelePhone { get; set; }
       public int isMobile { get; set; }
       public int isInternet { get; set; }
       public int isNews { get; set; }
       public int others { get; set; }







    }
}
