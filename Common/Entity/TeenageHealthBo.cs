﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class TeenAgeHealthBo

    {
        public int OwnerId { get; set; }

        public int DiseaseId { get; set; }
        public int SexId { get; set; }
        public string Remarks { get; set; }
        public int Fatality { get; set; }
        public int DeadReason { get; set; }

        public int ChildFatalityId { get; set; }
        public int TeenageFatalityId { get; set; }
        public int TeenAgeHealthId { get; set; }
        public int AdultHealthId { get; set; }
        
        
    }
}
