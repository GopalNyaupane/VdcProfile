﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
   public  class DisasterReportBo
    {
       public int ownerId { get; set; }
       public int hidAffect { get; set; }
       public int hidLoss { get; set; }
       public int hidIO { get; set; }
       public int flood { get; set; }
       public int landslide { get; set; }
       public int hailStorm { get; set; }
       public int storm { get; set; }
       public int fire { get; set; }
       public int earthquake { get; set; }
       public int drought { get; set; }
       public int artibristi { get; set; }
       public int influenza { get; set; }
       public int insecticide { get; set; }
       public int other { get; set; }

       public decimal  floodLoss { get; set; }
       public decimal landslideLoss { get; set; }
       public decimal hailStormLoss { get; set; }
       public decimal stormLoss { get; set; }
       public decimal fireLoss { get; set; }
       public decimal earthquakeLoss { get; set; }
       public decimal droughtLoss { get; set; }
       public decimal artibristiLoss { get; set; }
       public decimal influenzaLoss { get; set; }
       public decimal insecticideLoss { get; set; }
       public decimal otherLoss { get; set; }
       public string provider { get; set; }
       public string collector { get; set; }
       public string validater { get; set; }
       public DateTime dataTaken { get; set; }
            


    }
}
