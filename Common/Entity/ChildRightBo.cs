﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class ChildRightBo
    {
        public int ownerId { get;set; }
        public int hiddenId1 { get; set; }
        public int hiddenId2{ get; set; }
        public int hiddenId3 { get; set; }
        public int hiddenId4 { get; set; }
        public int hiddenId5 { get; set; }
        public int hiddenId6 { get; set; }
        public int hiddenId7 { get; set; }
        public int sexId { get; set; }
        public int sexExp { get; set; }
        public int childExploitationId { get; set; }
        public int childDifferentiateId {get; set;}
        public int punishment { get; set; }
        public int  punishmentTypeId {get;set;}
        public int  sexDescriminationId {get;set;}
        public int sexDescriminationTypeId { get; set; }
        public int  violenceTypeId {get;set;}
        public int  orgId {get;set;}
        public int  streetId {get;set;}
        public int rightsId {get;set;}
        public int  smoking {get;set;}
        public int  cannabis {get;set;}
        public int  nonAddiction {get;set;}
      
        public int  clothingId {get;set;}
        public int  diffRoomId {get;set;}
        public int  homeShedId { get; set; }

    }
}
