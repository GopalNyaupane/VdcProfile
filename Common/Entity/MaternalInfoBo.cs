﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
   public class MaternalInfoBo
    {
       public int hiddenId { get; set; }
        public int MainID { get; set; }
        public int OwnerID { get; set; }
        public int PregTest { get; set; }
        public int IronTablet { get; set; }
        public int TtVaccine { get; set; }
        public int Maternity { get; set; }
        public int MaternalAddressId { get; set; }
        public int InfantHlthChk { get; set; }
        public int HivInfection { get; set; }
        public int Arv { get; set; }
    }
}
