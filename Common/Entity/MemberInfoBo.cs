﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
   public class MemberInfoBo
   {
       public int userId { get; set; }
       public int OwnerId { get; set; }
       public int OccupationType { get; set; }
       public int AgeGroup { get; set; }
       public int MaleNum { get; set; }
       public int FemaleNum { get; set; }

       public int IsOutMigrant { get; set; }

       public string Name { get; set; }
       public int Age { get; set; }
       public int SexId { get; set; }
       public string MigratedPlace { get; set; }
       public String OutReason { get; set; }


       public int DisableType { get; set; }
       public int MaleChildCount { get; set; }
       public int FemaleChildCount { get; set; }





     
    }
}
