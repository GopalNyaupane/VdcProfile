﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
   public  class ChildNutritionBo
    {
        public int ownerId { get; set; }
        public int hidden1 { get; set; }
        public int hidden2 { get; set; }
        public int hidden3 { get; set; }


        public int ageGroup { get; set; }
       
        public int birthCertificate { get; set; }
        public int sex { get; set; }
        public int noBirthCertificate { get; set; }
        public int salt { get; set; }
        public int nutritionFood {get; set;}
        public int  childWeightInfo {get; set;}
        public int age {get; set;}
       public decimal weight {get; set;}
       public int malNutrition { get; set; }
        

    }
}
