﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class EducationSection2Bo
    {
        public int ownerId { get; set; }
        public int hidUnadmitted { get; set; }
        public int hidDiscontinuation { get; set; }
        public int hidUnofficial { get; set; }
        public int hidDiscussion { get; set; }
        public int hidClubParticipation { get; set; }
        public int hidMembership { get; set; }
        public int hidMembership1 { get; set; }
        public int hidLocalOrg { get; set; }

        public int ageGroupId { get; set; }
        public int sexId { get; set; }
        public int normal { get; set; }
        public int abNormal { get; set; }
        public int discontinuedNo { get; set; }
        public string reason { get; set; }
        public int male { get; set; }
        public int female { get; set; }
        public int isDiscussion { get; set; }
        public int clubTypeId { get; set; }
        public int maleChildCount { get; set; }
        public int femaleChildCount { get; set; }
        public int localBody { get; set; }
        public int wardForum { get; set; }
        public int schoolMgmt { get; set; }
        public int childProCom { get; set; }
        public int childClub { get; set; }
        public int orgTypeId { get; set; }
        public int isParaLegalCom { get; set; }
        public int paraLegalReason { get; set; }
        public int workSatisfy { get; set; }

    }
}
