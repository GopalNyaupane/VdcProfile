﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Entity
{
    public class EducationInfoBo
    {
        public int SexId { set; get; }

        public int OwnerId { get; set; }
        public int BoyCount { set; get; }
        public int GirlCount { set; get; }
        public int OtherCount { set; get; }
        public int AgeId { set; get; }
        public int Literate { set; get; }
        public int Illeterate { set; get; }
        public int SlcPassed { set; get; }
        public int ProfCertLvl { set; get; }
        public int BachelorLvl { set; get; }
        public int MasterLvl { set; get; }
        public int PhdPass { set; get; }

        public int LvlId { set; get; }
        public int VdcSchool { set; get; }
        public int NonVdcSchool { set; get; }
        public int PrimaryChildCenter { set; get; }

        public int Duration { set; get; }

        public int ChldFrndlyTlt { set; get; }
        public int SpcEduForDisabled { set; get; }





    }
}
