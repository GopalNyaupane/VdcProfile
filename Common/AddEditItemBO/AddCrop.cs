﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.AddEditItemBO
{
    public class CropBo
    {
        public int cropId { get; set; }
        public string cropName { get; set; }

    }
    public class DisasterBo
    {
        public int disasterId { get; set; }
        public string disasterName
        {
            get;
            set;
        }
    }
     public class ExpenditureNameBo
     {
         public int expenditureId { get; set; }
         public string expenditureName
         {
             get;
             set;
         }

     }
    public class FruitBo
    {
        public int fruitId
        {
            get;
            set;
        }
        public string fruitName
        {
            get;
            set;
        }
    }

    public class IncomeNameBo
    {
        public int incomeNameId { get; set; }
        public string incomeName { get; set; }

    }

    public class OccupationBo
    {
        public int occupationId { get; set; }
        public string occupationName
        {
            get;
            set;
        }
    }


    public class RoleBo
    {
        public int roleId
        {
            get;
            set;
        }
        public string roleName
        {
            get;
            set;

        }
    }

    public class VegetablesBo
    {
        public int vegetablesId { get; set; }
        public string vegetablesName { get; set; }
    }

    public class CashCropsBo
    {
        public int cashCropId { get; set; }
        public string cashCropName { get; set; }
    }

    public class casteBo
    {
        public int casteId { get; set; }
        public string casteName { get; set; }
    }

    public class ChildLabourProblemBo
    {
        public int problemId { get; set; }
        public string problemName { get; set; }
    }

    public class ChildDifferentiateBo
    {
        public int childDifferentiateId { get; set; }
        public string childDifferentiateName { get; set; }
    }

    public class ChildPunishmentBo
    {
        public int childPunishmentId { get; set; }
        public string childPunishmentName { get; set; }
    }
    public class CommunityBo
    {
        public int communityId { get; set; }
        public string communityName { get; set; }
    }

    public class CookingFuelBo
    {
        public int cookingFuelId { get; set; }
        public string cookingFuelName { get; set; }
    }

    public class CookingOvenBo
    {
        public int cookingOvenId { get; set; }
        public string cookingOvenName { get; set; }
    }

    public class DecisionBo
    {
        public int decisionId { get; set; }
        public string decisionName { get; set; }
    }

    public class DiseaseBo
    {
        public int diseaseId { get; set; }
        public string diseaseName { get; set; }
    }

    public class ChildDiseaseBo
    {
        public int ChildDiseaseId { get; set; }
        public String ChildDiseaseName { get; set; }
    }

    public class DrinkingWaterBo
    {
        public int drinkingWaterId { get; set; }
        public string drinkingWaterName { get; set; }
    }

    public class DrinkingWaterTimeBo
    {
        public int drinkingWaterTimeId { get; set; }
        public string drinkingWaterTimeName { get; set; }
    }

    public class ElectricitySourceBo
    {
        public int electricityId { get; set; }
        public string electricityName { get; set; }
    }
    public class FloorBo
    {
        public int floorId { get; set; }
        public string floorName { get; set; }
    }

    public class HandiCappedBo
    {
        public int handicappedId { get; set; }
        public string handicappedName { get; set; }
    }

    public class HandWashBo
    {
        public int handWashId { get; set; }
        public string handWashName { get; set; }
    }
    public class HealthCheckUpBo
    {
        public int healthCheckUPId { get; set; }
        public string healthCheckUpName { get; set; }
    }

    public class LanguageBo
    {
        public int languageId { get; set; }
        public string languageName { get; set; }
    }
    public class MaternityAddressBo
    {
        public int addressId { get; set; }
        public string addressName { get; set; }
    }
    public class MigrationReasonBo
    {
        public int reasonId { get; set; }
        public string reasonName { get; set; }
    }
   
    public class ParalegalCommunityBo
    {
        public int communityId { get; set; }
        public string communityName { get; set; }
    }
    public class RelationBo
    {
        public int relationId { get; set; }
        public string relationName { get; set; }
    }

    public class ReligionBo
    {
        public int religionId { get; set; }
        public string religionName { get; set; }
    }

    public class RoofStructureBo
    {
        public int roofStructureId { get; set; }
        public string roofStuructureName { get; set; }
    }

    public class SocialViolanceBo
    {
        public int violanceId { get; set; }
        public string violanceName { get; set; }
    }

    public class SchoolPunishmentBo
    {
        public int punishmentId { get; set; }
        public string punishmentName { get; set; }
    }

    public class TelecommunicationBo
    {
        public int sourceId { get; set; }
        public string sourceName { get; set; }
    }
    public class ToiletBo
    {
        public int ToiletId { get; set; }
        public string toiletName { get; set; }
    }
    public class TrainingBo
    {
        public int trainingId { get; set; }
        public string trainingName { get; set; }
    }

    public class trainingTypeBo
    {
        public int trainingTypeId { get; set; }
        public string trainingTypeName { get; set; }
    }





}
