﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.AddEditItem
{
    public class CasteDAO : DbConnection.ConnectionHelper
    {
        public int InsertCaste(Common.AddEditItemBO.casteBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_CASTE");
                Db.AddInParameter(cmd, "@casteName", DbType.String, objBl.casteName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class ChildLabourProblemDAO : DbConnection.ConnectionHelper
    {
        public int InsertChildLabourProblem(Common.AddEditItemBO.ChildLabourProblemBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_CHILD_LABOUR_PROBLEM");
                Db.AddInParameter(cmd, "@problemName", DbType.String, objBl.problemName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class ChildDiseaseDAO : DbConnection.ConnectionHelper
    {
        public int InsertChildDisease(Common.AddEditItemBO.ChildDiseaseBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_CHILD_DISEASE");
                Db.AddInParameter(cmd, "@ChildDiseaseName", DbType.String, objBl.ChildDiseaseName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class ChildDifferentiateDAO : DbConnection.ConnectionHelper
    {
        public int InsertChildDifferentiate(Common.AddEditItemBO.ChildDifferentiateBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_CHILDDIFFERENTIATE");
                Db.AddInParameter(cmd, "@childDifferentiate", DbType.String, objBl.childDifferentiateName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class ChildPunishmentDAO : DbConnection.ConnectionHelper
    {
        public int InsertChildPunishment(Common.AddEditItemBO.ChildPunishmentBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_CHILDPUNISHMENT");
                Db.AddInParameter(cmd, "@childpunishment", DbType.String, objBl.childPunishmentName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class CommunityDAO : DbConnection.ConnectionHelper
    {
        public int InsertCommunity(Common.AddEditItemBO.CommunityBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_COMMUNITY");
                Db.AddInParameter(cmd, "@communityName", DbType.String, objBl.communityName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class CookingFuelNameDAO : DbConnection.ConnectionHelper
    {
        public int InsertCookingFuelName(Common.AddEditItemBO.CookingFuelBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_COOKING_FUEL_NAME");
                Db.AddInParameter(cmd, "@cookingFuelName", DbType.String, objBl.cookingFuelName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class CookingOvenNameDAO : DbConnection.ConnectionHelper
    {
        public int InsertCookingOven(Common.AddEditItemBO.CookingOvenBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_COOKING_OVEN_NAME");
                Db.AddInParameter(cmd, "@cookingOvenName", DbType.String, objBl.cookingOvenName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class CropDAO : DbConnection.ConnectionHelper
    {
        public int InsertCrop(Common.AddEditItemBO.CropBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_CROPS");
                Db.AddInParameter(cmd, "@cropName", DbType.String, objBl.cropName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class DecisionDAO : DbConnection.ConnectionHelper
    {
        public int InsertDecision(Common.AddEditItemBO.DecisionBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_DECISION");
                Db.AddInParameter(cmd, "@decisionName", DbType.String, objBl.decisionName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class DiseaseDAO : DbConnection.ConnectionHelper
    {
        public int InsertDisease(Common.AddEditItemBO.DiseaseBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_DISEASE");
                Db.AddInParameter(cmd, "@diseaseName", DbType.String, objBl.diseaseName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class DrinkingWaterDAO : DbConnection.ConnectionHelper
    {
        public int InsertDrinkingWater(Common.AddEditItemBO.DrinkingWaterBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_DRINKINGWATER");
                Db.AddInParameter(cmd, "@drinkingWaterName", DbType.String, objBl.drinkingWaterName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class DrinkingWaterTimeDAO : DbConnection.ConnectionHelper
    {
        public int InsertDrinkingWaterTime(Common.AddEditItemBO.DrinkingWaterTimeBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_DRINKINGWATER_TIME");
                Db.AddInParameter(cmd, "@drinkingWaterTimeName", DbType.String, objBl.drinkingWaterTimeName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class ElectricitySourceDAO : DbConnection.ConnectionHelper
    {
        public int InsertElectricitySource(Common.AddEditItemBO.ElectricitySourceBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_ELECTRICITYsOURCE");
                Db.AddInParameter(cmd, "@electricityName", DbType.String, objBl.electricityName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
    public class FloorDAO : DbConnection.ConnectionHelper
    {
        public int InsertFloor(Common.AddEditItemBO.FloorBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_FLOOR");
                Db.AddInParameter(cmd, "@floorName", DbType.String, objBl.floorName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class FruitDAO : DbConnection.ConnectionHelper
    {
        public int InsertFloor(Common.AddEditItemBO.FruitBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_FRUITS");
                Db.AddInParameter(cmd, "@fruitName", DbType.String, objBl.fruitName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class HandiCappedDAO : DbConnection.ConnectionHelper
    {
        public int InsertHandiCapped(Common.AddEditItemBO.HandiCappedBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_HANDICAPPED");
                Db.AddInParameter(cmd, "@handicappedName", DbType.String, objBl.handicappedName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
    public class HandWashDAO : DbConnection.ConnectionHelper
    {
        public int InsertHandWash(Common.AddEditItemBO.HandWashBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_HANDWASH");
                Db.AddInParameter(cmd, "@handWashName", DbType.String, objBl.handWashName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class HealthCheckupDAO : DbConnection.ConnectionHelper
    {
        public int InsertHealthCheckup(Common.AddEditItemBO.HealthCheckUpBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_HEALTHCHECKUP");
                Db.AddInParameter(cmd, "@healthCheckupName", DbType.String, objBl.healthCheckUpName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class MaternityAddressDAO : DbConnection.ConnectionHelper
    {
        public int InsertMaternityAddress(Common.AddEditItemBO.MaternityAddressBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_MATERNITYADDRESS");
                Db.AddInParameter(cmd, "@addressName", DbType.String, objBl.addressName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }


    public class LanguageDAO : DbConnection.ConnectionHelper
    {
        public int InsertLanguage(Common.AddEditItemBO.LanguageBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_LANGUAGE");
                Db.AddInParameter(cmd, "@LanguageName", DbType.String, objBl.languageName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class MigrationReasonDAO : DbConnection.ConnectionHelper
    {
        public int InsertMigrationReason(Common.AddEditItemBO.MigrationReasonBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_MIGRATIONREASON");
                Db.AddInParameter(cmd, "@reasonName", DbType.String, objBl.reasonName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
    public class OccupationDAO : DbConnection.ConnectionHelper
    {
        public int InsertOccupation(Common.AddEditItemBO.OccupationBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_OCCUPATION");
                Db.AddInParameter(cmd, "@occupationName", DbType.String, objBl.occupationName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
    public class ParalegalCommuniytDAO : DbConnection.ConnectionHelper
    {
        public int InsertParalegalcommunity(Common.AddEditItemBO.ParalegalCommunityBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_PARALEGALCOMMUNITY");
                Db.AddInParameter(cmd, "@communityName", DbType.String, objBl.communityName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class RelationDAO : DbConnection.ConnectionHelper
    {
        public int InsertRelation(Common.AddEditItemBO.RelationBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_RELATION");
                Db.AddInParameter(cmd, "@healthCheckupName", DbType.String, objBl.relationName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class ReligionDAO : DbConnection.ConnectionHelper
    {
        public int InsertReligion(Common.AddEditItemBO.ReligionBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_RELIGION");
                Db.AddInParameter(cmd, "@religionName", DbType.String, objBl.religionName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class RoofStructureDAO : DbConnection.ConnectionHelper
    {
        public int InsertRoofStructure(Common.AddEditItemBO.RoofStructureBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_ROOFSTRUCTURE");
                Db.AddInParameter(cmd, "@roofStructureName", DbType.String, objBl.roofStuructureName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
    public class SchoolPunishmentDAO : DbConnection.ConnectionHelper
    {
        public int InsertSchoolPunishment(Common.AddEditItemBO.SchoolPunishmentBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_SCHOOL_PUNISHMENT");
                Db.AddInParameter(cmd, "@punishmentName", DbType.String, objBl.punishmentName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class SocialViolanceDAO : DbConnection.ConnectionHelper
    {
        public int InsertSocialViolance(Common.AddEditItemBO.SocialViolanceBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_SOCIAL_COMMUNITY");
                Db.AddInParameter(cmd, "@violanceName", DbType.String, objBl.violanceName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class TelecommunicationDAO : DbConnection.ConnectionHelper
    {
        public int InsertTelecommunication(Common.AddEditItemBO.TelecommunicationBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_TELECOMMUNICATION");
                Db.AddInParameter(cmd, "@sourceName", DbType.String, objBl.sourceName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class ToiletDAO : DbConnection.ConnectionHelper
    {
        public int InsertToilet(Common.AddEditItemBO.ToiletBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_TOILET");
                Db.AddInParameter(cmd, "@toiletName", DbType.String, objBl.toiletName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
    public class TrainingDAO : DbConnection.ConnectionHelper
    {
        public int InsertTraining(Common.AddEditItemBO.TrainingBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_TRAINING");
                Db.AddInParameter(cmd, "@trainingName", DbType.String, objBl.trainingName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
    public class AddAnimalDAO : DbConnection.ConnectionHelper
    {
        public int InsertAnimal(Common.Entity.AddAnimal objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_ANIMALS");
                Db.AddInParameter(cmd, "@animalName", DbType.String, objBl.animalName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }



    public class AddCashCropDAO : DbConnection.ConnectionHelper
    {
        public int InsertCashCrop(Common.AddEditItemBO.AddCashCropBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_CASH_CROP");

                Db.AddInParameter(cmd, "@cashCrop", DbType.String, objBl.cashCropName);



                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

    public class TrainingTypeDAO : DbConnection.ConnectionHelper
    {
        public int InsertTrainingType(Common.AddEditItemBO.trainingTypeBo objBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_COM_TRAININGTYPE");
                Db.AddInParameter(cmd, "@trainingTypeName", DbType.String, objBl.trainingTypeName);
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public class VegetablesDAO : DbConnection.ConnectionHelper
        {
            public int InsertVegetables(Common.AddEditItemBO.VegetablesBo objBl)
            {
                try
                {
                    DbCommand cmd = Db.GetStoredProcCommand("USP_COM_VEGETABLES");
                    Db.AddInParameter(cmd, "@vegetablesName", DbType.String, objBl.vegetablesName);
                    int sqdr = Db.ExecuteNonQuery(cmd);
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
    }
}


