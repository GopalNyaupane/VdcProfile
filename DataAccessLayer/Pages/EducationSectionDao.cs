﻿using Common.Entity;
using DataAccessLayer.DbConnection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Pages
{
    public class EducationSectionDao:ConnectionHelper
    {
        public int Insert_Org_Edu_Dis(EducationSection2Bo obj)
        {
            try
            {
               

                //For Inserting for remainig Table 
                DbCommand cmd3 = Db.GetStoredProcCommand("USP_INSERT_ORG_EDU_DIS");
                Db.AddInParameter(cmd3, "@OWNER_ID", DbType.String, obj.ownerId);
               
                //for table unofficail education
                Db.AddInParameter(cmd3, "MALE", DbType.String, obj.male);
                Db.AddInParameter(cmd3, "FEMALE", DbType.String, obj.female);

                //for table checked_discussion
                Db.AddInParameter(cmd3, "IS_DISCUSSION", DbType.String, obj.isDiscussion);


                //FOR tABLE LOCAL ORGANIZATION MEMBERSHIP

                Db.AddInParameter(cmd3, "ORG_TYPE_ID", DbType.String, obj.orgTypeId);
               
                Db.AddInParameter(cmd3, "IS_PARA_LEGAL_COM", DbType.String, obj.isParaLegalCom);
                Db.AddInParameter(cmd3, "PARA_LEGAL_REASON_ID", DbType.String, obj.paraLegalReason);
                Db.AddInParameter(cmd3, "IS_PARA_LEGAL_WORK_SATISFYING", DbType.String, obj.workSatisfy);

                
               int i = Db.ExecuteNonQuery(cmd3);

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int Insert_Discontinuation(EducationSection2Bo obj)
        {
            try
            {


                //For Inserting for remainig Table 
                DbCommand cmd3 = Db.GetStoredProcCommand("USP_INSERT_DISCONTINUATION");
                Db.AddInParameter(cmd3, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd3, "sex_id", DbType.String, obj.sexId);
                Db.AddInParameter(cmd3, "DISCONTINUED_NO", DbType.String, obj.discontinuedNo);
                Db.AddInParameter(cmd3, "DISCONTINUED_REASON", DbType.String, obj.reason);

                            


                int i = Db.ExecuteNonQuery(cmd3);

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        //Insert for tabke TBL_school_unadmitted
        public int School_Unadmitted_Insert(EducationSection2Bo obj)
        {
            try
            { 
                
                DbCommand cmd = Db.GetStoredProcCommand("USP_SCHOOL_UNADMITTED_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "AGE_GROUP_ID", DbType.String, obj.ageGroupId);
                Db.AddInParameter(cmd, "SEX_ID", DbType.String, obj.sexId);
                Db.AddInParameter(cmd, "NORMAL", DbType.String, obj.normal);
                Db.AddInParameter(cmd, "ABNORMAL", DbType.String, obj.abNormal);
                int i = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch(Exception ex)
            {
                     return 0;
            }            
       
        }

        //For Table Club_participation USP_CLUB_PARTICIPATION_INSERT
        public int Club_Participation_Insert(EducationSection2Bo obj)
        {
            try
            {           
                DbCommand cmd = Db.GetStoredProcCommand("USP_CLUB_PARTICIPATION_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@CLUB_TYPE_ID", DbType.String, obj.clubTypeId);
                Db.AddInParameter(cmd, "@MALE_CHILD_COUNT", DbType.Int32, obj.maleChildCount);
                Db.AddInParameter(cmd, "@FEMALE_CHILD_COUNT", DbType.Int32, obj.femaleChildCount);
                int i = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        //FOR Table Committee Membership
        public int Committee_Membership_Insert(EducationSection2Bo obj)
        {
            try{
                
                DbCommand cmd2 = Db.GetStoredProcCommand("USP_COMMITTEE_MEMBERSHIP_INSERT");
                Db.AddInParameter(cmd2, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd2, "@SEX_ID", DbType.String, obj.sexId);
                Db.AddInParameter(cmd2, "@LOCAL_BODY", DbType.String, obj.localBody);
                Db.AddInParameter(cmd2, "@WARD_CITIZEN_FORUM", DbType.String, obj.wardForum);
                Db.AddInParameter(cmd2, "@SCHOOL_HLTH_MGMT", DbType.String, obj.schoolMgmt);
                Db.AddInParameter(cmd2, "@CHILD_PRO_COM", DbType.String, obj.childProCom);
                Db.AddInParameter(cmd2, "@CHILD_CLUB", DbType.String, obj.childClub);
                int i = Db.ExecuteNonQuery(cmd2);
                return 1;
            }
            catch(Exception ex)
            { 
                return 0;
            }
        }

        public DataSet Fetch_Education(EducationSection2Bo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_EDUCATION", objBo.ownerId);
            return ds;
        }

        public DataTable Fetch_School_Unadmitted(EducationSection2Bo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_SCHOOL_UNADMITTED", objBo.ownerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable Fetch_Club_Participation(EducationSection2Bo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_CLUB_PARTICIPATION", objBo.ownerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable Fetch_Committee_Membership(EducationSection2Bo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_COMMITTEE_MEMBERSHIP", objBo.ownerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }
        public int Update_Org_Edu_Dis(EducationSection2Bo obj)
        {
            try
            {
                //For Inserting for remainig Table 
                DbCommand cmd3 = Db.GetStoredProcCommand("USP_UPDATE_ORG_EDU_DIS");
                Db.AddInParameter(cmd3, "@OWNER_ID", DbType.String, obj.ownerId);
                //For table school_discontinued
              

                //for table unofficail education
                Db.AddInParameter(cmd3, "@unofficial_id", DbType.String, obj.hidUnofficial);
                Db.AddInParameter(cmd3, "@MALE", DbType.String, obj.male);
                Db.AddInParameter(cmd3, "@FEMALE", DbType.String, obj.female);

                //for table checked_discussion
                Db.AddInParameter(cmd3, "@child_discussion_id", DbType.String, obj.hidDiscussion);
                Db.AddInParameter(cmd3, "@IS_DISCUSSION", DbType.String, obj.isDiscussion);

               
                //FOR tABLE LOCAL ORGANIZATION MEMBERSHIP
                Db.AddInParameter(cmd3, "@local_org_membership_id", DbType.String, obj.hidLocalOrg);
                Db.AddInParameter(cmd3, "@ORG_TYPE_ID", DbType.String, obj.orgTypeId);
                Db.AddInParameter(cmd3, "@IS_PARA_LEGAL_COM", DbType.String, obj.isParaLegalCom);
                Db.AddInParameter(cmd3, "@PARA_LEGAL_REASON_ID", DbType.String, obj.paraLegalReason);
                Db.AddInParameter(cmd3, "@IS_PARA_LEGAL_WORK_SATISFYING", DbType.String, obj.workSatisfy);


                int i = Db.ExecuteNonQuery(cmd3);

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int Update_Discontinuation(EducationSection2Bo obj)
        {
            try
            {
                //For Inserting for remainig Table 
                DbCommand cmd3 = Db.GetStoredProcCommand("USP_UPDATE_DISCONTINUATION");
                Db.AddInParameter(cmd3, "@OWNER_ID", DbType.String, obj.ownerId);
                 Db.AddInParameter(cmd3, "@sex_id", DbType.String, obj.sexId);
                 Db.AddInParameter(cmd3, "@school_discontinued_id", DbType.String, obj.hidDiscontinuation);
                Db.AddInParameter(cmd3, "@DISCONTINUED_NO", DbType.String, obj.discontinuedNo);
                Db.AddInParameter(cmd3, "@DISCONTINUED_REASON", DbType.String, obj.reason);

               
                int i = Db.ExecuteNonQuery(cmd3);

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Update_School_Unadmitted(EducationSection2Bo obj)
        {
            try
            {
                //For Inserting for remainig Table 
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_SCHOOL_UNADMITTED");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@unadmitted_id", DbType.String, obj.hidUnadmitted);
                Db.AddInParameter(cmd, "@AGE_GROUP_ID", DbType.String, obj.ageGroupId);
                Db.AddInParameter(cmd, "@SEX_ID", DbType.String, obj.sexId);
                Db.AddInParameter(cmd, "@NORMAL", DbType.String, obj.normal);
                Db.AddInParameter(cmd, "@ABNORMAL", DbType.String, obj.abNormal);
                int i = Db.ExecuteNonQuery(cmd);
                
                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Update_Club_Participation(EducationSection2Bo obj)
        {
            try
            {
                //For Inserting for remainig Table 
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CLUB_PARTICIPATION");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@club_participation_id", DbType.String, obj.hidClubParticipation);
                Db.AddInParameter(cmd, "@CLUB_TYPE_ID", DbType.String, obj.clubTypeId);
                Db.AddInParameter(cmd, "@MALE_CHILD_COUNT", DbType.Int32, obj.maleChildCount);
                Db.AddInParameter(cmd, "@FEMALE_CHILD_COUNT", DbType.Int32, obj.femaleChildCount);
                int i = Db.ExecuteNonQuery(cmd);

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Update_Committee_Membership(EducationSection2Bo obj)
        {
            try
            {
                //For Inserting for remainig Table 
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_COMMITTEE_MEMBERSHIP");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@committee_membership_id", DbType.String, obj.hidMembership);
                Db.AddInParameter(cmd, "sex_id", DbType.String, obj.sexId);
                Db.AddInParameter(cmd, "@LOCAL_BODY", DbType.String, obj.localBody);
                Db.AddInParameter(cmd, "@WARD_CITIZEN_FORUM", DbType.String, obj.wardForum);
                Db.AddInParameter(cmd, "@SCHOOL_HLTH_MGMT", DbType.String, obj.schoolMgmt);
                Db.AddInParameter(cmd, "@CHILD_PRO_COM", DbType.String, obj.childProCom);
                Db.AddInParameter(cmd, "@CHILD_CLUB", DbType.String, obj.childClub);
                int i = Db.ExecuteNonQuery(cmd)+1;

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }


    }
}
            