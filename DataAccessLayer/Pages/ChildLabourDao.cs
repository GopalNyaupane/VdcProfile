﻿using Common.Entity;
using DataAccessLayer.DbConnection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Pages
{
    public class ChildLabourDao : ConnectionHelper
    {
        public int Child_Labour_Insert(List<ChildLabourBo> objList)
        {
            int j = 0;
            try
            {
                 foreach(ChildLabourBo objChildMember in objList)
                 {
                     DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_LABOUR_INSERT");
                     Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, objChildMember.ownerId);
                     Db.AddInParameter(cmd, "@CHILD_LABOUR", DbType.String, objChildMember.childLabour);
                     Db.AddInParameter(cmd, "@AGE", DbType.String, objChildMember.age);
                     Db.AddInParameter(cmd, "@SEX_ID", DbType.String, objChildMember.sexId);
                     Db.AddInParameter(cmd, "@LABOUR_TYPE_ID", DbType.String, objChildMember.labourType);
                     Db.AddInParameter(cmd, "@WORK_HOUR", DbType.String, objChildMember.workHour);
                     Db.AddInParameter(cmd, "@WORK_YEAR", DbType.String, objChildMember.workYear);
                     Db.AddInParameter(cmd, "@YEARLY_INCOME", DbType.String, objChildMember.yearlyIncome);
                     Db.AddInParameter(cmd, "@ANNUAL_VISIT", DbType.String, objChildMember.annualVisit);
                     Db.AddInParameter(cmd, "@IS_SCHOOL", DbType.String, objChildMember.isSchool);

                     int cli = Db.ExecuteNonQuery(cmd);
                     j += 1;
                 }
               
                
            }
            catch(Exception ex)
            {
                return 0;
            }
            if (j > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public int Child_Labour_Exploitation(ChildLabourBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD__LABOUR_EXPLOITATION_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@LABOUR_EXPLOITATION_TYPE_ID", DbType.String, obj.labourExploitationType);
                int cle = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch(Exception ex)
            {
                return 0;

            }
        }

        public DataTable Fetch_ChildLabour(ChildLabourBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_LABOUR", objBo.ownerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
           
        }  
        public DataTable Fetch_ChildLabour_Exp(ChildLabourBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_LABOUR_EXP", objBo.ownerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];

            }
            return dt;
        }

        public int Update_ChildLabour(ChildLabourBo obj)
        {
            
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_LABOUR");

                Db.AddInParameter(cmd, "@child_labour_id", DbType.Int32, obj.hiddenId1);
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@CHILD_LABOUR", DbType.String, obj.childLabour);
                Db.AddInParameter(cmd, "@AGE", DbType.String, obj.age);
                Db.AddInParameter(cmd, "@SEX_ID", DbType.String, obj.sexId);
                Db.AddInParameter(cmd, "@LABOUR_TYPE_ID", DbType.String, obj.labourType);
                Db.AddInParameter(cmd, "@WORK_HOUR", DbType.String, obj.workHour);
                Db.AddInParameter(cmd, "@WORK_YEAR", DbType.String, obj.workYear);
                Db.AddInParameter(cmd, "@YEARLY_INCOME", DbType.String, obj.yearlyIncome);
                Db.AddInParameter(cmd, "@ANNUAL_VISIT", DbType.String, obj.annualVisit);
                Db.AddInParameter(cmd, "@IS_SCHOOL", DbType.String, obj.isSchool);

                Db.ExecuteNonQuery(cmd);               
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Update_Child_Labour_Exp(ChildLabourBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_LABOUR_EXP");
                Db.AddInParameter(cmd, "@labour_exploitation_id", DbType.Int32, objBo.hiddenId2);
                Db.AddInParameter(cmd, "@owner_id", DbType.String, objBo.ownerId);
                Db.AddInParameter(cmd, "@labour_exploitation_type_id", DbType.String, objBo.labourExploitationType);
                int cle = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public void DeleteOldRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_CHILD_LABOUR");
            Db.AddInParameter(cmd, "@Owner_Id", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);


        }
        
        }
}
