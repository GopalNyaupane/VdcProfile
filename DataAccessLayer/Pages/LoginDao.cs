﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Common.Entity;
using DataAccessLayer.DbConnection;

namespace DataAccessLayer
{
    public class LoginDao : ConnectionHelper
    {
        public DataTable CheckUser(LoginBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_CHECKUSER", objBo.Username, objBo.Password);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }
    }
}
