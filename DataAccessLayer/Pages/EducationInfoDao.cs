﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DataAccessLayer.DbConnection;
using System.Data.Common;

namespace DataAccessLayer
{
   public class EducationInfoDao :ConnectionHelper
    {
        public int InsertUnofficialSchool(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_UNOFFICIAL_SCHOOLING");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@malecount", DbType.Int32, objEducationInfo.BoyCount);
            Db.AddInParameter(cmd, "@femalecount", DbType.Int32, objEducationInfo.GirlCount);
            Db.AddInParameter(cmd, "@othercount", DbType.Int32, objEducationInfo.OtherCount);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;

        }

        public int InsertFamilyEduInfo(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_FAMILY_EDU");
 

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@ageId", DbType.Int32, objEducationInfo.AgeId);
            Db.AddInParameter(cmd, "@literate", DbType.Int32, objEducationInfo.Literate);
            Db.AddInParameter(cmd, "@illiterate", DbType.Int32, objEducationInfo.Illeterate);
            Db.AddInParameter(cmd, "@slcpassed", DbType.Int32, objEducationInfo.SlcPassed);
            Db.AddInParameter(cmd, "@plstwopass", DbType.Int32, objEducationInfo.ProfCertLvl);
            Db.AddInParameter(cmd, "@bachelorpass", DbType.Int32, objEducationInfo.BachelorLvl);
            Db.AddInParameter(cmd, "@masterpass", DbType.Int32, objEducationInfo.MasterLvl);
            Db.AddInParameter(cmd, "@phd", DbType.Int32, objEducationInfo.PhdPass);
            Db.AddInParameter(cmd, "@sexId", DbType.Int32, objEducationInfo.SexId);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
        }

        public int InsertChildSchool(Common.Entity.EducationInfoBo objEducationInfo)
        {

            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_CHILD_SCHOOL");
 

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@levelId", DbType.Int32, objEducationInfo.LvlId);
            Db.AddInParameter(cmd, "@sexId", DbType.Int32, objEducationInfo.SexId);
            Db.AddInParameter(cmd, "@pmrychldcenter", DbType.Int32, objEducationInfo.PrimaryChildCenter);
            Db.AddInParameter(cmd, "@vdcschool", DbType.Int32, objEducationInfo.VdcSchool);
            Db.AddInParameter(cmd, "@nonvdcschool", DbType.Int32, objEducationInfo.NonVdcSchool);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;

        }

        public int InsertSchoolStatus(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_SCHOOL_STATUS");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@childfriendly", DbType.Int32, objEducationInfo.ChldFrndlyTlt);
            Db.AddInParameter(cmd, "@sceciapeducation ", DbType.Int32, objEducationInfo.SpcEduForDisabled);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
        }

        public int InsertTimeToSchool(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_SCHOOL_REACHTIME");
            
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@level", DbType.Int32, objEducationInfo.LvlId);
            Db.AddInParameter(cmd, "@duration", DbType.Int32, objEducationInfo.Duration);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
        }



        public DataTable FetchUnoffSchooling(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_UNOFFICIAL_SCHOOLING");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_UNOFFICIAL_SCHOOLING", objEducationInfo.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchFamilyEdu(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_FAMILY_EDU");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_FAMILY_EDU", objEducationInfo.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchChildSchool(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_CHILD_SCHOOL");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_CHILD_SCHOOL", objEducationInfo.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchSchoolReachTime(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_SCHOOL_REACH_TIME");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_SCHOOL_REACH_TIME", objEducationInfo.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchSchoolStatus(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_SCHOOL_STATUS");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_SCHOOL_STATUS", objEducationInfo.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public void UpdateUnoffSchool(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_UNOFFICIAL_SCHOOLING");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@malecount", DbType.Int32, objEducationInfo.BoyCount);
            Db.AddInParameter(cmd, "@femalecount", DbType.Int32, objEducationInfo.GirlCount);
            Db.AddInParameter(cmd, "@othercount", DbType.Int32, objEducationInfo.OtherCount);

            Db.ExecuteNonQuery(cmd);
        }

        public int UpdateFamilyEduInfo(Common.Entity.EducationInfoBo objEducationInfo)
        {

            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_FAMILY_EDU");


            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@ageId", DbType.Int32, objEducationInfo.AgeId);
            Db.AddInParameter(cmd, "@literate", DbType.Int32, objEducationInfo.Literate);
            Db.AddInParameter(cmd, "@illiterate", DbType.Int32, objEducationInfo.Illeterate);
            Db.AddInParameter(cmd, "@slcpassed", DbType.Int32, objEducationInfo.SlcPassed);
            Db.AddInParameter(cmd, "@plstwopass", DbType.Int32, objEducationInfo.ProfCertLvl);
            Db.AddInParameter(cmd, "@bachelorpass", DbType.Int32, objEducationInfo.BachelorLvl);
            Db.AddInParameter(cmd, "@masterpass", DbType.Int32, objEducationInfo.MasterLvl);
            Db.AddInParameter(cmd, "@phd", DbType.Int32, objEducationInfo.PhdPass);
            Db.AddInParameter(cmd, "@sexId", DbType.Int32, objEducationInfo.SexId);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
        }

        public int UpdateChildSchool(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_SCHOOL");


            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@levelId", DbType.Int32, objEducationInfo.LvlId);
            Db.AddInParameter(cmd, "@sexId", DbType.Int32, objEducationInfo.SexId);
            Db.AddInParameter(cmd, "@pmrychldcenter", DbType.Int32, objEducationInfo.PrimaryChildCenter);
            Db.AddInParameter(cmd, "@vdcschool", DbType.Int32, objEducationInfo.VdcSchool);
            Db.AddInParameter(cmd, "@nonvdcschool", DbType.Int32, objEducationInfo.NonVdcSchool);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
        }

        public int UpdateTimeToSchool(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_SCHOOL_REACH_TIME");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@level", DbType.Int32, objEducationInfo.LvlId);
            Db.AddInParameter(cmd, "@duration", DbType.Int32, objEducationInfo.Duration);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
        }

        public void UpdateSchoolStatus(Common.Entity.EducationInfoBo objEducationInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_SCHOOL_STATUS");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objEducationInfo.OwnerId);
            Db.AddInParameter(cmd, "@childfriendly", DbType.Int32, objEducationInfo.ChldFrndlyTlt);
            Db.AddInParameter(cmd, "@sceciapeducation ", DbType.Int32, objEducationInfo.SpcEduForDisabled);
                
            Db.ExecuteNonQuery(cmd);
         
        }
    }
}
