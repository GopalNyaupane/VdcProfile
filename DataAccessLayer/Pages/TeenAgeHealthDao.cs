﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Common.Entity;
using DataAccessLayer.DbConnection;

namespace DataAccessLayer
{
    public class TeenAgeHealthDao:ConnectionHelper
    {
        public int InsertTeenAgeHealth(List<TeenAgeHealthBo> infoBo)
        {
            int j = 0;
            try
            {
                foreach (TeenAgeHealthBo objTeenAgeHealth in infoBo)
                {

                    DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_TEEN_AGE_HEALTH_INFO");
                    Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, objTeenAgeHealth.OwnerId);
                    Db.AddInParameter(cmd, "@DiseaseId", DbType.String, objTeenAgeHealth.DiseaseId);
                    Db.AddInParameter(cmd, "@SexId", DbType.Int32, objTeenAgeHealth.SexId);
                    Db.AddInParameter(cmd, "@Remarks", DbType.Int32, objTeenAgeHealth.Remarks);
                    j = Db.ExecuteNonQuery(cmd) + j;

                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            if (j > 0)
                return 1;
            else
            {
                return 0;
            }


        }
        public int InsertAdultHealth(List<TeenAgeHealthBo> infoBo)
        {
            int j = 0;
            try
            {
                foreach (TeenAgeHealthBo objAdultHealth in infoBo)
                {

                    DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_ADULT_HEALTH_INFO");
                    Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, objAdultHealth.OwnerId);
                    Db.AddInParameter(cmd, "@DiseaseId", DbType.String, objAdultHealth.DiseaseId);
                    Db.AddInParameter(cmd, "@SexId", DbType.Int32, objAdultHealth.SexId);
                    j = Db.ExecuteNonQuery(cmd) + j;

                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            if (j > 0)
                return 1;
            else
            {
                return 0;
            }


        }


        public int UpdateTeenAgeHealth(List<TeenAgeHealthBo> infoBo)
        {
            int j = 0;
            try
            {
                foreach (TeenAgeHealthBo objTeenAgeHealth in infoBo)
                {

                    DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_TEEN_AGE_HEALTH_INFO");
                    Db.AddInParameter(cmd, "@TeenAgeHealthID", DbType.Int32, objTeenAgeHealth.TeenAgeHealthId);
                    Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, objTeenAgeHealth.OwnerId);
                    Db.AddInParameter(cmd, "@DiseaseId", DbType.String, objTeenAgeHealth.DiseaseId);
                    Db.AddInParameter(cmd, "@SexId", DbType.Int32, objTeenAgeHealth.SexId);
                    Db.AddInParameter(cmd, "@Remarks", DbType.Int32, objTeenAgeHealth.Remarks);
                    j = Db.ExecuteNonQuery(cmd) + j;

                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            if (j > 0)
                return 1;
            else
            {
                return 0;
            }


        }

        public DataTable FetchTeenAgeHealth(TeenAgeHealthBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_TEEN_AGE_HEALTH", objBo.OwnerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }
        public DataTable FetchAdultHealth(TeenAgeHealthBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_ADULT_HEALTH", objBo.OwnerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }
        public int InsertTeenAgeFatality(List<TeenAgeHealthBo> infoBos)
        {
            int j = 0;
            try
            {
                foreach (TeenAgeHealthBo objTeenAgeFatality in infoBos)
                {

                    DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_TEEN_AGE_FATALITY_INFO");
                    Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, objTeenAgeFatality.OwnerId);
                    Db.AddInParameter(cmd, "@DiseaseId", DbType.String, objTeenAgeFatality.DiseaseId);
                    Db.AddInParameter(cmd, "@SexId", DbType.Int32, objTeenAgeFatality.SexId);
                    Db.AddInParameter(cmd, "@Fatality", DbType.Int32, objTeenAgeFatality.Fatality);
                    j = Db.ExecuteNonQuery(cmd) + j;

                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            if (j > 0)
                return 1;
            else
            {
                return 0;
            }

        }


        public DataTable FetchTeenAgeFatality(TeenAgeHealthBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_TEEN_AGE_FATALITY", objBo.OwnerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public int InsertChildFatality(List<TeenAgeHealthBo> infoBo)
        {
            int j = 0;
            try
            {
                foreach (TeenAgeHealthBo objChildFatality in infoBo)
                {

                    DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_CHILD_FATALITY_INFO");
                    Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, objChildFatality.OwnerId);
                    Db.AddInParameter(cmd, "@DeadReason", DbType.String, objChildFatality.DeadReason);
                    Db.AddInParameter(cmd, "@SexId", DbType.Int32, objChildFatality.SexId);
                    j = Db.ExecuteNonQuery(cmd) + j;

                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            if (j > 0)
                return 1;
            else
            {
                return 0;
            }

        }
        public int UpdateTeenAgeFatality(List<TeenAgeHealthBo> infoBos)
        {
            int j = 0;
            try
            {
                foreach (TeenAgeHealthBo objTeenAgeFatality in infoBos)
                {

                    DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_TEEN_AGE_FATALITY_INFO");
                    Db.AddInParameter(cmd, "@TeenageFatalityId", DbType.Int32, objTeenAgeFatality.TeenageFatalityId);
                    Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, objTeenAgeFatality.OwnerId);
                    Db.AddInParameter(cmd, "@DiseaseId", DbType.String, objTeenAgeFatality.DiseaseId);
                    Db.AddInParameter(cmd, "@SexId", DbType.Int32, objTeenAgeFatality.SexId);
                    Db.AddInParameter(cmd, "@Fatality", DbType.Int32, objTeenAgeFatality.Fatality);
                    j = Db.ExecuteNonQuery(cmd) + j;

                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            if (j > 0)
                return 1;
            else
            {
                return 0;
            }

        }
        public int UpdateChildFatality(List<TeenAgeHealthBo> infoBos)
        {
            int j = 0;
            try
            {
                foreach (TeenAgeHealthBo objChildFatality in infoBos)
                {

                    DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_FATALITY_INFO");
                   
                    Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, objChildFatality.OwnerId);
                    Db.AddInParameter(cmd, "@DeadReason", DbType.String, objChildFatality.DeadReason);
                    Db.AddInParameter(cmd, "@SexId", DbType.Int32, objChildFatality.SexId);
                    Db.AddInParameter(cmd, "@ChildFatalityId", DbType.Int32, objChildFatality.ChildFatalityId);
                    j = Db.ExecuteNonQuery(cmd) + j;

                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            if (j > 0)
                return 1;
            else
            {
                return 0;
            }

        }
        public DataTable FetchChildFatality(TeenAgeHealthBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_FATALITY", objBo.OwnerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public void DeleteOldRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_TEENAGE_HEALTH");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);


        }
    }
}
