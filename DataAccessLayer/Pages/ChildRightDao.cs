﻿using Common.Entity;
using DataAccessLayer.DbConnection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Pages
{
    public class ChildRightDao:ConnectionHelper
    {
        public int Child_Abuse_Insert(ChildRightBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_ABUSE_INSERT");

                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@IS_SEX_EXPLOITATION", DbType.String, obj.sexExp);
                Db.AddInParameter(cmd, "@IS_CHILD_ORG", DbType.String, obj.orgId);
                Db.AddInParameter(cmd, "@IS_STREET_CHILD", DbType.String, obj.streetId);
                Db.AddInParameter(cmd, "@IS_CHILD_RIGHTS", DbType.String, obj.rightsId);
                int cai = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
           



        }

        public int Child_Drug_Abuse_Insert(ChildRightBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_DRUG_ABUSE_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.Int32, obj.ownerId);
                Db.AddInParameter(cmd, "@SMOKING", DbType.Int32, obj.smoking);
                Db.AddInParameter(cmd, "@CANNABIS", DbType.Int32, obj.cannabis);
                Db.AddInParameter(cmd, "@NON_ADDICTION", DbType.Int32, obj.nonAddiction);
                Db.AddInParameter(cmd, "@SEX_ID", DbType.Int32, obj.sexId);

                int cdai = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            } 


        }

        public int Child_Sex_Desc_Insert(ChildRightBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_SEX_DESCRIMINATION_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.Int32, obj.ownerId);
                Db.AddInParameter(cmd, "@IS_SEX_DESCRIMINATION", DbType.String, obj.sexDescriminationId);
                Db.AddInParameter(cmd, "@SEX_DESCRIMINATION_TYPE", DbType.String, obj.sexDescriminationTypeId);

                int csdi = Db.ExecuteNonQuery(cmd);
                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            } 


            
           
        }

        public int Child_Women_Violence_Insert(ChildRightBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_WOMEN_VIOLENCE_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.Int32, obj.ownerId);
                Db.AddInParameter(cmd, "@VIOLENCE_TYPE_ID", DbType.Int32, obj.violenceTypeId);
                int cwvi = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            } 

        }

        public int Home_Child_Punishment_Insert(ChildRightBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_HOME_CHILD_PUNISHMENT_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.Int32, obj.ownerId);
                Db.AddInParameter(cmd, "@PUNISHMENT_TYPE_ID", DbType.Int32, obj.punishmentTypeId);

                int hcpi = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
            
            
        }

        public int Home_Shelter_Insert(ChildRightBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_HOME_SHELTER_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.Int32, obj.ownerId);
                Db.AddInParameter(cmd, "@ANNUAL_CHILD_CLOTHING ", DbType.Int32, obj.clothingId);
                Db.AddInParameter(cmd, "@DIFF_ROOM", DbType.Int32, obj.diffRoomId);
                Db.AddInParameter(cmd, "@SEPERATE_HOME_SHED", DbType.Int32, obj.homeShedId);
                int hsi = Db.ExecuteNonQuery(cmd);
                return 1;

            }
            catch(Exception ex)
            {
                return 0;
            }
           
            
        }

        public int School_Child_Punishment_Insert(ChildRightBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_SCHOOL_CHILD_PUNISHMENT_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.Int32, obj.ownerId);

                Db.AddInParameter(cmd, "@PUNISHMENT_TYPE_ID", DbType.Int32, obj.punishment);
                int scpi = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }

           
        }

        public DataSet Fetch_ChildRights(ChildRightBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_ABUSE", objBo.ownerId);           
            return ds;
        }  

        public int Update_ChildRights(ChildRightBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_ABUSE ");

                Db.AddInParameter(cmd, "@child_punishment_id", DbType.Int32, obj.hiddenId1); 
                Db.AddInParameter(cmd, "@sex_descrimination_id", DbType.Int32, obj.hiddenId2);
                Db.AddInParameter(cmd, "@school_child_punishment_id", DbType.Int32, obj.hiddenId3);
                Db.AddInParameter(cmd, "@child_women_violence_id", DbType.Int32, obj.hiddenId4);
                Db.AddInParameter(cmd, "@child_abuse_id", DbType.Int32, obj.hiddenId5);             
                Db.AddInParameter(cmd, "@child_drug_abuse_id", DbType.Int32, obj.hiddenId6);
                Db.AddInParameter(cmd, "@home_shelter_id", DbType.Int32, obj.hiddenId7);

                //UPDATE CODE for TABLE TBL_CHILD_ABUSE
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@IS_SEX_EXPLOITATION", DbType.String, obj.sexExp);
                Db.AddInParameter(cmd, "@IS_CHILD_ORG", DbType.String, obj.orgId);
                Db.AddInParameter(cmd, "@IS_STREET_CHILD", DbType.String, obj.streetId);
                Db.AddInParameter(cmd, "@IS_CHILD_RIGHTS", DbType.String, obj.rightsId);

                //Update code for TBL_CHILD_DRUG_ABUSE
                Db.AddInParameter(cmd, "@SMOKING", DbType.Int32, obj.smoking);
                Db.AddInParameter(cmd, "@CANNABIS", DbType.Int32, obj.cannabis);
                Db.AddInParameter(cmd, "@NON_ADDICTION", DbType.Int32, obj.nonAddiction);
                Db.AddInParameter(cmd, "@SEX_ID", DbType.Int32, obj.sexId);

                //update code for TBL_CHILD_SEX_DESCRIMINATION
               
                Db.AddInParameter(cmd, "@IS_SEX_DESCRIMINATION", DbType.String, obj.sexDescriminationId);
                Db.AddInParameter(cmd, "@SEX_DESCRIMINATION_TYPE", DbType.String, obj.sexDescriminationTypeId);

                //update code ofr TBL_CHILD_WOMEN_VIOLENCE
               
                
                Db.AddInParameter(cmd, "@VIOLENCE_TYPE_ID", DbType.Int32, obj.violenceTypeId);

                //update code for TBL_HOME_CHILD_PUNISHMENT               
                
                Db.AddInParameter(cmd, "@PUNISHMENT_TYPE_ID", DbType.Int32, obj.punishmentTypeId);

                //Update code for School_child_Punishment
                Db.AddInParameter(cmd, "@punishment_id", DbType.Int32, obj.punishment);

                //Update Code for TBL_HOME_SHELTER                
               
                Db.AddInParameter(cmd, "@ANNUAL_CHILD_CLOTHING ", DbType.Int32, obj.clothingId);
                Db.AddInParameter(cmd, "@DIFF_ROOM", DbType.Int32, obj.diffRoomId);
                Db.AddInParameter(cmd, "@SEPERATE_HOME_SHED", DbType.Int32, obj.homeShedId);

               
               

                int i = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }
    }
}
