﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using DataAccessLayer.DbConnection;
using System.Data;

namespace DataAccessLayer
{
    public class MarriageDao: ConnectionHelper
    {

        public int InsertMrgInfo(Common.Entity.MarriageBo objMrgInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_MARITALINFO");

            Db.AddInParameter(cmd, "ownerId", DbType.Int32, objMrgInfo.OwnerId);
            Db.AddInParameter(cmd, "@childAge", DbType.Int32, objMrgInfo.MarriageAge);
            Db.AddInParameter(cmd, "@sex", DbType.Int32, objMrgInfo.SexId);

            int retval = 0;
            retval = Db.ExecuteNonQuery(cmd);
            return retval;

        }

        public DataTable FetchMaritalInfo(Common.Entity.MarriageBo objMrgInfo)
        {
            DataTable dt = new DataTable();

            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_MARITAL_INFO");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_MARITAL_INFO", objMrgInfo.OwnerId);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            return dt;
        }

        public void UpdateMarriageInfo(Common.Entity.MarriageBo objMrgInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_MARITAL_INFO");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMrgInfo.OwnerId);
            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objMrgInfo.userId);
            Db.AddInParameter(cmd, "@ageId", DbType.Int32, objMrgInfo.MarriageAge);
            Db.AddInParameter(cmd, "@sexId", DbType.Int32, objMrgInfo.SexId);

            Db.ExecuteNonQuery(cmd);
        }
        public void DeleteOldRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_MARITAL_INFO");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);


        }
    }
 }
