﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Common.Entity;
using DataAccessLayer.DbConnection;

namespace DataAccessLayer.Pages
{
    public class InfoDao : ConnectionHelper
    {
        public DataTable PopulateMyagdiVdc()
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_POPULATE_MYAGDI_VDC");
            DataSet ds = null;
            DataTable dt = null;
            ds = Db.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable PopulateToleBasti(int vdcId, int wardId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_POPULATE_VDC_TOLEBASTI");
            Db.AddInParameter(cmd, "@vdc_id", DbType.Int32, vdcId);
            Db.AddInParameter(cmd, "@ward_id", DbType.Int32, wardId);
            DataSet ds = null;
            DataTable dt = null;

            ds = Db.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }
        public int InsertBasti(InfoBo infoBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_BASTI");
                Db.AddInParameter(cmd, "@vdc_id", DbType.Int32, infoBo.VdcId);
                Db.AddInParameter(cmd, "@ward_Id", DbType.Int32, infoBo.WardNum);
                Db.AddInParameter(cmd, "@basti_name", DbType.String, infoBo.VillageName);
                int i = Db.ExecuteNonQuery(cmd);
                return i;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public int InsertVillageInfo(InfoBo infoBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_VILLAGE_INFO");
            Db.AddInParameter(cmd, "@vdcId", DbType.Int32, infoBo.VdcId);
            Db.AddInParameter(cmd, "@bastiId", DbType.Int32, infoBo.BastiId);
            Db.AddInParameter(cmd, "@wardNum", DbType.Int32, infoBo.WardNum);
            Db.AddInParameter(cmd, "@house_no", DbType.String, infoBo.HouseNo);
            Db.AddInParameter(cmd, "@family_sn", DbType.String, infoBo.FamilySn);

            // Db.AddInParameter(cmd, "@village_name", DbType.String, infoBo.VillageName);
            return int.Parse(Db.ExecuteDataSet(cmd).Tables[0].Rows[0]["village_id"].ToString());
        }

        public DataTable FetchVillageInfo(InfoBo infoBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_VILLAGE_INFO");
            Db.AddInParameter(cmd, "@VillageId", DbType.Int32, infoBo.VillageId);
            DataSet ds = null;
            DataTable dt = null;
            ds = Db.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }
        public int UpdateVillageInfo(InfoBo infoBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_VILLAGE_INFO");
            Db.AddInParameter(cmd, "@VillageId", DbType.Int32, infoBo.VillageId);
            Db.AddInParameter(cmd, "@vdcId", DbType.Int32, infoBo.VdcId);
            Db.AddInParameter(cmd, "@wardNum", DbType.Int32, infoBo.WardNum);
            Db.AddInParameter(cmd, "@house_no", DbType.String, infoBo.HouseNo);
            Db.AddInParameter(cmd, "@family_sn", DbType.String, infoBo.FamilySn);
            Db.AddInParameter(cmd, "@bastiId", DbType.String, infoBo.BastiId);
            int i = Db.ExecuteNonQuery(cmd);
            return i;
        }
        public int UpdateBasti(InfoBo infoBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_BASTI");
            Db.AddInParameter(cmd, "@basti_id", DbType.Int32, infoBo.BastiId);
            Db.AddInParameter(cmd, "@basti_name", DbType.String, infoBo.VillageName);
            int i = Db.ExecuteNonQuery(cmd);
            return i;
        }

        public DataTable FetchOwnerIdByVDCId(InfoBo infoBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_REPORT_FETCH_OWNER_ID_BY_VDC_ID");
            Db.AddInParameter(cmd, "@vdcId", DbType.Int32, infoBo.VdcId);
            DataSet ds = null;
            DataTable dt = null;
            ds = Db.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchOwnerIdByVDCWard(InfoBo infoBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_REPORT_FETCH_OWNER_ID_BY_VDC_WARD");
            Db.AddInParameter(cmd, "@vdcId", DbType.Int32, infoBo.VdcId);
            Db.AddInParameter(cmd, "@wardNum", DbType.Int32, infoBo.WardNum);
            DataSet ds = null;
            DataTable dt = null;
            ds = Db.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchOwnerIdByVDCWardBasti(InfoBo infoBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_REPORT_FETCH_OWNER_ID_BY_VDC_WARD_BASTI");
            Db.AddInParameter(cmd, "@vdcId", DbType.Int32, infoBo.VdcId);
            Db.AddInParameter(cmd, "@wardNum", DbType.Int32, infoBo.WardNum);
            Db.AddInParameter(cmd, "@bastiId", DbType.Int32, infoBo.BastiId);
            DataSet ds = null;
            DataTable dt = null;
            ds = Db.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }
    }
}
