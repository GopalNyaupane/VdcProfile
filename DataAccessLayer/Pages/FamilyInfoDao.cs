﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Common.Entity;
using DataAccessLayer.DbConnection;

namespace DataAccessLayer.Pages
{
    public class FamilyInfoDao : ConnectionHelper
    {
        public int InsertFamilyMember(List<FamilyInfoBo> infoBo)
        {
            int j = 0;
            try
            {
                foreach (FamilyInfoBo objFamilyMember in infoBo)
                {

                    DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_FAMILYMEMBER_INFO");
                    Db.AddInParameter(cmd, "@owner_id", DbType.Int32, objFamilyMember.OwnerId);
                    Db.AddInParameter(cmd, "@name", DbType.String, objFamilyMember.Name);
                    Db.AddInParameter(cmd, "@rel_type_id", DbType.Int32, objFamilyMember.RelationTypeId);
                    Db.AddInParameter(cmd, "@age", DbType.Int32, objFamilyMember.Age);
                    Db.AddInParameter(cmd, "@sex_id", DbType.Int32, objFamilyMember.SexId);
                    j = Db.ExecuteNonQuery(cmd) + j;

                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            if (j > 0)
                return 1;
            else
            {
                return 0;
            }

        }

        public int InsertWash(WashBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_WASH");
                Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, objBo.OwnerId);
                Db.AddInParameter(cmd, "@WaterSourceId", DbType.Int32, objBo.WaterSourceId);
                Db.AddInParameter(cmd, "@WaterFetchTimeID", DbType.Int32, objBo.WaterFetchTimeId);
                Db.AddInParameter(cmd, "@ToiletStatusId", DbType.Int32, objBo.ToiletStatusId);
                Db.AddInParameter(cmd, "@HandWashId", DbType.Int32, objBo.HandWashId);
                Db.AddInParameter(cmd, "@HealthCheckupID", DbType.Int32, objBo.HealthCheckUpId);
                Db.AddInParameter(cmd, "@HealthPostDistance", DbType.String, objBo.HealthPostDistance);
                Db.AddInParameter(cmd, "@HealthPostTime", DbType.String, objBo.HealthPostTime);
                Db.AddInParameter(cmd, "@HospitalDistance", DbType.String, objBo.HospitalDistance);
                Db.AddInParameter(cmd, "@HospitalTime", DbType.String, objBo.HospitalTime);
                int sqdr = Db.ExecuteNonQuery(cmd);
               
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }



        public DataTable FetchFamilyMember(FamilyInfoBo objMemebrBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_FAMILY_MEMBER");
            Db.AddInParameter(cmd, "@owner_id", DbType.Int32, objMemebrBo.OwnerId);
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds = Db.ExecuteDataSet(cmd);
            if(ds != null && ds.Tables.Count >0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchWash(WashBo objWash)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_WASH");
            Db.AddInParameter(cmd, "@owner_id", DbType.Int32, objWash.OwnerId);
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds = Db.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

       

       


        public void DeleteOldRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_MEMBER_RECORD");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);
            
            int sqdr = Db.ExecuteNonQuery(cmd);

            
        }
    }
}
