﻿using Common.Entity;
using DataAccessLayer.DbConnection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Pages
{
   public  class TrainingOccupationDao:ConnectionHelper
    {

        ///DAL for inserting FAMILY_AWARNESS_INSERT TABLE
       public int Family_Awareness_Insert(List<TrainingOccupationBo> obj1)
       {
           
           int j = 0;
           try
           {
               
               foreach (TrainingOccupationBo objAwarness in obj1)
               {
                   DbCommand cmd = Db.GetStoredProcCommand("USP_FAMILY_AWARENESS_INSERT");
                   Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, objAwarness.ownerId);
                   Db.AddInParameter(cmd, "@TRAINING_NAME", DbType.String, objAwarness.trainingName);
                   Db.AddInParameter(cmd, "@MALE_NO", DbType.String, objAwarness.maleNo);
                   Db.AddInParameter(cmd, "@FEMALE_NO", DbType.String, objAwarness.femaleNo);
                   Db.AddInParameter(cmd, "@DURATION_ID", DbType.String, objAwarness.durationId);
                   int i = Db.ExecuteNonQuery(cmd);
                   j += 1;
               }

           }
           catch (Exception ex)
           {
               return 0;
           }
           if (j > 0)
           {
               return 1;
           }
           else
           {
               return 0;
           }
             
       }

       //DAL for inserting FAMILY_ECONOMIC_INSERT TABLE
       public int Family_Economic_Insert(List<TrainingOccupationBo> obj2)
       {

           int k = 0;
           try
           {
              
               foreach (TrainingOccupationBo objTraining in obj2)
               {
                   DbCommand cmd = Db.GetStoredProcCommand("USP_FAMILY_ECONOMIC_INSERT");
                   Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, objTraining.ownerId);
                   Db.AddInParameter(cmd, "@TRAINING_NAME", DbType.String, objTraining.ecoTrainingName);
                   Db.AddInParameter(cmd, "@MALE_NO", DbType.String, objTraining.ecoMaleNo);
                   Db.AddInParameter(cmd, "@FEMALE_NO", DbType.String, objTraining.ecoFemaleNo);
                   Db.AddInParameter(cmd, "@DURATION_ID", DbType.String, objTraining.ecoDurationId);
                   int i= Db.ExecuteNonQuery(cmd) ;
                   k += 1;
               }

           }
           catch (Exception ex)
           {
               return 0;
           }
           if (k > 0)
           {
               return 1;
           }
           else
           {
               return 0;
           }

       }

       //DAL for inserting Other Simple tables
       public int Occupation_Insert(TrainingOccupationBo obj)
       {
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_TRAINING_OCCUPATION_INSERT");
               Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
               Db.AddInParameter(cmd, "@OCCUPATION_TYPE_ID", DbType.String, obj.occupationTypeId);


               Db.AddInParameter(cmd, "@IS_MIGRATION", DbType.String, obj.isMigration);

               Db.AddInParameter(cmd, "@MIGRATION_ID", DbType.String, obj.migrationId);
               Db.AddInParameter(cmd, "@REASON_TYPE_ID", DbType.String, obj.reasonTypeId);

               Db.AddInParameter(cmd, "@IS_RADIO", DbType.String, obj.isRadio);
               Db.AddInParameter(cmd, "@IS_TV", DbType.String, obj.isTV);
               Db.AddInParameter(cmd, "@IS_TELEPHONE", DbType.String, obj.isTelePhone);
               Db.AddInParameter(cmd, "@IS_MOBILE", DbType.String, obj.isMobile);
               Db.AddInParameter(cmd, "@IS_INTERNET", DbType.String, obj.isInternet);
               Db.AddInParameter(cmd, "@IS_NEWSPAPER", DbType.String, obj.isNews);
               Db.AddInParameter(cmd, "@OTHERS", DbType.String, obj.others);

               Db.AddInParameter(cmd, "@ELECTRICITY_SOURCE_ID", DbType.String, obj.electricitySourceId);
               Db.AddInParameter(cmd, "@FUEL_SOURCE_ID", DbType.String, obj.fuelSourceId);
               Db.AddInParameter(cmd, "@STOVE_TYPE_ID", DbType.String, obj.stoveTypeId);
               Db.AddInParameter(cmd, "@ROOF_MATERIAL_ID", DbType.String, obj.roofMaterialId);
               Db.AddInParameter(cmd, "@homeTypeId", DbType.String, obj.homeType);
               Db.AddInParameter(cmd, "@firewood_id", DbType.String, obj.firewoodId);

               int i = Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch(Exception ex)
           {
               return 0;
           }
          
           


       }

        //DAL for Fetching Fetch_FAMILY_AWARNESS TABLE
       public DataTable Fetch_Family_Awareness(TrainingOccupationBo objBo)
       {
           DataSet ds = Db.ExecuteDataSet("USP_FETCH_FAMILY_AWARENESS", objBo.ownerId);
           DataTable dt = null;
           if (ds != null && ds.Tables.Count > 0)
           {
               dt = ds.Tables[0];
           }
           return dt;

       }
       public DataTable Fetch_Family_Economic(TrainingOccupationBo objBo)
       {
           DataSet ds = Db.ExecuteDataSet("USP_FETCH_FAMILY_ECONOMIC", objBo.ownerId);
           DataTable dt = null;
           if (ds != null && ds.Tables.Count > 0)
           {
               dt = ds.Tables[0];
           }
           return dt;

       }
       public DataSet Fetch_Occupation(TrainingOccupationBo objBo)
       {
           DataSet ds = Db.ExecuteDataSet("USP_FETCH_TRAINING_OCCUPATION", objBo.ownerId);
           return ds;

       }

       public int Update_Family_Awareness(TrainingOccupationBo objBo)
       {

           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_FAMILY_AWARENESS");

               Db.AddInParameter(cmd, "@family_awareness_training_id", DbType.Int32, objBo.hidAwareness);
               Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, objBo.ownerId);
               Db.AddInParameter(cmd, "@training_name", DbType.String, objBo.trainingName);
               Db.AddInParameter(cmd, "@male_no", DbType.String, objBo.maleNo);
               Db.AddInParameter(cmd, "@female_no", DbType.String, objBo.femaleNo);
               Db.AddInParameter(cmd, "@duration_id", DbType.String, objBo.durationId);
              

               Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }
       public int Update_Family_Economic(TrainingOccupationBo objBo)
       {

           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_FAMILY_ECONOMIC");

               Db.AddInParameter(cmd, "@family_economic_training_id", DbType.Int32, objBo.hidEconomic);
               Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, objBo.ownerId);
               Db.AddInParameter(cmd, "@training_name", DbType.String, objBo.ecoTrainingName);
               Db.AddInParameter(cmd, "@male_no", DbType.String, objBo.ecoMaleNo);
               Db.AddInParameter(cmd, "@female_no", DbType.String, objBo.ecoFemaleNo);
               Db.AddInParameter(cmd, "@duration_id", DbType.String, objBo.ecoDurationId);


               Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }
       public int Update_Occupation(TrainingOccupationBo objBo)
       {

           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_TRAINING_OCCUPATION");
               Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, objBo.ownerId);
               Db.AddInParameter(cmd, "@family_main_occupation_id", DbType.Int32, objBo.hidOccupation);
               Db.AddInParameter(cmd, "@family_migration_id", DbType.Int32, objBo.hidMigration);
               Db.AddInParameter(cmd, "@migration_reason_id", DbType.Int32, objBo.hidReason );
               Db.AddInParameter(cmd, "@house_status_id", DbType.Int32, objBo.hidStatus );
               Db.AddInParameter(cmd, "@communication_id", DbType.Int32, objBo.hidComm);

               Db.AddInParameter(cmd, "@OCCUPATION_TYPE_ID", DbType.String, objBo.occupationTypeId);


               Db.AddInParameter(cmd, "@IS_MIGRATION", DbType.String, objBo.isMigration);

               Db.AddInParameter(cmd, "@MIGRATION_ID", DbType.String, objBo.migrationId);
               Db.AddInParameter(cmd, "@REASON_TYPE_ID", DbType.String, objBo.reasonTypeId);

               Db.AddInParameter(cmd, "@IS_RADIO", DbType.String, objBo.isRadio);
               Db.AddInParameter(cmd, "@IS_TV", DbType.String, objBo.isTV);
               Db.AddInParameter(cmd, "@IS_TELEPHONE", DbType.String, objBo.isTelePhone);
               Db.AddInParameter(cmd, "@IS_MOBILE", DbType.String, objBo.isMobile);
               Db.AddInParameter(cmd, "@IS_INTERNET", DbType.String, objBo.isInternet);
               Db.AddInParameter(cmd, "@IS_NEWSPAPER", DbType.String, objBo.isNews);
               Db.AddInParameter(cmd, "@OTHERS", DbType.String, objBo.others);

               Db.AddInParameter(cmd, "@ELECTRICITY_SOURCE_ID", DbType.String, objBo.electricitySourceId);
               Db.AddInParameter(cmd, "@FUEL_SOURCE_ID", DbType.String, objBo.fuelSourceId);
               Db.AddInParameter(cmd, "@STOVE_TYPE_ID", DbType.String, objBo.stoveTypeId);
               Db.AddInParameter(cmd, "@ROOF_MATERIAL_ID", DbType.String, objBo.roofMaterialId);
               Db.AddInParameter(cmd, "@homeTypeId", DbType.String, objBo.homeType);
               Db.AddInParameter(cmd, "@firewood_id", DbType.String, objBo.firewoodId);


               Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }
       public void DeleteOldRecord(int ownerId)
       {
           DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_FAMILY");
           Db.AddInParameter(cmd, "@Owner_Id", DbType.Int32, ownerId);

           int sqdr = Db.ExecuteNonQuery(cmd);


       }
    }
}
