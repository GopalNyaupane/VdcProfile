﻿using Common.Entity;
using DataAccessLayer.DbConnection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Pages
{
    public class DisasterReportDao:ConnectionHelper
    {
        public int Disaster_Insert(DisasterReportBo obj )
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_DISASTER_INSERT");

                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@FLOOD", DbType.String, obj.flood);
                Db.AddInParameter(cmd, "@LANDSLIDE", DbType.String, obj.landslide);
                Db.AddInParameter(cmd, "@HAILSTORM", DbType.String, obj.hailStorm);
                Db.AddInParameter(cmd, "@STORM", DbType.String, obj.storm);
                Db.AddInParameter(cmd, "@FIRE", DbType.String, obj.fire);
                Db.AddInParameter(cmd, "@EARTHQUAKE", DbType.String, obj.earthquake);
                Db.AddInParameter(cmd, "@DROUGHT", DbType.String, obj.drought);
                Db.AddInParameter(cmd, "@ATIBRISTI", DbType.String, obj.artibristi);
                Db.AddInParameter(cmd, "@INFLUENZA", DbType.String, obj.influenza);
                Db.AddInParameter(cmd, "@INSECTICIDE", DbType.String, obj.insecticide);
                Db.AddInParameter(cmd, "@OTHER", DbType.String, obj.other);

                Db.AddInParameter(cmd, "@FLOOD_LOSS", DbType.String, obj.floodLoss);
                Db.AddInParameter(cmd, "@LANDSLIDE_LOSS", DbType.String, obj.landslideLoss);
                Db.AddInParameter(cmd, "@HAILSTORM_LOSS", DbType.String, obj.hailStormLoss);
                Db.AddInParameter(cmd, "@STORM_LOSS", DbType.String, obj.stormLoss);
                Db.AddInParameter(cmd, "@FIRE_LOSS", DbType.String, obj.fireLoss);
                Db.AddInParameter(cmd, "@EARTHQUAKE_LOSS", DbType.String, obj.earthquakeLoss);
                Db.AddInParameter(cmd, "@DROUGHT_LOSS", DbType.String, obj.droughtLoss);
                Db.AddInParameter(cmd, "@ATIBRISTI_LOSS", DbType.String, obj.artibristiLoss);
                Db.AddInParameter(cmd, "@INFLUENZA_LOSS", DbType.String, obj.influenzaLoss);
                Db.AddInParameter(cmd, "@INSECTICIDE_LOSS", DbType.String, obj.insecticideLoss);
                Db.AddInParameter(cmd, "@OTHER_LOSS", DbType.String, obj.otherLoss);

                Db.AddInParameter(cmd, "@INFORMATION_PROVIDER", DbType.String, obj.provider);
                Db.AddInParameter(cmd, "@INFORMATION_COLLECTOR", DbType.String, obj.collector);
                Db.AddInParameter(cmd, "@INFORMATION_VALIDATER", DbType.String, obj.validater);
                Db.AddInParameter(cmd, "@DATA_TAKEN", DbType.Date, obj.dataTaken);

                int i = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public DataSet Fetch_Disaster(DisasterReportBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_DISASTER", objBo.ownerId);
            return ds;
        }

        public int Update_Disaster(DisasterReportBo obj)
        {
            try
            {
                //For Inserting for remainig Table 
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_DISASTER");

                Db.AddInParameter(cmd, "@disaster_affect_id", DbType.String, obj.hidAffect);
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@FLOOD", DbType.String, obj.flood);
                Db.AddInParameter(cmd, "@LANDSLIDE", DbType.String, obj.landslide);
                Db.AddInParameter(cmd, "@HAILSTORM", DbType.String, obj.hailStorm);
                Db.AddInParameter(cmd, "@STORM", DbType.String, obj.storm);
                Db.AddInParameter(cmd, "@FIRE", DbType.String, obj.fire);
                Db.AddInParameter(cmd, "@EARTHQUAKE", DbType.String, obj.earthquake);
                Db.AddInParameter(cmd, "@DROUGHT", DbType.String, obj.drought);
                Db.AddInParameter(cmd, "@ATIBRISTI", DbType.String, obj.artibristi);
                Db.AddInParameter(cmd, "@INFLUENZA", DbType.String, obj.influenza);
                Db.AddInParameter(cmd, "@INSECTICIDE", DbType.String, obj.insecticide);
                Db.AddInParameter(cmd, "@OTHER", DbType.String, obj.other);

                Db.AddInParameter(cmd, "@disaster_loss_id", DbType.String, obj.hidLoss);
                Db.AddInParameter(cmd, "@FLOOD_LOSS", DbType.String, obj.floodLoss);
                Db.AddInParameter(cmd, "@LANDSLIDE_LOSS", DbType.String, obj.landslideLoss);
                Db.AddInParameter(cmd, "@HAILSTORM_LOSS", DbType.String, obj.hailStormLoss);
                Db.AddInParameter(cmd, "@STORM_LOSS", DbType.String, obj.stormLoss);
                Db.AddInParameter(cmd, "@FIRE_LOSS", DbType.String, obj.fireLoss);
                Db.AddInParameter(cmd, "@EARTHQUAKE_LOSS", DbType.String, obj.earthquakeLoss);
                Db.AddInParameter(cmd, "@DROUGHT_LOSS", DbType.String, obj.droughtLoss);
                Db.AddInParameter(cmd, "@ATIBRISTI_LOSS", DbType.String, obj.artibristiLoss);
                Db.AddInParameter(cmd, "@INFLUENZA_LOSS", DbType.String, obj.influenzaLoss);
                Db.AddInParameter(cmd, "@INSECTICIDE_LOSS", DbType.String, obj.insecticideLoss);
                Db.AddInParameter(cmd, "@OTHER_LOSS", DbType.String, obj.otherLoss);

                Db.AddInParameter(cmd, "@official_id", DbType.String, obj.hidIO);
                Db.AddInParameter(cmd, "@INFORMATION_PROVIDER", DbType.String, obj.provider);
                Db.AddInParameter(cmd, "@INFORMATION_COLLECTOR", DbType.String, obj.collector);
                Db.AddInParameter(cmd, "@INFORMATION_VALIDATER", DbType.String, obj.validater);
                Db.AddInParameter(cmd, "@DATA_TAKEN", DbType.Date, obj.dataTaken);

                int i = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
