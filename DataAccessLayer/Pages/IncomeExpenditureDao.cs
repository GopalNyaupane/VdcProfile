﻿using Common.Entity;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using DataAccessLayer.DbConnection;
using System.Data;

namespace DataAccessLayer
{
    public class IncomeExpenditureDao:ConnectionHelper
    {
        public int SaveAgroIncome(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_AGRO_INCOME");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@IncomeCoverTimeId", DbType.String, objBo.IncomeCoverTimeId);
                
    
                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public int UpdateAgroIncome(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_AGRO_INCOME");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@IncomeCoverTimeId", DbType.String, objBo.IncomeCoverTimeId);
                Db.AddInParameter(cmd, "@AgroIncomeId", DbType.String, objBo.AgroIncomeId);

                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public DataTable FetchAgroIncome(IncomeExpenditureBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_AGRO_INCOME", objBo.OwnerId);
            DataTable dt = null;
            if(ds!=null && ds.Tables.Count>0)
            {
                dt=ds.Tables[0];
            }
            return dt;
        }


        public int SaveAnnualIncome(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_ANNUAL_INCOME");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@Agriculture", DbType.String, objBo.Agriculture);
                Db.AddInParameter(cmd, "@Business", DbType.String, objBo.Business);
                Db.AddInParameter(cmd, "@Jobs", DbType.String, objBo.Jobs);
                Db.AddInParameter(cmd, "@ForeignEmployment", DbType.String, objBo.ForeignEmployment);
                Db.AddInParameter(cmd, "@WageLabour", DbType.String, objBo.WageLabour);
                Db.AddInParameter(cmd, "@HouseRent", DbType.String, objBo.HouseRent);
                Db.AddInParameter(cmd, "@OthersIncome", DbType.String, objBo.OthersIncome);



                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public int UpdateAnnualIncome(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_ANNUAL_INCOME");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@Agriculture", DbType.String, objBo.Agriculture);
                Db.AddInParameter(cmd, "@Business", DbType.String, objBo.Business);
                Db.AddInParameter(cmd, "@Jobs", DbType.String, objBo.Jobs);
                Db.AddInParameter(cmd, "@ForeignEmployment", DbType.String, objBo.ForeignEmployment);
                Db.AddInParameter(cmd, "@WageLabour", DbType.String, objBo.WageLabour);
                Db.AddInParameter(cmd, "@HouseRent", DbType.String, objBo.HouseRent);
                Db.AddInParameter(cmd, "@OthersIncome", DbType.String, objBo.OthersIncome);
                Db.AddInParameter(cmd, "@IncomeSourceId", DbType.String, objBo.IncomeSourceId);




                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public DataTable FetchIncomeSource(IncomeExpenditureBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_ANNUAL_INCOME", objBo.OwnerId);
                DataTable dt=null;
            if(ds!=null && ds.Tables.Count>0)
            {
                dt=ds.Tables[0];
                

            }
            return dt;

        }

      
        public int SaveAnnualExpenditure(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_ANNUAL_EXPENDITURE");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@Fooding", DbType.String, objBo.Fooding);
                Db.AddInParameter(cmd, "@Clothing", DbType.String, objBo.Clothing);
                Db.AddInParameter(cmd, "@Education", DbType.String, objBo.Education);
                Db.AddInParameter(cmd, "@Health", DbType.String, objBo.Health);
                Db.AddInParameter(cmd, "@Festivity", DbType.String, objBo.Festivity);
                Db.AddInParameter(cmd, "@PaymentCharge", DbType.String, objBo.PaymentCharge);
                Db.AddInParameter(cmd, "@OthersExpenditure", DbType.String, objBo.OthersExpenditure);



                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int UpdateAnnualExpenditure(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_ANNUAL_EXPENDITURE");
                Db.AddInParameter(cmd, "@AnnualExpenditureId", DbType.String, objBo.AnnualExpenditureId);
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@Fooding", DbType.String, objBo.Fooding);
                Db.AddInParameter(cmd, "@Clothing", DbType.String, objBo.Clothing);
                Db.AddInParameter(cmd, "@Education", DbType.String, objBo.Education);
                Db.AddInParameter(cmd, "@Health", DbType.String, objBo.Health);
                Db.AddInParameter(cmd, "@Festivity", DbType.String, objBo.Festivity);
                Db.AddInParameter(cmd, "@PaymentCharge", DbType.String, objBo.PaymentCharge);
                Db.AddInParameter(cmd, "@OthersExpenditure", DbType.String, objBo.OthersExpenditure);



                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public DataTable FetchAnnualExpenditure(IncomeExpenditureBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_ANNUAL_EXPENDITURE", objBo.OwnerId);
            DataTable dt = null;
            if(ds!=null && ds.Tables.Count>0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }




        public int SaveIsLoanInfo(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_ISLOAN");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@IsLoan", DbType.String, objBo.IsLoan);
               


                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int UpdateIsLoanInfo(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_ISLOAN");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@IsLoan", DbType.String, objBo.IsLoan);
                Db.AddInParameter(cmd, "@LoanId", DbType.String, objBo.LoanId);


                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public DataTable FetchIsLoan(IncomeExpenditureBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_ISLOAN", objBo.OwnerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];


            }
            return dt;

        }



        public int SaveLoanDonor(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_LOAN_DONER");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@LoanDonorTypeId", DbType.String, objBo.LoanDonorTypeId);
                Db.AddInParameter(cmd, "@LoanPurposeTypeId", DbType.String, objBo.LoanPurposeTypeId);




                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int UpdateLoanDonor(IncomeExpenditureBo objBo)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_LOAN_DONER");
                Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
                Db.AddInParameter(cmd, "@LoanDonorTypeId", DbType.String, objBo.LoanDonorTypeId);
                Db.AddInParameter(cmd, "@LoanPurposeTypeId", DbType.String, objBo.LoanPurposeTypeId);
                Db.AddInParameter(cmd, "@LoanDetailId", DbType.String, objBo.LoanDetailsId);



                int sqdr = Db.ExecuteNonQuery(cmd);
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }



        public DataTable FetchLoanPurpose(IncomeExpenditureBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_LOAN_PURPOSE", objBo.OwnerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];


            }
            return dt;

        }

        
    }
}
