﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.DbConnection;
using System.Data;
using System.Data.Common;

namespace DataAccessLayer.Pages
{
    public class ListApplicantsDAO:ConnectionHelper
    {
        public DataTable FetchData()
        {
            DbCommand cmd = Db.GetStoredProcCommand("usp_Fetch_Applicants");
            DataSet ds = null;
            DataTable dt = null;
            ds = Db.ExecuteDataSet(cmd);
            if(ds!=null && ds.Tables.Count>0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public void DeleteData(Int32 ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_LIST_DATA");          
            Db.AddInParameter(cmd, "@ownerid", DbType.Int32, ownerId);           
            Db.ExecuteNonQuery(cmd);
        }
       
    }
}
