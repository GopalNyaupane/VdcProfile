﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.Common;
using Common.Entity;
using DataAccessLayer.DbConnection;

namespace DataAccessLayer
{
    public class MaternalInfoDao :ConnectionHelper
    {


        public void InsertMaternalHiv(Common.Entity.MaternalInfoBo objMtrlInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_MATERNAL_HIVINFO");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMtrlInfo.OwnerID);
            Db.AddInParameter(cmd, "@hivInfecction", DbType.Int32, objMtrlInfo.HivInfection);
            Db.AddInParameter(cmd, "@arv", DbType.Int32, objMtrlInfo.Arv);

            Db.ExecuteNonQuery(cmd);
        }

        public int InsertMaternalInfo(Common.Entity.MaternalInfoBo objMtrlInfo)
        {
              DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_MATERNALINFO");

              Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMtrlInfo.OwnerID);
              Db.AddInParameter(cmd, "@prgnentTest", DbType.Int32, objMtrlInfo.PregTest);
              Db.AddInParameter(cmd, "@ironTab", DbType.Int32, objMtrlInfo.IronTablet);
              Db.AddInParameter(cmd, "@ttVaccine", DbType.Int32, objMtrlInfo.TtVaccine);
              Db.AddInParameter(cmd, "@maternity", DbType.Int32, objMtrlInfo.Maternity);
              Db.AddInParameter(cmd, "@maternalAddress", DbType.Int32, objMtrlInfo.MaternalAddressId);
              Db.AddInParameter(cmd, "@infantHealth", DbType.Int32, objMtrlInfo.InfantHlthChk);

              int returnval = 0;
              returnval = Db.ExecuteNonQuery(cmd);
              return returnval;

        }

        public DataTable FetchMaterialInfo(MaternalInfoBo objMtrlInfo)
        {
            DataTable dt = new DataTable();

            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_MATERIALINFO");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_MATERIALINFO", objMtrlInfo.OwnerID);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            return dt;

        }

        public DataTable FetchChildHivInfo(MaternalInfoBo objMtrlInfo)
        {
            DataTable dt = new DataTable();

            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_MATERNAL_CHILD_HIV_INFECTION");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_MATERNAL_CHILD_HIV_INFECTION", objMtrlInfo.OwnerID);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            return dt;
        }

        public void UpdateMaternalInfo(MaternalInfoBo objMtrlInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_MATERNALINFO");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMtrlInfo.OwnerID);
            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objMtrlInfo.MainID);
            Db.AddInParameter(cmd, "@prgnentTest", DbType.Int32, objMtrlInfo.PregTest);
            Db.AddInParameter(cmd, "@ironTab", DbType.Int32, objMtrlInfo.IronTablet);
            Db.AddInParameter(cmd, "@ttVaccine", DbType.Int32, objMtrlInfo.TtVaccine);
            Db.AddInParameter(cmd, "@maternity", DbType.Int32, objMtrlInfo.Maternity);
            Db.AddInParameter(cmd, "@maternalAddress", DbType.Int32, objMtrlInfo.MaternalAddressId);
            Db.AddInParameter(cmd, "@infantHealth", DbType.Int32, objMtrlInfo.InfantHlthChk);


            Db.ExecuteNonQuery(cmd);

        }

        public void UpdateMaternalHiv(MaternalInfoBo objMtrlInfo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_MATERNAL_HIVINFO");
            Db.AddInParameter(cmd, "@hivInfoId", DbType.Int32, objMtrlInfo.hiddenId);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMtrlInfo.OwnerID);
            Db.AddInParameter(cmd, "@hivInfecction", DbType.Int32, objMtrlInfo.HivInfection);
            Db.AddInParameter(cmd, "@arv", DbType.Int32, objMtrlInfo.Arv);

            Db.ExecuteNonQuery(cmd);
        }

        public void DeleteOldRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_MATERIALINFO");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);


        }
    }
}
