﻿using Common.Entity;
using DataAccessLayer.DbConnection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Pages
{
   public  class ChildNutritionDao : ConnectionHelper
    {
        public int Birth_Certificate_Insert(ChildNutritionBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_BIRTH_CERTIFICATE_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@AGE_GROUP_ID", DbType.Int32, obj.ageGroup);
                Db.AddInParameter(cmd, "@BIRTH_CERTIFICATE", DbType.Int32, obj.birthCertificate);
                Db.AddInParameter(cmd, "@SEX_ID", DbType.String, obj.sex);
                Db.AddInParameter(cmd, "@NO_BIRTH_CERTIFICATE", DbType.Int32, obj.noBirthCertificate);

                int bci = Db.ExecuteNonQuery(cmd);


                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public int Child_Nutrition_Insert(ChildNutritionBo obj)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_NUTRITION_INSERT");
                Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
                Db.AddInParameter(cmd, "@IO_SALT", DbType.String, obj.salt);
                Db.AddInParameter(cmd, "@NUTRITION_FOOD", DbType.String, obj.nutritionFood);
                Db.AddInParameter(cmd, "@CHILD_WEIGHT", DbType.String, obj.childWeightInfo);
                int cni = Db.ExecuteNonQuery(cmd);
               

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Child_Weight_Insert(List<ChildNutritionBo> obj)
        {
            int j = 0;
            try
            {
                foreach(ChildNutritionBo objChildMember in obj)
                {
                    DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_WEIGHT_INSERT");
                    Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, objChildMember.ownerId);
                    Db.AddInParameter(cmd, "@CHILD_AGE", DbType.String, objChildMember.age);
                    Db.AddInParameter(cmd, "@CHILD_WEIGHT", DbType.String, objChildMember.weight);
                    Db.AddInParameter(cmd, "@MALNUTRITION_ID", DbType.String, objChildMember.malNutrition);
                     j = Db.ExecuteNonQuery(cmd) +1;
                }
              
            }
            catch (Exception ex)
            {
                return 0;
            }
            if(j>0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

       public DataTable Fetch_Child_Weight(ChildNutritionBo objBo)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_WEIGHT", objBo.ownerId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
       }
     

       public DataSet Fetch_Child_Birth_Nutrition(ChildNutritionBo objBo)
       {
           DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_BIRTH_NUTRITION", objBo.ownerId);
           return ds;
       }

       public int Update_Child_Weight(ChildNutritionBo obj)
       {
           
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_WEIGHT");
              
               Db.AddInParameter(cmd, "@child_weight_id", DbType.Int32, obj.hidden1);
               Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
               Db.AddInParameter(cmd, "@CHILD_AGE", DbType.String, obj.age);
               Db.AddInParameter(cmd, "@CHILD_WEIGHT", DbType.String, obj.weight);
               Db.AddInParameter(cmd, "@MALNUTRITION_ID", DbType.String, obj.malNutrition);
               int i = Db.ExecuteNonQuery(cmd)+1;
              
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }

       public int Update_Child_Birth_Nutrition(ChildNutritionBo obj)
       {
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_BIRTH_NUTRITION");
               //For Table TBL_CHILD_NURITION
               Db.AddInParameter(cmd, "@child_nutrition_id", DbType.Int32, obj.hidden2);
               Db.AddInParameter(cmd, "@OWNER_ID", DbType.String, obj.ownerId);
               Db.AddInParameter(cmd, "@IO_SALT", DbType.String, obj.salt);
               Db.AddInParameter(cmd, "@NUTRITION_FOOD", DbType.String, obj.nutritionFood);
               Db.AddInParameter(cmd, "@CHILD_WEIGHT", DbType.String, obj.childWeightInfo);
               //For Table TBL_CHILD_BITRTH_CERTIFICATE
               Db.AddInParameter(cmd, "@birth_certificate_id", DbType.Int32, obj.hidden3);
               Db.AddInParameter(cmd, "@AGE_GROUP_ID", DbType.Int32, obj.ageGroup);
               Db.AddInParameter(cmd, "@BIRTH_CERTIFICATE", DbType.Int32, obj.birthCertificate);
               Db.AddInParameter(cmd, "@SEX_ID", DbType.String, obj.sex);
               Db.AddInParameter(cmd, "@NO_BIRTH_CERTIFICATE", DbType.Int32, obj.noBirthCertificate);


               int cnu = Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }

       public void DeleteOldRecord(int ownerId)
       {
           DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_CHILD_WEIGHT");
           Db.AddInParameter(cmd, "@owner_id", DbType.Int32, ownerId);

           int sqdr = Db.ExecuteNonQuery(cmd);


       }


    }
}
