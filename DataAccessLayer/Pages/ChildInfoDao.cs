﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.DbConnection;
using Common.Entity;
using System.Data;
using System.Data.Common;

namespace DataAccessLayer
{
   public class ChildInfoDao: ConnectionHelper
    {
       public int SaveChildInfo(ChildInfoBo objBo)
       {
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_HEALTH_INFO");
               Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
               Db.AddInParameter(cmd, "@ImmediateChildCure", DbType.String, objBo.ImmediateChildCure);
               Db.AddInParameter(cmd, "@ChildDiseaseVaccine", DbType.String, objBo.ChildDiseaseVaccine);
               int sqdr = Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }


       public int UpdateChildInfo(ChildInfoBo objBo)
       {
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_HEALTH_INFO");
               Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
               Db.AddInParameter(cmd, "@ImmediateChildCure", DbType.String, objBo.ImmediateChildCure);
               Db.AddInParameter(cmd, "@ChildDiseaseVaccine", DbType.String, objBo.ChildDiseaseVaccine);
               Db.AddInParameter(cmd, "@ChildHealthInfo", DbType.String, objBo.ChildHealthInfo);
               int sqdr = Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }


       public DataTable FetchChildInfo(ChildInfoBo objBo)
       {
           DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_INFO", objBo.OwnerId);
           DataTable dt = null;
           if (ds != null && ds.Tables.Count > 0)
           {
               dt = ds.Tables[0];
           }
           return dt;
       }
       public int SaveChildBreastFeedInfo(ChildInfoBo objBo)
       {
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_BREAST_FEED_INFO");
               Db.AddInParameter(cmd, "@SexId", DbType.String, objBo.SexId);
               Db.AddInParameter(cmd, "@AliveBornChild", DbType.String, objBo.AliveBornChild);
               Db.AddInParameter(cmd, "@NotBreastFeedBornTime", DbType.String, objBo.NotBreastFeedBornTime);
               Db.AddInParameter(cmd, "@BreastFeedBornTime", DbType.String, objBo.BreastFeedBornTime);
               Db.AddInParameter(cmd, "@BreastFeedOnlySixMonth", DbType.String, objBo.BreastFeedOnlySixMonth);
               Db.AddInParameter(cmd, "@BreastFeedPlusOther", DbType.String, objBo.BreastFeedPlusOther);
               Db.AddInParameter(cmd, "@BreastFeedLessTwoMonth", DbType.String, objBo.BreastFeedLessTwoMonth);
               Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
    
               int sqdr = Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }


       public int UpdateChildBreastFeedInfo(ChildInfoBo objBo)
       {
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_BREAST_FEED_INFO");
               Db.AddInParameter(cmd, "@SexId", DbType.String, objBo.SexId);
               Db.AddInParameter(cmd, "@AliveBornChild", DbType.String, objBo.AliveBornChild);
               Db.AddInParameter(cmd, "@NotBreastFeedBornTime", DbType.String, objBo.NotBreastFeedBornTime);
               Db.AddInParameter(cmd, "@BreastFeedBornTime", DbType.String, objBo.BreastFeedBornTime);
               Db.AddInParameter(cmd, "@BreastFeedOnlySixMonth", DbType.String, objBo.BreastFeedOnlySixMonth);
               Db.AddInParameter(cmd, "@BreastFeedPlusOther", DbType.String, objBo.BreastFeedPlusOther);
               Db.AddInParameter(cmd, "@BreastFeedLessTwoMonth", DbType.String, objBo.BreastFeedLessTwoMonth);
               Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
               Db.AddInParameter(cmd, "@ChildRecordId", DbType.String, objBo.ChildRecordId);
               

               int sqdr = Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }

       public DataTable FetchChildBreasetFeedInfo(ChildInfoBo objBo)
       {
           DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_BREAST_FEED_INFO", objBo.OwnerId);
           DataTable dt = null;
           if (ds != null && ds.Tables.Count > 0)
           {
               dt = ds.Tables[0];
           }
           return dt;
       }
       public int SaveChildVaccineInfo(ChildInfoBo objBo)
       {
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_CHILD_VACCINE");
               Db.AddInParameter(cmd, "@Vitamina", DbType.String, objBo.Vitamina);
               Db.AddInParameter(cmd, "@WormDrugs", DbType.String, objBo.WormDrugs);
               Db.AddInParameter(cmd, "@PolioVaccine", DbType.String, objBo.PolioVaccine);
               Db.AddInParameter(cmd, "@SexId", DbType.String, objBo.SexId);
               Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
               Db.AddInParameter(cmd, "@Bcg", DbType.String, objBo.Bcg);
               Db.AddInParameter(cmd, "@Dpt1", DbType.String, objBo.Dpt1);
               Db.AddInParameter(cmd, "@Dpt2", DbType.String, objBo.Dpt2);
               Db.AddInParameter(cmd, "@Dpt3", DbType.String, objBo.Dpt3);
               Db.AddInParameter(cmd, "@Dadhura", DbType.String, objBo.Dadhura);
               Db.AddInParameter(cmd, "@Hb", DbType.String, objBo.Hb);

               int sqdr = Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }

       public int UpdateChildVaccineInfo(ChildInfoBo objBo)
       {
           try
           {
               DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CHILD_VACCINE");
               Db.AddInParameter(cmd, "@Vitamina", DbType.String, objBo.Vitamina);
               Db.AddInParameter(cmd, "@WormDrugs", DbType.String, objBo.WormDrugs);
               Db.AddInParameter(cmd, "@PolioVaccine", DbType.String, objBo.PolioVaccine);
               Db.AddInParameter(cmd, "@SexId", DbType.String, objBo.SexId);
               Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
               Db.AddInParameter(cmd, "@Bcg", DbType.String, objBo.Bcg);
               Db.AddInParameter(cmd, "@Dpt1", DbType.String, objBo.Dpt1);
               Db.AddInParameter(cmd, "@Dpt2", DbType.String, objBo.Dpt2);
               Db.AddInParameter(cmd, "@Dpt3", DbType.String, objBo.Dpt3);
               Db.AddInParameter(cmd, "@Dadhura", DbType.String, objBo.Dadhura);
               Db.AddInParameter(cmd, "@Hb", DbType.String, objBo.Hb);
               Db.AddInParameter(cmd, "@ChildVaccineId", DbType.String, objBo.ChildVaccineId);
               int sqdr = Db.ExecuteNonQuery(cmd);
               return 1;
           }
           catch (Exception ex)
           {
               return 0;
           }
       }
       public DataTable FetchChildVaccineInfo(ChildInfoBo objBo)
       {
           DataSet ds = Db.ExecuteDataSet("USP_FETCH_CHILD_VACCINE", objBo.OwnerId);
           DataTable dt = null;
           if (ds != null && ds.Tables.Count > 0)
           {
               dt = ds.Tables[0];
           }
           return dt;
       }
    }

}
