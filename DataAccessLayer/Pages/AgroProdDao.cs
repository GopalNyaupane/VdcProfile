﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using DataAccessLayer.DbConnection;

namespace DataAccessLayer
{
   public class AgroProdDao :ConnectionHelper
    {
        public int InsertHouseHoldDecisions(Common.Entity.AgroProdBo objAgroBl)
        {
            
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_HOUSEHOLD_DECISIONS");

            Db.AddInParameter(cmd,"@ownerId",DbType.Int32,objAgroBl.OwnerId);
            Db.AddInParameter(cmd,"@housedecision",DbType.Int32,objAgroBl.HousHoldDecision);
            Db.AddInParameter(cmd,"@householdinvolved",DbType.Int32,objAgroBl.HouseHldInvolved);
            Db.AddInParameter(cmd,"@bankacc",DbType.Int32,objAgroBl.BankAcc);
            Db.AddInParameter(cmd,"@consumercom",DbType.Int32,objAgroBl.ConsumerCom);
            Db.AddInParameter(cmd,"@schoolmngmt",DbType.Int32,objAgroBl.SchoolMngmnt);
            Db.AddInParameter(cmd,"@bsnsparticipation",DbType.Int32,objAgroBl.BusinessParticipation);
            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;


        }

        public int InsertCropIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_CROPS_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@cashcropId", DbType.Int32, objAgroBl.CropGroup);
            Db.AddInParameter(cmd, "@landarea", DbType.Decimal, objAgroBl.LandArea);
            Db.AddInParameter(cmd, "@income", DbType.String, objAgroBl.Income);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@fiscalYear", DbType.String, objAgroBl.Date);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;

        }

        public int InsertVegetableIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            try
            {
                DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_TBL_AGRO_VEG_INCOME_DETAILS");

                Db.AddInParameter(cmd, "@vegId", DbType.Int32, objAgroBl.VegId);
                Db.AddInParameter(cmd, "@landarea", DbType.Decimal, objAgroBl.VegLand);
                Db.AddInParameter(cmd, "@income", DbType.String, objAgroBl.VegIncome);
                Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
                Db.AddInParameter(cmd, "@fiscalYear", DbType.String, objAgroBl.Date);


                int returnval = 0;
                returnval = Db.ExecuteNonQuery(cmd);
                return returnval;
            }
            catch(Exception e)
            {
                return 0;
            }
           
        }

        public int InsertCashCropIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_CASHCROPS_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@cashcropId", DbType.Int32, objAgroBl.CashCropId);
            Db.AddInParameter(cmd, "@landarea", DbType.Decimal, objAgroBl.CashCropLand);
            Db.AddInParameter(cmd, "@income", DbType.String, objAgroBl.CashCropIncome);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@fiscalYear", DbType.String, objAgroBl.Date);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
        }

        public int InsertAnimalIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_TBL_ANIMAL_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@animalId", DbType.Int32, objAgroBl.AnimalId);
            Db.AddInParameter(cmd, "@localAnimal", DbType.Int32, objAgroBl.LocalAnimal);
            Db.AddInParameter(cmd, "@hybridAnimal", DbType.Int32, objAgroBl.HybridAnimal);
            Db.AddInParameter(cmd, "@milkProd", DbType.Decimal, objAgroBl.MilkProd);
            Db.AddInParameter(cmd, "@meatProd", DbType.Decimal, objAgroBl.MeatProd);
            Db.AddInParameter(cmd, "@woolProd", DbType.Decimal, objAgroBl.WoolProd);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@eggProd", DbType.Decimal, objAgroBl.EggProd);
            Db.AddInParameter(cmd, "@honeyProd", DbType.Decimal, objAgroBl.HoneyProd);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;


        }

        public int InsertFruitsIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_FRUITS_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@cashcropId", DbType.Int32, objAgroBl.FruitId);
            Db.AddInParameter(cmd, "@landarea", DbType.Decimal, objAgroBl.FruitLand);
            Db.AddInParameter(cmd, "@income", DbType.String, objAgroBl.FruitIncome);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@fiscalYear", DbType.String, objAgroBl.Date);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
           
        }

        public int InsertOtherIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_AGRO_OTHERS_INCOME");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@ghaselnd", DbType.Decimal, objAgroBl.GhaseLnd);
            Db.AddInParameter(cmd, "@ghaseincome", DbType.Decimal, objAgroBl.GhaseIncome);
            Db.AddInParameter(cmd, "@bhuilnd", DbType.Decimal, objAgroBl.BhuiLnd);
            Db.AddInParameter(cmd, "@bhuiincome", DbType.Decimal, objAgroBl.BhuiIncome);
            Db.AddInParameter(cmd, "@resamlnd", DbType.Decimal, objAgroBl.ResamLnd);
            Db.AddInParameter(cmd, "@resamincome", DbType.Decimal, objAgroBl.ResamIncome);
            Db.AddInParameter(cmd, "@jdblnd", DbType.Decimal, objAgroBl.JdbLnd);
            Db.AddInParameter(cmd, "@jdbincome", DbType.Decimal, objAgroBl.JdbIncome);
            Db.AddInParameter(cmd, "@otherlnd", DbType.Decimal, objAgroBl.OtherLnd);
            Db.AddInParameter(cmd, "@otherincome", DbType.Decimal, objAgroBl.OtherIncome);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;

            
        }

        public DataTable FetchHouseHoldDecision(Common.Entity.AgroProdBo objAgroBl)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_HOUSEHOLD_DECISIONS");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_HOUSEHOLD_DECISIONS", objAgroBl.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchCropIncome(Common.Entity.AgroProdBo objAgroBl)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_AGRO_CROPS_INCOME_DETAILS");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_AGRO_CROPS_INCOME_DETAILS", objAgroBl.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchVegIncome(Common.Entity.AgroProdBo objAgroBl)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_AGRO_VEG_INCOME_DETAILS");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_AGRO_VEG_INCOME_DETAILS", objAgroBl.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchCashCropIncome(Common.Entity.AgroProdBo objAgroBl)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_AGRO_CASHCROPS_INCOME_DETAILS");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_AGRO_CASHCROPS_INCOME_DETAILS", objAgroBl.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchFruitsIncome(Common.Entity.AgroProdBo objAgroBl)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_AGRO_FRUITS_INCOME_DETAILS");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_AGRO_FRUITS_INCOME_DETAILS", objAgroBl.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchAnimalIncome(Common.Entity.AgroProdBo objAgroBl)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_ANIMAL_INCOME_DETAILS");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_ANIMAL_INCOME_DETAILS", objAgroBl.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

        public DataTable FetchOtherIncome(Common.Entity.AgroProdBo objAgroBl)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_AGRO_OTHERS_INCOME");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_AGRO_OTHERS_INCOME", objAgroBl.OwnerId);

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }



        public void UpdateAnimalIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_ANIMAL_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objAgroBl.UserId);
            Db.AddInParameter(cmd, "@animalId", DbType.Int32, objAgroBl.AnimalId);
            Db.AddInParameter(cmd, "@localAnimal", DbType.Int32, objAgroBl.LocalAnimal);
            Db.AddInParameter(cmd, "@hybridAnimal", DbType.Int32, objAgroBl.HybridAnimal);
            Db.AddInParameter(cmd, "@milkProd", DbType.Decimal, objAgroBl.MilkProd);
            Db.AddInParameter(cmd, "@meatProd", DbType.Decimal, objAgroBl.MeatProd);
            Db.AddInParameter(cmd, "@woolProd", DbType.Decimal, objAgroBl.WoolProd);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@eggProd", DbType.Decimal, objAgroBl.EggProd);
            Db.AddInParameter(cmd, "@honeyProd", DbType.Decimal, objAgroBl.HoneyProd);
            
            Db.ExecuteNonQuery(cmd);
           
        }

        public void UpdateOtherIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_AGRO_OTHERS_INCOME");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objAgroBl.UserId);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@ghaselnd", DbType.Decimal, objAgroBl.GhaseLnd);
            Db.AddInParameter(cmd, "@ghaseincome", DbType.Decimal, objAgroBl.GhaseIncome);
            Db.AddInParameter(cmd, "@bhuilnd", DbType.Decimal, objAgroBl.BhuiLnd);
            Db.AddInParameter(cmd, "@bhuiincome", DbType.Decimal, objAgroBl.BhuiIncome);
            Db.AddInParameter(cmd, "@resamlnd", DbType.Decimal, objAgroBl.ResamLnd);
            Db.AddInParameter(cmd, "@resamincome", DbType.Decimal, objAgroBl.ResamIncome);
            Db.AddInParameter(cmd, "@jdblnd", DbType.Decimal, objAgroBl.JdbLnd);
            Db.AddInParameter(cmd, "@jdbincome", DbType.Decimal, objAgroBl.JdbIncome);
            Db.AddInParameter(cmd, "@otherlnd", DbType.Decimal, objAgroBl.OtherLnd);
            Db.AddInParameter(cmd, "@otherincome", DbType.Decimal, objAgroBl.OtherIncome);

            Db.ExecuteNonQuery(cmd);
        }

        public void UpdateVegetableIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_AGRO_VEG_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objAgroBl.UserId);
            Db.AddInParameter(cmd, "@vegId", DbType.Int32, objAgroBl.VegId);
            Db.AddInParameter(cmd, "@landarea", DbType.Decimal, objAgroBl.VegLand);
            Db.AddInParameter(cmd, "@income", DbType.String, objAgroBl.VegIncome);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@fiscalYear", DbType.String, objAgroBl.Date);

            Db.ExecuteNonQuery(cmd);
        }

        public void UpdateCashCropIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CASHCROPS_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objAgroBl.UserId);
            Db.AddInParameter(cmd, "@cashcropId", DbType.Int32, objAgroBl.CashCropId);
            Db.AddInParameter(cmd, "@landarea", DbType.Decimal, objAgroBl.CashCropLand);
            Db.AddInParameter(cmd, "@income", DbType.String, objAgroBl.CashCropIncome);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@fiscalYear", DbType.String, objAgroBl.Date);

            Db.ExecuteNonQuery(cmd);
        }

        public void UpdateFruitsIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_FRUITS_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objAgroBl.UserId);
            Db.AddInParameter(cmd, "@cashcropId", DbType.Int32, objAgroBl.FruitId);
            Db.AddInParameter(cmd, "@landarea", DbType.Decimal, objAgroBl.FruitLand);
            Db.AddInParameter(cmd, "@income", DbType.String, objAgroBl.FruitIncome);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@fiscalYear", DbType.String, objAgroBl.Date);

            Db.ExecuteNonQuery(cmd);
        }

        public void UpdateCropIncomeDetail(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_CROPS_INCOME_DETAILS");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objAgroBl.UserId);
            Db.AddInParameter(cmd, "@cashcropId", DbType.Int32, objAgroBl.CropGroup);
            Db.AddInParameter(cmd, "@landarea", DbType.Decimal, objAgroBl.LandArea);
            Db.AddInParameter(cmd, "@income", DbType.String, objAgroBl.Income);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@fiscalYear", DbType.String, objAgroBl.Date);

           Db.ExecuteNonQuery(cmd);
        }

        public void UpdateHouseHoldDecisions(Common.Entity.AgroProdBo objAgroBl)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_HOUSEHOLD_DECISIONS");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objAgroBl.OwnerId);
            Db.AddInParameter(cmd, "@housedecision", DbType.Int32, objAgroBl.HousHoldDecision);
            Db.AddInParameter(cmd, "@householdinvolved", DbType.Int32, objAgroBl.HouseHldInvolved);
            Db.AddInParameter(cmd, "@bankacc", DbType.Int32, objAgroBl.BankAcc);
            Db.AddInParameter(cmd, "@consumercom", DbType.Int32, objAgroBl.ConsumerCom);
            Db.AddInParameter(cmd, "@schoolmngmt", DbType.Int32, objAgroBl.SchoolMngmnt);
            Db.AddInParameter(cmd, "@bsnsparticipation", DbType.Int32, objAgroBl.BusinessParticipation);
            
            Db.ExecuteNonQuery(cmd);
            
        }

        public void DeleteOldCashCropRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_AGRO_CASHCROPS_INCOME_DETAILS");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);

        }
        public void DeleteOldFruitsRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_AGRO_FRUITS_INCOME_DETAILS");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);

        }
        public void DeleteOldCropsRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_AGRO_CROPS_INCOME_DETAILS");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);

        }
        public void DeleteOldAnimalRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DALETE_ANIMAL_INCOME_DETAILS");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);

        }
        public void DeleteOldVegetableRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_AGRO_VEG_INCOME_DETAILS");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);

        }
    }
}
