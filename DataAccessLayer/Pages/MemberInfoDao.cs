﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using DataAccessLayer.DbConnection;

namespace DataAccessLayer
{
   public class MemberInfoDao :ConnectionHelper
    {
        public int InsertOccupation(Common.Entity.MemberInfoBo objMmbr)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_TBL_OCCUPATION");
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMmbr.OwnerId);
            Db.AddInParameter(cmd, "@occupationType", DbType.Int32, objMmbr.OccupationType);
            Db.AddInParameter(cmd, "@ageGroup", DbType.Int32, objMmbr. AgeGroup);
            Db.AddInParameter(cmd, "@maleCount", DbType.Int32, objMmbr.MaleNum);
            Db.AddInParameter(cmd, "@femaleCount", DbType.Int32, objMmbr.FemaleNum);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
            
        }

        public int InsertDisableInfo(Common.Entity.MemberInfoBo objMmbr)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_TBL_DISABILITY");
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMmbr.OwnerId);
            Db.AddInParameter(cmd, "@disableType", DbType.Int32, objMmbr.DisableType);
            Db.AddInParameter(cmd, "@malechildcount ", DbType.Int32, objMmbr.MaleChildCount);
            Db.AddInParameter(cmd, "@femalechildcount", DbType.Int32, objMmbr.FemaleChildCount);    
           
            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
           
        }

        public int InsertMigrationDeatil(Common.Entity.MemberInfoBo objMmbr)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_TBL_OUT_MIGRATION_DETAIL");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMmbr.OwnerId);
            Db.AddInParameter(cmd, "@name", DbType.String, objMmbr.Name);
            Db.AddInParameter(cmd, "@age ", DbType.Int32, objMmbr.Age);
            Db.AddInParameter(cmd, "@sexId", DbType.Int32, objMmbr.SexId);
            Db.AddInParameter(cmd, "@migratedPlace", DbType.String, objMmbr.MigratedPlace);
            Db.AddInParameter(cmd, "@outreason", DbType.String, objMmbr.OutReason);
           
            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;

        }

        public int InsertMigration(Common.Entity.MemberInfoBo objMmbr)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_TBL_OUT_MIGRATION");

            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMmbr.OwnerId);
            Db.AddInParameter(cmd, "@isoutMigration", DbType.Int32, objMmbr.IsOutMigrant);

            int returnval = 0;
            returnval = Db.ExecuteNonQuery(cmd);
            return returnval;
        }

        public DataTable FetchMigration(Common.Entity.MemberInfoBo objMmbr)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_OUT_MIGRATION");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_OUT_MIGRATION",objMmbr.OwnerId);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            return dt;
        }

        public DataTable FetchOccupation(Common.Entity.MemberInfoBo objMmbr)
        {
            DataTable dt = new DataTable();

            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_OCCUPATION");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_OCCUPATION", objMmbr.OwnerId);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            return dt;
        }

        public DataTable FetchMigrationDetail(Common.Entity.MemberInfoBo objMmbr)
        {
           DataTable dt = new DataTable();
           DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_OUT_MIGRATION_DETAIL");
           DataSet ds = null;
           ds = Db.ExecuteDataSet("USP_FETCH_OUT_MIGRATION_DETAIL",objMmbr.OwnerId);
           if (ds != null && ds.Tables.Count > 0)
           {
               dt = ds.Tables[0];
           }

           return dt;
        }

        public DataTable FetchDisability(Common.Entity.MemberInfoBo objMmbr)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = Db.GetStoredProcCommand("USP_FETCH_DISABILITY");
            DataSet ds = null;
            ds = Db.ExecuteDataSet("USP_FETCH_DISABILITY", objMmbr.OwnerId);
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            return dt;
        }

        public void UpdateOccupationData(Common.Entity.MemberInfoBo objMmbr)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_OCCUPATION");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objMmbr.userId);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMmbr.OwnerId);
            Db.AddInParameter(cmd, "@occupationType", DbType.Int32, objMmbr.OccupationType);
            Db.AddInParameter(cmd, "@maleCount", DbType.Int32, objMmbr.MaleNum);
            Db.AddInParameter(cmd, "@femaleCount", DbType.Int32, objMmbr.FemaleNum);
            Db.AddInParameter(cmd, "@ageGroup ", DbType.Int32, objMmbr.AgeGroup);

            Db.ExecuteNonQuery(cmd);
        }

        public void UpdateIsOutMigrationData(Common.Entity.MemberInfoBo objMmbr)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_MIGRATION");

            Db.AddInParameter(cmd, "@ownerId",DbType.Int32,objMmbr.OwnerId);
            Db.AddInParameter(cmd, "@isoutMigration", DbType.Int32, objMmbr.IsOutMigrant);

            Db.ExecuteNonQuery(cmd);
        }

        public void UpdateMigrationDetailData(Common.Entity.MemberInfoBo objMmbr)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_OUT_MIGRATION_DETAIL");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objMmbr.userId);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMmbr.OwnerId);
            Db.AddInParameter(cmd, "@name", DbType.String, objMmbr.Name);
            Db.AddInParameter(cmd, "@age ", DbType.Int32, objMmbr.Age);
            Db.AddInParameter(cmd, "@sexId", DbType.Int32, objMmbr.SexId);
            Db.AddInParameter(cmd, "@migratedPlace", DbType.String, objMmbr.MigratedPlace);
            Db.AddInParameter(cmd, "@outreason", DbType.String, objMmbr.OutReason);

            Db.ExecuteNonQuery(cmd);

        }

        public void UpdateDisabilityData(Common.Entity.MemberInfoBo objMmbr)
        {

            DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_DISABILITY");

            Db.AddInParameter(cmd, "@mainId", DbType.Int32, objMmbr.userId);
            Db.AddInParameter(cmd, "@ownerId", DbType.Int32, objMmbr.OwnerId);
            Db.AddInParameter(cmd, "@disableType", DbType.Int32, objMmbr.DisableType);
            Db.AddInParameter(cmd, "@malechildcount ", DbType.Int32, objMmbr.MaleChildCount);
            Db.AddInParameter(cmd, "@femalechildcount", DbType.Int32, objMmbr.FemaleChildCount);

            Db.ExecuteNonQuery(cmd);
        }
        public void DeleteOldRecord(int ownerId)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_DELETE_MEMBERINFO");
            Db.AddInParameter(cmd, "@OwnerId", DbType.Int32, ownerId);

            int sqdr = Db.ExecuteNonQuery(cmd);


        }
    }
}
