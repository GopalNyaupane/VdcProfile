﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Common.Entity;
using DataAccessLayer.DbConnection;

namespace DataAccessLayer.Pages
{
   public class OwnerDao : ConnectionHelper
    {
        public int InsertSection1(OwnerBo objBo)
        {
            DbCommand cmd = Db.GetStoredProcCommand("USP_INSERT_OWNER_INFO");
           
            Db.AddInParameter(cmd, "@VillageId", DbType.Int32, objBo.VillageId);
            
            Db.AddInParameter(cmd, "@OwnerName", DbType.String, objBo.OwnerName);
           
            Db.AddInParameter(cmd, "@SexId", DbType.String, objBo.SexId);
            Db.AddInParameter(cmd, "@CasteId", DbType.String, objBo.CasteId);
            Db.AddInParameter(cmd, "@ReligionId", DbType.String, objBo.ReligionId);
            Db.AddInParameter(cmd, "@LanguageId", DbType.String, objBo.LanguageId);

            return int.Parse(Db.ExecuteDataSet(cmd).Tables[0].Rows[0][0].ToString());
        }

        public DataTable FetchSection1(int InfoId)
        {
            DataSet ds = Db.ExecuteDataSet("USP_FETCH_OWNER", InfoId);
            DataTable dt = null;
            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }

       public int UpdateSection(OwnerBo objBo)
       {
           DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_OWNER_INFO");

           Db.AddInParameter(cmd, "@VillageId", DbType.Int32, objBo.VillageId);

           Db.AddInParameter(cmd, "@OwnerName", DbType.String, objBo.OwnerName);

           Db.AddInParameter(cmd, "@SexId", DbType.String, objBo.SexId);
           Db.AddInParameter(cmd, "@CasteId", DbType.String, objBo.CasteId);
           Db.AddInParameter(cmd, "@ReligionId", DbType.String, objBo.ReligionId);
           Db.AddInParameter(cmd, "@LanguageId", DbType.String, objBo.LanguageId);
           int i = Db.ExecuteNonQuery(cmd);
           return i;
       }

       public int UpdateIsComplete(OwnerBo objBo)
       {
           DbCommand cmd = Db.GetStoredProcCommand("USP_UPDATE_IS_COMPLETE");
           Db.AddInParameter(cmd, "@IsComplete", DbType.String, objBo.IsComplete);
           Db.AddInParameter(cmd, "@OwnerId", DbType.String, objBo.OwnerId);
           int i = Db.ExecuteNonQuery(cmd);
           return i;
       }

       public DataTable FetchIncomplete()
       {
           DataSet ds = Db.ExecuteDataSet("USP_FETCH_INCOMPLETE_LIST");
           DataTable dt = null;
           if (ds != null && ds.Tables.Count > 0)
           {
               dt = ds.Tables[0];
           }
           return dt;
       }
    }
}
