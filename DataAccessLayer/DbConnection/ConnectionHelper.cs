﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccessLayer.DbConnection
{
    public class ConnectionHelper
    {
        protected Database Db
        {
            get
            {
                var db = DatabaseFactory.CreateDatabase();
                return db;
            }
        }
    }
}
